// JavaScript Document
$(document).ready(function(e) {
	//获取用户文本框
	var userName=$("#userName");
	//获取用户密码框
	var password=$("#password");
	//获取验证码框
	var verification=$("#verification");
	//获取文本框前面图片
	var userimg=$("#userimg");
	//获取密码框前面图片
	var pwimg=$("#pwimg");
	
	//给用户文本框置焦点
	userName.focus();
	userName.css("background-color","#E5E5E5");
	
	//绑定用户文本框焦点事件，失去焦点事件，按下某件事件
	userName.focus(function(){
		userName.css("background-color","#E5E5E5");
	}).blur(function(){
		userName.css("background-color","");
	}).keypress(function(e){
		var key=e.which;
		if(key==13){
			password.focus();
		}
	});
	
	//绑定密码文本框焦点事件，失去焦点事件，按下某件事件
	password.focus(function(){
		password.css("background-color","#E5E5E5");
		
	}).blur(function(){
		password.css("background-color","");
	}).keypress(function(e){
		var key=e.which;
		if(key==13){
			$("#verification").focus();
		}
	});
		
	
	//绑定验证码文本框焦点事件，失去焦点事件，按下某件事件
	verification.focus(function(){
		verification.css("background-color","#E5E5E5");
		
	}).blur(function(){
		verification.css("background-color","");
	}).keypress(function(e){
		var key=e.which;
		if(key==13){
			$("#login").click();
		}
	});

	//绑定登陆按钮点击事件，移入事件，移出事件
    $("#login").click(function(){
		var username = $.trim($('#username').val());
		var password = $.trim($('#password').val());
		var verify = $.trim($('#verification').val());
		if(verify != ''){
			$.post('<c:url value="/login/ajaxverify"/>',{verify:verify},function(date){
				if(date =="success"){
					if(username == ''){
						alert("用户名不能为空");
					}else if(password == ''){
						alert("密码不能为空");
					}else{
						$.post('<c:url value="/adminlogin"/>',{username:username,password:password},function(d){
							if(d == "noUser"){
								alert("不存在该用户");
							}else if(d=="fail"){
								alert("密码错误");
							}else{
								location.href='<c:url value="/admin/publicinfo"/>';
							}
						});
					}
				}else{
					alert("验证码错误");
				}
			});
		}else{
			alert("验证码不能为空");
		}		
	}).mousemove(function(){
		$(this).css("background-position","-138px");
	}).mouseout(function(){
		$(this).css("background-position","0px");
	});
		
	
});
