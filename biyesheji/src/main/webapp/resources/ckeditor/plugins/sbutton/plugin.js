(function(){
    //Section 1 : 按下自定义按钮时执行的代码
    var a= {
        exec:function(editor){
        	//自定义代码，也可以调用函数
        	getContents();
        }
    },
    //Section 2 : 创建自定义按钮、绑定方法
    b='sbutton';
    CKEDITOR.plugins.add(b,{
        init:function(editor){
            editor.addCommand(b,a);
            editor.ui.addButton('sbutton',{
                label:'保存修改',
                icon: this.path + 'filesave.png',
                command:b
            });
        }
    });
})();