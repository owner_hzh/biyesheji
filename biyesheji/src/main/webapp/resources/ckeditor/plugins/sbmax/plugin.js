(function(){
    //Section 1 : 按下自定义按钮时执行的代码
    var a= {
        exec:function(editor){
        	//自定义代码，也可以调用函数
        	fullscreen();
        	editor.execCommand( "maximize" );
        }
    },
    //Section 2 : 创建自定义按钮、绑定方法
    b='sbmax';
    CKEDITOR.plugins.add(b,{
        init:function(editor){
            editor.addCommand(b,a);
            editor.ui.addButton('sbmax',{
                label:'全屏/缩小',
                icon: this.path + 'full.png',
                command:b
            });
        }
    });
})();