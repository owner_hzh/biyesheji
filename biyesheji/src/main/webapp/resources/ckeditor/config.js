/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	config.language = 'zh-cn';
	config.uiColor = '#CCCCCC';
	
	// 设置宽高
    config.height = 500; 
    
    config.extraPlugins="sbmax,sbutton";//自定义按钮
    
    config.filebrowserImageUploadUrl = "ckeditorupload"; //上传图片的controller
	
	//注释：’/’表示换行,’-‘标识分隔符 。 
	config.toolbar =
		[
            ['sbutton'],//自定义按钮
            ['sbmax'],//自定义按钮           
		    ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
		    ['Image','Table','HorizontalRule','Smiley','SpecialChar'],
		    '/',
		    ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
		    ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
		    ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
		    ['Link','Unlink','Anchor'],		    
		    '/',
		    ['Styles','Format','Font','FontSize'],
		    ['TextColor','BGColor']		    
		];		
	config.fullPage = true;
	config.allowedContent=true;
	/*'h1 h2 h3 p blockquote strong em;' +
	'a[!href];' +
	'img(left,right)[!src,alt,width,height];' +
	'table tr th td caption;' +
	'span{!font-family};' +
	'span{!color};' +
	'span(!marker);' +
	'del ins';*/
	
	config.startupMode ='wysiwyg';
	//设置HTML文档类型
    config.docType ='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">' ;
    
    //进行表格编辑功能 如：添加行或列 目前仅firefox支持plugins/wysiwygarea/plugin.js
    config.disableNativeTableHandles =true; //默认为不开启
    //是否使用HTML实体进行输出 plugins/entities/plugin.js
    config.entities = true;
    
    //对应后台语言的类型来对输出的HTML内容进行格式化
    config.protectedSource.push( /<\?[\s\S]*?\?>/g );   // PHP Code
    
    //是否强制复制来的内容去除格式plugins/pastetext/plugin.js
    config.forcePasteAsPlainText =false;//不去除
    
    //是否强制用“&”来代替“&amp;”plugins/htmldataprocessor/plugin.js
    config.forceSimpleAmpersand = false;
    
    //当从word里复制文字进来时，是否进行文字的格式化去除plugins/pastefromword/plugin.js
    config.pasteFromWordIgnoreFontFace = true; //默认为忽略格式

    //是否使用<h1><h2>等标签修饰或者代替从word文档中粘贴过来的内容plugins/pastefromword/plugin.js
    config.pasteFromWordKeepsStructure = false;

    //从word中粘贴内容时是否移除格式plugins/pastefromword/plugin.js
    config.pasteFromWordRemoveStyle =false;
    
    //对address标签进行格式化 plugins/format/plugin.js
    config.format_address = { element : 'address', attributes : { class :'styledAddress' } };

    //对DIV标签自动进行格式化 plugins/format/plugin.js
    config.format_div = { element : 'div', attributes : { class :'normalDiv' } };

    //对H1标签自动进行格式化 plugins/format/plugin.js
    config.format_h1 = { element : 'h1', attributes : { class :'contentTitle1' } };

    //对H2标签自动进行格式化 plugins/format/plugin.js
    config.format_h2 = { element : 'h2', attributes : { class :'contentTitle2' } };

    //对H3标签自动进行格式化 plugins/format/plugin.js
    config.format_h1 = { element : 'h3', attributes : { class :'contentTitle3' } };

    //对H4标签自动进行格式化 plugins/format/plugin.js
    config.format_h1 = { element : 'h4', attributes : { class :'contentTitle4' } };

    //对H5标签自动进行格式化 plugins/format/plugin.js
    config.format_h1 = { element : 'h5', attributes : { class :'contentTitle5' } };

    //对H6标签自动进行格式化 plugins/format/plugin.js
    config.format_h1 = { element : 'h6', attributes : { class :'contentTitle6' } };

    //对P标签自动进行格式化 plugins/format/plugin.js
    config.format_p = { element : 'p', attributes : { class : 'normalPara' }};

    //对PRE标签自动进行格式化 plugins/format/plugin.js
    config.format_pre = { element : 'pre', attributes : { class : 'code'} };
};
