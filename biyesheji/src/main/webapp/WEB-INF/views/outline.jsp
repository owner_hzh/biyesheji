<%@ page language="java" import="java.util.* " pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'content.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<meta content="UTF-8" http-equiv="application/json; charset=UTF-8">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>
	<link rel="stylesheet" href="<c:url value='resources/css/jquery.treeview.css'/>" />
	<script src=' <c:url value='/resources/js/jquery.cookie.js'/>' type="text/javascript"></script>
	<script src='<c:url value='/resources/js/jquery.treeview.js'/>' type="text/javascript"></script>
     <script type="text/javascript">
        //显示内容列表
        function showlist(code)
        {       
            //消除原来点击的样式  
            $("#tree").children().each(function(){
              $(this).children().eq(2).children().removeClass("active");
              $(this).children().eq(2).children().css("background-color","");
           }); 
           
           $("#container").css("display","none");
           $("#pageoffice").attr("src","outline/outlineList?code="+code+"&noCache="+Math.floor(Math.random()*100000));
           $("#li_"+code).addClass("active");
           $("#li_"+code).css("background-color","#cccccc");
           $("#li_"+code).siblings().removeClass("active");
           $("#li_"+code).siblings().css("background-color","");
           
           
        }
        
        //课程item的点击函数       
        function menuclick(code,ocode)
        {          
           //获取点击item的详细数据
           $.ajax({
			   type: "GET",
			   dataType: "json",//返回json格式的数据
			   url: "outline/getDataByCode?noCache="+Math.floor(Math.random()*100000),
			   data: {
			            code:code,
			            ocode:ocode
			         },
			   success: function(data){
			      $("#hcode").val(code);
			      try{
			          //console.log(data.filename);
			          console.log(data.htmlfile);
			      }catch(err){
			          //工具栏隐藏
                      $("#container").css("display","none");
			          $("#pageoffice").attr("src","outline/nofile?code="+code+"&ocode="+ocode+"&noCache="+Math.floor(Math.random()*100000));
			      }
			      if(data.filename!="")			     
			      {
			         if(data.filename=="IOException")
			         {
			            alert("此文档格式有问题。请打开此文档，点击另存为，重新上传。");
			            zhuanhuashibaishanchu(code,ocode);
			         }else
			         {
			            //工具栏显示
	                     $("#container").css("display","block");			         
					     $("#pageoffice").attr("src","<c:url value='"+data.htmlfile+"'/>"+"?noCache="+Math.floor(Math.random()*100000));
					     //文档名称
					     $("#name").text(data.co_name);
					     $("#delete").click(function(){
					          deleteDoc(code,data.curriculumoutline_code,data.location+data.filename);
					      });
					      //为下载按钮赋值 文档所在路径
					      var rFile=data.location+data.filename;
					      $("#download").attr("href","${basePath}"+rFile);
			         }
			         
			     }else
			     {
			         $("#pageoffice").attr("src","outline/nofile?code="+code+"&ocode="+ocode+"&noCache="+Math.floor(Math.random()*100000));
			     }			    
			   },
			   	error:function (XMLHttpRequest, textStatus, errorThrown)
			 	{
			 	     $("#pageoffice").attr("src","outline/nofile?code="+code+"&ocode="+ocode+"&noCache="+Math.floor(Math.random()*100000));
			 	}
		   });
        }
        
        function zhuanhuashibaishanchu(code,ocode)//转化失败删除
	   {
	      $.ajax({
               type: "POST",
               dataType: "text",//返回json格式的数据
			   url: "outline/deleteDoc1"+"?noCache="+Math.floor(Math.random()*100000),
			   data: {
			            code:code,
			            ocode:ocode
			         },
			   success:function(data){
			      if(data=="true")
			      {
			        showlist(code);
			      }
			   }
           }); 
	   };
        
        //删除文档
        function deleteDoc(code,ocode,doc)
        {
           $.ajax({
               type: "POST",
               dataType: "text",//返回json格式的数据
			   url: "outline/deleteDoc"+"?noCache="+Math.floor(Math.random()*100000),
			   data: {
			            code:code,
			            ocode:ocode,
			            doc:doc
			         },
			   success:function(data){
			      if(data=="true")
			      {
			        //alert("文档删除成功！");
			        //工具栏隐藏
                    $("#container").css("display","none");
			        //跳转到上传页面 传code值
			        showlist(code);
			      }else
			      {
			         alert("文档删除失败！");
			      }
			   },
			   error:function(XMLHttpRequest, textStatus, errorThrown){
			         //alert("error-->文档删除失败！");
			   }
           });                     
        }
        
       //返回
       function goback()
       {          
          var rcode=$("#hcode").val();
          showlist(rcode);  
          $("#container").css("display","none");        
       };
        
        $(function(){
               //全屏显示功能
               $("#fullScreen").toggle(function(){ 
                    $("#fullScreen").text("还原");  
                    $(".pageoffice").css('position','absolute').css('left',0).css('top',0);               
                    $(".pageoffice").width('100%');  
                    $(".pageoffice").height('100%');
                    $("#back").css("display","none");
                    $('#content_iframe',window.parent.document).css('position','absolute').css('left',0).css('top',0);
                    //$('#content_iframe',window.parent.document).height('100%');
               },function(){
                  $("#fullScreen").text("全屏");
                  $(".pageoffice").css('position','').css('left','').css('top','');
                  $(".pageoffice").width('80%');
                  $(".pageoffice").height('100%'); 
                  $("#back").css("display","block");
                  $('#content_iframe',window.parent.document).css('position','').css('left','').css('top','');
                  //$('#content_iframe',window.parent.document).height('100%');
               });
               
               /* //Jquery TreeView控件的初始化
			   $("#tree").treeview({
					collapsed : true,
					animated : "medium",
					persist : "location",
					unique:true
			   });
			   
			   //初始时让树结构菜单的第一项展开
			   $("#tree").children().eq(0).find("div").click();	
			   //初始时让树结构菜单的第一项的的子菜单执行点击事件
			   $("#tree").children().eq(0).children().eq(2).children().eq(0).click(); */
			   
			$("#menutwobox").children().eq(0).addClass("activetwo");//初始化第一个菜单选项被选中
			$("#menutwobox").children().eq(0).children(".menuthreebox").show(800);
			$("#menutwobox").children().eq(0).children(".menuthreebox").children().eq(0).addClass("threeaction");
			$("#menutwobox").children().eq(0).children(".menuthreebox").children().eq(0).click();
			//$("#pageoffice").attr("src","outline/outlineList?code="+code+"&noCache="+Math.floor(Math.random()*100000));
			
           });
           
           function fullscreen()
           {
              //全屏显示功能
               $("#fullScreen").click();
           }
           
        function getContent(obj){
			$(".activetwo").children(".menuthreebox").hide(800);
			$(".activetwo").removeClass("activetwo");
			$(obj).parent().addClass("activetwo");
			$(obj).parent().children(".menuthreebox").show(800);
		}
		function gotochoose(obj,code){
			$(".threeaction").removeClass("threeaction");
			$(obj).addClass("threeaction");
			//alert($(obj).attr("urldate"))
			$("#pageoffice").attr("src","outline/outlineList?code="+code+"&noCache="+Math.floor(Math.random()*100000));
		}
       
    </script>
    <style type="text/css">
       body{border: 0;margin: 0;padding: 0;overflow: hidden;}
       .courselist{width: 20%;height: 100%;float: left;text-align: center; margin: 0 auto;background-color: #e8e8e8;}
       .courselist ul{list-style: none;text-align: center;}
       .courselist ul li{cursor: pointer; margin: 10px;}

       .courselist ul ul{background-color: #e8e8e8;}
       .courselist ul ul li:HOVER{color: blue;}
       .pageoffice{width: 80%;height: 100%;float: right; background-color:#CCCCCC;}

       .pageoffice iframe{width: 100%;height: 100%;margin: 0;padding: 0;border: 0;}
       .container{height: 30px; background-color: #cac;display: none; text-align: center;padding-top: 1%;padding-left: 1%;padding-right: 1%;}
       #name{ }
       #fullScreen{cursor: pointer; float: left;}
       #fullScreen:HOVER{color: blue;}
       #back{cursor: pointer;float: right;}
       #back:HOVER{color: blue;}
       .active{background-color: #CCCCCC;color: blue;} 
       
       html body{ height:100%; width:100%;overflow:hidden;}
/* 	   .menu{width:15%; height: 100%; float: left; background-color: #e8e8e8; overflow: auto;} 
	   #tree li ul li{ cursor: pointer;} */
	   .menubox{width:20%; height: 100%; 
	   		float: left; background-color: #e8e8e8; overflow: auto;
	   		border-top-left-radius:15px; 
	   		border-bottom-left-radius:15px; 
	   		color: #AA531E;
	   } 
	   .activetwo{color: blue;}
	   .menutwo{ cursor: pointer;
	   			  border-bottom: 1px dashed gray;
	   			  width:100%;
	   			  font-size: 19px;
	   			  font-family: '幼圆';
	   			  font-weight: normal;
	   			  line-height: 40px;
	   			  text-align: center;
	   	}
	   .menutwo:HOVER{color: blue;}/* 909EDA  F2F2F2 cccccc*/
	   .menuthreebox{
	   				display: none;
	   				background-color: #B5B5B5;
	   				}
	   .menuthree{color:#ffffff;
	   			  font-size: 16px;
	   			  border-top: 1px solid #ffffff;
	   			  font-weight: lighter;
	   			  }
	   .menuthree:HOVER{background-color:#CCCCCC;color:#0066CC;}
	   .threeaction{background-color:#CCCCCC;color:#0066CC;}
	   .content { width:85%; height:100%; float: right; background-color: #CCCCCC;
	   	   			  border-top-right-radius:15px; 
	   			  border-bottom-right-radius:15px;}	  
	   .iframe_content{ width: 100%; height: 100%; overflow: auto;border-top-right-radius:15px; 
	   			        border-bottom-right-radius:15px;}
       
    </style>
  </head>
<body>
    <div id="menutwobox" class="menubox"><%-- courselist --%>
       <%-- <ul id="tree">
	       <c:forEach items="${majors}" var="major">
	          <li><a>${major.major_name}</a>
	             <ul>
	                <c:forEach items="${major.curriculums}" var="curriculum">
	                   <li id="li_${curriculum.curriculum_code}" onclick="showlist(${curriculum.curriculum_code})">${curriculum.curriculum_name}</li>
	                </c:forEach>
	             </ul>
	          </li>	          
	       </c:forEach>
       </ul> --%>
       
       <c:forEach items="${majors}" var="major">
   		<div class="menutwo">
   			<span onclick="getContent(this)">${major.major_name}</span>
   			<div class="menuthreebox">
   			<c:forEach items="${major.curriculums}" var="curriculum">
	   			<div class="menuthree" onclick="gotochoose(this,${curriculum.curriculum_code})">
	   				<span>${curriculum.curriculum_name}</span>
	   			</div>
   			</c:forEach>
   			</div>
   		</div>
	</c:forEach>
       
    </div>
            
    <div class="pageoffice"><%-- pageoffice --%>
       <div class="container" id="container">
          <center><span><a id="fullScreen">全屏</a><a id="name"></a><a id="back" onclick="goback()" >返&nbsp;回</a></span></center>
       </div>
       
       <iframe id="pageoffice"></iframe><!-- src="outlineInitial"  -->
       <input type="hidden" id="hcode"/>
    </div> 
   
</body>
</html>
