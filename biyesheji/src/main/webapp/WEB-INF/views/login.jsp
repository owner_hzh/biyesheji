<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="false" %>
<html>
<head>
	<link rel="icon" href="<c:url value='/resources/img/biaozhi.ico'/>" type="image/x-icon">
	<title>网络资源共享平台</title>
</head>
<script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>
<script type="text/javascript">
$(document).ready(function(e) {
	//获取用户文本框
	var username=$("#username");
	//获取用户密码框
	var password=$("#password");
	//获取验证码框
	var verification=$("#verification");
	//获取文本框前面图片
	var userimg=$("#userimg");
	//获取密码框前面图片
	var pwimg=$("#pwimg");
	
	//给用户文本框置焦点
	username.focus();
	username.css("background-color","#E5E5E5");
	
	//绑定用户文本框焦点事件，失去焦点事件，按下某件事件
	username.focus(function(){
		username.css("background-color","#E5E5E5");
		$("#msg").text("")
	}).blur(function(){
		username.css("background-color","");
	}).keypress(function(e){
		var key=e.which;
		if(key==13){
			password.focus();
		}
	});
	
	//绑定密码文本框焦点事件，失去焦点事件，按下某件事件
	password.focus(function(){
		password.css("background-color","#E5E5E5");
		$("#msg").text("")
		
	}).blur(function(){
		password.css("background-color","");
	}).keypress(function(e){
		var key=e.which;
		if(key==13){
			$("#verification").focus();
		}
	});
		
	
	//绑定验证码文本框焦点事件，失去焦点事件，按下某件事件
	verification.focus(function(){
		verification.css("background-color","#E5E5E5");
		$("#msg").text("")
		
	}).blur(function(){
		verification.css("background-color","");
	}).keypress(function(e){
		var key=e.which;
		if(key==13){
			$("#login").click();
		}
	});

	//绑定登陆按钮点击事件，移入事件，移出事件
    $("#login").click(function(){
		var username = $.trim($('#username').val());
		var password = $.trim($('#password').val());
		var verify = $.trim($('#verification').val());
		if(verify != ''){
			$.get('<c:url value="/login/ajaxverify"/>',{verify:verify},function(date){
				if(date =="success"){
					if(username == ''){
						$("#username").css("background-color","#FEADAD");
						$("#msg").text("用户名不能为空")
					}else if(password == ''){
						$("#password").css("background-color","#FEADAD");
						$("#msg").text("密码不能为空")
					}else{
						$.post('<c:url value="/login/userlogin"/>',{user_id:username,password:password},function(d){
							if(d == "noUser"){
								$("#msg").text("不存在该用户");
								$("#username").css("background-color","#FEADAD");
							}else if(d=="fail"){
								$("#msg").text("密码错误");
								$("#password").css("background-color","#FEADAD");
							}else{
								location.href='<c:url value="/home"/>';
							}
						},"text");
					}
				}else{
					$("#verification").css("background-color","#FEADAD");
					$("#msg").text("验证码错误")
				}
			},"text");
		}else{
			$("#verification").css("background-color","#FEADAD");
			$("#msg").text("验证码不能为空")
			
		}		
	}).mousemove(function(){
		$(this).css("background-position","-138px");
	}).mouseout(function(){
		$(this).css("background-position","0px");
	});	
});
//切换验证码
function changevify(){
	$("#verifyimg").attr("src","<c:url value='/login/verfy_admin'/>?data="+new Date());
}

function youke(){
	$.post('<c:url value="/login/userlogin"/>', {
			user_id : 0,
			password : 123456
		}, function(d) {
			if (d == "noUser") {
				$("#msg").text("不存在该用户");
				$("#username").css("background-color", "#FEADAD");
			} else if (d == "fail") {
				$("#msg").text("密码错误");
				$("#password").css("background-color", "#FEADAD");
			} else {
				location.href = '<c:url value="/home"/>';
			}
		}, "text");
}
</script>

<!-- <link rel=stylesheet href="css/index1.css" type="text/css"> -->
<link rel=stylesheet href="<c:url value='/resources/css/login.css'/>" type="text/css">
<style type="text/css" >

</style>
</head>

<body>
<div id="main">
	<div id="bg">
		<span id="title" ><img src="<c:url value='/resources/loginimg/xiaobiao.jpg'/>"  style="border-radius: 50%;width:40px;vertical-align:middle;"/>&nbsp;重庆交通大学</span>
        <span id="projectName">专业共享教学资源平台</span>
        <span id="msg"></span>
        <span id="userimg">帐&nbsp;&nbsp;号：</span>
		<input type="text" id="username" name="username" value=""  placeholder="登录帐号"/>
		<span id="noname" onclick="youke()">游客进入</span>
        <span id="pwimg">密&nbsp;&nbsp;码：</span>
        <input type="password" id="password" name="password" value="123456"  placeholder="密码"/>
<!-- 		<span id="forgetpwd" href="javascript:void(0)">忘记密码</span> -->
		<span id="verificationimg">验证码：</span>
        <input type="text" id="verification" name="verification" value=""  placeholder="验证码"/>
        <img id="verifyimg" src="<c:url value='/login/verfy_admin?data=<%=System.currentTimeMillis() %>'/>" title="点击切换验证码" onclick="changevify()"/>
        <span id="login" class="login">登 陆</span>	
	</div>	
</div>
<%-- <img src = "<c:url value='/resources/loginimg/showschool2.jpg'/>" style="border-radius: 50%;position:absolute;left:12%;top:10%;width:350px;">
<img src = "<c:url value='/resources/loginimg/showschool.jpg'/>" style="border-radius: 50%;position:absolute;left:40%;top:20%;width:50px;">
<img src = "<c:url value='/resources/loginimg/qw.jpg'/>" style="border-radius: 50%;position:absolute;left:48%;top:1%;width:170px;">
<img src = "<c:url value='/resources/loginimg/showschool.jpg'/>" style="border-radius: 50%;position:absolute;left:63%;top:20%;width:50px;">
<img src = "<c:url value='/resources/loginimg/showschool10.jpg'/>" style="border-radius: 50%;position:absolute;left:71%;top:15%;width:170px;">
<img src = "<c:url value='/resources/loginimg/showschool.jpg'/>" style="border-radius: 50%;position:absolute;left:30%;top:45%;width:50px;">
<img src = "<c:url value='/resources/loginimg/showschool7.jpg'/>" style="border-radius: 50%;position:absolute;left:83%;top:40%;width:130px;">
<img src = "<c:url value='/resources/loginimg/showschool.jpg'/>" style="border-radius: 50%;position:absolute;left:73%;top:45%;width:50px;">
<img src = "<c:url value='/resources/loginimg/showschool4.jpg'/>" style="border-radius: 50%;position:absolute;left:12%;top:45%;width:120px;">
<img src = "<c:url value='/resources/loginimg/showschool6.jpg'/>" style="border-radius: 50%;position:absolute;left:23%;top:60%;width:170px;">
<img src = "<c:url value='/resources/loginimg/showschool.jpg'/>" style="border-radius: 50%;position:absolute;left:40%;top:75%;width:50px;">
<img src = "<c:url value='/resources/loginimg/showschool11.jpg'/>" style="border-radius: 50%;position:absolute;left:70%;top:65%;width:350px;">
<img src = "<c:url value='/resources/loginimg/showschool.jpg'/>" style="border-radius: 50%;position:absolute;left:63%;top:75%;width:50px;">
<img src = "<c:url value='/resources/loginimg/showschool5.jpg'/>" style="border-radius: 50%;position:absolute;left:48%;top:75%;width:170px;">
<div id="top" style="background-color: #0033CC;height:25%;"></div>
<div id="middle" style="background-color: #0066CC;height:50%;"></div>
<div id="buttom" style="background-color: #0033FF;height:25%;"></div> --%>
</body>
</html>
