<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'practicing.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
    <style type="text/css">
	   html body{ height:100%; width:100%;overflow:hidden;}
	   .menu{width:15%; height: 100%; 
	   		float: left; background-color: #e8e8e8; overflow: auto;
	   		border-top-left-radius:15px; 
	   		border-bottom-left-radius:15px; 
	   		color: #AA531E;
	   } 
	   .activetwo{background-color: #CCCCCC;color: blue;}
	   .menu div{ cursor: pointer;
	   			  border-bottom: 1px dashed gray;
	   			  height:40px;width:100%;
	   			  font-size: 15px;
	   			  font-family: '幼圆';
	   			  font-weight: normal;
	   			  line-height: 40px;
	   			  text-align: center;
	   	}
	   .menu div:HOVER{background-color: #CCCCCC;color: blue;}/* 909EDA  F2F2F2*/
	   .content { width:85%; height:100%; 
	   			  float: right;
	   			  background-color:#EDEDED;
	   			  border-top-right-radius:15px; 
	   			  border-bottom-right-radius:15px; 
	   }	  
	   .iframe_content{ width: 100%; height: 100%; overflow: auto;background-color:#CCCCCC;
	   					border-top-right-radius:15px; 
	   			        border-bottom-right-radius:15px;  }
	</style>
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/general2.css'/>">   
    <script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>
    <script type="text/javascript" src="<c:url value='resources/js/popuptwo.js'/>"></script>
  	<script type="text/javascript">	 
 		$(function(){
			$("#menutwo").children().eq(0).addClass("activetwo");
			var obj = $("#menutwo").children().eq(0);
			obj.click();
		}); 
		function getContent(obj,id){
			$(".activetwo").removeClass("activetwo");
			$(obj).addClass("activetwo");
			$("#iframe").attr("src" , "<c:url value='/major/showdetailmajor?id="+id+"'/>");
		}
		
		//根据菜单id触发菜单点击事件
		function menuclick(id)
		{
		   $("#mdiv"+id).click();
		}
		
		//显示弹出框
		function showPage(html)
		{
		   //显示弹出框
           centerPopup2();
		   loadPopup2();
		   $("#contactArea").empty();
		   $("#contactArea").append(html);
		}
		
		function update(id)
		{
		  $("#iframe").attr("src" , "<c:url value='/major/updatemajor?id="+id+"'/>");
		}
		
	</script>
  </head>
  
  <body>
      <!-- 专业菜单开始 -->
    <div id="menutwo" class="menu">
   		<c:forEach items="${majors}" var="major">
   			<div id="mdiv${major.major_code}" onclick="getContent(this,${major.major_code})"><span>${major.major_name }</span></div>
   		</c:forEach>
	</div>
	<!-- 专业菜单结束-->
	
	<!-- 专业内容开始-->
	<div class="content" align="center"> 
      	<iframe id="iframe" class="iframe_content" marginheight="0" marginwidth="0" hspace="0" vspace="0" frameborder="0">
    	</iframe>
	</div>
	<!-- 专业内容结束-->
	
	<!-- 弹出框代码 -->
	<div id="popupContact">
		<a id="popupContactClose">x</a>
		<h1>文档信息</h1>
		<p id="contactArea">
				
		</p>
	</div>
	<div id="backgroundPopup"></div>
	<!-- 弹窗结束 -->
	
  </body>
</html>




<%-- <%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>专业介绍页面</title>
    <meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
    <style type="text/css">
	   html body{ height:100%; width:100%; margin:0px; border:0px;overflow:hidden;}
	   .list{height:99%; margin:5px; width:25%;float:left; border:0px; overflow:auto;background-color: #e8e8e8; }
	   .list ul{ list-style:none; overflow:auto;  padding-bottom:20%;}
	   .list ul li{ cursor: pointer;}
	   .describe{ width:73%; height:99%; padding-bottom:-50px; border:0px; float:left; overflow: auto;background-color:#CCCCCC;}
   </style>
   <script type="text/javascript" src='<c:url value='/resources/js/jquery.js'/>'></script>
   <script type="text/javascript">	 
	  
	$(document).ready(function(){
       creatMenuLi();
    });
	
	function creatMenuLi()
	{
	    var ul=$("#major_list");
	    var firstUlLi=null;
	    <c:forEach items="${majors}" var="major" varStatus="status">      
	       var li= $("<li></li>");
	       if("${status.count}"=="1")
	       {
	          firstUlLi=li;
	       }
	       li.text("${major.major_name}");
	       li.click(function(){	           
	           $("#describe").html("<strong>核心课程:</strong><br/>"+"&nbsp;&nbsp;&nbsp;&nbsp;"+"${major.core_curriculum}"+"<br/><br/>"+
	               "<strong>就业方向:</strong><br/>"+"&nbsp;&nbsp;&nbsp;&nbsp;"+"${major.occupational_direction}"+"<br/><br/>"+
	               "<strong>培养目标:</strong><br/>"+"&nbsp;&nbsp;&nbsp;&nbsp;"+"${major.goal}"+"<br/><br/>"+
	               "<strong>办学历史:</strong><br/>"+"&nbsp;&nbsp;&nbsp;&nbsp;"+"${major.history}"+"<br/><br/>"
	               );
	       });
	       ul.append(li);	       
	    </c:forEach>
	    firstUlLi.click();	   
	}
	</script>
</head>

<body>
  <div id="professional_list" class="list">
     <ul id="major_list">
        <!--  <li>海洋科学</li>
         <li>力学</li>
         <li>农业工程</li>					
         <li>环境科学</li>
         <li>心理学</li>
         <li>统计学</li>
         <li>系统科学</li>	
         <li>地矿</li>
         <li>机械</li>
         <li>仪器仪表</li>
         <li>能源动力</li>
         <li>电气信息</li>	
         <li>土建</li>
         <li>测绘</li>
         <li>环境与安全</li>	
         <li>化工与制药</li>
         <li>交通运输</li>	
         <li>海洋工程</li>
         <li>航空航天</li>
         <li>武器</li>
         <li>工程力学</li>
         <li>生物工程	</li> 
         <li>公安技术</li>
         <li>材料科学	</li>
         <li>材料</li>
         <li> 水利</li>
         <li>航空航天</li>
         <li>武器</li>
         <li>工程力学</li>
         <li>生物工程	</li> 
         <li>公安技术</li>
         <li>材料科学	</li>
         <li>材料</li>
         <li> 水利</li>
         <li>工程力学</li>
         <li>生物工程	</li> 
         <li>公安技术</li>
         <li>材料科学	</li>
         <li>材料</li>
         <li> 水利</li> -->
     </ul>
  </div>
  <div id="describe" class="describe">
     <!-- <iframe id="iframe"></iframe> -->
  </div>
</body>
</html> --%>
