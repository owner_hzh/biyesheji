<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%   String swfFilePath=session.getAttribute("fileName").toString();
System.out.println("-------------------------"+swfFilePath);%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
  <head>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<link rel="stylesheet" type="text/css" href="<c:url value='../resources/css/courseware.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='../resources/css/flexpaper.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='../resources/flexpaper/css/flexpaper.css'/>" />
	<style type="text/css">
		.title{
			height:4%;
			width:95%;
			margin-top: 20px;
			background-color: blue;
			line-height:30px;
			color:#FFFFFF;
			font-size:17px;
			border-radius:15px; 
			text-align: left;
			font-weight: bold;
		}
	</style>
	
	<script type="text/javascript" src="<c:url value='../resources/js/jquery.js'/>"></script>  
	<script type="text/javascript" src="<c:url value='../resources/flexpaper/js/flexpaper.js'/>"></script>
	<script type="text/javascript" src="<c:url value='../resources/js/flexpaper_flash.js'/>"></script>
	<script type="text/javascript" src="<c:url value='../resources/flexpaper/js/flexpaper_handlers.js'/>"></script>
	<script type="text/javascript" src="<c:url value='../resources/flexpaper/js/flexpaper_handlers_debug.js'/>"></script>
    <script type="text/javascript">	 
		
  	</script>
  </head>	
	<!-- 课件内容开始-->
	<div class="content" align="center">
		<div class="title"><span style="margin-left: 14px;">${courseware_name }</span><a onclick="javascript:history.back(-1)" style="float: right;margin-right: 10px;cursor:pointer;">返&nbsp;回</a></div>
		<c:if test="${url!=null }">
	    	<div style="position:absolute;left:80px;top:60px;">           
			<a id="viewerPlaceHolder" style="width:950px;;height:650px;display:block"></a>                      
			<script type="text/javascript">              
					var fp = new FlexPaperViewer("<c:url value='../resources/FlexPaperViewer'/>",'viewerPlaceHolder', { config : { SwfFile : escape('../../uploads/<%=swfFilePath%>'),
						 Scale : 0.6, 
						 ZoomTransition : 'easeOut',
						 ZoomTime : 0.5,
						 ZoomInterval : 0.2,
						 FitPageOnLoad : true,
						 FitWidthOnLoad : false,
						 PrintEnabled : false,
						 FullScreenAsMaxWindow : false,
						 ProgressiveLoading : true,
						 MinZoomSize : 0.2,
						 MaxZoomSize : 5,
						 SearchMatchAll : false,
						 InitViewMode : 'Portrait',
						 
						 ViewModeToolsVisible : true,
						 ZoomToolsVisible : true,
						 NavToolsVisible : true,
						 CursorToolsVisible : true,
						 SearchToolsVisible : true,
							
							 localeChain: 'zh_CN'
	                       
																									}
																						});
			</script>                    
			</div>
	</c:if>
	<c:if test="${url==null }">
		<span style="margin-top:20px;">对不起，您查看的资料已被移动或删除！</span>
	</c:if>
	</div>
	<!-- 课件内容结束-->