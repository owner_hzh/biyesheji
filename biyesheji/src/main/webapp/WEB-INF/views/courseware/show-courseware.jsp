<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
  <head>
    <base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/courseware.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/js/uploadify/uploadify.css'/>">
	<link rel='stylesheet' href="<c:url value="/resources/css/pagination.css"/>" />
	<link rel='stylesheet' href="<c:url value="/resources/css/bootstrap.min.css"/>" />
	<style type="text/css">
		.title{
			height:4%;
			width:95%;
			margin-top: 20px;
			background-color: blue;
			line-height:30px;
			color:#FFFFFF;
			font-size:17px;
			border-radius:15px; 
			text-align: left;
			font-weight: bold;
		}
	</style>
	
	<script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>  
    <script type="text/javascript" src="<c:url value='resources/js/uploadify/jquery.uploadify.min.js'/>"></script>
    <script type="text/javascript">	 
    	//下载课件
		function downCourseWare(code,url,name,obj){
    		var temp =$(obj).parent().parent().children().eq(3).text();
    		var downcount = (parseInt(temp)+1); //下载次数
    		$(obj).parent().parent().children().eq(3).text(downcount)
			$.post("<c:url value='/courseware/down'/>",{courseware_code:code,downcount:downcount},function(date){
				if(date=="success"){
					$(obj).attr("href",url).on("click",function(){
					});
				}else{
					$(obj).parent().parent().children().eq(3).text(downcount-1)
					//location.reload();
				}
			});

		}


		//删除课件
		function deleteCourseWare(courseware_code,path ){
			if(confirm("是否删除？")){
				$.get("<c:url value='/courseware/delete'/>",{courseware_code:courseware_code,location:path},function(date){
					if(date=="success"){
						location.reload();
					}else{
						
					}
					if(date=="noauth"){
						var pp = window.parent.document.getElementById("iframe");//获得父级iframe
						$(pp).attr("src" , "<c:url value='/login/noauth'/>");//改变父级iframe的src属性
					}
				},"text");
			}
		}
		
		//返回专业页面
		function goback(){
			var pp = window.parent.document.getElementById("menutwo");//获得父级iframe
			$(pp).children(".activetwo").click();
		}
		
		//初始化
		$(function() {
			var code = ${curriculum_code};
			//上传课件文件
			 $("#upload").uploadify({
				 'height'         : 30,//表示按钮的高度，默认30PX。若要改为50PX，如下设置：'height' : 50，  
		         'width'          : 120,//表示按钮的宽度  
			     'swf'            : "<c:url value='/resources/js/uploadify/uploadify.swf' />",  
			     'uploader'       : "<c:url value='/courseware/upload/?dir=file&curriculum_code="+"${curriculum_code}&user_id="+"${sessionScope.loginuser.user_id} '/>",
			     'cancelImg'      : "<c:url value='/resources/js/uploadify/uploadify-cancel.png' />",
			     'buttonText'     :'选择上传课件' ,
			     'formData'       : {'folder' : 'file','curriculum_code':"${curriculum_code}"},
			     'auto'           : true,  
			     'multi'          : true,
			     'wmode'          : 'transparent', 
			     'fileObjName'    : 'file',//文件对象名称。用于在服务器端获取文件。
			     'simUploadLimit' : 1,  
			     'fileTypeExts'   : '*.zip;*.rar;*.doc;*.docx;*.ppt;*.txt;*.ptf',  
			     'fileTypeDesc'   : '文件(*.zip,*.rar,*.doc,*.docx,*.ppt,*.txt;)', 
			     'onUploadSuccess': function(file, data) {
			    	 //alert(data)
			     	var message=eval("("+data+")");
			         if(message.error==0){//上传
			        	 location.reload();
			         }else{
			        	 alert(message.message);
			         }
			     }  
			 }); 
			
			
		});
  	</script>
  </head>	
	<!-- 课件内容开始-->
	<div class="content" align="center">
		<div class="title"><span style="margin-left: 14px;">${curriculum_name }</span><a onclick="goback()" style="float: right;margin-right: 10px;cursor:pointer;">返&nbsp;回</a></div>
    	<c:if test="${pagecondition.totalCount!=0}">
    		<table class="table" style="margin-top: 10px;">
    			<tr>
    				<th>课件名称</th>
    				<th>发布日期</th>
    				<th>现在阅读次数</th>
    				<th>下载次数</th>
    				<th>资源类型</th>
    				<th>版本</th>
    				<th>操作</th>
    			</tr>
    			<c:forEach items="${pagecondition.dataList}" var="cwl">
	    			<tr>
	    				<c:choose>
							<c:when test="${cwl.filename.length() >7}">
								<td style="text-align: left;" title="${cwl.filename}">
								<img src="<c:url value='/resources/img/coursewareTypeImg/${cwl.sourcetype }.gif '/>" align="middle" style="margin-right: 5px;vertical-align:middle;"/>${cwl.filename.substring(0,7)}..</td>
							</c:when>
							<c:otherwise>
								<td style="text-align: left;"><img src="<c:url value='/resources/img/coursewareTypeImg/${cwl.sourcetype }.gif '/>" align="middle" style="margin-right: 5px;vertical-align:middle;"/>${cwl.filename}</td>
							</c:otherwise>
						</c:choose>
	    				<td>${cwl.uploaddate}</td>
	    				<td>${cwl.onlinelook}</td>
	    				<td>${cwl.downcount}</td>
	    				<td>${cwl.sourcetype}</td>
	    				<td>${cwl.courseware_version}</td>
	    				<td style="text-align: left;">
	    					<a onclick="downCourseWare('${cwl.courseware_code}','${cwl.location }','${cwl.filename}',this)" href="${cwl.location }">下载</a>
	    				    <c:if test="${cwl.sourcetype!='ZIP'}">
		    					<a href="<c:url value='/courseware/onlinelook?filename=${cwl.filename}&location=${cwl.location }'/>">在线预览</a>
					    	</c:if>
	    				    <c:if test="${sessionScope.loginuser.role_code!=2}"><!-- 排除学生 -->
	    				    	<c:if test="${sessionScope.loginuser.role_code==6||sessionScope.loginuser.user_id==cwl.user_id}">
		    						<a onclick="deleteCourseWare('${cwl.courseware_code}','${cwl.location }')">删除</a>
		    					</c:if>
					    	</c:if>
	    				</td>
	    			</tr>
    			</c:forEach>
    		</table>
    		${pagecondition.show }
    	</c:if>
    	<c:if test="${pagecondition.totalCount==0}">
    		<div style = "margin-top: 10px;font-size: 16px;font-weight: lighter;">暂无任何课件</div>
    	</c:if>
    	<c:if test="${sessionScope.loginuser.role_code!=2}">
	    	<div style="float: right;width:122px;margin-top: 10px;margin-right: 60px;"><input type="file" id="upload"/></div>
    	</c:if>
	</div>
	<!-- 课件内容结束-->