<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'show-detail.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>
	<script type="text/javascript" src="<c:url value='resources/ckeditor/ckeditor.js'/>"></script>
	<script type="text/javascript" src="<c:url value='resources/ckeditor/adapters/jquery.js'/>"></script> 
    <style type="text/css">
       .title{ margin-left: 20px;}
       .content{  margin-left: 20px;
                  margin-top: 20px;
                  margin-bottom: 10px;
                  margin-right: 60px;}
       .dtitle{
              margin: 20px;
              font-size: xx-large;
       }
       
       div.content
		{
			border: solid 2px transparent;
			padding-left: 15px;
			padding-right: 15px;
		}

		div.content:hover
		{
			border-color: black;		
		}
		
		.c{
		    cursor: pointer;
		    margin-right: 30px;
		    background-color: #e8e8e8;
		    color:#AA531E;
		    font-size: x-large;
            border-radius: 5px;
            padding: 5px;
		}
		
		.c:HOVER {
	       color: blue;
	       text-decoration: underline;
         }
		
    </style>
    
    <script type="text/javascript">
        $(function(){
           $("#update").click(function(){
              parent.menuclick("${major.major_code}");
           });
        });
		window.onload = function() {
			// Listen to the double click event.
			if ( window.addEventListener )
				document.body.addEventListener( 'dblclick', onDoubleClick, false );
			else if ( window.attachEvent )
				document.body.attachEvent( 'ondblclick', onDoubleClick );

		};

		function onDoubleClick( ev ) {
			// Get the element which fired the event. This is not necessarily the
			// element to which the event has been attached.
			var element = ev.target || ev.srcElement;

			// Find out the div that holds this element.
			var name;

			do {
				element = element.parentNode;
			}
			while ( element && ( name = element.nodeName.toLowerCase() ) &&
				( name != 'div' || element.className.indexOf( 'content' ) == -1 ) && name != 'body' );

			if ( name == 'div' && element.className.indexOf( 'content' ) != -1 )
				replaceDiv( element );
		}

		var editor,whichId;

		function replaceDiv( div ) {
			if ( editor )
				editor.destroy();

			editor = CKEDITOR.replace( div );
			whichId=div.id;
		}
		
		//提交修改后的数据
		function getContents()
		{
		   var s=qubiaoqian(editor.getData());
	       //alert(  );
	       //alert(whichId);
		   $.ajax({
               type: "POST",
               dataType: "text",//返回json格式的数据
			   url: "major/updatehtml"+"?noCache="+Math.floor(Math.random()*100000),
			   data: {
			            fild:whichId,
			            major_code:"${major.major_code}",
			            fildString:s
			         },
			   success:function(data){
                   alert("修改成功！");
                   if ( editor )
				      editor.destroy();
			   },
			   error:function(XMLHttpRequest, textStatus, errorThrown){
			         alert("修改出错！");
			   }
           }); 
		}
		
		function qubiaoqian(str)
		{
		  str=removeHTML(removeBODY(removeHEAD(removeTitle(str))));
		  str = str.replace(/<p>&nbsp;&nbsp;&nbsp;&nbsp;/g,'').trim();
		  str = str.substring(0,str.length-4);
		  return str;
		}
		
		//去掉title标签
		function removeTitle(str)
		{
		  str = str.replace(/<title>.*<\/title>/g,''); //去除HTML tag
		  return str;
		}
		//去掉html标签
		function removeHTML(str)
		{
		  str = str.replace(/<html>/g,''); //去除HTML tag
		  str = str.replace(/<\/html>/g,''); //去除HTML tag
		  return str;
		}
		
		//去掉head标签
		function removeHEAD(str)
		{
		  str = str.replace(/<head>/g,''); //去除HTML tag
		  str = str.replace(/<\/head>/g,''); //去除HTML tag
		  return str;
		}
		
		//去掉body标签
		function removeBODY(str)
		{
		  str = str.replace(/<body>&nbsp;<\/body>/g,''); //去除HTML tag
		  str = str.replace(/<body>/g,''); //去除HTML tag
		  str = str.replace(/<\/body>/g,''); //去除HTML tag
		  return str;
		}
		
		function removeHTMLTag(str) {
            str = str.replace(/<\/?[^>]*>/g,''); //去除HTML tag
            str = str.replace(/[ | ]*\n/g,'\n'); //去除行尾空白
            //str = str.replace(/\n[\s| | ]*\r/g,'\n'); //去除多余空行
            str=str.replace(/&nbsp;/ig,'');//去掉&nbsp;
            return str;
     }
     
     
		function seashowtip(flag, iwidth) {
			var my_tips = document.all.mytips;
			if (flag) {
				my_tips.innerHTML = "双击进行编辑";
				my_tips.style.display = "";
				my_tips.style.width = iwidth;
				my_tips.style.left = event.clientX + 10
						+ document.body.scrollLeft;
				my_tips.style.top = event.clientY + 10
						+ document.body.scrollTop;
			} else {
				my_tips.style.display = "none";
			}
		}
	</script>
  </head>
  
  <body>
  
  
    <div style="float: right;"><a id="update" class="c">确认</a></div>
    
    <center class="dtitle">
        ${major.major_name}(专业代码:${major.major_code})
    </center>
  
    <strong class="title">一、基本学制与学习年限:</strong><br/>
    <div class="content" id="schooling_length" onmouseover="seashowtip(1,100)" onmouseout="seashowtip(0,140)"><p>&nbsp;&nbsp;&nbsp;&nbsp;${major.schooling_length}</p></div><br/>
    
    <strong class="title" >二、学位授予:</strong><br/>
    <div class="content" id="degree_awarding" onmouseover="seashowtip(1,100)" onmouseout="seashowtip(0,140)"><p>&nbsp;&nbsp;&nbsp;&nbsp;${major.degree_awarding}</p></div><br/>
    
    <strong class="title">三、专业定位:</strong><br/>
    <div class="content" id="occupational_direction" onmouseover="seashowtip(1,100)" onmouseout="seashowtip(0,140)"><p>&nbsp;&nbsp;&nbsp;&nbsp;${major.occupational_direction}</p></div><br/>
    
    <strong class="title">四、培养目标:</strong><br/>
    <div class="content" id="goal" onmouseover="seashowtip(1,100)" onmouseout="seashowtip(0,140)"><p>&nbsp;&nbsp;&nbsp;&nbsp;${major.goal}</p></div><br/>
    
    <strong class="title">五、基本规格要求:</strong><br/>
    <div class="content" id="required" onmouseover="seashowtip(1,100)" onmouseout="seashowtip(0,140)"><p>&nbsp;&nbsp;&nbsp;&nbsp;${major.required}</p></div><br/>
    
    <strong class="title">六、主干学科和核心课程:</strong><br/>  
    <div class="content" id="core_curriculum" onmouseover="seashowtip(1,100)" onmouseout="seashowtip(0,140)"><p>&nbsp;&nbsp;&nbsp;&nbsp;${major.core_curriculum}</p></div><br/>
        
    <strong class="title">七、办学历史:</strong><br/>  
    <div class="content" id="history" onmouseover="seashowtip(1,100)" onmouseout="seashowtip(0,140)"><p>&nbsp;&nbsp;&nbsp;&nbsp;${major.history}</p></div><br/>
       
       
    <div id=mytips style="position:absolute;text-align:center; background-color:#ffffff; border-radius:5px; font-size:15px; width:121;height:16;border:0px solid ;display:none;filter: progid:DXImageTransform.Microsoft.Shadow(color=#999999,direction=135,strength=3); left:0; top:5"></div>
  </body>
</html>
