<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'show-detail.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>
	<script type="text/javascript" src="<c:url value='resources/ckeditor/ckeditor.js'/>"></script>
	<script type="text/javascript" src="<c:url value='resources/ckeditor/adapters/jquery.js'/>"></script> 
    <style type="text/css">
       .title{ margin-left: 20px;}
       .content{  margin-left: 20px;
                  margin-top: 20px;
                  margin-bottom: 10px;
                  margin-right: 60px;}
       .dtitle{
              margin: 20px;
              font-size: xx-large;
       }
		
		.c{
		    cursor: pointer;
		    margin-right: 30px;
		    background-color: #e8e8e8;
		    color:#AA531E;
		    font-size: x-large;
            border-radius: 5px;
            padding: 5px;
		}
		
		.c:HOVER {
	       color: blue;
	       text-decoration: underline;
         }
		
    </style>
    
    <script type="text/javascript">
        $(function(){
           $("#update").click(function(){
              parent.update("${major.major_code}");
           });
        });

	</script>
  </head>
  
  <body>
    <div style="float: right;"><a id="update" class="c">更新</a></div>
    
    <center class="dtitle">${major.major_name}</center>
  
    <strong class="title">一、基本学制与学习年限:</strong><br/>
    <div class="content" id="schooling_length"><p>&nbsp;&nbsp;&nbsp;&nbsp;${major.schooling_length}</p></div><br/>
    
    <strong class="title" >二、学位授予:</strong><br/>
    <div class="content" id="degree_awarding"><p>&nbsp;&nbsp;&nbsp;&nbsp;${major.degree_awarding}</p></div><br/>
    
    <strong class="title">三、专业定位:</strong><br/>
    <div class="content" id="occupational_direction"><p>&nbsp;&nbsp;&nbsp;&nbsp;${major.occupational_direction}</p></div><br/>
    
    <strong class="title">四、培养目标:</strong><br/>
    <div class="content" id="goal"><p>&nbsp;&nbsp;&nbsp;&nbsp;${major.goal}</p></div><br/>
    
    <strong class="title">五、基本规格要求:</strong><br/>
    <div class="content" id="required"><p>&nbsp;&nbsp;&nbsp;&nbsp;${major.required}</p></div><br/>
    
    <strong class="title">六、主干学科和核心课程:</strong><br/>  
    <div class="content" id="core_curriculum"><p>&nbsp;&nbsp;&nbsp;&nbsp;${major.core_curriculum}</p></div><br/>
        
    <strong class="title">七、办学历史:</strong><br/>  
    <div class="content" id="history"><p>&nbsp;&nbsp;&nbsp;&nbsp;${major.history}</p></div><br/>
       
  </body>
</html>
