<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'content.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<script type="text/javascript" src='<c:url value='resources/js/jquery.js'/>'></script>
    <script type="text/javascript">
       $(document).ready(function(){
          $("#submit").click(function(){
          
              var major_name=$("#major_name").val().trim();
              if(major_name==null||major_name=="")
              {
                 alert("专业名称不能为空！");
              }else
              {
                 $.ajax({
				   type: "POST",
				   dataType: "text",//返回text格式的数据
				   url: "major/sysmain/submitMajor",
				   data: {
				            major_code:$("#major_code").val(),
		                    major_name:major_name,
		                    schooling_length:$("#schooling_length").html(),
		                    degree_awarding:$("#degree_awarding").html(),
						    core_curriculum: $("#core_curriculum").html(),
						    occupational_direction:$("#occupational_direction").html(),
						    goal:$("#goal").html(),
						    required:$("#required").html(),
						    history:$("#history").html()
				         },
				   success: function(msg,status){
				     if(status=="success"&&msg=="true")
				     {
				        alert("数据更新成功");
				        parent.closePopWindow();				        	                    
				     }else
				     {
				        alert("数据更新失败！");
				     }
				   },
			   error:function(XMLHttpRequest, textStatus, errorThrown){
			         alert("服务器出错！");
			        }
				});
              }
              
          });
       });
       
       function rec(obj)
       {
          $(obj).removeClass("kong");
       }
       
    </script>
    
    <style type="text/css">
       input{
			font-size:18px;
			width:250px;
		}
		.commit{
			cursor: pointer;
			background-color: #FFCC66;
			margin-top:20px;
			margin-bottom:40px;
			margin-left:19%;
			height:40px;
			font-size:20px;
			font-family:"幼圆";
			border-radius:10px;
			border: 0px;
		}
		.commit:hover{
			background-color: #0033CC;
			color:#ffffff;
		}
		
		.title{
		   font-size:25px;
		   font-weight:bolder;
		   font-family:"幼圆";
		   margin-top: 20px;
		   margin-bottom: 20px;
		}
		
		.item{
		   font-family:"幼圆";
		   font-size:15px;
		   margin-top: 20px;
		}
		
		.itemTitle{
		   padding-bottom: 10px;
		}
		
	.editTextarea{
       border:1px solid #999;
       font-size:12px;
       font-family:"幼圆";
       padding:1px;
       height:140px;
       overflow:auto;
       width:70%;
       text-align:left;
       padding:5px;
       }
      .kong{
      border-color: red;}
    </style>
    
  </head>
  
  <body>
    <div id="content_2_1" class="item_content">
	           
	       <div style="margin-left: 20%;">	          
	          <div class="item">
	             <input type="hidden" id="major_code" value="${major.major_code}"/>
	                                      专业名称：<input type="text" id="major_name" onkeydown="rec(this)" placeholder="专业名称" value="${major.major_name}"/>
	          </div>
	          <div class="item">
	             <span class="itemTitle">基本学制与学习年限:</span><br>
	             <div contenteditable="true" class="editTextarea" id="schooling_length" placeholder="描述基本学制与学习年限...">${major.schooling_length}</div>
	          </div>
	          <div class="item">
	             <span class="itemTitle">学位授予:</span><br>
	             <div contenteditable="true" class="editTextarea" id="degree_awarding" placeholder="描述学位授予情况...">${major.degree_awarding}</div>
	          </div>
	          <div class="item">
	             <span class="itemTitle">专业定位:</span><br>
	             <div contenteditable="true" class="editTextarea" id="occupational_direction" placeholder="描述专业定位情况...">${major.occupational_direction}</div>
	          </div>
	          <div class="item">
	             <span class="itemTitle">培养目标:</span><br>
	             <div contenteditable="true" class="editTextarea" id="goal" placeholder="描述培养和目标的情况...">${major.goal}</div>
	          </div>
	          <div class="item">
	             <span class="itemTitle" > 基本规格要求:</span><br>
	             <div contenteditable="true" id="required" class="editTextarea" placeholder="描述基本规格要求的情况...">
                                         ${major.required}        
                 </div>
	             <%-- <div class="editTextarea" id="required" name="required" placeholder="描述基本规格要求的情况...">${major.required}</div> --%>
	          </div>
	          <div class="item">
	             <span class="itemTitle">主干学科和核心课程:</span><br>
	             <div contenteditable="true" class="editTextarea" id="core_curriculum" placeholder="描述主干学科和核心课程情况...">${major.core_curriculum}</div>
	          </div>
	          <div class="item">
	             <span class="itemTitle">办学历史:</span><br>
	             <div contenteditable="true" class="editTextarea" id="history" placeholder="描述办学历史...">${major.history}</div>
	          </div>
	          
	          <input type="button" class="commit" value="提交" id="submit" />	         
	          
	       </div>

       </div>
  </body>
</html>
