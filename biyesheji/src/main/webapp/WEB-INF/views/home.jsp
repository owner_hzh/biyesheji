<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page session="false" %>
<%@page import="com.cqjt.pojo.User"%>
<html>
<head>
	<link rel="icon"href="<c:url value='/resources/img/biaozhi.ico'/>" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/popwindow.css'/>">
	<title>网络资源共享平台</title>
</head>
<script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>
<script type="text/javascript" src='<c:url value='/resources/js/index1.js'/>'></script>
<script type="text/javascript" src="<c:url value='resources/js/popwindow/popwindow.js'/>"></script>
<script type="text/javascript">
 	$(function(){
		$("#menu").children().eq(0).addClass("active");
	    $("#content_iframe").attr("src",$("#menu").children().eq(0).attr("urldate"));
	}); 
	function gotoTwomenu(urlstr,obj)
	{
	    $("#content_iframe").attr("src",urlstr);
	    $(".active").removeClass("active");
		$(obj).addClass("active");
	}
	function gohome(){
		$(".active").removeClass("active");
		$("#menu").children().eq(0).addClass("active");
		$("#content_iframe").attr("src",$("#menu").children().eq(0).attr("urldate"));
	}
	function changepassword()
	{
		 $(window).ShowDialog({ width: 400, height: 300, title: "修改密码", src: "displaychangepassword" });
	}
	
	//个人中心
	function showMyDiv(){
		var div = $("#mydiv");
		var info = $("#info");
		var XY = info.offset();
		if(div.css("display")=='none'){
			div.offset({ top: XY.top+20, left:XY.left+10 });
			div.show();
		}
	}
	function hideMyDiv(){
		var div = $("#mydiv");
		div.offset({ top:0, left:0 });
		div.hide();
	}
	// 修改教师信息
    function showTeacherModify(user_id)
    {
    	 $(window).ShowDialog({ width: 800, height: 600, title: "教师信息修改", src: "<c:url value='/teacherinfo/displayTeacherModify?user_id="+user_id+"'/>" });
    }
</script>

<!-- <link rel=stylesheet href="css/index1.css" type="text/css"> -->
<style type="text/css" >
   .header{ height:17.5%; width:100%; margin:0 auto; padding:0; 
   			border:1px; background-image:  url('<c:url value='/resources/img/header1.gif'/>'); background-repeat:no-repeat; background-position:center;background-size:cover;
   			border-radius:10px; 
   			position:relative;}
   .footer{height:2.5%;width:100%; background-color:#cccccc; text-align:center;margin:0 auto; padding:0;}
   .menu{ width:90%;  height: 5%; 
   		  /* background-color:#990;  */
   		  padding:0 ;
   		  margin-top:130px;
   		  margin-left:8.5%;
   		  /* margin-top:130px; */
   		  bottom:15%;
   		  list-style:none;
   		  color:#AA531E;
   		  text-align: left;
   		  position:absolute;}
   .menu a{font-size:16px; font-weight:bold;
   		   cursor:pointer;line-height:30px;
   		   background-color:#ffffff;
   		   padding-left:6px;padding-right:6px;padding-top: 5px;padding-bottom: 5px;
   		   margin-left:8px;
   		   border-radius:5px; 
   		   }
   .menu a:hover{cursor:pointer;text-decoration: underline;color:blue;}
   .logininfo{
   		  width:30%;  height: 5%; 
   		  padding:0 ;
   		  margin-top:10px;
   		  margin-left:70%;
   		  color:white;
   		  position:absolute;
   }
   .logininfo div{
	    float:right;
	    margin-right:10px;
    }
   .logininfo div img{
	    vertical-align:middle;
    }
   .logininfo div a{
	    color: #FFFFFF;
	    font-size: 16px;
	    text-decoration: none;
    }
   .body{ height:80%; width:100%; margin:6px auto; padding:0}
   .content_iframe{ width:100%; height:100%;overflow:auto; margin:0 auto; padding:0}
   .active{color:blue;}
   .mydiv{
   		display: none;
   		position: fixed;
   		width:68px;
   }
</style>
</head>


<body style="height:950px; width:1300px; margin-top:1px; border:0px;margin-left:-650px;;left:50%;position:absolute;">

<!-- <body style="height:950px; width:90%;margin:0 auto; border:0px;"> -->

   <div id="header" class="header">
   	<div id="logininfo" class="logininfo">
<%
	Object usersession=request.getSession().getAttribute("loginuser");
	if(usersession!=null){
		User us = (User) usersession;
%>
   		<div id="exit" class="exit">
			<img src="<c:url value='/resources/img/out.png'/>" /><a href="<c:url value='/login/loginout'/>"><% if(us.getUser_id().equals("0")){%>登录<% }else{%>退出<%} %></a>
		</div>
   		<div id="home" class="home">
			<img src="<c:url value='/resources/img/home.png'/>" /><a href="javascript:gohome()">返回首页</a>
		</div>
		<div id="info" class="info">
			<a style="line-height: 35px;font-size:17px;font-weight: bold;font-family: '幼圆';" <% if(!us.getUser_id().equals("0")){%>href="javascript:void(0)" onmouseover="showMyDiv()" onmouseout="hideMyDiv()" <% }%>><%=us.getName()%> 您好</a>
		</div>
		<div id="mydiv" class="mydiv"  onmouseover="showMyDiv()"  onmouseout="hideMyDiv()">
			 <% if(us.getRole_code()!=2){%>
			 	<a style="line-height: 55px;font-family: '幼圆';" onmouseover="showMyDiv()" onmouseout="hideMyDiv()" href="javascript:showTeacherModify( '<%=us.getUser_id()%>' )" title="点击进入个人中心">个人中心</a>
			 	<a style="line-height: 5px;font-family: '幼圆';" href="javascript:changepassword();" onmouseover="showMyDiv()" onmouseout="hideMyDiv()">修改密码</a>
			 <% }%>
			 <% if(us.getRole_code()==2){%>
			 	<a style="line-height: 55px;font-family: '幼圆';" href="javascript:changepassword();" onmouseover="showMyDiv()" onmouseout="hideMyDiv()">修改密码</a>
			 <% }%><%}%>
		</div>
   	</div>
    <div id="menu" class="menu">
	<c:forEach items="${menuList}" var="menu">
		<a onclick="gotoTwomenu('${menu.mapping_page}',this)" urldate="${menu.mapping_page}">${menu.menu_level_one_name}</a>
	</c:forEach>
    </div> 
   </div>
   <div id="body" class="body">
        <iframe id="content_iframe" class="content_iframe" marginheight="0" marginwidth="0" hspace="0" vspace="0" frameborder="0"></iframe>
   </div> 
   <div id="footer" class="footer">@copyright 2014 auther:owner</div>
</body>
</html>
