<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
  <head>
    <base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/courseware.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/js/uploadify/uploadify.css'/>">
	<style type="text/css">
		.title{
			height:4%;
			width:95%;
			margin-top: 20px;
			background-color: blue;
			line-height:30px;
			color:#FFFFFF;
			font-size:17px;
			border-radius:15px; 
			text-align: left;
			font-weight: bold;
		}
	</style>
	
	<script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>  
    <script type="text/javascript" src="<c:url value='resources/js/uploadify/jquery.uploadify.min.js'/>"></script>
   	 <script type="text/javascript">	 
    	//下载教辅资料
		function downTeachingdata(code,url,name,obj){
			$.post("<c:url value='/teachingdata/downTeachingdata'/>",{teachingdata_code:code},function(date){
				if(date=="success")
				{
					$(obj).attr("href",url).on("click",function(){
					});
				}
			});
		}	
		//删除作业
		function deleteTeachingdata(teachingdata_code){
			if(confirm("是否删除？")){
				$.get("<c:url value='/teachingdata/deleteTeachingdata'/>",{teachingdata_code:teachingdata_code},function(date){
					if(date=="success"){
						location.reload();
					}
				});
			}
		}
		//返回专业页面
		function goback(){
			var pp = window.parent.document.getElementById("menutwo");//获得父级iframe
			$(pp).children(".activetwo").click();
		}
		//初始化
		$(function() {
			var code = ${curriculum_code};
			//上传作业文件
			 $("#upload").uploadify({
				 'height'         : 30,//表示按钮的高度，默认30PX。若要改为50PX，如下设置：'height' : 50，  
		         'width'          : 120,//表示按钮的宽度  
			     'swf'            : "<c:url value='/resources/js/uploadify/uploadify.swf' />",  
			     'uploader'       : "<c:url value='/teachingdata/uploadTeachingdata/?dir=file&curriculum_code="+"${curriculum_code}"+" '/>",
			     'cancelImg'      : "<c:url value='/resources/js/uploadify/uploadify-cancel.png' />",
			     'buttonText'     :'选择上传教辅资料' ,
			     'formData'       : {'folder' : 'file','curriculum_code':"${curriculum_code}"},
			     'auto'           : true,  
			     'multi'          : true, 
			     'method' : 'post',
			     'wmode'          : 'transparent', 
			     'fileObjName'    : 'file',//文件对象名称。用于在服务器端获取文件。
			     'simUploadLimit' : 1,  
			     'fileTypeExts'        : '*.zip;*.rar;*.doc;*.docx;*.xls;*.xlsx;*.ppt;*.txt;',  
			     'fileTypeDesc'       : '文件(*.zip,*.rar,*.doc,*.docx,*.xls,*.xlsx,*.ppt,*.txt;)', 
			     'onUploadSuccess': function(file, data) {
			    	 //alert(data)
			     	var message=eval("("+data+")");
			         if(message.error==0){//上传
			        	 location.reload();
			         }else{
			        	 alert(message.message);
			         }
			     }  
			 }); 
		});
	</script>
   	
  </head>	
	<!-- 课件内容开始-->
	<div class="content" align="center">
		<div class="title"><span style="margin-left: 14px;">${curriculum_name }</span><a onclick="goback()" style="float: right;margin-right: 10px;cursor:pointer;">返&nbsp;回</a></div>
    	<c:if test="${teachingDataList!=null }">
    		<table class="table" style="margin-top: 10px;">
    			<tr>
    				<th>教辅资料名称</th>
    				<th>操作</th>
    			</tr>
    			<c:forEach items="${teachingDataList}" var="tdl">
	    			<tr>
	    				
	    				<td>${tdl.filename}</td>
	    				<td>
	    					<%-- <a href="<c:url value='/teachingdata/onlinelook'/>">在线预览</a> --%>
	    					<a onclick="downTeachingdata('${tdl.teachingdata_code}','${tdl.location }','${tdl.filename}')" href="${tdl.location }">下载</a>
	    					<c:if test="${loginuserRole==3||loginuserRole==6 }"><a onclick="deleteTeachingdata(${tdl.teachingdata_code})">删除</a></c:if>
	    					
	    				</td>
	    				
	    			</tr>
    			</c:forEach>
    		</table>
    	</c:if>
    	<c:if test="${teachingDataList==null }">
    		<div style = "margin-top: 10px;font-size: 16px;font-weight: lighter;">暂无任何教辅资料</div>
    	</c:if>
    	<c:if test="${loginuserRole==3||loginuserRole==6 }">	
    		<div style="float: right;width:122px;margin-top: 10px;margin-right: 50px;"><input type="file" id="upload" width="120px" height="30px"/></div>
		</c:if>
	</div>
	<!-- 课件内容结束-->