<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%   String swfFilePath=session.getAttribute("fileName").toString();
System.out.println("-------------------------"+swfFilePath);%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
  <head>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<link rel="stylesheet" type="text/css" href="<c:url value='../resources/css/courseware.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='../resources/css/flexpaper.css'/>">
    <link rel="stylesheet" type="text/css" href="<c:url value='../resources/flexpaper/css/flexpaper.css'/>" />
	<style type="text/css">
		.title{
			height:4%;
			width:95%;
			margin-top: 20px;
			background-color: blue;
			line-height:30px;
			color:#FFFFFF;
			font-size:17px;
			border-radius:15px; 
			text-align: left;
			font-weight: bold;
		}
	</style>
	
	<script type="text/javascript" src="<c:url value='../resources/js/jquery.js'/>"></script>  
	<script type="text/javascript" src="<c:url value='../resources/flexpaper/js/flexpaper.js'/>"></script>
	<script type="text/javascript" src="<c:url value='../resources/js/flexpaper_flash.js'/>"></script>
	<script type="text/javascript" src="<c:url value='../resources/flexpaper/js/flexpaper_handlers.js'/>"></script>
	<script type="text/javascript" src="<c:url value='../resources/flexpaper/js/flexpaper_handlers_debug.js'/>"></script>
    <script type="text/javascript">	 
		//返回专业页面
		function goback(){
			var pp = window.parent.document.getElementById("menutwo");//获得父级iframe
			$(pp).children(".activetwo").click();
		}
  	</script>
  </head>	
	<!-- 课件内容开始-->
	<div class="content" align="center">
		<div class="title"><span style="margin-left: 14px;">${curriculum_name }</span><a onclick="goback()" style="float: right;margin-right: 10px;cursor:pointer;">返&nbsp;回</a></div>
		<%--
        <div id="documentViewer" class="flexpaper_viewer" style="width:770px;height:500px;"></div>
        <script type="text/javascript">
        function getDocumentUrl(document){
        	return "php/services/view.php?doc={doc}&format={format}&page={page}".replace("{doc}",document);	        
        }
        			
        var td = (function(){
        			try {return 'ontouchstart' in document.documentElement;} 
        			catch (e) {return false;} 
        		    })();
        if(td){$('#documentViewer').css({left : '0px', top : '0px', width:'100%',height:'100%'});}	       

        var searchServiceUrl	= escape('php/services/containstext.php?doc=Monash_Magazine&page=[page]&searchterm=[searchterm]');
        		         
        var startDocument = "Paper";
        				$('#documentViewer').FlexPaperViewer(
        						 { config : {

        						 SwfFile : "{samples/Monash_Magazine.pdf_[*,0].swf,28}",
        						 IMGFiles : "samples/Monash_Magazine.pdf_{page}.jpg",
        						 ThumbIMGFiles : "samples/Monash_Magazine.pdf_{page}_res_200.jpg", 
        						 JSONFile : "samples/Monash_Magazine.pdf.js",
        						 PDFFile : "samples/Monash_Magazine.pdf",
        						 HighResIMGFiles : 'png2jpeg.php?path=Monash_Magazine.pdf_{page}_highres.png&sector={sector}',
        						 key : '$8ed9d54d152a6cfe113',
        						 Scale : 0.6, 
        						 ZoomTransition : 'easeOut',
        						 ZoomTime : 0.5, 
        						 ZoomInterval : 0.2,
        						 FitPageOnLoad : true,
        						 FitWidthOnLoad : false, 
        						 FullScreenAsMaxWindow : false,
        						 ProgressiveLoading : false,
        						 MinZoomSize : 0.2,
        						 MaxZoomSize : 5,
        						 SearchMatchAll : false,
        						
        						 RenderingOrder : 'html5,flash',
        						 MixedMode : true,
        						 EnableCornerDragging : true,
        						 SearchServiceUrl : searchServiceUrl,
        						 ViewModeToolsVisible : true,
        						 ZoomToolsVisible : true,
        						 NavToolsVisible : true,
        						 CursorToolsVisible : true,
        						 SearchToolsVisible : true,
        						 UIConfig : 'UIConfig_monash_magazine.pdf.xml?x',
          						 //BackgroundColor : '#dddddd', 
                                 //PanelColor : '#888888',
        						 WMode : 'transparent',
        						 TrackingNumber : 'UA-10148899-1',
          						 localeChain: 'en_US'
        						 }});
    </script>--%>
    	<div style="position:absolute;left:80px;top:60px;">           
		<a id="viewerPlaceHolder" style="width:820px;height:650px;display:block"></a>                      
		<script type="text/javascript">              
				var fp = new FlexPaperViewer("<c:url value='../resources/FlexPaperViewer'/>",'viewerPlaceHolder', { config : { SwfFile : escape('../../uploads/<%=swfFilePath%>'),
					 Scale : 0.6, 
					 ZoomTransition : 'easeOut',
					 ZoomTime : 0.5,
					 ZoomInterval : 0.2,
					 FitPageOnLoad : true,
					 FitWidthOnLoad : false,
					 PrintEnabled : false,
					 FullScreenAsMaxWindow : false,
					 ProgressiveLoading : true,
					 MinZoomSize : 0.2,
					 MaxZoomSize : 5,
					 SearchMatchAll : false,
					 InitViewMode : 'Portrait',
					 
					 ViewModeToolsVisible : true,
					 ZoomToolsVisible : true,
					 NavToolsVisible : true,
					 CursorToolsVisible : true,
					 SearchToolsVisible : true,
						
						 localeChain: 'zh_CN'
                       
																								}
																					});
		</script>                    
		</div>
	</div>
	<!-- 课件内容结束-->