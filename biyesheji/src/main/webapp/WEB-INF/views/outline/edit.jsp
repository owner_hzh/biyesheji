<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'edit.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>
	<!-- 下面这两个js文件位置不能交换 -->
    <script type="text/javascript" src="<c:url value='resources/ckeditor/ckeditor.js'/>"></script> 
    <script type="text/javascript" src="<c:url value='resources/ckeditor/adapters/jquery.js'/>"></script> 
    <script type="text/javascript">	
    
		$(document).ready(function(){
		    $("#editor1").ckeditor();	    			
		});
		
		
        var editor;
		// The instanceReady event is fired, when an instance of CKEditor has finished
		// its initialization.
		CKEDITOR.on( 'instanceReady', function( ev ) {
				   
		    ev.editor.dataProcessor.writer.setRules( 'p', 
		     { 
		       indent : false, 
		       breakBeforeOpen : true, 
		       breakAfterOpen : false, 
		       breakBeforeClose : false, 
		       breakAfterClose : false 
		     }); 
		    ev.editor.dataProcessor.writer.setRules( 'li',
		     { 
		       indent : false, 
		       breakBeforeOpen : true, 
		       breakAfterOpen : false, 
		       breakBeforeClose : false, 
		       breakAfterClose : false 
		     }); 
		     ev.editor.dataProcessor.writer.indentationChars = ' '; 
		     ev.editor.dataProcessor.writer.lineBreakChars = '\n';
		
			editor = ev.editor;
            editor.insertHtml($("#htmlString").val());
		}); 
			
	    //提交修改后的数据
		function getContents()
		{
		   var data=editor.getData();
		  /*  var editor = CKEDITOR.instances.editor1;
			   //var text=editor.document.getBody().getText(); //取得纯文本
           var html=editor.document.getBody().getHtml(); //取得html文本 */
		   $.ajax({
               type: "POST",
               dataType: "text",//返回json格式的数据
			   url: "outline/savehtml"+"?noCache="+Math.floor(Math.random()*100000),
			   data: {
			            data:data,
			            file:"${htmlfile}"
			         },
			   success:function(data){
                    if(data=="true")
                    {
                       parent.menuclick("${code}","${ocode}");
                    }else
                    {
                      alert("提交失败，请重试！");
                    }
			   },
			   error:function(XMLHttpRequest, textStatus, errorThrown){
			         //alert("error-->文档删除失败！");
			   }
           }); 
		}
		
		function fullscreen()
		{
		   parent.fullscreen();
		}
	</script>     
  </head>
  
  <body>
    <textarea cols="100" id="editor1" name="editor1" rows="10"></textarea>  
    <textarea style="display: none;" cols="100" id="htmlString" name="htmlString" rows="10">${htmlString}</textarea>
  </body>
</html>
