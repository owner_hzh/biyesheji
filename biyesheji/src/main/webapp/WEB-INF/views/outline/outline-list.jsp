<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'outline-list.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/js/uploadify/uploadify.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/courseware.css'/>">
	
	<script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>  
    <script type="text/javascript" src="<c:url value='resources/js/uploadify/jquery.uploadify.min.js'/>"></script>	
	<script type="text/javascript">
	   function show(code,ocode)
	   {
	      parent.menuclick(code,ocode); 
	   };  
	   
	   function edithtml(code,ocode)
	   {
	      $('#pageoffice',window.parent.document).attr("src","outline/edithtml?code="+code+"&ocode="+ocode+"&noCache="+Math.floor(Math.random()*100000));
	   }; 
	   
	   $(function() {
			var code = "${code}";
			var ocode="${ocode}";
			//上传课件文件
			 $("#upload").uploadify({
				 'height'         : 30,//表示按钮的高度，默认30PX。若要改为50PX，如下设置：'height' : 50，  
		         'width'          : 120,//表示按钮的宽度  
			     'swf'            : "<c:url value='/resources/js/uploadify/uploadify.swf' />",  
			     'uploader'       : "<c:url value='/outline/uploadDoc?dir=file&code=${code}'/>",
			     'cancelImg'      : "<c:url value='/resources/js/uploadify/uploadify-cancel.png' />",
			     'buttonText'     :'选择上传文件' ,
			     'formData'       : {'code' : code},
			     'progressData' : 'all', // 'percentage''speed''all'//队列中显示文件上传进度的方式：all-上传速度+百分比，percentage-百分比，speed-上传速度  
                 'preventCaching' : true,//若设置为true，一个随机数将被加载swf文件URL的后面，防止浏览器缓存。默认值为true  
                 'timeoutuploadLimit' : 5,//能同时上传的文件数目  
                 'removeCompleted' : true,//默认是True，即上传完成后就看不到上传文件进度条了。  
                 'removeTimeout' : 3,//上传完成后多久删除队列中的进度条，默认为3，即3秒。  
                 'requeueErrors' : true,//若设置为True，那么在上传过程中因为出错导致上传失败的文件将被重新加入队列。  
                 'successTimeout' : 30,//表示文件上传完成后等待服务器响应的时间。不超过该时间，那么将认为上传成功。默认是30，表示30秒。 
			     'auto'           : true,  
			     'multi'          : true, 
			     'method' : 'post',//默认是’post’,也可以设置为’get’ 
			     'fileObjName'    : 'file',//文件对象名称。用于在服务器端获取文件。
			     'uploadLimit' : 999,//最多上传文件数量，默认999 
			     'fileSizeLimit' : '50MB',//上传文件大小限制，默认单位是KB，若需要限制大小在100KB以内，可设置该属性为：'100KB'  
			     'fileTypeExts'        : '*.doc;*.docx',  
			     'fileTypeDesc'       : '文件(*.doc,*.docx)', 
			     'wmode'          : 'transparent',
			     'onUploadSuccess': function(file, data) {
			    	 parent.showlist(code);
			     }  
			 }); 			
		});
		
		//code,ocode,doc1
	   function piceoff(code,ocode)//
	   {
	      $.ajax({
               type: "POST",
               dataType: "text",//返回json格式的数据
			   url: "outline/deleteDoc1"+"?noCache="+Math.floor(Math.random()*100000),
			   data: {
			            code:code,
			            ocode:ocode
			         },
			   success:function(data){
			      if(data=="true")
			      {
			        //alert("文档删除成功！");
			        //工具栏隐藏
                    //$("#container").css("visibility","hidden");
			        //跳转到上传页面 传code值
			        parent.showlist(code);
			      }else
			      {
			         alert("文档删除失败！");
			      }
			   },
			   error:function(XMLHttpRequest, textStatus, errorThrown){
			         //alert("error-->文档删除失败！");
			   }
           }); 
	   };	
	</script>

  </head>
  
  <body> 
    <%-- <table>   
       <tr>
           <input type="file" name="upload" id="upload" />         
       </tr>  
       <tr>
          <th>文件</th>
          <th>查看</th>
          <th>编辑</th>
          <th>删除</th>
          <th>下载</th>
       </tr>
       <c:forEach items="${outLineList}" var="outline">
	        <tr>
	           <td>"${outline.co_name}"</td>
	           <td><a onclick="show(${outline.curriculum_code},${outline.curriculumoutline_code})">查看</a></td>
	           <td><a>编辑</a></td>
	           <td><a onclick="piceoff(${outline.curriculum_code},${outline.curriculumoutline_code})">删除</a></td>
	           <td><a href="${outline.location}${outline.filename}">下载</a></td>	           
	        </tr>
	   </c:forEach>
    </table> --%>
    
    <div class="content" align="center">		
    	<c:if test="${outLineList!= null && fn:length(outLineList) > 0}">
    		<table class="table" style="margin-top: 10px;">
    			<tr>
    				<th>大纲名称</th>
    				<th>查看</th>
    				<c:if test="${ role==3||role==6 }">
			           <th>编辑</th>			        
			           <th>删除</th>
			        </c:if>
			        <th>下载</th>
    			</tr>
    			<c:forEach items="${outLineList}" var="outline">
	    			<tr>
			           <td>${outline.co_name}</td>
			           <td><a onclick="show(${outline.curriculum_code},${outline.curriculumoutline_code})">查看</a></td>
			           <c:if test="${ role==3||role==6 }">
			                <td><a onclick="edithtml(${outline.curriculum_code},${outline.curriculumoutline_code})">编辑</a></td>
			                <td><a onclick="piceoff(${outline.curriculum_code},${outline.curriculumoutline_code})">删除</a></td>
			           </c:if>
			           <td><a href="${outline.location}">下载</a></td>	           
	                </tr>
    			</c:forEach>
    		</table>
    	</c:if>
    	<c:if test="${fn:length(outLineList) <= 0||outLineList== null }">
    		<div style = "margin-top: 10px;font-size: 16px;font-weight: lighter;">暂无任何大纲</div>
    	</c:if>
    	
    	<c:if test="${ role==3||role==6 }">
    	  <div style="float: right;width:122px;margin-top: 10px;margin-right: 50px;"><input type="file" name="upload" id="upload" /></div>
    	</c:if>
    	
	</div>
  </body>
</html>
