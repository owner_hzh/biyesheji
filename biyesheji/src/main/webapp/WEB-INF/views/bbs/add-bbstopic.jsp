<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'add-topic.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/courseware.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/popwindow.css'/>">
	<style type="text/css">
		.title{
			height:4%;
			width:95%;
			margin-top: 20px;
			background-color: blue;
			line-height:30px;
			color:#FFFFFF;
			font-size:17px;
			border-radius:15px; 
			text-align: left;
			font-weight: bold;
		}
		.pagechange:hover
		{
		    color:blue;
			text-decoration: underline;
		}
		.pagechange
		{
		    font-size: 20px;
		    font-weight:lighter;
		    font-family:'幼圆';
		    cursor:pointer;
		}
		a{
    		text-decoration : none	;	
		}
	</style>
 
	<script type="text/javascript" src='<c:url value='/resources/js/jquery.js'/>'></script>
	<script type="text/javascript" src="<c:url value='/resources/ckeditor/adapters/jquery.js'/>"></script>
	<script type="text/javascript" src="<c:url value='resources/ckeditor/ckeditor.js'/>"></script>
  </head>
  
  <body>
  	<div class="content" align="center">
  	 <form action="/biyesheji/bbs/add-bbstopic?sid=${sid}" method="post">
    	<div id="content_2_1" class="item_content">
	         <div class="title"><span style="margin-left: 14px;">发帖</span></div>    
	      <div style="padding-top:5px;">
    		<table id="table" class="table">  
				<tr>
					<td style="text-align:left;margin-left:20px">       
				                                    主版块:<select id="tsid" name="tsid">
				 	             	<c:choose>
								  		<c:when test="${!empty bbsSections }">
								  			<c:forEach items="${bbsSections}" var="bs">   
							  	 		    	<option value='<c:out value="${bs.sid}"/>'  <c:if test="${ sid == bs.sid}">selected</c:if>  > 
							  	 		    		<c:out value="${bs.sname}"/>
							  	 		    	</option>
								  			</c:forEach>
								  		</c:when>
				  					</c:choose>	
				 	       </select>        
	                         <p>主贴表情  ：<input type="text" id="temotion" name="temotion" /></p>
	                         <p>主贴标题  ：<input id="ttopic" name="ttopic" /></p>
	                         <p>主贴内容 ：</p>
	                          <textarea class="ckeditor"   rows="10px" cols="110px" id="tcontents" name="tcontents"></textarea>
				           <p><input type="submit" id="submit" value="提交" /></p>
	          		</td>
	          	</tr>
	          </table>
	        </div>
       </div>
      </form>
      </div>
  </body>
</html>
