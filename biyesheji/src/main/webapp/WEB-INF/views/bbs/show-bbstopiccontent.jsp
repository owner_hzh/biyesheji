<%@page import="com.cqjt.pojo.User"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'show-content.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/courseware.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/popwindow.css'/>">
	<style type="text/css">
		.title{
			height:4%;
			width:95%;
			margin-top: 20px;
			background-color: blue;
			line-height:30px;
			color:#FFFFFF;
			font-size:17px;
			border-radius:15px; 
			text-align: left;
			font-weight: bold;
		}
		.pagechange:hover
		{
		    color:blue;
			text-decoration: underline;
		}
		.pagechange
		{
		    font-size: 20px;
		    font-weight:lighter;
		    font-family:'幼圆';
		    cursor:pointer;
		}
		a{
    		text-decoration : none	;	
		}
	</style>
 
	<script type="text/javascript" src='<c:url value='/resources/js/jquery.js'/>'></script>
	<script type="text/javascript" src="<c:url value='/resources/ckeditor/adapters/jquery.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/resources/ckeditor/ckeditor.js'/>"></script>
    <script type="text/javascript">
    	 $(document).ready(function(){   
   				$("#reply").click(function(){
   					$("#rcontents").focus();
   				});
    	 }); 
    </script>
  </head>
  	
  <body>
    <div class="content" align="center">
  		<div class="title"><span style="margin-left: 14px;">主题浏览及回复</span></div>
	  	<div style="float:left;margin-left: 10%;margin-top:10px">
		    <a href="javascript:history.go(-1);" >[返回上级页面]</a>
		</div><br>
  		<form action="/biyesheji/bbs/add-bbsreply" method="POST" >
		   	<div style="margin-top:5px;">
		   		<table id="table" class="table">
		   			<tr>
	    				<td style="text-align:left;margin-left:20px">
	    					<div>
	    						<p style="float:right"><span style="font-color:red">发帖时间：</span>${bbsTopic.ttime} </p>
	    						<p style="float:left">主贴标题：${bbsTopic.ttopic} </p>
	    						<br>
	    					</div>
	    					<br>
	    					<div>
		    					<p>发帖人 ：${bbsTopic.tuid} <p> 
		    					<p>内&nbsp;&nbsp;容          ：
		    						${bbsTopic.tcontents} </p>
		    					<p>点击次数：${bbsTopic.tclickcount} </p>
	    					</div>
	    				</td>
	  				</tr>
	  			</table>
	  		</div>
	  		 <div style="padding-top:10px;">
	    		<c:choose>
			  		<c:when test="${!empty bbsReplies}">
			  			<c:forEach items="${bbsReplies}" var="br" varStatus="status">  
			  				<table id="table1" class="table"> 
			  					<tr>
			  							
				  					<td style="text-align:left;margin-left:20px">
					  					<div>
					  										  						<p style="float:left">第${page.currentPage}页 &nbsp;${status.count}楼</p>
						  					<%-- <p style="float:left"><c:out value="${br.rid}"/>楼	</p> --%>
						  					<p style="float:right"><span style="font-color:red">回帖时间：</span><c:out value="${br.rtime}"/>
						  						&nbsp;&nbsp;&nbsp;  
						  					</p>
						  					
						  					<br>
						  				</div>
						  				<br>
			    						<div>
				  	 		    			<p>${br.rcontents}</p>
				  	 		    			<p>回复人:<c:out value="${br.ruid}"/></p>
				  	 		    		</div>
			  	 		    		</td>
		  	 		    		<tr>
		  	 		    	</table>
			  			</c:forEach>
			  			<jsp:include page="../pager.jsp"> 
	                   		<jsp:param name="url" value="/bbs/show-bbstopiccontent" />  
	                	</jsp:include>   
			  		</c:when>
				</c:choose>		
	    	</div>
	    	
	  		<div style="padding-top:5px;">
	    		<table id="table2" class="table"> 
					<tr>
						<td style="text-align:left;margin-left:20px">
			    			<p>发表回复</p>
				    		<input id="rtid" name="rtid" value="${bbsTopic.tid }" type="hidden">
				    		<input id="rsid" name="rsid" value="${bbsTopic.tsid }" type="hidden"/>
				    		<input id="ruid" name="ruid" value="${ruid} " type="hidden"/>
				    		<input id="rtopic" name="rtopic" value="${bbsTopic.ttopic }" type="hidden"/>
				    		<input id="remotion" name="remotion" type="hidden" value="表情"/>
				    	    <p> 回复内容：</p>
				    	    <textarea class="ckeditor" rows="10px" cols="115px" id="rcontents" name="rcontents"></textarea><br>
			    			<input type="submit" id="submit" value="回复"/>
	    				</td>
	    			</tr>
	    		</table>
	    		
	    	</div> 
  	</form>
	</div>  
	<script type="text/javascript">
   		 $(document).ready(function(){   
   			del();
    	 }); 
    	 function del(){
    	 	$("#table1 tr").each(function(){
    			var tr = $(this);
    			var edit = tr.find("td").find("#delete");
    			edit.click(function(){
    				var tds = edit.parent().parent().find("td");
    					alert(tds.eq(1).text()); 
    				/* $.ajax({
						    type: "POST",
		        		   processData : true,//contentType为xml时，些值为false   				   
						   url: "<c:url value='/bbs/delete-bbsreply'/>" ,  //获取修习方式studymode的list
						   dataType : "json",
						   data:{
						   		rtid : tds.eq(0).text() ,
						   		
						   		rtopic :
						   		remotents
						   },
						   success: function(){
						       alert("删除数据成功");
							   location.reload();
						   },
						   error: function (XMLHttpRequest, textStatus, errorThrown) {
				              alert("删除数据失败！");  
				        
		            		}
					});   */
    			});
    		});
    	 }
    </script>	    
  </body>
</html>
