<%@page import="com.cqjt.pojo.User"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'add-bbssection.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<script type="text/javascript" src='resources/js/jquery.js'></script>
   
	
  </head>
  
  <body>
   	  <div id="content_4_1" class="item_content" align="center" style="font-family:'幼圆'">
		 <div align="center" >添加互动版块</div>
		  <div align="center" style=" width:600px; margin-top:10px;">
	         <div style="width:500px;" align="left">
	             	 版块名称 ： <input type="text" id="sname" name="sname" style="width:400px; "/>
	          </div>          
	          <div style="width:500px;" align="left">
	         		 版主编号 ：<input type="text" style="width:400px; margin-top:10px" readonly id="smasterid" name="smasterid"
	              	 value="<% User user = (User)request.getSession().getAttribute("loginuser");out.print(user.getUser_id());  %>"/>
	          </div>	          
	          <div style=" width:500px;margin-top:10px;" align="left">
				 <div align="left"><span>版块说明 ：</span>
				 <textarea rows="5" cols="50" id="sstatement" name="sstatement" style="resize: none;width:500px;" ></textarea>
	          </div>	          
	          <div>
	              <input type="button" id="submit" value="提交" />
	          </div>
	       </div>
       </div>
       </div>
       <script type="text/javascript">
   
       $(document).ready(function(){
           $("#submit").click(function(){
          	//  alert($("#sstatement").val());
              $.ajax({
				   type: "POST",
				   processData : true,//contentType为xml时，些值为false   	
				   url: "/biyesheji/bbs/add-bbssection",
				   dataType : "json",
				   data: {
				            sname:$("#sname").val(),
				            smasterid:$("#smasterid").val(),
				            sstatement:$("#sstatement").val(),
				            
				    },
				   success: function(){
				        alert("数据添加成功");
				        $("#sname").val("");
				      //  $("#smasterid").val("");
				        $("#sstatement").val("");
				       
				   },
				   error: function (XMLHttpRequest, textStatus, errorThrown) {
		               alert("添加数据失败！");   
		                alert(XMLHttpRequest.status);
		                alert(XMLHttpRequest.readyState);
		                alert(textStatus); 
            		}
				});
          }); 
       });
    </script>
  </body>
</html>
