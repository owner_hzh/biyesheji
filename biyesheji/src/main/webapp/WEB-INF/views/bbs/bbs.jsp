<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'show-bbs.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
    <style type="text/css">
	   html body{ height:100%; width:100%;overflow:hidden;}
	   .menu{width:15%; height: 100%; 
	   		float: left; background-color: #e8e8e8; overflow: auto;
	   		border-top-left-radius:15px; 
	   		border-bottom-left-radius:15px; 
	   		color: #AA531E;
	   } 
	   .activetwo{background-color: #CCCCCC;color: blue;}
	   .menu div{ cursor: pointer;
	   			  border-bottom: 1px dashed gray;
	   			  height:40px;width:100%;
	   			  font-size: 15px;
	   			  font-family: '幼圆';
	   			  font-weight: normal;
	   			  line-height: 40px;
	   			  text-align: center;
	   	}
	   .menu div:HOVER{background-color: #CCCCCC;color: blue;}/* 909EDA  F2F2F2*/
	   .content { width:85%; height:100%; 
	   			  float: right;
	   			  background-color:#EDEDED;
	   			  border-top-right-radius:15px; 
	   			  border-bottom-right-radius:15px; 
	   }	  
	   .iframe_content{ width: 100%; height: 100%; overflow: auto;background-color:#CCCCCC;
	   					border-top-right-radius:15px; 
	   			        border-bottom-right-radius:15px;  }
	</style>
	<link rel="stylesheet" href="<c:url value='resources/css/jquery.treeview.css'/>" />
    <script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>
	<script src=' <c:url value='/resources/js/jquery.cookie.js'/>' type="text/javascript"></script>
	<script src='<c:url value='/resources/js/jquery.treeview.js'/>' type="text/javascript"></script>
  	<script type="text/javascript">	 
 		$(function(){
 			loadSection(null);
		    $("#menutwo").children().eq(0).addClass("activetwo");
		}); 
		function loadSection(obj){
			$("#iframe").attr("src" , "<c:url value='/bbs/show-bbssection'/>");
			$(".activetwo").removeClass("activetwo");
			$(obj).addClass("activetwo");
		}
		function postMessage(obj){
			$("#iframe").attr("src" , "<c:url value='/bbs/add-bbstopic?sid=1&method=load'/>");
			$(".activetwo").removeClass("activetwo");
			$(obj).addClass("activetwo");
		}
	</script>
  </head>
  
  <body>
	  	  <!-- bbs菜单开始 -->
	    <div id="menutwo" class="menu">
	   		 <div onclick="loadSection(this)"><span>版块</span></div>
	   		 <div onclick="postMessage(this)"><span>发帖</span></div>
	   		   <!--  <div onclick="getContent(this)"><span>版块</span></div>
	   			<div onclick="getContent(this)"><span>发帖</span></div>  -->
		</div>
		<!-- bbs菜单结束-->
		
		<!-- bbs内容开始-->
		<div class="content" align="center"> 
	     <iframe id="iframe" class="iframe_content" marginheight="0" marginwidth="0" hspace="0" vspace="0" frameborder="0">
	     </iframe> 	
		</div>
		<!-- bbs内容结束-->
  </body>
</html>
