<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'show-bbstopic.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/courseware.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/popwindow.css'/>">
	<style type="text/css">
	
		.title{
			height:4%;
			width:95%;
			margin-top: 20px;
			background-color: blue;
			line-height:30px;
			color:#FFFFFF;
			font-size:17px;
			border-radius:15px; 
			text-align: left;
			font-weight: bold;
		}
		.pagechange:hover
		{
		    color:blue;
			text-decoration: underline;
		}
		.pagechange
		{
		    font-size: 20px;
		    font-weight:lighter;
		    font-family:'幼圆';
		    cursor:pointer;
		}
		a{
    		text-decoration : none	;	
		}
		.content { width:100%; height:100%; 
	   			  float: right;
	   			  border-top-right-radius:15px; 
	   			  border-bottom-right-radius:15px; 
	    }	
	</style>
	
	<script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>  
 
  </head>
  
  <body>
  	<span>你当前的位置：[<a href="<c:url value='/bbs/show-bbssection'/>">版块</a>]-[主贴]</span>
  	<div class="content" align="center">
 	
  	<div class="title"><span style="margin-left: 14px;">主贴列表：</span></div>
     <form id="form" name="form" >
	   <div style="margin-top:5px;">
	   		<table id="table" class="table">
	   			<tr>
    				<th>帖子编号</th>
    				<th>发帖人</th>
    				<th>题目</th>
    				<th>发帖时间</th>
    				<th>操作</th>
    			</tr>
	             	<c:choose>
				  		<c:when test="${!empty bbsTopics }">
					  		<c:forEach items="${bbsTopics}" var="bt">  
					  				<tr>	 		   	 		
					  	 		    		<td><c:out value="${bt.tid}"/></td>
					  	 		    		<td><c:out value="${bt.tuid}"/></td>
					  	 		    		<td><c:out value="${bt.ttopic}"/></td>
					  	 		    		<td><c:out value="${bt.ttime}"/></td>
					  	 		    		<!-- count用来访问帖子计数 -->
					  	 		    		<td><a href="/biyesheji/bbs/show-bbstopiccontent?tid=${bt.tid}&count=1">进入帖子</a></td>
					  	 		    </tr>
					  		</c:forEach>
							<%-- <% request.setAttribute("sid","${tsid}"); %>  	 --%>
							
					  		<jsp:include page="../pager.jsp"> 
                   				  <jsp:param name="url" value="/bbs/show-bbstopiclist" />  
                			</jsp:include>  
				  		</c:when>
				  		<c:otherwise>
				  			<tr>
					  			<td colspan="6">没查询课程性质到相关数据</td>
					  		</tr>
				  		</c:otherwise>
					</c:choose>	
	 	      </table>
	   </div>
	  </form>
	  </div>
  </body>
</html>
