<%@ page language="java" contentType="text/html; charset=UTF-8" import="java.util.*"  pageEncoding="UTF-8" %> 

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'menu-content-add-curriculum.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
  <script type="text/javascript" src='resources/js/jquery.js'></script>
  </head>
  
  <body>
  	 <form action="curriculum/addcurriculum">
   		 <div id="content_2_1" class="item_content">
	           <div>添加课程</div>
	       <div>
	       
	          <div>
	                                      课程代码：<input type="text" id="curriculum_code" name="curriculum_code"/>
	          </div>
	          
	          <div>
	                                      课程性质：<select id="curriculumcharacter" name="cc_name"></select>
	          </div>
	          
	          <div>
	                                      专业：<select id="major" name="major_name"></select>
	          </div>
	          
	          <div>
	       	         修习方式:<select id="studymode" name="studymode_name"></select>                       
	          </div>
	          
	          <div>
	           	课程名：<input type="text" id="curriculum_name" name="curriculum_name"/>                        
	          </div>
	          
	          <div>
	                                      课程英文名：<input type="text" id="curriculum_englishname" name="curriculum_englishname"/>                      
	          </div>
	          
	          <div>
	                                               学分：<input type="text" id="credit" name="credit"/>                        
	          </div>
	          
	          <div>
	            	总学时：<input type="text" id="all_time" name="all_time"/>
	          </div>
	          <div>
	          	理论学时：<input type="text" id="theory_time" name="theory_time"/>
	          </div>
	          
	          <div>
	          	实验学时：<input type="text" id="practice_time" name="practice_time"/>
	          </div>
	          <div>
	          	上机学时:<input type="text" id="computer_time" name="computer_time"/>
	          </div>
	          <div>
	          	开课学期:<input type="text" id="semester_code" name="semester_code">
	          </div>
	          <div>
	          	总章节数：<input type="text" id="allchapter" name="allchapter"/>
	          </div>
	          <div>
	          	考核方式:<input type="text" id="exam_way" name="exam_way"/>
	          </div>
	          <div>
	          	说明：<textarea rows="" cols="" id="explain" name="explain"></textarea>
	          </div>
	       </div>
       </div>
       <!-- <input type="submit" id="submit" name="submit" value="提交" />
       <input type="reset"  id="reset"  name="reset" value="重置"/>   -->
       <input type="button" id="submit" value="提交"/>
     </form>
        <script type="text/javascript">
       
   		  $(document).ready(function(){   
   			 getCurriculumCharacterList();
   			 getStudymodeList();
   			 getMajorList();
   			 $("#submit").click(function(){
   			 	$.ajax({
				   type: "POST",
				//   contentType : "application/json",//application/xml   
        		   processData : true,//contentType为xml时，些值为false   				   
				   url: "/biyesheji/curriculum/addcurriculum",  //获取修习方式studymode的list
				   dataType : "json",
				   data:{
						   	  url: "/menu/menu-content-add-curriculum",
						      curriculum_code: 	$("#curriculum_code").val(),
			                  curriculum_name:  $("#curriculum_name").val(), 
						curriculum_englishname: $("#curriculum_englishname").val(),
				              credit: 	        $("#credit").val(),		
							  all_time: 		$("#all_time").val(),	
				 			  theory_time:   	$("#theory_time").val(),		
						      practice_time:	$("#practice_time").val(),
				              computer_time:	$("#computer_time").val(),
							  semester_code:	$("#semester_code").val(),		
				              exam_way:			$("#exam_way").val(),
				               explain: 		$("#explain").val(),
				      		 studymode_id  :	$("#studymode").find("option:selected").val(),      
				       			studymode_name: $("#studymode").find("option:selected").text(),
				   					cc_code:	$("#curriculumcharacter").find("option:selected").val(),
	   			   					cc_name:    $("#curriculumcharacter").find("option:selected").text(),
	   			   					major_code: $("#major").find("option:selected").val(), 
	   			   					major_name: $("#major").find("option:selected").text(), 
	   			   					allchapter: $("#allchapter").val()
				   },
				   success: function(studymodelist){
					   alert("添加数据成功");
				   },
				   error: function (XMLHttpRequest, textStatus, errorThrown) {
		               alert("添加数据失败！");   
		                /* alert(XMLHttpRequest.status);
		                alert(XMLHttpRequest.readyState);
		                alert(textStatus); */
            		}
				});  	
   			 });
    	  }); 
    	  function getMajorList(){    //获取专业下拉列表
    	  		$.ajax({
				   type: "GET",
				   contentType : "application/json;charset=UTF-8",//application/xml   
        		   processData : true,//contentType为xml时，些值为false   				   
				   url: "/biyesheji/getallmajor",  //获取修习方式studymode的list
				   dataType : "json",
				   data:{
				   	  url: "/menu/menu-content-add-curriculum"
				   },
				   success: function(majorlist){
					    $.each(majorlist, function (n, value) {
					    	for(var i = 0;i<n.length;i++)	
					    		 $("#major").append( "<option value=\"" + value[i].major_code + "\" >" + value[i].major_name + "</option>");
						});  
				   },
				   error: function (XMLHttpRequest, textStatus, errorThrown) {
		               alert("载入专业列表出错！");   
            		}
				});  
    	  }  
    	  function  getStudymodeList(){  //获取修学方式的下拉列表
    	   	 
    	   		$.ajax({
				   type: "GET",
				   contentType : "application/json;charset=UTF-8",//application/xml   
        		   processData : true,//contentType为xml时，些值为false   				   
				   url: "/biyesheji/curriculum/getstudymodelist",  //获取修习方式studymode的list
				   dataType : "json",
				   data:{
				   	  url: "/menu/menu-content-add-curriculum"
				   },
				   success: function(studymodelist){
					    $.each(studymodelist, function (n, value) {
					    	for(var i = 0;i<n.length;i++)	
					    		 $("#studymode").append( "<option value=\"" + value[i].studymode_id + "\" >" + value[i].studymode_name + "</option>");
						});  
				   },
				   error: function (XMLHttpRequest, textStatus, errorThrown) {
		               alert("载入修习模式列表出错！");   
            		}
				});  
    	   }
    	   function getCurriculumCharacterList(){   //获得课程性质的下拉列表
                $.ajax({
				   type: "GET",
				   contentType : "application/json",//application/xml   
        		   processData : true,//contentType为xml时，些值为false   				   
				   url: "/biyesheji/curriculum/getcurriculumcharacterlist",  //获取课程性质curriculumcharacter的list
				   dataType : "json",
				   data: {
				           url: "/menu/menu-content-add-curriculum"
				   },  
				   success: function(curriculumcharacterlist){
				        $.each(curriculumcharacterlist, function (n, value) {
					    	for(var i = 0;i<n.length;i++)	
					    		 $("#curriculumcharacter").append( "<option value=\"" + value[i].cc_code + "\" >" + value[i].cc_name + "</option>");
						});  
				   },
				   error : function(e) {   
            			alert("载入课程性质列表出错！");   
       			   }   
				}); 
		   }  
   		</script>
  </body>
</html>
