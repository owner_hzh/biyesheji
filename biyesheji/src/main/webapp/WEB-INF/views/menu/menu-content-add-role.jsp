<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>添加角色</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
		<style type="text/css">
		.title{
			height:4%;
			width:95%;
			margin-top: 20px;
			background-color: blue;
			line-height:30px;
			color:#FFFFFF;
			font-size:17px;
			border-radius:15px; 
			text-align: left;
			font-weight: bold;
			margin-left:3%;
		}
	</style>
    <script type="text/javascript" src='<c:url value="/resources/js/jquery.js" />'></script>
    <script type="text/javascript">
       $(function(){
          $("#submit").click(function(){
              $.ajax({
				   type: "POST",
				   url: "role/addRole",
				   data: {
				            role_name:$("#role_name").val(),
				            role_comment:$("#role_comment").val()
				         },
				   success: function(msg,status){
				     if(status=="success"&&msg=="true")
				     {
				        alert("数据添加成功");
				        $("#role_name").val("");
				        $("#role_comment").val("");
				     }
				   }
				});
          });
       });
    </script>
  </head>
  
  <body>
	<div class="title" align="center"><span style="margin-left: 14px;">添加角色</span></div>
    <div id="content_1_1" class="item_content">
	             角色名：<input type="text" name="role_name" id="role_name" />
	              备注：<input type="text"  name="role_comment" id="role_comment" />
	     <input type="button" value="提交" id="submit" />
    </div>
  </body>
</html>
