<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'content.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<script type="text/javascript" src='<c:url value='resources/js/jquery.js'/>'></script>
    <script type="text/javascript">
       $(document).ready(function(){
          $("#submit").click(function(){
          
              var major_name=$("#major_name").val().trim();
              if(major_name==null||major_name=="")
              {
                 alert("专业名称不能为空！");
                 $("#major_name").focus();
                 $("#major_name").addClass("kong");
              }else
              {
                 $.ajax({
				   type: "POST",
				   dataType:"text",
				   url: "major/addMajor",
				   data: {
		                    major_name:major_name,
		                    schooling_length:$("#schooling_length").val(),
		                    degree_awarding:$("#degree_awarding").val(),
						    core_curriculum: $("#core_curriculum").val(),
						    occupational_direction:$("#occupational_direction").val(),
						    goal:$("#goal").val(),
						    required:$("#required").val(),
						    history:$("#history").val()
				         },
				   success: function(msg,status){
				     if(status=="success"&&msg=="true")
				     {
				        alert("数据添加成功");
	                    $("#major_name").val("");
	                    $("#schooling_length").val("");
	                    $("#degree_awarding").val("");
					    $("#core_curriculum").val("");
					    $("#occupational_direction").val("");
					    $("#goal").val("");
					    $("#required").val("");
					    $("#history").val("");
				     }else
				     {
				        alert("数据添加失败！");
				     }
				   },
			   error:function(XMLHttpRequest, textStatus, errorThrown){
			         alert("服务器出错！");
			        }
				});
              }
              
          });
       });
       
       function rec(obj)
       {
          $(obj).removeClass("kong");
       }
       
       function checksamename(obj)
       {
          var name=$(obj).val().trim();
          if(name!=null&&name!="")
          {
             $.post("<c:url value='/major/checkmajor'/>",{major_name:name},function(data){
					if(data=="false"){
						$(".info").text("");
			 		}else{
			 			$(".info").show().text("专业名已存在");
			 			$(obj).val("");
			 		}
				},"text");
          }
          
       }
       
       function lostfocus(obj)
       {
         $(".info").text("");
       }
    </script>
    <style type="text/css">
       input{
			font-size:18px;
			width:250px;
		}
		.commit{
			cursor: pointer;
			background-color: #FFCC66;
			margin-top:20px;
			margin-bottom:40px;
			margin-left:19%;
			height:40px;
			font-size:20px;
			font-family:"幼圆";
			border-radius:10px;
			border: 0px;
		}
		.commit:hover{
			background-color: #0033CC;
			color:#ffffff;
		}
		
		.title{
		   font-size:25px;
		   font-weight:bolder;
		   font-family:"幼圆";
		   margin-top: 20px;
		   margin-bottom: 20px;
		}
		
		.item{
		   font-family:"幼圆";
		   font-size:15px;
		   margin-top: 20px;
		}
		
		.itemTitle{
		   padding-bottom: 10px;
		}
		
	.editTextarea{
       border:1px solid #999;
       font-size:12px;
       font-family:"幼圆";
       padding:1px;
       height:140px;
       overflow:auto;
       width:70%;
       text-align:left;
       padding:5px;
       }
      .kong{
      border-color: red;}
      .info{color:red;font-size: 13px;margin-top: 20px;margin-bottom: 20px;}
    </style>
  </head>
  
  <body>
    <div id="content_2_1" class="item_content">
	           <center><div class="title">添加方向</div></center>
	       <div style="margin-left: 20%;">	          
	          <div class="item">
	                                      专业名称：<input type="text" id="major_name" onblur="checksamename(this)" onfocus="lostfocus(this)" onkeydown="rec(this)" placeholder="专业名称"/><span class="info"></span>
	          </div>
	          <div class="item">
	             <span class="itemTitle">基本学制与学习年限:</span><br>
	             <textarea class="editTextarea" id="schooling_length" placeholder="描述基本学制与学习年限..."></textarea>
	          </div>
	          <div class="item">
	             <span class="itemTitle">学位授予:</span><br>
	             <textarea class="editTextarea" id="degree_awarding" placeholder="描述学位授予情况..."></textarea>
	          </div>
	          <div class="item">
	             <span class="itemTitle">专业定位:</span><br>
	             <textarea class="editTextarea" id="occupational_direction" placeholder="描述专业定位情况..."></textarea>
	          </div>
	          <div class="item">
	             <span class="itemTitle">培养目标:</span><br>
	             <textarea class="editTextarea" id="goal" placeholder="描述培养和目标的情况..."></textarea>
	          </div>
	          <div class="item">
	             <span class="itemTitle" > 基本规格要求:</span><br>
	             <textarea class="editTextarea" id="required" placeholder="描述基本规格要求的情况..."></textarea>
	          </div>
	          <div class="item">
	             <span class="itemTitle">主干学科和核心课程:</span><br>
	             <textarea class="editTextarea" id="core_curriculum" placeholder="描述主干学科和核心课程情况..."></textarea>
	          </div>
	          <div class="item">
	             <span class="itemTitle">办学历史:</span><br>
	             <textarea class="editTextarea" id="history" placeholder="描述办学历史..."></textarea>
	          </div>
	          
	          <input type="button" class="commit" value="提交" id="submit" />	         
	          
	       </div>

       </div>
  </body>
</html>
