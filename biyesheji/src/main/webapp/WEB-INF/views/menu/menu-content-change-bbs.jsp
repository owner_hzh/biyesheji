<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'show-bbs.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/courseware.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/popwindow.css'/>">
	<style type="text/css">
		.title{
			height:4%;
			width:95%;
			margin-top: 20px;
			background-color: blue;
			line-height:30px;
			color:#FFFFFF;
			font-size:17px;
			border-radius:15px; 
			text-align: left;
			font-weight: bold;
		}
		.pagechange:hover
		{
		    color:blue;
			text-decoration: underline;
		}
		.pagechange
		{
		    font-size: 20px;
		    font-weight:lighter;
		    font-family:'幼圆';
		    cursor:pointer;
		}
		a{
    		text-decoration : none	;	
		}
	</style>
	
	<script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>  
 
  </head>
  
  <body>
  	<div class="content" align="center">
  	<div class="title"><span style="margin-left: 14px;">板块列表：</span></div>
    <form id="form" name="form" >
	   <div style="margin-top:5px;">
	   		<table id="table" class="table">
	   			<tr>
    				<th>版块编号</th>
    				<th>版块名</th>
    				<th>版主</th>
    				<th>操作</th>
    			</tr>
	 	             	<c:choose>
					  		<c:when test="${!empty bbsSections }">
					  			<c:forEach items="${bbsSections}" var="bs">   
						  			<tr>
						  	 		    		<td><c:out value="${bs.sid}"/></td>
						  	 		    		<td><c:out value="${bs.sname}"/></td>
						  	 		    		<td><c:out value="${bs.smasterid}"/></td>
						  	 		    		<td><a href='javascript:void(0);' id="delete" name="delete">删除</a></td>
						  	 		    		<%-- <td><a href="/biyesheji/bbs/show-bbstopiclist?sid=${bs.sid}">删除</a></td> --%>
						  			</tr>
					  			</c:forEach>
					  		</c:when>
					  		<c:otherwise>
					  			<tr>
						  			<td colspan="6">没查询课程性质到相关数据</td>
						  		</tr>
					  		</c:otherwise>
	  					</c:choose>	
	 	    </table>
	   </div>
	  </form>
	 </div>
	 <script type="text/javascript">
	 	 $(document).ready(function(){   
   			del();
    	 }); 
    	 function del(){
    	 	$("#table tr").each(function(){
    			var tr = $(this);
    			var edit = tr.find("td").find("#delete");
    			edit.click(function(){
	    				var tds = edit.parent().parent().find("td");
	    				//alert(tds.eq(0).text() );
	    			    $.ajax({
						    type: "GET",
						//   contentType : "application/json",//application/xml   
		        		   processData : true,//contentType为xml时，些值为false   				   
						//   url: "<c:url value='/bbs/delete-bbssection'/>" ,  //获取修习方式studymode的list
						   url:"/biyesheji/bbs/delete-bbssection",
						   dataType : "json",
						   data:{
						   		sid : tds.eq(0).text() 
						   },
						   success: function(){
							   alert("删除数据成功");
							   location.reload();
						   },
						   error: function (XMLHttpRequest, textStatus, errorThrown) {
				               alert("删除数据失败！");   
		            		}
						});   
    			});
    		});
    	 }
	 </script>
  </body>
</html>
