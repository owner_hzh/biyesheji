<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'content.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/courseware.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/general.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/popwindow.css'/>">
    <script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>
    <script type="text/javascript" src="<c:url value='resources/js/popup.js'/>"></script>
    <script type="text/javascript" src="<c:url value='resources/js/popwindow/popwindow.js'/>"></script>
    <script type="text/javascript">
    function modify(qt_code)
    {
        $(window).ShowDialog({ width: 800, height: 600, title: "修改/查看题型信息", src: "<c:url value='questiontypes/showQusetionTypeDetail?qt_code="+qt_code
        +"'/>" });
    }
    function deleteQuestionType(qt_code)
    {
		if(confirm("是否删除？")){
			$.get("<c:url value='questiontypes/deletequestiontypes'/>",{qt_code:qt_code},function(date){
				if(date=="success")
				{
					location.reload();
				}
			});
		}
    }
    </script>
  </head>
  
  <body>

	<div class="content" align="center">
    	<c:if test="${questionTypesList!=null}">
    		<table class="table" style="margin-top: 10px;">
    		    <tr>
    				<th>题型代码</th>
    				<th>题型名称</th>
    				<th>是否客观题</th>
    				<th>操作</th>
    			</tr>
    			<c:forEach items="${questionTypesList}" var="questionType">
	    			<tr>
	    				<td>${questionType.qt_code}</td>
	    				<td>${questionType.qt_name}</td>
	    				<td>${questionType.is_objective_CN}</td>
	    				<td>
	    				    <a onclick="modify('${questionType.qt_code}')">详情/修改</a>
	    				    <c:if test="${questionType.qt_code!=1&&questionType.qt_code!=2&&questionType.qt_code!=3}"><a onclick="deleteQuestionType('${questionType.qt_code}')">删除</a></c:if>
	    				</td>
	    			</tr>
    			</c:forEach>
    		</table>
    	</c:if>
    	<c:if test="${questionTypesList==null}">
    		<div style = "margin-top: 10px;font-size: 16px;font-weight: lighter;">暂无专业信息</div>
    	</c:if>
	</div>
  </body>
</html>
