<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'review.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/courseware.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/general.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/popwindow.css'/>">
    <script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>
    <script type="text/javascript" src="<c:url value='resources/js/popup.js'/>"></script>
    <script type="text/javascript" src="<c:url value='resources/js/popwindow/popwindow.js'/>"></script>
    <script type="text/javascript">
       function change(code)
       {          
		   $(window).ShowDialog({ width: 800, height: 600, title: "修改专业信息", src: "<c:url value='major/sysmain/updatemajor?code="+code
             +"'/>" });
          /* $.ajax({
				   type: "GET",
				   url: "major/sysmain/updatemajor",
				   dataType: "json",//返回text格式的数据
				   data: {
		                    code:code	                    
				         },
				   success: function(data){
				     if(data!=null)
				     {
				        //显示弹出框
				           centerPopup();
						   loadPopup();
						$("#major_code").val(data.major_code);   
	                    $("#major_name").val(data.major_name);
	                    $("#schooling_length").val(data.schooling_length);
	                    $("#degree_awarding").val(data.degree_awarding);
					    $("#core_curriculum").val(data.core_curriculum);
					    $("#occupational_direction").val(data.occupational_direction);
					    $("#goal").val(data.goal);
					    $("#required").val(data.required);
					    $("#history").val(data.history);
				     }else
				     {
				       alert("数据出错了！");
				     }
				   },
			   error:function(XMLHttpRequest, textStatus, errorThrown){
			           alert("服务器出错了！");
			        }
				}); */
       }
       
       function dchange(code,obj)
       {  
          if(confirm("确定删除？"))  
          {
              $.ajax({
				   type: "GET",
				   url: "major/sysmain/deletemajor",
				   dataType: "text",//返回text格式的数据
				   data: {
		                    code:code	                    
				         },
				   success: function(data){
				     if(data=='true')
				     {
                        $(obj).parent().parent().remove();
				     }else
				     {
				       alert("删除失败！");
				     }
				   },
			   error:function(XMLHttpRequest, textStatus, errorThrown){
			           alert("服务器出错了！");
			        }
				}); 
          }      
         
       }
       
       function closePopWindow()
       {
         var d=$("#dClose");
         if(d)
           d.click();
       }
       
       
    </script>
  </head>
  
  <body>&nbsp; 
    <!-- 要审的题目列表 -->
	<div class="content" align="center">
    	<c:if test="${majors!=null && fn:length(majors) > 0}">
    		<table class="table" style="margin-top: 10px;">
    		    <tr>
    				<th>专业代码</th>
    				<th>专业名称</th>
    				<th>操作</th>
    			</tr>
    			<c:forEach items="${majors}" var="cwl">
	    			<tr>
	    				<td>${cwl.major_code}</td>
	    				<td>${cwl.major_name}</td>
	    				<td>
	    				    <a onclick="change(${cwl.major_code})">修改</a>
	    				    <a onclick="dchange(${cwl.major_code},this)">删除</a>
	    				</td>
	    			</tr>
    			</c:forEach>
    		</table>
    	</c:if>
    	<c:if test="${fn:length(majors) <= 0||majors==null }">
    		<div style = "margin-top: 10px;font-size: 16px;font-weight: lighter;">暂无专业信息</div>
    	</c:if>
	</div>
	<!-- 要审的题目列表结束-->
	
	<!-- 弹出框代码 -->
	<div id="popupContact">
		<a id="popupContactClose">x</a>
		<h1>修改专业信息</h1>
		<div id="contactArea">
					
		</div>
	</div>
	<div id="backgroundPopup"></div>
	<!-- 弹窗结束 -->
	
  </body>
</html>
