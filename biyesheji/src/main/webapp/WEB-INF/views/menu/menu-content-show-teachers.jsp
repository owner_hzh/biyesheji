<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
  <head>
    <base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/courseware.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/js/uploadify/uploadify.css'/>">
	<link rel='stylesheet' href="<c:url value="/resources/css/pagination.css"/>" />
	<link rel='stylesheet' href="<c:url value="/resources/css/bootstrap.min.css"/>" />
	<style type="text/css">
	</style>
	
	<script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>  
    <script type="text/javascript" src="<c:url value='resources/js/uploadify/jquery.uploadify.min.js'/>"></script>
    <script type="text/javascript">	 
		//删除教师
		function deleteTeacher(userId){
			if(confirm("是否删除？")){
				$.get("<c:url value='/teacher/delete'/>",{userId:userId},function(date){
					if(date=="success"){
						location.reload();
					}
				});
			}
		}
		
		//返回专业页面
		function goback(){
			var pp = window.parent.document.getElementById("menutwo");//获得父级iframe
			$(pp).children(".activetwo").click();
		}
		
		//初始化
		$(function() {
			//上传课件文件
			 $("#upload").uploadify({
				 'height'         : 30,//表示按钮的高度，默认30PX。若要改为50PX，如下设置：'height' : 50，  
		         'width'          : 120,//表示按钮的宽度  
			     'swf'            : "<c:url value='/resources/js/uploadify/uploadify.swf' />",  
			     'uploader'       : "<c:url value='/teacher/upload/?dir=excel'/>",
			     'cancelImg'      : "<c:url value='/resources/js/uploadify/uploadify-cancel.png' />",
			     'buttonText'     :'批量导入教师信息' ,
			     'auto'           : true,  
			     'multi'          : true,
			     'wmode'          : 'transparent', 
			     'fileObjName'    : 'file',//文件对象名称。用于在服务器端获取文件。
			     'simUploadLimit' : 1,  
			     'fileTypeExts'   : '*.xls;*.xlsx', 
			    // 'fileTypeDesc'   : '文件(*.xls,*.xlsx;)', 
			     'onUploadSuccess': function(file, data) {
			     	 var message=eval("("+data+")");
			         if(message.error==0){//上传
			         	$.get("<c:url value='/excel/importteacher'/>",{url:message.allpath},function(date){
			        		var msg = eval("("+date+")");
			         		if(msg.error==0){
								alert(msg.msg);
				        		location.reload();
			        		}else{
								alert(msg.msg);
			        		}
			        	},"text");
			         }else{
			        	 alert(message.message);
			         }
			     }  
			 }); 
		});
		
	//查询
	function query(){
		var name=$('[name=teacher_name]').val();
		name = encodeURIComponent(encodeURIComponent(name));
		var teacher_sex=$("[name=teacher_sex]").val().trim();
		var hlink='<c:url value="teacher/menu-content-show-teachers" />?teacher_name='+name+"&teacher_sex="+teacher_sex;
	// ("#queryform").attr("action",hlink);
		window.location.href=hlink;
	}
  	</script>
  </head>	
	<!-- 教师内容开始-->
	<div class="content" align="center">
		<div class="title">
			<span style="margin-left: 14px;font-size: 16px;">教师列表</span>
			<a onclick="javascript:history.back(-1)" style="float: right;margin-right: 10px;cursor:pointer;">返&nbsp;回</a>
			<a href="<c:url value='/excel/exportteacher'/>" class="rightA" >批量导出教师信息 | </a>
			<a href="<c:url value='/resources/doc/teachermodel.xlsx'/>" class="rightA" >下载导入模板 | </a>
		</div>
    	<c:if test="${pagecondition.totalCount!=0}">
    		<div style="margin-left:55px;margin-top:15px;font-size:16px;width:90%;text-align: left;">
				教师姓名：<input type="text" name="teacher_name" value="${teacher_name}"/> &nbsp;&nbsp; 
				教师性别：<select name="teacher_sex">
							<option value=''>--全部--</option>
							<option value="1" <c:if test="${teacher_sex == 1}" > selected="selected" </c:if> >男</option>
							<option value="0" <c:if test="${teacher_sex == 0}" > selected="selected" </c:if> >女</option>
						</select>&nbsp;&nbsp; &nbsp;
				<input type="button"  value="查询" onclick="query();">
			</div>
    		<table class="table" style="margin-top: 10px;">
    			<tr>
    				<th>教师工号</th>
    				<th>教师角色</th>
    				<th>教师姓名</th>
    				<th>教师职称</th>
    				<th>教师性质</th>
    				<th>教师性别</th>
    				<!-- <th>出生日期</th> -->
    				<th>操作</th>
    			</tr>
    			<c:forEach items="${pagecondition.dataList }" var="tl">
	    			<tr>
	    				<td style="text-align: left;">${tl.user_id}</td>
	    				<td>
	    					<c:if test="${tl.role_code==3}">教师</c:if>
	    					<c:if test="${tl.role_code==6}">系统管理员</c:if>
	    				</td>
	    				<td>${tl.teacher_name}</td>
	    				<td>
	    					<c:if test="${tl.technical_code==0}">暂无信息</c:if>
	    					<c:if test="${tl.technical_code==1}">教授</c:if>
	    					<c:if test="${tl.technical_code==2}">副教授</c:if>
	    					<c:if test="${tl.technical_code==3}">讲师</c:if>
	    					<c:if test="${tl.technical_code==4}">助教</c:if>
	    					<c:if test="${tl.technical_code==5}">高级实验师 </c:if>
	    					<c:if test="${tl.technical_code==6}">实验师</c:if>
	    					<c:if test="${tl.technical_code==7}">高级工程师</c:if>
	    					<c:if test="${tl.technical_code==8}">工程师</c:if>
	    				</td>
	    				<td>
	    					<c:if test="${tl.tc_code==0}">暂无信息</c:if>
	    					<c:if test="${tl.tc_code==1}">院内教师</c:if>
	    					<c:if test="${tl.tc_code==2}">外聘教师</c:if>
	    				</td>
	    				<td>
	    					<c:if test="${tl.teacher_sex==1}">男</c:if>
	    					<c:if test="${tl.teacher_sex==0}">女</c:if>
	    				</td>
	    				<!--  <td>${tl.teacher_date}</td>-->
	    				<td>
	    					<a href="teacher/menu-content-add-teachers?id=${tl.user_id}">修改</a>
	    					<a onclick="deleteTeacher(${tl.user_id})">删除</a>
	    				</td>
	    			</tr>
    			</c:forEach>
    		</table>
    		${pagecondition.show }
    	</c:if>
    	<c:if test="${pagecondition.totalCount==0}">
    		<div style = "margin-top: 10px;font-size: 16px;font-weight: lighter;">暂无任何教师信息</div>
    	</c:if>
    	<div style="float: right;width:122px;margin-top: 10px;margin-right: 60px;"><input type="file" id="upload" width="120px" height="30px"/></div>
	</div>
	<!-- 教师内容结束-->