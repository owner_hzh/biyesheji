<%@ page language="java" contentType="text/html; charset=UTF-8" import="java.util.*"  pageEncoding="UTF-8" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'menu-content-add-curriculum.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/datepicker.css" />">
	<link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.min.css"/>" />
	<style type="text/css">
		.info{color:red;font-size: 13px;margin-left: 37%;margin-top: 20px;margin-bottom: 20px;}
		.title{
			height:4%;
			width:95%;
			margin-top: 20px;
			background-color: blue;
			line-height:30px;
			color:#FFFFFF;
			font-size:17px;
			border-radius:15px; 
			text-align: left;
			font-weight: bold;
			margin-left:3%;
		}
		.infoform{
			width:95%;
			margin-top: 20px;
			margin-left:3%;
			font-size:18px;
		}
		input,select{
			font-size:18px;
			width:150px;
		}
		.commit{
			cursor: pointer;
			background-color: #FFCC66;
			margin-top:20px;
			height:40px;
			font-size:20px;
			font-family:"幼圆";
			margin-left: 35%;
			border-radius:10px;
			border: 0px;
		}
		.commit:hover{
			background-color: #0033CC;
			color:#ffffff;
		}
	</style>
	<script type="text/javascript" src="<c:url value='/resources/js/jquery.js'/>"></script>
  </head>
  
  <body>
  	 <form action="curriculum/addcurriculum">
   		 <div id="content_2_1" class="item_content">
	           <div class="title"><span style="margin-left: 14px;">&nbsp;修改课程</span></div>
	       <div>
	       	
	          <div style="hidden">
	              <span style="color:red;margin-left: 10%;">* </span>课程代码：<input type="text" id="curriculum_code" name="curriculum_code" value="${curriculum.curriculum_code}"/>
	          </div>
	          
	          <div>
	              <span style="color:red;margin-left: 10%;">* </span>课程性质：<select id="curriculumcharacter" name="cc_name"></select>
	          </div>
	          
	          <div>
	              <span style="color:red;margin-left: 10%;">* </span>专业：<select id="major" name="major_name"></select>
	          </div>
	          
	          <div>
	       	     <span style="color:red;margin-left: 10%;">* </span>修习方式:<select id="studymode" name="studymode_name"></select>                       
	          </div>
	          
	          <div>
	           	<span style="color:red;margin-left: 10%;">* </span>课程名：<input type="text" id="curriculum_name" name="curriculum_name" value="${curriculum.curriculum_name}"/>                        
	          </div>
	          
	          <div>
	            <span style="color:red;margin-left: 10%;">* </span>课程英文名：<input type="text" id="curriculum_englishname" name="curriculum_englishname" value="${curriculum.curriculum_englishname}"/>                      
	          </div>
	          
	          <div>
	            <span style="color:red;margin-left: 10%;">* </span>学分：<input type="text" id="credit" name="credit" value="${curriculum.credit}"/>                        
	          </div>
	          
	          <div>
	            <span style="color:red;margin-left: 10%;">* </span>	总学时：<input type="text" id="all_time" name="all_time" value="${curriculum.all_time}"/>
	          </div>
	          <div>
	            <span style="color:red;margin-left: 10%;">* </span>	理论学时：<input type="text" id="theory_time" name="theory_time" value="${curriculum.theory_time}"/>
	          </div>
	          <div>
	          	<span style="color:red;margin-left: 10%;">* </span>实验学时：<input type="text" id="practice_time" name="practice_time" value="${curriculum.practice_time}"/>
	          </div>
	          <div>
	          	<span style="color:red;margin-left: 10%;">* </span>上机学时:<input type="text" id="computer_time" name="computer_time" value="${curriculum.computer_time}"/>
	          </div>
	          <div>
	          	<span style="color:red;margin-left: 10%;">* </span>开课学期:<input type="text" id="semester_code" name="semester_code" value="${curriculum.semester_code}">
	          </div>
	          <div>
	          	<span style="color:red;margin-left: 10%;">* </span>总章节数:<input type="text" id="allchapter" name="allchapter" value="${curriculum.allchapter}">
	          </div>
	          <div>
	          	<span style="color:red;margin-left: 10%;">* </span>考核方式:<input type="text" id="exam_way" name="exam_way" value="${curriculum.exam_way}"/>
	          </div>
	          <div>
	          	<span style="color:red;margin-left: 10%;">* </span>说明：<textarea id="explain" name="explain" >${curriculum.explain}</textarea>
	          </div>
	        
	       </div>
       </div>
       <!-- <input type="submit" id="submit" name="submit" value="提交" />
       <input type="reset"  id="reset"  name="reset" value="重置"/>   -->
       <input type="button" id="submit" value="提交"/>
     </form>
        <script type="text/javascript">
       
   		  $(document).ready(function(){   
   			 getCurriculumCharacterList();
   			 getStudymodeList();
   			 getMajorList();
   			 $("#submit").click(function(){
   			 	$.ajax({
				   type: "POST",
				//   contentType : "application/json",//application/xml   
        		   processData : true,//contentType为xml时，些值为false   				   
				   url: "/biyesheji/curriculum/updatecurriculum",  //获取修习方式studymode的list
				   dataType : "json",
				   data:{
						   	  url: "/menu/menu-content-update-curriculum",   //返回到对应页面的url
						      curriculum_code: 	$("#curriculum_code").val(),
			                  curriculum_name:  $("#curriculum_name").val(), 
						curriculum_englishname: $("#curriculum_englishname").val(),
				              credit: 	        $("#credit").val(),		
							  all_time: 		$("#all_time").val(),	
				 			  theory_time:   	$("#theory_time").val(),		
						      practice_time:	$("#practice_time").val(),
				              computer_time:	$("#computer_time").val(),
							  semester_code:	$("#semester_code").val(),		
				              exam_way:			$("#exam_way").val(),
				               explain: 		$("#explain").val(),
				      		 studymode_id  :	$("#studymode").find("option:selected").val(),      
				       			studymode_name: $("#studymode").find("option:selected").text(),
				   					cc_code:	$("#curriculumcharacter").find("option:selected").val(),
	   			   					cc_name:    $("#curriculumcharacter").find("option:selected").text(),
	   			   					major_code: $("#major").find("option:selected").val(), 
	   			   					major_name: $("#major").find("option:selected").text(),
	   			   					allchapter: $("#allchapter").val() 
	   			   					  
				   },
				   success: function(studymodelist){
					   alert("修改数据成功");
				   },
				   error: function (XMLHttpRequest, textStatus, errorThrown) {
		               alert("修改数据失败！");   
		                /* alert(XMLHttpRequest.status);
		                alert(XMLHttpRequest.readyState);
		                alert(textStatus); */
            		}
				});  	
   			 });
    	  }); 
    	  function getMajorList(){    //获取专业下拉列表
    	  		$.ajax({
				   type: "GET",
				   contentType : "application/json;charset=UTF-8",//application/xml   
        		   processData : true,//contentType为xml时，些值为false   				   
				   url: "/biyesheji/major/getallmajor",  //获取修习方式studymode的list
				   dataType : "json",
				   data:{
				   	  url: "/menu/menu-content-add-curriculum"
				   },
				   success: function(studymodelist){
					    $.each(studymodelist, function (n, value) {
					    	for(var i = 0;i<n.length;i++)	
					    		 $("#major").append( "<option value=\"" + value[i].major_code + "\" >" + value[i].major_name + "</option>");
						});  
				   },
				   error: function (XMLHttpRequest, textStatus, errorThrown) {
		               alert("载入专业列表出错！");   
            		}
				});  
    	  }  
    	  function  getStudymodeList(){  //获取修学方式的下拉列表
    	   	 
    	   		$.ajax({
				   type: "GET",
				   contentType : "application/json;charset=UTF-8",//application/xml   
        		   processData : true,//contentType为xml时，些值为false   				   
				   url: "/biyesheji/curriculum/getstudymodelist",  //获取修习方式studymode的list
				   dataType : "json",
				   data:{
				   	  url: "/menu/menu-content-add-curriculum"
				   },
				   success: function(studymodelist){
					    $.each(studymodelist, function (n, value) {
					    	for(var i = 0;i<n.length;i++)	
					    		 $("#studymode").append( "<option value=\"" + value[i].studymode_id + "\" >" + value[i].studymode_name + "</option>");
						});  
				   },
				   error: function (XMLHttpRequest, textStatus, errorThrown) {
		               alert("载入修习模式列表出错！");   
            		}
				});  
    	   }
    	   function getCurriculumCharacterList(){   //获得课程性质的下拉列表
                $.ajax({
				   type: "GET",
				   contentType : "application/json",//application/xml   
        		   processData : true,//contentType为xml时，些值为false   				   
				   url: "/biyesheji/curriculum/getcurriculumcharacterlist",  //获取课程性质curriculumcharacter的list
				   dataType : "json",
				   data: {
				           url: "/menu/menu-content-add-curriculum"
				   },  
				   success: function(curriculumcharacterlist){
				        $.each(curriculumcharacterlist, function (n, value) {
					    	for(var i = 0;i<n.length;i++)	
					    		 $("#curriculumcharacter").append( "<option value=\"" + value[i].cc_code + "\" >" + value[i].cc_name + "</option>");
						});  
				   },
				   error : function(e) {   
            			alert("载入课程性质列表出错！");   
       			   }   
				}); 
		   }  
   		</script>
  </body>
</html>
