<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
  <head>
    <base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/courseware.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/js/uploadify/uploadify.css'/>">
	<link rel='stylesheet' href="<c:url value="/resources/css/pagination.css"/>" />
	<link rel='stylesheet' href="<c:url value="/resources/css/bootstrap.min.css"/>" />
	<style type="text/css">

	</style>
	
	<script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>  
    <script type="text/javascript" src="<c:url value='resources/js/uploadify/jquery.uploadify.min.js'/>"></script>
    <script type="text/javascript">	 
		//删除学生
		function deleteStudent(userId){
			if(confirm("是否删除？")){
				$.get("<c:url value='/student/delete'/>",{userId:userId},function(date){
					if(date=="success"){
						location.reload();
					}
				});
			}
		}
		
		//返回专业页面
		function goback(){
			var pp = window.parent.document.getElementById("menutwo");//获得父级iframe
			$(pp).children(".activetwo").click();
		}
		
		//初始化
		$(function() {
			//上传课件文件
			 $("#upload").uploadify({
				 'height'         : 30,//表示按钮的高度，默认30PX。若要改为50PX，如下设置：'height' : 50，  
		         'width'          : 120,//表示按钮的宽度  
			     'swf'            : "<c:url value='/resources/js/uploadify/uploadify.swf' />",  
			     'uploader'       : "<c:url value='/student/upload/?dir=excel'/>",
			     'cancelImg'      : "<c:url value='/resources/js/uploadify/uploadify-cancel.png' />",
			     'buttonText'     :'批量导入学生信息' ,
			     'auto'           : true,  
			     'multi'          : true,
			     'wmode'          : 'transparent', 
			     'fileObjName'    : 'file',//文件对象名称。用于在服务器端获取文件。
			     'simUploadLimit' : 1,  
			     'fileTypeExts'   : '*.xls;*.xlsx', 
					// 'fileTypeDesc'   : '文件(*.xls,*.xlsx;)', 
			     'onUploadSuccess': function(file, data) {
			     	var message=eval("("+data+")");
			         if(message.error==0){//上传
				         	$.get("<c:url value='/excel/importstudent'/>",{url:message.allpath},function(date){
				        		var msg = eval("("+date+")");
				         		if(msg.error==0){
									alert(msg.msg);
					        		location.reload();
				        		}else{
									alert(msg.msg);
				        		}
				        	},"text");
			         }else{
			        	 alert(message.message);
			         }
			     }  
			 }); 
		});
		
		//查询
		function query(){
			//姓名
			var name=$("[name=student_name]").val();
			name = encodeURIComponent(encodeURIComponent(name));
			//帐号
			var id=$("[name=user_id]").val();
			//年级
			var grade=$("[name=grade]").val();
			//性别
			var student_sex=$("[name=student_sex]").val().trim();
			
			var hlink="<c:url value='student/menu-content-show-students' />?student_name="+name+"&student_sex="+student_sex+"&user_id="+id+"&grade="+grade;
		// ("#queryform").attr("action",hlink);
			window.location.href=hlink;
		}
  	</script>
  </head>	
	<!-- 学生内容开始-->
	<div class="content" align="center">
		<div class="title">
			<span style="margin-left: 14px;font-size:16px;">学生列表</span>
			<a onclick="javascript:history.back(-1)" style="float: right;margin-right: 10px;cursor:pointer;">返&nbsp;回</a>
			<a href="<c:url value='/excel/exportstudent'/>" class="rightA" >批量导出学生信息 | </a>
			<a href="<c:url value='/resources/doc/studentmodel.xlsx'/>" class="rightA" >下载导入模板 | </a>
		</div>
    	<c:if test="${pagecondition.totalCount!=0}">
    		<div style="margin-left:55px;margin-top:15px;font-size:16px;width:95%;text-align: left;">
				学生姓名：<input style="width:90px;" type="text" name="student_name" value="${student_name}" placeholder="姓名"/> &nbsp;&nbsp;
				学生学号：<input style="width:90px;" type="text" name="user_id" id="user_id" value="${user_id}" placeholder="学号" onkeyup="value=value.replace(/[^(\d+)$]/ig,'')"/>&nbsp;&nbsp;
				学生性别：<select style="width:90px;" name="student_sex">
							<option value=''>-全部-</option>
							<option value="1" <c:if test="${student_sex == 1}" > selected="selected" </c:if> >男</option>
							<option value="0" <c:if test="${student_sex == 0}" > selected="selected" </c:if> >女</option>
						</select>&nbsp;&nbsp; &nbsp;
				所属年级：<input style="width:70px;" type="text" name="grade" id="grade" value="${grade}" placeholder="年级" onkeyup="value=value.replace(/[^(\d+)$]/ig,'')"/><span style="color:blue;font-size: 12px;"> (如输入“2010”，表示2010级)</span> 
				<input type="button"  value="查询" onclick="query();">
			</div>
    		<table class="table" style="margin-top: 10px;">
    			<tr>
    				<th>学生学号</th>
    				<th>学生角色</th>
    				<th>学生姓名</th>
    				<th>所属专业</th>
    				<th>所属年级</th>
    				<th>学生性别</th>
    				<th>毕业时间</th>
    				<th>操作</th>
    			</tr>
    			<c:forEach items="${pagecondition.dataList}" var="sl">
	    			<tr>
	    				<td style="text-align: left;">${sl.user_id}</td>
	    				<td>
	    					<c:if test="${sl.role_code==2}">学生</c:if>
	    					<c:if test="${sl.role_code==6}">系统管理员</c:if>
	    				</td>
	    				<td>${sl.student_name}</td>
	    				<td>
	    					<c:if test="${sl.major_code==1}">计算机科学与技术</c:if>
	    					<c:if test="${sl.major_code==2}">通信工程</c:if>
	    					<c:if test="${sl.major_code==3}">电子工程</c:if>
	    				</td>
	    				<td>
	    					${sl.grade}
	    				</td>
	    				<td>
	    					<c:if test="${sl.student_sex==1}">男</c:if>
	    					<c:if test="${sl.student_sex==0}">女</c:if>
	    				</td>
	    				<td>${sl.gradute_date}</td>
	    				<td>
	    					<a onclick="deleteStudent(${sl.user_id})">删除</a>
	    				</td>
	    			</tr>
    			</c:forEach>
    		</table>
    		${pagecondition.show }
    	</c:if>
    	<c:if test="${pagecondition.totalCount==0}">
    		<div style = "margin-top: 10px;font-size: 16px;font-weight: lighter;">暂无任何学生信息</div>
    	</c:if>
    	<div style="float: right;width:122px;margin-top: 10px;margin-right: 60px;"><input type="file" id="upload" width="120px" height="30px"/></div>
	</div>
	<!-- 学生内容结束-->