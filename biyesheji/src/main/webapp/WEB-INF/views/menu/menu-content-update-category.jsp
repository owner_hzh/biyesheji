<%@ page language="java" contentType="text/html; charset=UTF-8" import="java.util.*"  pageEncoding="UTF-8" %> 

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'menu-content-add-curriculum.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
  <script type="text/javascript" src='resources/js/jquery.js'></script>
    <script type="text/javascript">
       
   		  $(document).ready(function(){   
   			 $("#submit").click(function(){
   			 	$.ajax({
				   type: "POST",
				//   contentType : "application/json",//application/xml   
        		   processData : true,//contentType为xml时，些值为false   				   
				   url: "/biyesheji/questiontypes/updateQusetionType",  //获取修习方式studymode的list
				   dataType : "json",
				   data:{
						   	qt_code:'${qusrtiontype.qt_code}',
						   	qt_name:$("#qt_name").val(),
						   	describe:$("#describe").val(),
						   	is_objective:$('input[name="is_objective"]:checked').val()
				   },
				   success: function(studymodelist){
					   alert("修改数据成功");
				   },
				   error: function (XMLHttpRequest, textStatus, errorThrown) {
		               alert("修改数据失败！");   
            		}
				});  	
   			 });
    	  }); 

   		</script>
  </head>
  
  <body>
  	 <form action="curriculum/addcurriculum">
   		 <div id="content_2_1" class="item_content" align="center" style="font-family:'幼圆'">
	           <div>详情/修改题型</div>
	       <div style="width:500px; margin-top:10px;" align="left">
	          <div>
	                                      题型代码：<span>${qusrtiontype.qt_code}</span>
	          </div>
	          <div style="margin-top:10px;">
	           	题型名称：<input type="text" id="qt_name" name="qt_name" value="${qusrtiontype.qt_name}"/>                        
	          </div>
	          <div style="margin-top:10px;">
				是否为客观题:
				<input type="radio" name="is_objective" value="1" <c:if test="${qusrtiontype.is_objective==1}">checked</c:if> />是
				<input name="is_objective"  type="radio" value="0"  <c:if test="${qusrtiontype.is_objective==0}">checked</c:if> />否
			  </div>
	          <div style="width:500px;margin-top:10px;" align="left">
	            <div>标题描述：</div>
	          	<textarea id="describe" name="describe" rows="5" cols="50" maxlength="250" name="describe" style="resize: none;width:500px;" >${qusrtiontype.describe}</textarea>
	          </div>
	        
	       </div>
	        <input type="button" id="submit" value="提交"/>
       </div>
     </form>
  </body>
</html>
