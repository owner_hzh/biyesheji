<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'review.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<style type="text/css">
	a.redlink{cursor: pointer;}
    
     a:link {
    color:#FF0000;
    text-decoration:underline;
    }
    a:visited {
    color:#00FF00;
    text-decoration:none;
    }
    a:hover {
    color:blue;
    text-decoration:underline;
    }
    a:active {
    color:#FFFFFF;
    text-decoration:none;
    }
    
	</style>
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/courseware.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/popwindow.css'/>">
    <script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>
    <script type="text/javascript" src="<c:url value='resources/js/popwindow/popwindow.js'/>"></script>
    <script type="text/javascript">
       function change(code)
       {          
		   $(window).ShowDialog({ width: 800, height: 600, title: "修改课程信息", src: "<c:url value='curriculum/sys/updatecurriculum?code="+code
             +"'/>" });
       }
       
       //删除
       function dchange(code,obj)
       {  
          if(confirm("确定删除？"))  
          {
              $.ajax({
				   type: "GET",
				   url: "curriculum/sys/deletecurriculum",
				   dataType: "text",//返回text格式的数据
				   data: {
		                    code:code	                    
				         },
				   success: function(data){
				     if(data=='true')
				     {
                        $(obj).parent().parent().remove();
				     }else
				     {
				       alert("删除失败！");
				     }
				   },
			   error:function(XMLHttpRequest, textStatus, errorThrown){
			           alert("服务器出错了！");
			        }
				}); 
          }      
         
       }
       
       function closePopWindow()
       {
         var d=$("#dClose");
         if(d)
           d.click();
       }
       
       function paging(pageNo)
	   {
	      $('#iframe',window.parent.document).attr("src","menu-content-change-property?"+"pageNo="+pageNo+"&noCache="+Math.floor(Math.random()*100000));
	   };
	   
	   //页数选择事件
	   function selectchange(obj)
	   {
	      var pageNo=$(obj).val();
	      paging(pageNo);
	   } 
	   //上一页
	   function pageup()
	   {
	     var pageNo=$("#selectpage").val();
	     pageNo=pageNo-1;
	     $("#selectpage option[value='"+pageNo+"']").attr("selected", true);
	     $("#selectpage").change();
	   }
	   //下一页
	   function pagedown()
	   {
	     var pageNo=$("#selectpage").val();
	     pageNo=parseInt(pageNo)+1;
	     $("#selectpage option[value='"+pageNo+"']").attr("selected", true);
	     $("#selectpage").change();
	   }
	       
       
    </script>
  </head>
  
  <body>
    <!-- 课程列表 -->
	<div class="content" align="center">
    	<c:if test="${curriculums!=null && fn:length(curriculums) > 0}">
    		<table class="table" style="margin-top: 10px;">
    		    <tr>
    				<th>课程名称</th>
    				<th>课程性质</th>
    				<th>修读方式</th>
    				<th>所值学分</th>
    				<th>总学时</th>
    				<th>考核方式</th>
    				<th>操作</th>
    			</tr>
    			<c:forEach items="${curriculums}" var="cwl">
	    			<tr>
	    				<td>${cwl.curriculum_name}</td>
	    				<td>${cwl.cc_name}</td>
	    				<td>${cwl.studymode_name}</td>
	    				<td>${cwl.credit}</td>
	    				<td>${cwl.all_time}</td>
	    				<td>${cwl.exam_way}</td>
	    				<td>
	    				    <a onclick="change(${cwl.curriculum_code})">修改</a>
	    				    <%-- <a onclick="dchange(${cwl.curriculum_code},this)">删除</a> --%>
	    				</td>
	    			</tr>
    			</c:forEach>
    		</table>
    	</c:if>
    	<c:if test="${fn:length(curriculums) <= 0||curriculums==null }">
    		<div style = "margin-top: 10px;font-size: 16px;font-weight: lighter;">暂无课程信息</div>
    	</c:if>
	</div>
	<!-- 课程列表结束-->
	<c:if test="${page.currentCount<page.totalCount}">
	   <center><div>
	   
	   <c:if test="${page.pageNo>1}">
	     <a class="redlink" id="pageup" onclick="pageup()">上一页</a>
	   </c:if>
	   
	   <select id="selectpage" onchange="selectchange(this)">
	         <c:forEach begin="1" end="${page.totalPage}" step="1" varStatus="status">
	            <c:if test="${page.pageNo==status.index}">
	               <option value="${status.index}" selected="selected">第${status.index}页</option>
	            </c:if>
	            <c:if test="${page.pageNo!=status.index}">
	               <option value="${status.index}">第${status.index}页</option>
	            </c:if>
	         </c:forEach>
       </select>
       
       <c:if test="${page.pageNo<page.totalPage}">
          <a class="redlink" id="pagedown" onclick="pagedown()">下一页</a>
       </c:if>
       
    </div></center> 
	</c:if>
	 
  </body>
</html>
