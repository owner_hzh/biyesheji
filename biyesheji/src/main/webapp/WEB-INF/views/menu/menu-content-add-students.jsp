<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'content.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/datepicker.css" />">
	<link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.min.css"/>" />
	<style type="text/css">
		.info{color:red;font-size: 13px;margin-left: 37%;margin-top: 20px;margin-bottom: 20px;}
		.title{
			height:4%;
			width:95%;
			margin-top: 20px;
			background-color: blue;
			line-height:30px;
			color:#FFFFFF;
			font-size:17px;
			border-radius:15px; 
			text-align: left;
			font-weight: bold;
			margin-left:3%;
		}
		.infoform{
			width:95%;
			margin-top: 20px;
			margin-left:3%;
			font-size:18px;
		}
		input,select{
			font-size:18px;
			width:150px;
		}
		.commit{
			cursor: pointer;
			background-color: #FFCC66;
			margin-top:20px;
			height:30px;
			widht:60px;
			font-size:20px;
			font-family:"幼圆";
			margin-left: 28%;
			border-radius:10px;
			border: 0px;
		}
		.commit:hover{
			background-color: #0033CC;
			color:#ffffff;
		}
		.goback{
			cursor: pointer;
			background-color: #FFCC66;
			margin-top:20px;
			height:30px;
			widht:60px;
			font-size:20px;
			font-family:"幼圆";
			margin-left:20px;;
			border-radius:10px;
			border: 0px;
		}
		.goback:hover{
			background-color: #0033CC;
			color:#ffffff;
		}
	</style>
	<script type="text/javascript" src="<c:url value='/resources/js/jquery.js'/>"></script>
	<script type="text/javascript" src='<c:url value="/resources/js/bootstrap-datepicker.js" />'></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#user_id").focus();
			//输入时间
			$("#gradute_date").datepicker();
			$('.evenchilds tr:odd').css({
					background : "#fff"
			});
		});
		//检查用户名
		var checkAccount = false ;
		function checkaccount(){
			var id=$("#user_id").val().trim();
			if(id==""){
				$(".info").show().text("用户名不能为空");
			 	$("#user_id").css("background-color","#FEADAD");
				$("#user_id").focus(function(){$("#user_id").css("background-color","");});
				checkAccount = false;
			}else{
				$.post("<c:url value='/student/checkuser'/>",{user_id:id},function(date){
					if(date=="no"){
						$(".info").text("");
						checkAccount = true;
			 		}else{
			 			$(".info").show().text("用户名已存在");
			 			$("#user_id").val("");
			 			$("#user_id").css("background-color","#FEADAD");
			 			$("#user_id").focus(function(){$("#user_id").css("background-color","");});
			 			checkAccount = false;
			 		}
				},"text");
			}
		}
		
		//检查学生姓名
		function checkname(){
			var name=$("#student_name").val().trim();
			if(name==""){
				$(".info").show().text("教师姓名不能为空");
			 	$("#student_name").css("background-color","#FEADAD");
				$("#student_name").focus(function(){$("#student_name").css("background-color","");});
				return false;
			}else{
				$(".info").text("");
				return true;
			}
		}
		//检查学生年级
		function checkgrade(){
			var name=$("#grade").val().trim();
			if(name==""){
				$(".info").show().text("所属年级不能为空");
			 	$("#grade").css("background-color","#FEADAD");
				$("#grade").focus(function(){$("#grade").css("background-color","");});
				return false;
			}else{
				$(".info").text("");
				return true;
			}
		}
		//检查学生毕业时间
		function checkgraduate(){
			var name=$("#gradute_date").val().trim();
			if(name==""){
				$(".info").show().text("学生毕业时间不能为空");
			 	$("#gradute_date").css("background-color","#FEADAD");
				$("#gradute_date").focus(function(){$("#gradute_date").css("background-color","");});
				return false;
			}else{
				$(".info").text("");
				return true;
			}
		}
		
		//自动获取年级
		function getGrade(){
			var gradute_date = $("#gradute_date").val().trim();
			if(gradute_date!="" && gradute_date!=null){
				var grade = gradute_date.substring(0,4);
				grade = grade - 4 ;
				$("#grade").val(grade);
			}
		}
	
		//提交表单
		function commitForm(){
			if(checkAccount&&checkname()&&checkgrade()&&checkgraduate()){
				return true;
			}else{
				return false;
			}
		}
	</script>
  </head>
  
  <body>
  <div class="title"><span style="margin-left: 14px;">&nbsp;添加学生</span></div>
  <form action="<c:url value='student/menu-content-add-students'/>" method="post" class="infoform" onsubmit="return commitForm()">
  	<input type="hidden" name="password" value="123456" />
  	<span class="info"></span><br/>
  	<span style="color:red;margin-left: 10%;">* </span>用&nbsp;户&nbsp;名：<input style="margin-top: 20px;" type="text" name="user_id" id="user_id" value="" placeholder="用户名" onkeyup="value=value.replace(/[^(\d+)$]/ig,'')" onblur="checkaccount()"/><span style="color:blue;font-size: 13px;"> (学生的学号)</span>
  	<span style="color:red;margin-left: 40px;">* </span>学生姓名：<input type="text" name="student_name" id="student_name" value="" placeholder="学生姓名" onblur="checkname()"/><br/>
  	
  	<span style="color:red;margin-left: 10%;">* </span>角&nbsp;&nbsp;&nbsp;&nbsp;色：<select style="margin-top:20px;" name="role_code">
  											<option value="2">学生</option>
  										  </select>
  	<span style="color:red;margin-left: 127px;">* </span>所属专业：<select style="margin-top:20px;" name="major_code">
  											<option value="1">计算机科学与技术</option>
  											<option value="2">通信工程</option>
  											<option value="3">电子工程</option>
  										  </select><br/>
  										  
  	<span style="color:red;margin-left:10%;">* </span>性&nbsp;&nbsp;&nbsp;&nbsp;别：<select style="margin-top:20px;" name="student_sex">
  											<option value="1">男</option>
  											<option value="0">女</option>
  										  </select>
  	<span style="color:red;margin-left: 127px;">* </span>毕业时间：<input style="margin-top:20px;" type="text" name="gradute_date" id="gradute_date" value="" placeholder="毕业时间"  onblur="checkgraduate()"/><br/>
  	<span style="color:red;margin-left: 10%;">* </span>所属年级：<input style="margin-top:20px;" type="text" name="grade" id="grade" value="" placeholder="年级" onkeyup="value=value.replace(/[^(\d+)$]/ig,'')" onblur="checkgrade()" onfocus="getGrade()"/><span style="color:blue;font-size: 13px;"> (如输入“2010”，表示2010级学生)</span><br/>
  	<input type="submit" class="commit" value="提交">
  	<input type="button" class="goback" value="返回" onclick="javascript:history.back(-1)">
  </form>
  </body>
</html>
