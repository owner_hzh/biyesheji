<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'content.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<style type="text/css">
		.info{color:red;font-size: 13px;margin-left: 37%;margin-top: 20px;margin-bottom: 20px;}
		.title{
			height:4%;
			width:95%;
			margin-top: 20px;
			background-color: blue;
			line-height:30px;
			color:#FFFFFF;
			font-size:17px;
			border-radius:15px; 
			text-align: left;
			font-weight: bold;
			margin-left:3%;
		}
		.infoform{
			width:95%;
			margin-top: 20px;
			margin-left:3%;
			font-size:18px;
		}
		input,select{
			font-size:18px;
			width:150px;
		}
		.commit{
			cursor: pointer;
			background-color: #FFCC66;
			margin-top:20px;
			height:30px;
			widht:60px;
			font-size:20px;
			font-family:"幼圆";
			margin-left: 25%;
			border-radius:10px;
			border: 0px;
		}
		.commit:hover{
			background-color: #0033CC;
			color:#ffffff;
		}
		.goback{
			cursor: pointer;
			background-color: #FFCC66;
			margin-top:20px;
			height:30px;
			widht:60px;
			font-size:20px;
			font-family:"幼圆";
			margin-left:20px;;
			border-radius:10px;
			border: 0px;
		}
		.goback:hover{
			background-color: #0033CC;
			color:#ffffff;
		}
	</style>
	<script type="text/javascript" src="<c:url value='/resources/js/jquery.js'/>"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#user_id").focus();
		});
		//检查用户名
		var checkAccount = true ;
		function checkaccount(){
			var id=$("#user_id").val().trim();
			if(id==""){
				$(".info").show().text("用户名不能为空");
			 	$("#user_id").css("background-color","#FEADAD");
				$("#user_id").focus(function(){$("#user_id").css("background-color","");});
				checkAccount = false;
			}else{
				$.post("<c:url value='/teacher/checkuser'/>",{user_id:id},function(date){
					if(date=="no"){
						$(".info").text("");
						checkAccount = true;
			 		}else{
			 			$(".info").show().text("用户名已存在");
			 			$("#user_id").val("");
			 			$("#user_id").css("background-color","#FEADAD");
			 			$("#user_id").focus(function(){$("#user_id").css("background-color","");});
			 			checkAccount = false;
			 		}
				},"text");
			}
		}
		
		//检查教师姓名
		function checkname(){
			var name=$("#teacher_name").val().trim();
			if(name==""){
				$(".info").show().text("教师姓名不能为空");
			 	$("#teacher_name").css("background-color","#FEADAD");
				$("#teacher_name").focus(function(){$("#teacher_name").css("background-color","");});
				return false;
			}else{
				$(".info").text("");
				return true;
			}
		}
	
		//提交表单
		function commitForm(){
		//alert(checkAccount)
			$("input[disabled='disabled']").removeAttr("disabled");
			$("input[disabled='disabled']").attr("disabled", false);
			if(checkAccount&&checkname()){
				return true;
			}else{
				return false;
			}
		}
	</script>
  </head>
  
  <body>
  <div class="title"><span style="margin-left: 14px;">&nbsp;<c:if test="${oldteacher==null}">添加教师</c:if><c:if test="${oldteacher!=null}">修改教师</c:if></span></div>
  <form <c:if test="${oldteacher==null}"> action="<c:url value='teacher/menu-content-add-teachers'/>"</c:if><c:if test="${oldteacher!=null}"> action="<c:url value='teacher/menu-content-update-teachers'/>"</c:if> method="post" class="infoform" onsubmit="return commitForm()">
  	<c:if test="${oldteacher==null}"><input type="hidden" name="password" value="123456" /></c:if>
  	<span class="info"></span><br/>
  	<span style="color:red;margin-left: 10%;">* </span>用户名：<input style="margin-top: 20px;" type="text" name="user_id" id="user_id" value="${oldteacher.user_id }" <c:if test="${oldteacher!=null}"> disabled="disabled" </c:if> placeholder="用户名" onkeyup="value=value.replace(/[^(\d+)$]/ig,'')" onblur="checkaccount()" /><span style="color:blue;font-size: 13px;"> (教师的工号)</span>
  	<span style="color:red;margin-left: 40px;">* </span>教师姓名：<input type="text" name="teacher_name" id="teacher_name" value="${oldteacher.teacher_name }" placeholder="教师姓名" onblur="checkname()"/><br/>
  	
  	<span style="color:red;margin-left: 10%;">* </span>角&nbsp;&nbsp;色：<select style="margin-top:20px;" name="role_code">
  											<option value="3">教师</option>
  											<option value="6" <c:if test="${oldteacher.role_code==6}">selected="selected"</c:if> >系统管理员</option>
  										  </select>
  	<span style="color:red;margin-left: 127px;">* </span>教师性质：<select style="margin-top:20px;" name="tc_code">
  											<option value="1">院内教师</option>
  											<option value="2" <c:if test="${oldteacher.tc_code==2}">selected="selected"</c:if>>外聘教师</option>
  										  </select><br/>
  										  
  	<span style="color:red;margin-left:10%;">* </span>性&nbsp;&nbsp;别：<select style="margin-top:20px;" name="teacher_sex" <c:if test="${oldteacher!=null}"> disabled="disabled"</c:if>>
  											<option value="1">男</option>
  											<option value="0" <c:if test="${oldteacher.teacher_sex==0}">selected="selected"</c:if>>女</option>
  										  </select>
  	<span style="color:red;margin-left: 127px;">* </span>教师职称：<select style="margin-top:20px;" name="technical_code">
  											<option value="1">教授</option>
  											<option value="2" <c:if test="${oldteacher.technical_code==2}">selected="selected"</c:if>>副教授</option>
  											<option value="3" <c:if test="${oldteacher.technical_code==3}">selected="selected"</c:if>>讲师</option>
  											<option value="4" <c:if test="${oldteacher.technical_code==4}">selected="selected"</c:if>>助教</option>
  											<option value="5" <c:if test="${oldteacher.technical_code==5}">selected="selected"</c:if>>高级实验师 </option>
  											<option value="6" <c:if test="${oldteacher.technical_code==6}">selected="selected"</c:if>>实验师</option>
  											<option value="7" <c:if test="${oldteacher.technical_code==7}">selected="selected"</c:if>>高级工程师</option>
  											<option value="8" <c:if test="${oldteacher.technical_code==8}">selected="selected"</c:if>>工程师</option>
  										  </select><br/>
  	<input type="submit" class="commit" value="提交">
  	<input type="button" class="goback" value="返回" onclick="javascript:history.back(-1)">
  </form>
  </body>
</html>
