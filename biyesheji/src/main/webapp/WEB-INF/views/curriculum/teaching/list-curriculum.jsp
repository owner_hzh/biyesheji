<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'show-bbs.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/courseware.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/popwindow.css'/>">
	<style type="text/css">
		.title{
			height:4%;
			width:95%;
			margin-top: 20px;
			background-color: blue;
			line-height:30px;
			color:#FFFFFF;
			font-size:17px;
			border-radius:15px; 
			text-align: left;
			font-weight: bold;
		}
		a{
    		text-decoration : none	;	
		}
		.iframe_content{ width: 100%; height: 100%; overflow: auto;background-color:#CCCCCC;
	   					border-top-right-radius:15px; 
	   			        border-bottom-right-radius:15px;  }
	</style>
	
	<script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script> 
	<script type="text/javascript" src="<c:url value='resources/js/json2.js'/>"></script>  
 	<script type="text/javascript">
 	  $().ready( function() {
 		
 		 var $listForm = $("#listForm");// 列表表单
 		 var jsonArr = new Array();
 		 var $idsCheck;
 		 if ($listForm.size() > 0) { 
 		 	 $idsCheck= $("#listTable input[name='ids']");
 		 }	// ID复选框    	                    
		  
		  $("#confirm").click(function(){
		  	
		  	if($("#userid").val()=="")
		      	alert("老师工号不能为空!");
		   else if($("#listTable input[name='ids']:checked").size()<=0)
		      	alert("请选择相应课程！");
		   else{
		      	 $("input:checkbox").each(function(){
 						if(this.checked){
 							var jsonObj = {}; 
							jsonObj["id"] = $(this).val();
							jsonArr.push(jsonObj);   
 						}
				 }); 
		  		  $.ajax({
						url : "<c:url value='/curriculum/teaching/add-teaching'/>",
					    data :{
					    	ids: JSON.stringify(jsonArr),
					    	userid:$("#userid").val()
					    },  
						type : "POST",
						dataType : "json",
						success : function(data) {
							alert("添加成功");
						},
						error: function(data){
							alert("老师添加课程失败！");
						}
						
				});   
			}
		  });
		  
		  
		});
	
 	</script>
  </head>
  
  <body>
	<div class="content" align="center">
  	  	<form id="listForm" name="form2" action="<c:url value='/curriculum/teaching/search'/>" method="POST">
  	  		<input type="text" id="userid" readonly name="userid"  value="<c:if test="${userid!=null}">${userid}</c:if>"/>
		  	<input id="confirm" type="button" value="确认添加"/>
		  <div style="margin-top:5px">
	 	             专业:<select id="major_select" name="major_select">
	 	             <option value="-1">全部</option>
	 	             	<c:choose>
					  		<c:when test="${!empty majorlist }">
					  			<c:forEach items="${majorlist}" var="major">   
				  	 		    	<option value='<c:out value="${major.major_code}"/>'  <c:if test="${ majorcode_save == major.major_code}">selected</c:if>  > 
				  	 		    		<c:out value="${major.major_name}"/>
				  	 		    	</option>
					  			</c:forEach>
					  		</c:when>
					  		<c:otherwise>
					  			<tr>
						  			<td colspan="6">没查询课程性质到相关数据</td>
						  		</tr>
					  		</c:otherwise>
	  					</c:choose>	
	 	       </select>
	  		课程性质:<select id="curriculumcharacter_select" name="curriculumcharacter_select">
	  					<option value="-1" <%-- <c:if test="${_info.curriculumcharacter_select==-1}">selected</c:if> --%>>全部</option>
	  					<c:choose>
					  		<c:when test="${!empty curriculumcharacterclist }">
					  			<c:forEach items="${curriculumcharacterclist }" var="ccl">    
				  	 		    	<option value='<c:out value="${ccl.cc_code}"/>' <c:if test="${ cccode_save == ccl.cc_code}">selected</c:if> >
				  	 		    		<c:out value="${ccl.cc_name}" />
				  	 		    	</option>
					  			</c:forEach>
					  		</c:when>
					  		<c:otherwise>
					  			<tr>
						  			<td colspan="6">没查询课程性质到相关数据</td>
						  		</tr>
					  		</c:otherwise>
	  					</c:choose>	
				  </select>
	  		学期:<select id="semester_select" name="semester_select">
	  				<option value="-1">全部</option>
	  				  <c:forEach var="i" begin="1" end="8" step="1"> 
     					<option value='<c:out value="${i}"/>' <c:if test="${ semestercode_save == i}">selected</c:if>>
     						 <c:out value="${i}"/>
     					</option> 
    				  </c:forEach>
	  		    </select>
	  		    
	  		修习方式:<select id="studymode_select" name="studymode_select">
	  					<option value="-1" >全部</option>
	  					<c:choose>
					  		<c:when test="${!empty studymodelist }">
					  			<c:forEach items="${studymodelist }" var="sml">    
				  	 		    	<option value='<c:out value= "${sml.studymode_id}" />' <c:if test="${sml.studymode_id == studymodeid_save}">selected</c:if>>
				  	 		    		<c:out value="${sml.studymode_name}" />
				  	 		    	</option>
					  			</c:forEach>
					  		</c:when>
					  		<c:otherwise>
					  			<tr>
						  			<td colspan="13">没查询课程性质到相关数据</td>
						  		</tr>
					  		</c:otherwise>
	  					</c:choose>	
				  </select>
	 		<input id="submit" type="submit"/>
	  </div>	
  	  <div>
  	  		<table id="listTable" class="table">
  	 			<tr >
  	 			    <th></th> 
					<th>课程代码</th>
					<th>课程名称</th>
					<th>课程英文名称</th>
					<th>修读要求</th>
					<th>总学分</th>
					<th>总学时</th>
					<th>理论</th>
					<th>实验</th>
					<th>上机</th>
					<th>开课学期</th>
					<th>考核方式</th>
				</tr>
				
					<c:choose>
					  		<c:when test="${!empty curriculum_list }">
					  			<c:forEach items="${curriculum_list }" var="cl">    
					  				<tr>
					  					<td><input type="checkbox" id="ids" name="ids" value="${cl.curriculum_code}" /></td>
					  					<td style="text-align: middle;display:none;"> ${cl.curriculum_code}</td>	
					  	 		    	<td style="text-align: middle;"> ${cl.curriculum_stringcode}</td>
					  	 		    	<c:choose>
											<c:when test="${fn:length(cl.curriculum_name) >6}">
							  	 		    	<td style="text-align: middle;" alert="${cl.curriculum_name}">
							  	 		    		 <c:out value="${fn:substring(cl.curriculum_name, 0, 6)}.." />
							  	 		    	</td>
							  	 		    </c:when>
							  	 		    <c:otherwise>
												<td style="text-align: middle;"> ${cl.curriculum_name}</td>
											</c:otherwise>
							  	 		</c:choose>
					  	 		    	<c:choose>
											<c:when test="${fn:length(cl.curriculum_englishname) >10}">
							  	 		    	<td style="text-align: middle;" alert=" ${cl.curriculum_englishname}">
							  	 		    		 <c:out value="${fn:substring(cl.curriculum_englishname, 0, 10)}.." />
							  	 		    	</td>
							  	 		    </c:when>
							  	 		    <c:otherwise>
												<td style="text-align: middle;"> ${cl.curriculum_englishname}</td>
											</c:otherwise>
							  	 		</c:choose>
					  	 		    	<td style="text-align: middle;word-wrap:break-word;"> ${cl.studymode_id}</td>
					  	 		    	<td style="text-align: middle;word-wrap:break-word;"> ${cl.credit}</td>
					  	 		    	<td style="text-align: middle;word-wrap:break-word;"> ${cl.all_time}</td>
					  	 		    	<td style="text-align: middle;word-wrap:break-word;"> ${cl.theory_time}</td>
					  	 		    	<td style="text-align: middle;word-wrap:break-word;"> ${cl.practice_time}</td>
					  	 		    	<td style="text-align: middle;word-wrap:break-word;"> ${cl.computer_time}</td>
					  	 		    	<td style="text-align: middle;word-wrap:break-word;"> ${cl.semester_code}</td>
					  	 		    	<td style="text-align: middle;word-wrap:break-word;"> ${cl.exam_way}</td>
					  	 		    
				  	 		    	</tr>
					  			</c:forEach>
					  		</c:when>
					  		<c:otherwise>
					  			<tr>
						  			<td colspan="12">没查询到相关数据</td>
						  		</tr>
					  		</c:otherwise>
	  					</c:choose>	
			</table>
        
		</div>
	</form>
	 </div>
</body>
</html>