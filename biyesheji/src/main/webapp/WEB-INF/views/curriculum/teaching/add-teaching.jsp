<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'show-bbs.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/courseware.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/popwindow.css'/>">
	<style type="text/css">
		.title{
			height:4%;
			width:95%;
			margin-top: 20px;
			background-color: blue;
			line-height:30px;
			color:#FFFFFF;
			font-size:17px;
			border-radius:15px; 
			text-align: left;
			font-weight: bold;
		}
		a{
    		text-decoration : none	;	
		}
		.iframe_content{ width: 100%; height: 100%; overflow: auto;background-color:#CCCCCC;
	   					border-top-right-radius:15px; 
	   			        border-bottom-right-radius:15px;  }
	</style>
	
	<script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script> 
	<script type="text/javascript" src="<c:url value='resources/js/json2.js'/>"></script>  
  </head>
  
  <body>
    <span>你当前的位置：[<a href="<c:url value='/curriculum/teaching/show'/>">老师课程一览</a>]-[添加-老师课程]</span>
  	<div class="content" align="center">
	  	<div class="title"><span style="margin-left: 14px;">添加-老师课程</span></div>
	  	<div style="margin-top:10px">
	  	    <form id="form" name="form1" action="<c:url value='/curriculum/teaching/search'/>" method="POST">
	  			请输入老师工号:<input type="text" id="userid" name="userid" />
	  			 <input id="submit" type="submit" value="点击加载课程列表"/> 
	  	    </form>
	  	
  		</div>
	</div>
	
  </body>
</html>
