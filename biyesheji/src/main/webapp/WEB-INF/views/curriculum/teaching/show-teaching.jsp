<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'show-bbs.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/courseware.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/popwindow.css'/>">
	<style type="text/css">
		.title{
			height:4%;
			width:95%;
			margin-top: 20px;
			background-color: blue;
			line-height:30px;
			color:#FFFFFF;
			font-size:17px;
			border-radius:15px; 
			text-align: left;
			font-weight: bold;
		}
		
		a{
    		text-decoration : none	;	
		}
	</style>
	
	<script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>  
 
  </head>
  
  <body>
  	<span>你当前的位置：[老师课程一览]</span>
  	<div class="content" align="center">
  	
  	<c:if test="${loginuserRole==6}"><a href="<c:url value='/curriculum/teaching/add-teaching'/>?method=load">添加-老师课程</a></c:if>
  	<div class="title"><span style="margin-left: 14px;">老师-课程</span></div>
  	
  	<div style="margin-top:10px">
  		<form id="form" name="form" action="<c:url value='/curriculum/teaching/search-curriculum-by-teacherid'/>" method="POST">
  			I.老师工号:<input type="text" id="userid" name="userid" value="<c:if test="${teacherid!=null}">${teacherid}</c:if>"/>
  			<input id="submit" type="submit"/>
  		</form>
  	</div>
  	
  	 <div>
  	  		<table id="table" class="table">
  	 			<tr >
					<th>课程代码</th>
					<th>课程名称</th>
					<th>课程英文名称</th>
					<th>修读要求</th>
					<th>总章节</th>
					<th>总学分</th>
					<th>总学时</th>
					<th>理论</th>
					<th>实验</th>
					<th>上机</th>
					<th>开课学期</th>
					<th>考核方式</th>
					<th>操作</th>
				</tr>
				
					<c:choose>
					  		<c:when test="${!empty curriculum_list }">
					  			<c:forEach items="${curriculum_list }" var="cl">    
					  				<tr>
					  					<td style="text-align: middle;display:none;"> ${cl.curriculum_code}</td>	
					  	 		    	<td style="text-align: middle;"> ${cl.curriculum_stringcode}</td>
					  	 		    	<c:choose>
											<c:when test="${fn:length(cl.curriculum_name) >6}">
							  	 		    	<td style="text-align: middle;" alert=" ${cl.curriculum_name}">
							  	 		    		 <c:out value="${fn:substring(cl.curriculum_name, 0, 6)}.." />
							  	 		    	</td>
							  	 		    </c:when>
							  	 		    <c:otherwise>
												<td style="text-align: middle;"> ${cl.curriculum_name}</td>
											</c:otherwise>
							  	 		</c:choose>
					  	 		    	<c:choose>
											<c:when test="${fn:length(cl.curriculum_englishname) >10}">
							  	 		    	<td style="text-align: middle;" alert=" ${cl.curriculum_englishname}">
							  	 		    		 <c:out value="${fn:substring(cl.curriculum_englishname, 0, 10)}.." />
							  	 		    	</td>
							  	 		    </c:when>
							  	 		    <c:otherwise>
												<td style="text-align: middle;"> ${cl.curriculum_englishname}</td>
											</c:otherwise>
							  	 		</c:choose>
					  	 		    	<td style="text-align: middle;word-wrap:break-word;">
					  	 		    		<c:if test="${cl.studymode_id==1}">必修</c:if>
					  	 		    		<c:if test="${cl.studymode_id==2}">限选</c:if>
					  	 		    		<c:if test="${cl.studymode_id==3}">任选</c:if>
					  	 		    	</td>
					  	 		    	<td style="text-align: middle;word-wrap:break-word;"> ${cl.allchapter}</td>
					  	 		    	<td style="text-align: middle;word-wrap:break-word;"> ${cl.credit}</td>
					  	 		    	<td style="text-align: middle;word-wrap:break-word;"> ${cl.all_time}</td>
					  	 		    	<td style="text-align: middle;word-wrap:break-word;"> ${cl.theory_time}</td>
					  	 		    	<td style="text-align: middle;word-wrap:break-word;"> ${cl.practice_time}</td>
					  	 		    	<td style="text-align: middle;word-wrap:break-word;"> ${cl.computer_time}</td>
					  	 		    	<td style="text-align: middle;word-wrap:break-word;"> ${cl.semester_code}</td>
					  	 		    	<td style="text-align: middle;word-wrap:break-word;"> ${cl.exam_way}</td>
					  	 		        <td>
					  	 		    		<a href='javascript:void(0);' id="update" name="update">查看</a>
					  	 		    	  	<c:if test="${loginuserRole==6}"><a href='javascript:void(0);' id="delete" name="delete">删除</a></c:if>
					  	 		    	</td> 
				  	 		    	</tr>
					  			</c:forEach>
					  		
					  			
                	  			
					  		</c:when>
					  		<c:otherwise>
					  			<tr>
						  			<td colspan="13">没查询到相关数据</td>
						  		</tr>
					  		</c:otherwise>
	  					</c:choose>	
			</table>
		</div>
	 </div>
	  <script type="text/javascript">
   		 $(document).ready(function(){ 
   		 	update();  
   			del();
    	 }); 
    	 function del(){
    	 	$("#table tr").each(function(){
    			var tr = $(this);
    			var edit = tr.find("td").find("#delete");
    			edit.click(function(){
    			
    				var tds = edit.parent().parent().find("td");
    				$.ajax({
					    type: "POST",
	        		   processData : true,//contentType为xml时，些值为false   				   
					   url: "<c:url value='/curriculum/teaching/delete-teaching'/>" ,  //获取修习方式studymode的list
					   dataType : "json",
					   data:{
					   		curriculum_code : tds.eq(0).text(), 
					   		userid : $("#userid").val()
					   },
					   success: function(){
						   alert("删除数据成功");
						   location.reload();
					   },
					   error: function (XMLHttpRequest, textStatus, errorThrown) {
			          		   alert("删除数据失败！");  
			          	//	 location.reload(); 
	            		}
					});  
    			});
    		});
    	 }
    	 function update(){
	    	 	$("#table tr").each(function(){
	    	 		var tr = $(this);
	    			var edit = tr.find("td").find("#update");
	    			
	    	 		edit.click(function(){
	    	 			var tds = edit.parent().parent().find("td");
	    	 			window.location.href = "<c:url value='/menu-content-check-curriculum'/>?curriculum_code="+ tds.eq(0).text();
	    	 		});
	    		});
	    	 
    	 }
   </script>
   </script>
  
  </body>
</html>
