<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'content.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<style type="text/css">
	   .info{color:red;font-size: 13px;margin-top: 20px;margin-bottom: 20px;}
		.infoform{
			margin-top: 20px;
			margin-left:20%;
			font-size:18px;
		}
		input,select{
			font-size:18px;
			width:250px;
		}
		.commit{
			cursor: pointer;
			background-color: #FFCC66;
			margin-top:20px;
			margin-left:20%;
			height:40px;
			font-size:20px;
			font-family:"幼圆";
			border-radius:10px;
			border: 0px;
		}
		.commit:hover{
			background-color: #0033CC;
			color:#ffffff;
		}
	  textarea{
       border:1px solid #999;
       font-size:12px;
       font-family:"幼圆";
       padding:1px;
       height:140px;
       overflow:auto;
       width:70%;
       text-align:left;
       padding:5px;
       }
       
	</style>
   <script type="text/javascript" src="<c:url value='/resources/js/jquery.js'/>"></script>
	<script type="text/javascript">	
	    $(function(){
	       $("#curriculum_name").focus();
	       $("#cc_code option[value=${curriculum.cc_code}]").attr("selected", true);
	       $("#studymode_id option[value=${curriculum.studymode_id}]").attr("selected", true);
	       $("#semester_code option[value=${curriculum.semester_code}]").attr("selected", true);
	       $("#exam_way option[value=${curriculum.exam_way}]").attr("selected", true);
	    });	
	    function checkname(){
			var name=$("#curriculum_name").val().trim();
			if(name==""){
				$(".info").show().text("教师姓名不能为空");
			 	$("#curriculum_name").css("background-color","#FEADAD");
				$("#curriculum_name").focus(function(){$("#curriculum_name").css("background-color","");});
				return false;
			}else{
				$(".info").text("");
				
				return true;
			}
		}

        //提交表单
		function commitForm(){
			if(checkname()){
			    parent.closePopWindow();
				return true;
			}else{
				return false;
			}
		}
	</script>
  </head>
  
  <body>
    <div id="content" class="content">

			<%-- 	<form action="<c:url value='curriculum/sys/submitupdatecurriculum'/>" class="infoform" method="post" onsubmit="return commitForm()">
			 --%>
			 <form action="<c:url value='curriculum/updatecurriculum'/>" class="infoform" method="post" onsubmit="return commitForm()">
			 	 <input type="hidden" id="curriculum_code" name="curriculum_code" value="${curriculum.curriculum_code}" />  
			 	   课程代码:<input type="text" readonly="readonly" id="curriculum_stringcode" name="curriculum_stringcode" value="${curriculum.curriculum_stringcode}"/> <span class="info"></span><br/><br/>
                                                           课程名称:<input type="text" readonly="readonly" id="curriculum_name" name="curriculum_name" value="${curriculum.curriculum_name}" onblur="checkname()"/> <span class="info"></span><br/><br/>
                                                           英文名称:<input type="text" id="curriculum_englishname" name="curriculum_englishname" value="${curriculum.curriculum_englishname}"/><br/><br/>
					所属专业:<select name="major_code" id="major_code" >
								<c:forEach items="${majors}" var="major">
								   <c:if test="${major.major_code==curriculum.major_code}">
								      <option value="${major.major_code}" selected="selected">${major.major_name}</option>
								   </c:if>
								   <c:if test="${major.major_code!=curriculum.major_code}">
								      <option value="${major.major_code}">${major.major_name}</option>
								   </c:if>
								</c:forEach>
							  </select><br/><br/>
					课程性质:<select name="cc_code" id="cc_code">				            
								<option value="1">专业基础课</option>
								<option value="2">专业课</option>
								<option value="3">专业方向课</option>
								<option value="4">课程设计</option>
								<option value="5">课程实训</option>
								<option value="6">项目实训</option>
								<option value="7">实习</option>
							  </select><br/><br/>
					修读方式:<select name="studymode_id" id="studymode_id">
								<option value="1">必修</option>
								<option value="2">限选</option>
								<option value="3">任选</option>
								<option value="4">选修</option>
							  </select><br/><br/>
					
					总章节数:<input type="text" id="allchapter" name="allchapter" value="${curriculum.allchapter}"/><br/><br/>
					所值学分:<input type="text" id="credit" name="credit" value="${curriculum.credit}"/><br/><br/>

					理论学时:<input type="text" id="theory_time" name="theory_time" value="${curriculum.theory_time}"/><br/><br/>
					实验学时:<input type="text" id="practice_time" name="practice_time" value="${curriculum.practice_time}"/><br/><br/>
					上机学时:<input type="text" id="computer_time" name="computer_time" value="${curriculum.computer_time}"/><br/><br/>
					开课学期:<select name="semester_code" id="semester_code">
								<option value="1">第一学期</option>
								<option value="2">第二学期</option>
								<option value="3">第三学期</option>
								<option value="4">第四学期</option>
								<option value="5">第五学期</option>
								<option value="6">第六学期</option>
								<option value="7">第七学期</option>
								<option value="8">第八学期</option>
							  </select><br/><br/>
					考核方式:<select name="exam_way" id="exam_way">
								<option value="闭卷">闭卷</option>
								<option value="开卷">开卷</option>
							  </select><br/><br/>
					课程说明:<br/>
					<textarea  id="explain" name="explain">${curriculum.explain}</textarea><br/><br/>
                    <input type="submit" class="commit" value="提交" />
				</form>
		</div>
  </body>
</html>
