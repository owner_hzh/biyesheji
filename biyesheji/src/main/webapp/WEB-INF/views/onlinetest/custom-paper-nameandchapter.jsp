<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
  <head>
    <base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/courseware.css'/>">
	<style type="text/css">
	</style>
    <script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>
    <script type="text/javascript">	 
    
    function commit()
    {
    	var start_chapter=$("#start_chapter").val();
    	var end_chapter=$("#end_chapter").val();
    	var curriculum_code=${curriculum.curriculum_code};
    	var page_name=$("#page_name").val();
    	if($.trim($('#page_name').val())!="")
    	{
    	    page_name=encodeURIComponent(encodeURIComponent(page_name));
    	    var pp = window.parent.document.getElementById("iframe");//获得父级iframe
    	    $(pp).attr("src" , "<c:url value='/online/custompaper?curriculum_code="+curriculum_code+"&page_name="+page_name+"&start_chapter="+start_chapter+"&end_chapter="+end_chapter+"'/>");
    	}
    	else
    	{
    		alert("试卷名不能为空!");
    	}
    }
    function goback()
    {
    	window.location.href="<c:url value='online/showcurriculum?type=2&id=16'/>";//改变父级iframe的src属性
    }
    
    
    $(document).ready(function()
    {
    	//设置章节选择的select
    	var allChapter=0;
    	<c:if test="${curriculum.allchapter!=null}">allChapter=${curriculum.allchapter};</c:if>
    	//alert(allChapter);
    	if(allChapter>0)
    	{
    	    for(var i=1;i<=allChapter;i++)
    	    {
    	    	if(i==1)jQuery("#start_chapter").append("<option value='"+i+"'selected='selected'>第"+i+"章</option>");
    	    	else jQuery("#start_chapter").append("<option value='"+i+"'>第"+i+"章</option>");
    	    	if(i==allChapter)jQuery("#end_chapter").append("<option value='"+i+"'selected='selected'>第"+i+"章</option>");
    	    	else jQuery("#end_chapter").append("<option value='"+i+"'>第"+i+"章</option>");
    	    }
    	}
    	else
    	{
    		 jQuery("#start_chapter").append("<option value='"+0+"'>第"+0+"章</option>");
	    	 jQuery("#end_chapter").append("<option value='"+0+"'>第"+0+"章</option>");
    	}
    });
    
	</script>
  </head>	
	<!-- 设置试题的章节和试卷名-->
	<div class="content" align="center"  style="font-family:'幼圆';"> 
		<div class="title"><span style="margin-left: 14px;">${curriculum.curriculum_name}->定制试卷名和章节</span><a onclick="goback()" style="float: right;margin-right: 10px;cursor:pointer;">返&nbsp;回</a></div>
		<div style="margin-top: 14px;">
		  <span>选择章节范围：</span>
		  <select id="start_chapter" name="start_chapter">
		  </select>
		  <span>至</span>
		  <select id="end_chapter" name="end_chapter">
		  </select>
		</div>
		<div style="margin-top: 10px;">
		  <span>试卷名:</span> <input id="page_name" name="title" type="text" maxlength='20'/>
		</div>
		<input style="margin-top: 10px;" type="button" onclick="commit()" value="确定">
	</div>
