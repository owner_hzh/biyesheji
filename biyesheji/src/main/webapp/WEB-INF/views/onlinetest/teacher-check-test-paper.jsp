<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
  <head>
    <base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/courseware.css'/>">
	<style type="text/css">
	   a.redlink{cursor: pointer;}
    
     a:link {
    color:#FF0000;
    text-decoration:underline;
    }
    a:visited {
    color:#00FF00;
    text-decoration:none;
    }
    a:hover {
    color:blue;
    text-decoration:underline;
    }
    a:active {
    color:#FFFFFF;
    text-decoration:none;
    }
	</style>
    <script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>
    <script type="text/javascript">	 
		function setQuestion(curriculum_code,curriculum_name,id){
			var pp = window.parent.document.getElementById("iframe");//获得父级iframe
			curriculum_name = encodeURIComponent(encodeURIComponent(curriculum_name));
			$(pp).attr("src" , "<c:url value='/online/choosestudent?curriculum_code="+curriculum_code+"&curriculum_name="+curriculum_name+"&id="+id+"'/>");//改变父级iframe的src属性
		}
		
	   function paging(pageNo)
	   {
	      $('#iframe',window.parent.document).attr("src","online/checktestpaper?id=${id}"+"&pageNo="+pageNo+"&noCache="+Math.floor(Math.random()*100000));
	   };
	   
	   //页数选择事件
	   function selectchange(obj)
	   {
	      var pageNo=$(obj).val();
	      paging(pageNo);
	   } 
	   //上一页
	   function pageup()
	   {
	     var pageNo=$("#selectpage").val();
	     pageNo=pageNo-1;
	     $("#selectpage option[value='"+pageNo+"']").attr("selected", true);
	     $("#selectpage").change();
	   }
	   //下一页
	   function pagedown()
	   {
	     var pageNo=$("#selectpage").val();
	     pageNo=parseInt(pageNo)+1;
	     $("#selectpage option[value='"+pageNo+"']").attr("selected", true);
	     $("#selectpage").change();
	   }
	</script>
  </head>	
	<!-- 课程内容开始-->
	<div class="content" align="center"> 
	
	
    	<c:if test="${curriculums!=null }">
    		<table class="table">
    			<tr>
    				<th>课程名称</th>
    				<th>操作</th>
    			</tr>
    			<c:forEach items="${curriculums}" var="curr">
	    			<tr>
	    				<td>${curr.curriculum_name}</td>
		                <td><a onclick="setQuestion(${curr.curriculum_code },'${curr.curriculum_name }',${id})">选择学生</a></td>		
	    			</tr>
    			</c:forEach>
    		</table>
    	</c:if>
	</div>
	<!-- 课程内容结束-->
	<!-- 分页 -->
	<c:if test="${page.currentCount<page.totalCount}">
	   <center><div>
	   
	   <c:if test="${page.pageNo>1}">
	     <a class="redlink" id="pageup" onclick="pageup()">上一页</a>
	   </c:if>
	   
	   <select id="selectpage" onchange="selectchange(this)">
	         <c:forEach begin="1" end="${page.totalPage}" step="1" varStatus="status">
	            <c:if test="${page.pageNo==status.index}">
	               <option value="${status.index}" selected="selected">第${status.index}页</option>
	            </c:if>
	            <c:if test="${page.pageNo!=status.index}">
	               <option value="${status.index}">第${status.index}页</option>
	            </c:if>
	         </c:forEach>
       </select>
       
       <c:if test="${page.pageNo<page.totalPage}">
          <a class="redlink" id="pagedown" onclick="pagedown()">下一页</a>
       </c:if>
       
    </div></center> 
	</c:if>
	<!-- 分页 结束-->