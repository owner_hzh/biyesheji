<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
  <head>
    <base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/courseware.css'/>">
	<style type="text/css">
		.paper{
			text-align: left;
			margin-top:10px;
			width:90%;
		}
		.questiontype{
			font-size: 18px;
			font-weight: bold;
			margin-top:10px;
			background-color: #0066CC;
			color: #FFFFFF;
			height: 4%;
			border-radius: 15px;
			line-height: 30px;
		}
		.typetitle{
			margin-left:10px;
		}
		.questiontypeA{
			margin-right:20px;
			float: right;
			text-decoration: none;
			color: #FFFFFF;
		}
		.questiontypeA:hover{
			text-decoration:underline;
		}
		.questions{
			font-size: 17px;
			font-weight:lighter;
			border-left: 1px dashed #000000;
			border-right: 1px dashed #000000;
			border-bottom: 1px dashed #000000;
			margin-top:-10px;
			display:none;
		}
		.question{
			margin-top:20px;
			margin-left:10px;
			margin-bottom:10px;
		}
		.commit{
			cursor: pointer;
			background-color: #FFCC66;
			margin-top:5px;
			margin-bottom:15px;
			height:30px;
			font-size:17px;
			font-family:"幼圆";
			margin-left:85%;
			border-radius:10px;
			border: 0px;
		}
		.commited{
			background-color: #999999;
			margin-top:5px;
			margin-bottom:15px;
			height:30px;
			font-size:17px;
			font-family:"幼圆";
			margin-left:85%;
			border-radius:10px;
			border: 0px;
		}
		.commit:hover{
			background-color: #0033CC;
			color:#ffffff;
		}
		.shortInput{
			width:60px;
			height: 28px;
			font-size: 17px;
			margin-top: 10px;
		}
		.shortInputed{
			border: 0px;
			background-color: #CCCCCC;
		}
		.allCommit{
			cursor: pointer;
			background-color: #FFCC66;
			margin-top:5px;
			margin-bottom:15px;
			height:30px;
			font-size:17px;
			font-family:"幼圆";
			margin-left:88%;
			border-radius:10px;
			border: 0px;
		}
		.allCommit:hover{
			background-color: #0033CC;
			color:#ffffff;
		}
	</style>
    <script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>
    <script type="text/javascript">	 
    	//初始化页面
		$(function(){
			$(".questions").eq(0).show();
			$(".questiontypeA").eq(0).text("收起");
		}); 
		
		//显示或者隐藏一道大题
		function showQuestions(obj){
			var questions = $(obj).parent().next();
			if(questions.css("display")=='none'){
				questions.show(200);
				$(obj).text("收起")
			}else{
				questions.hide(200);
				$(obj).text("展开")
			}
		}
		
		//显示答案
		function showAnswer(obj,textareaId){
			var textarea = $("#"+textareaId);
			var answer = $("#"+textareaId).val().trim();
			if(answer!=""&&answer!=null){
				if(confirm("是否显示答案进行自评？显示后将无法修改答案！")){
					$(obj).prev().show();
					$("#"+textareaId).attr("disabled","disabled");
					$(obj).attr("disabled","disabled");
				}
			}else{
				textarea.css("background-color","#FEADAD");
				textarea.next().text("  请输入答案！")
				textarea.focus(function(){$(textarea).css("background-color","");textarea.next().text("");});
			}
		}
		
		//对答案排序
		function sortAnswer(obj){
			if($(obj).val().trim()==""){
			 	$(obj).css("background-color","#FEADAD");
			 	$(obj).next().text("  请输入答案！")
				$(obj).focus(function(){$(obj).css("background-color","");});
			}else{
				$(obj).next().text("")
				var answer = $(obj).val().trim().split("");
				var stranswer="";
				for(var i=0 ; i<answer.length; i++){
					answer[i]=answer[i].toUpperCase();
				}
				answer.sort();
				for(var i=0 ; i<answer.length; i++){
					stranswer+=answer[i]
				}
				$(obj).val(stranswer)
			}
		}
		
		//提交答案
		//题型名称(用于组合字符串)、考试卷编码、试卷编码、题型code、每题分值、提交类型（1:客观 ，2:主观）
		function commitAnswer(obj,inputName,test_code,number,qt_code,question_score,commitType){
			if(confirm("是否提交？提交后将无法修改！")){
				$(obj).attr("disabled","disabled");//禁止提交按钮
				var answerStr = "";
				var canCommit = true;
				if(commitType==1){//客观题
					$("input[name='"+inputName+"']").each(function(i){//i从0开始，每次加1
						if($(this).val().trim()==""){
							//alert($(this).val().trim())
							$(obj).attr("disabled","");
							$(this).css("background-color","#FEADAD");
							$(this).next().text("  请输入答案！")
							$(this).focus(function(){$(this).css("background-color","");$(this).next().text("");});
							canCommit=false;
							return false;
						}
						answerStr=answerStr+$(this).attr("self_test")+":"+$(this).val()+",";
						$(this).attr("disabled","disabled");
						$(this).addClass("shortInputed");
					 });
				}else{
					$("textarea[name='"+inputName+"']").each(function(i){//i从0开始，每次加1
						if($(this).val().trim()==""){
							$(obj).attr("disabled","");
							$(this).css("background-color","#FEADAD");
							$(this).next().text("  请输入答案！")
							$(this).focus(function(){$(this).css("background-color","");$(this).next().text("");});
							canCommit=false;
							return false;
						}
						answerStr=answerStr+$(this).attr("self_test")+":"+$(this).val()+":"+$(this).attr("slef_score")+",";
						$(this).attr("disabled","disabled");
					 });
				}
				//alert(answerStr)
				answerStr = answerStr.substring(0, answerStr.length-1);
				if(canCommit){
					$.post("<c:url value='/online/commitanswer'/>",{answerStr:answerStr,test_code:test_code,number:number,qt_code:qt_code,question_score:question_score,commitType:commitType},function(date){
						if(date=="success"){
							$(obj).val("完成提交");
							$(obj).attr("class","commited");
						}
					},"text");
				}else{
					$(obj).removeAttr("disabled");//回复提交按钮
				}
			}
		}
		
		//获得自评分数
		function getSlefScore(obj,id){
			var score = $(obj).val().trim();
			if(score==""){
			 	$(obj).css("background-color","#FEADAD");
			 	$(obj).next().text("  请输入自测评分！")
				$(obj).focus(function(){$(obj).css("background-color","");});
			}else{
				$("#"+id).attr("slef_score",score);
			}
		}
		
		//保存考试记录
		function commitRecord(obj,test_code){
			var commitButton = $(".commit").length;
			if(commitButton>0){
				if(confirm("还有未提交或者没有完成的试题!是否提交？")){
					if(commitButton>1){
						alert("系统检测到您未提交答案的题目太多，禁止提交。建议您提交后再试！");
					}else{
						$(".commit").removeClass("commit").addClass("commited");
						window.location.href="<c:url value='online/commitrecord?test_code="+test_code+"'/>"
					}
				}
			}else{
				if(confirm("确认提交？")){
					$(".commit").removeClass("commit").addClass("commited");
					window.location.href="<c:url value='online/commitrecord?test_code="+test_code+"'/>"
				}
			}
		}
	</script>
  </head>	
	<!--展示试卷开始-->
	<div class="content" align="center"> 	
		<div class="title">
			<span style="margin-left: 14px;font-size: 16px;">${title}</span>
			<a onclick="javascript:history.back(-1)" style="float: right;margin-right: 10px;cursor:pointer;">返&nbsp;回</a>
		</div>	
    	<c:if test="${testCustomizes!=null }">
    		<div class="paper">
    		<c:forEach items="${testCustomizes}" var="tc" varStatus="status">
    			<!--试卷每个大标题-->
    			<div class="questiontype">
	    			<!--隐藏域开始  保存本大题答案-->
	    			<input type="hidden" name="${tc.qt_code}" value="${tc.qt_code}"/>
	    			<!--隐藏域结束-->
    				<span class="typetitle">${tc.qt_name}(每题${tc.question_score}分,共${tc.question_num}小题,共${tc.question_allscore }分)</span>
    				<a class="questiontypeA" onclick="showQuestions(this)" href="javascript:void(0)">展开</a>
    			</div>
    			<!--试卷每个大标题结束-->
    			
    			<!--试卷每个题内容-->
    			
    			<!--客观题开始-->
    			<c:if test="${tc.qt_code==1||tc.qt_code==2||tc.qt_code==3}">
    			<div class="questions">
	    			<c:forEach items="${tc.tQuestions}" var="tc_tq" varStatus="status2">	    			
		    			<!--试题开始-->
	    				<div class="question">
	    					<span>${status2.index+1}、	${tc_tq.introduction}</span>
	    					<br/>
	    					<span>答案：</span>
	    					<c:if test="${tc.qt_code==1}"><!-- 单选 -->
		    					<input class="shortInput" type="text" value="" name="${tc.qt_name }" maxlength="1" onblur="sortAnswer(this)" self_test="${tc_tq.self_test_id}"  onkeyup="value=value.replace(/[^/a-zA-Z]/g,'')"/>
		    					<span style="font-size: 13px;color:red;"></span>
	    					</c:if>
	    					<c:if test="${tc.qt_code==3}"><!-- 判断 -->
		    					<input class="shortInput" type="text" value="" name="${tc.qt_name }" maxlength="1" onblur="sortAnswer(this)" self_test="${tc_tq.self_test_id}" onkeyup="value=value.replace(/[^/tTfF]/g,'')"/>
		    					<span style="font-size: 13px;color:red;"></span>
		    					<span style="font-size: 13px;">&nbsp;&nbsp;(只能输入"T、t、F、f")</span>
	    					</c:if>
	    					<c:if test="${tc.qt_code==2}"><!-- 多选 -->
		    					<input class="shortInput" type="text" value="" name="${tc.qt_name }" onblur="sortAnswer(this)" self_test="${tc_tq.self_test_id}" onkeyup="value=value.replace(/[^/a-zA-Z]/g,'')"/>
		    					<span style="font-size: 13px;color:red;"></span>
	    					</c:if>
	    				</div>
	    				<!--试题结束-->
	    			</c:forEach>
    				<input type="button" value="提交本大题" class="commit" onclick="commitAnswer(this,'${tc.qt_name }','${test_code}','${tc.number }','${tc.qt_code}','${tc.question_score }',1)">
    			</div>
    			</c:if>
    			<!--客观题结束-->
    			
    			<!--主观题开始-->
    			<c:if test="${tc.qt_code!=1&&tc.qt_code!=2&&tc.qt_code!=3}">
    			<div class="questions">
	    			<c:forEach items="${tc.tQuestions}" var="tc_tq" varStatus="status2">	    			
		    			<!--试题开始-->
	    				<div class="question">
	    					<span>${status2.index+1}、	${tc_tq.introduction}</span>
	    					<br/><br/>
	    					<div style="margin-top:60px;float: left;">答&nbsp;&nbsp;&nbsp;&nbsp;案：</div>
	    					<textarea style="resize: none;" rows="5" cols="60" name="${tc.qt_name }" id="${tc_tq.self_test_id}" self_test="${tc_tq.self_test_id}" slef_score="0" placeholder="答......"></textarea>
		    				<span style="font-size: 13px;color:red;"></span><br/>
		    				<div style="display: none;">
			    				<span>参考答案:</span>
			    				<span style="margin-top:20px;width:500px;border: 1px solid #000000;display:-moz-inline-box; display:inline-block; ">
			    					${tc_tq.answer }
			    				</span><br/>
		    					<span>自评得分:</span>
		    					<input type="text" class="shortInput" value="0" onkeyup="value=value.replace(/[^\d+(\.\d)+$]/g,'')" onblur="getSlefScore(this,'${tc_tq.self_test_id}')"/><br/>
		    				</div>
		    				<input type="button" class="" value="输入自评分数" onclick="showAnswer(this,'${tc_tq.self_test_id}')"/>
	    				</div>
	    				<!--试题结束-->
	    			</c:forEach>
			    	<input type="button" value="提交本大题" class="commit" onclick="commitAnswer(this,'${tc.qt_name }','${test_code}','${tc.number }','${tc.qt_code}','${tc.question_score }',2)">
    			</div>
    			</c:if>
    			<!--主观题结束-->
    			<!--试卷每个题内容结束-->
    		</c:forEach>
    			<input type="button" value="保存考试记录" class="allCommit" onclick="commitRecord(this,'${test_code}','commit')">
    		</div>
    	</c:if>
    	<c:if test="${testCustomizes==null }">
    		生成试卷失败
    	</c:if>
	</div>
	<!--展示试卷结束-->