<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
  <head>
    <base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/courseware.css'/>">
	<style type="text/css">
	</style>
    <script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>
    <script type="text/javascript">	 
    //提交
    function commit()
    {
    	if($("#total").val()!='0'&&$.trim($('#page_name').val())!="")
    	{
        	$.ajax({
    	    	   type: "POST",
    	    	   url: "online/customizetest",
    	    	   dataType:"text",
    	    	   data:{'data':data(),'start_chapter':$("#start_chapter").val(),'end_chapter':$("#end_chapter").val(),
    	    		    'curriculum_code':${curriculum.curriculum_code},'page_name':$("#page_name").val(),'total_score':$("#total").val()},
    	    	   success: function(msg)
    	    	   {
    	    	     alert( "Data Saved: " + msg );
    	    	    window.location.href="<c:url value='online/showcurriculum?type=2&id=16'/>";//改变父级iframe的src属性
    	    	   }
    	    	});
    	}
    	else
    	{
    		alert( "总分不能为0或试卷名不能为空! " );
    	}
    		//alert(data());
    }
    //返回
    function goback()
    {
    	window.location.href="<c:url value='online/showcurriculum?type=2&id=16'/>";//改变父级iframe的src属性
    }
    
    function data()
    {   //传回后台的选择题型和单题分数
    	var data="[";
    	<c:forEach items="${questionTypeList}" var="questionType">
    	    data+="{'qt_code':'"+${questionType.qt_code}+"','number':'"+$("select#select${questionType.qt_code}").val()+"','score':'"+$("input#score${questionType.qt_code}").val()+"'}";
    	</c:forEach>
    	data+="]";
    	return data;
    }
    //计算总分
    function totalscore()
    {
    	var score = new Array();
    	$("input.each_score").each(function ()
    	{
    		score.push(parseInt(this.value));
    	});
    	var num=new Array();
    	$("select.each_number").each(function (i)
    	{
    		num.push((this.value));
    	});
    	var total=0;
    	for(var i=0;i<score.length;i++)
    	{
    		var a=parseInt(score[i]);
    		var b=parseInt(num[i]);
    		total=total+(a*b);
    	}
    	$("#total").val(total);
    }
    

    //显示题型的
    $(document).ready(function()
    {
    	<c:forEach items="${questionTypeList}" var="questionType">
    	var total=${questionType.total};
    	for(var i=0;i<=total;i++)
    	{
    		jQuery("select#select${questionType.qt_code}").append("<option value='"+i+"'>"+i+"</option>");
    	}
    	</c:forEach>
    });
    
	</script>
  </head>	
	<!-- 出题开始-->
	<div class="content" align="center"  style="font-family:'幼圆';"> 
		<div class="title"><span style="margin-left: 14px;">${curriculum.curriculum_name}->定制试卷名和章节->试题定制</span><a onclick="goback()" style="float: right;margin-right: 10px;cursor:pointer;">返&nbsp;回</a></div>
		<div>
		  <span>选择章节范围：</span>
		  <select id="start_chapter" name="start_chapter">
		  <option value="${start_chapter}">第${start_chapter }章</option>
		  </select>
		  <span>至</span>
		  <select id="end_chapter" name="end_chapter">
		   <option value="${end_chapter}">第${end_chapter }章</option>
		  </select>
		</div>
		<div>
		<div align="center" style="width:400px; "> 
		<c:if test="${questionTypeList!=null}">
		<div style="margin-top: 10px;">
		  <span>试卷名:</span> <input readonly id="page_name" name="title" value="${page_name}" type="text" maxlength='20'/>
		</div>
		<div id="question">
		<c:forEach items="${questionTypeList}" var="questionType">
		  <div style="margin-top: 10px;">
		    <span>${questionType.qt_name}共</span>
		     <select class="each_number" onchange="totalscore()" id="select${questionType.qt_code}" name="select${questionType.qt_code}">
		     </select>
		     <span>题,每题</span>
		     <input type="text" class="each_score" onchange="totalscore()" onkeyup="value=value.replace(/[^(\d+)$]/ig,'')" value='0' id="score${questionType.qt_code}" style="width:40px;" name="score${questionType.qt_code}" maxlength='2'/>
		     <span>分；</span>
		  </div>
		</c:forEach>
		</div>
		<div style="margin-top: 10px;">
		    <span>分数总值:</span><input id="total" name="total" type="text" disabled readonly style="width:40px;" value="0">
		</div>
		</div>
		<div style="margin-top: 10px;"><input type="button" value="提交" onclick="commit()"></div>
		</c:if>
		</div>
		<c:if test="${questionTypeList==null}">
		<div>暂无该课程的试题</div>
		</c:if>
	</div>
	<!-- 出题结束-->