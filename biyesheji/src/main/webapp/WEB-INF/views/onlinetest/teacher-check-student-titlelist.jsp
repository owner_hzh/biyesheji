<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'teacher-check-student-titlelist.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<style type="text/css">
	   .title{
			height:4%;
			width:95%;
			margin-top: 20px;
			background-color: blue;
			line-height:30px;
			color:#FFFFFF;
			font-size:17px;
			border-radius:15px; 
			text-align: left;
			font-weight: bold;
		}
		.dati{
		  border-radius: 15px;
		  background-color: darkgray;
		  padding: 20;
		  margin: 7px;
		}
		.datititle
		{
		  margin-left: 30px;
          font-size: x-large;
		}
		.xiaoti{
		  border-radius: 5px;
		  background-color: lightgray;
		  padding: 10px;
          margin-top: 5px;
		}
		input,select{
			font-size:18px;
			width:250px;
			margin-top: 30px;
		}
		.commit{
			cursor: pointer;
			background-color: #FFCC66;
			margin-top:20px;
			margin-bottom:20px;
			height:40px;
			font-size:20px;
			font-family:"幼圆";
			border-radius:10px;
			border: 0px;
		}
		.commit:hover{
			background-color: #0033CC;
			color:#ffffff;
		}
	</style>
    <script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>
    <script type="text/javascript">
       //每小题得分后自动得到本大题的总分
       function selectchange(obj,total)
       {
          var f=0;
          //$("#input_"+total).val(Number($("#input_"+total).val())+ Number($(obj).val()));
          $(".xi_"+total).each(function(){
              f+=Number($(this).val());             
          });
          $("#input_"+total).val(f);
       }
       
       //提交数据
       function sstt()
       {
          var ArrayObjs = [];  //定义一个空串 
          var testcode="${test_code}";//考试卷编码
          <c:forEach items="${testCustomizes}" var="curr">
             var ArrayObj = {};       //定义一个空对象，存入数据依次写入空串
             var qtcode="${curr.qt_code}";//题型代码
             var number=$("#number_${curr.qt_code}").val();//试卷编码
             var score=$("#input_${curr.qt_code}").val();//大题得分
             ArrayObj["number"]=number;
             ArrayObj["qt_code"]=qtcode;
             ArrayObj["test_code"]=testcode;
             ArrayObj["teacher_score"]=score;
             ArrayObjs.push(ArrayObj);             
          </c:forEach>
          var JsonString = JSON.stringify(ArrayObjs);  // JSON.stringify() 转换为json串
          //post数据到后台
          $.post("<c:url value='/online/laoshidafen'/>",
                   {
                      JsonString:JsonString
                   },
                   function(data){
						if(data=="true")
						{
							alert("提交成功！");
							goback();
						}else
						{
						   alert("提交失败！");
						}
			   },"text").error(function() { alert("服务器出错！"); });
       }
       
       function goback()
       {
          var curriculum_code="${curriculum_code}";
          var curriculum_name="${curriculum_name}";
          var id="${id}";
          var pp = window.parent.document.getElementById("iframe");//获得父级iframe
		  curriculum_name = encodeURIComponent(encodeURIComponent(curriculum_name));
		  $(pp).attr("src" , "<c:url value='/online/choosestudent?curriculum_code="+curriculum_code+"&curriculum_name="+curriculum_name+"&id="+id+"'/>");//改变父级iframe的src属性
       }
    </script>
  </head>
  
  <body>
    <center><div class="title"><span style="margin-left: 14px;">${student_name}的试卷</span><a onclick="goback()" style="float: right;margin-right: 10px;cursor:pointer;">返&nbsp;回</a></div></center>
    <c:if test="${testCustomizes!=null && fn:length(testCustomizes) > 0}">
		<c:forEach items="${testCustomizes}" var="curr">
		    <!-- 试卷编码 隐藏项  begin-->
		    <input id="number_${curr.qt_code}" type="hidden" value="${curr.number}"/>
		    <!-- 试卷编码 隐藏项  end-->
		    
		    <p class="datititle">${curr.qt_name}(每题${curr.question_score}分 共${curr.question_num}题  总分${curr.question_allscore}分)</p>
        <div class="dati">
           	<c:forEach items="${curr.selfTests}" var="selftest" varStatus="status">
           	<div class="xiaoti">
           	   <p>${status.index+1}、    ${selftest.introduction}</p>
           	   <p style="margin-left: 30px;">参考答案：${selftest.answer}</p>
           	   <p style="margin-left: 30px;">${student_name}的答案：${selftest.self_answer}</p>
           	   
           	   <!-- 大题中有多道小题的时候   begin -->
           	   <c:if test="${curr.question_num>1 || curr.question_num>'1'}">
           	      <p style="margin-left: 30px;">本小题得分：
           	      <select class="xi_${curr.qt_code}" onchange="selectchange(this,${curr.qt_code})">
           	         <c:forEach begin="0" end="${curr.question_score}" step="1" varStatus="status2">
           	            <option value="${status2.index}" >${status2.index}分</option>
           	         </c:forEach>
           	      </select></p>
           	   </c:if>
           	   <!-- 大题中有多道小题的时候   end --> 
           	</div>          	
           	</c:forEach>
           	
           	
           	<!-- 大题中有多道小题的时候   begin -->
           	<c:if test="${curr.question_num>1 || curr.question_num>'1'}">
           	    <p>本大题得分：<input type="text" id="input_${curr.qt_code}" value="0"/></p>
           	</c:if>
           	<!-- 大题中有多道小题的时候   end -->
           	
           	<!-- 大题中只有一道小题的时候   begin-->
           	<c:if test="${curr.question_num==1 || curr.question_num=='1'}">
           	      <p>本大题得分：
           	      <select id="input_${curr.qt_code}">
           	         <c:forEach begin="0" end="${curr.question_score}" step="1" varStatus="status3">
           	            <option value="${status3.index}" >${status3.index}分</option>
           	         </c:forEach>
           	      </select></p>
           	</c:if>
           	<!-- 大题中只有一道小题的时候   end-->
           	
         </div>
           	<hr>
		</c:forEach>
		<center><input type="button" class="commit" onclick="sstt()" value="提交" /></center>
    </c:if>
    
    <c:if test="${testCustomizes==null || fn:length(testCustomizes) <= 0}">
       <center><p>暂无考试题目</p></center>
    </c:if>
  </body>
</html>
