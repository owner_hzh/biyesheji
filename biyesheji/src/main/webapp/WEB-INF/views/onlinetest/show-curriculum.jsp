<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
  <head>
    <base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/courseware.css'/>">
	<style type="text/css">
	</style>
    <script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>
    <script type="text/javascript">	 
        //教师出题
		function setQuestion(curriculum_code,curriculum_name,id){
			var pp = window.parent.document.getElementById("iframe");//获得父级iframe
			curriculum_name = encodeURIComponent(encodeURIComponent(curriculum_name));
			$(pp).attr("src" , "<c:url value='/online/setquestion?curriculum_code="+curriculum_code+"&curriculum_name="+curriculum_name+"&id="+id+"'/>");//改变父级iframe的src属性
		}
		//定制试卷
		function custompaper(curriculum_code,curriculum_name){
			var pp = window.parent.document.getElementById("iframe");//获得父级iframe
			curriculum_name = encodeURIComponent(encodeURIComponent(curriculum_name));
			$(pp).attr("src" , "<c:url value='/online/custom_papernameandchapter?curriculum_code="+curriculum_code+"&curriculum_name="+curriculum_name+"'/>");//改变父级iframe的src属性
		}
	</script>
  </head>	
	<!-- 课程内容开始-->
	<div class="content" align="center"> 
	
	
    	<c:if test="${curriculums!=null }">
    		<table class="table">
    			<tr>
    				<th>课程名称</th>
    				<th>操作</th>
    			</tr>
    			<c:forEach items="${curriculums}" var="curr">
	    			<tr>
	    				<td>${curr.curriculum_name}</td>
	    				<c:if test="${type=='1'}">
		                    <td><a onclick="setQuestion(${curr.curriculum_code },'${curr.curriculum_name }',${id})">出题</a></td>		
	                    </c:if>
	    				<c:if test="${type=='2'}">
	    				     <td><a onclick="custompaper(${curr.curriculum_code },'${curr.curriculum_name }')">定制试卷</a></td>
	                    </c:if>
	    			</tr>
    			</c:forEach>
    		</table>
    	</c:if>
	</div>
	<!-- 课程内容结束-->