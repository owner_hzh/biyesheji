<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
  <head>
    <base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/courseware.css'/>">
	<link rel='stylesheet' href="<c:url value="/resources/css/pagination.css"/>" />
	<link rel='stylesheet' href="<c:url value="/resources/css/bootstrap.min.css"/>" />
	<style type="text/css">
	</style>
    <script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>
    <script type="text/javascript">	 
		function showTest(curriculum_code,curriculum_name,type){
			var pp = window.parent.document.getElementById("iframe");//获得父级iframe
			curriculum_name = encodeURIComponent(encodeURIComponent(curriculum_name));
			if(type==1){
				$(pp).attr("src" , "<c:url value='/online/showtest?curriculum_code="+curriculum_code+"&curriculum_name="+curriculum_name+"'/>");//改变父级iframe的src属性
			}else{
				$(pp).attr("src" , "<c:url value='/online/showrecord?curriculum_code="+curriculum_code+"&curriculum_name="+curriculum_name+"'/>");//改变父级iframe的src属性
			}
		}
	</script>
  </head>	
	<!-- 课程内容开始-->
	<div class="content" align="center"> 	
    	<c:if test="${pagecondition.totalCount!=0}">
    		<table class="table">
    			<tr>
    				<th>课程代码</th>
    				<th>课程名称</th>
    				<th>操作</th>
    			</tr>
    			<c:forEach items="${pagecondition.dataList }" var="curr">
	    			<tr>
	    				<td>${curr.curriculum_code}</td>
	    				<td>${curr.curriculum_name}</td>
	    				<c:if test="${type==1 }">
		                	<td><a onclick="showTest(${curr.curriculum_code },'${curr.curriculum_name }','${type }')">查看该课程试卷</a></td>
		                </c:if>		
	    				<c:if test="${type==2 }">
		                	<td><a onclick="showTest(${curr.curriculum_code },'${curr.curriculum_name }','${type }')">查看该课程考试情况</a></td>
		                </c:if>		
	    			</tr>
    			</c:forEach>
    		</table>
    		${pagecondition.show }
    	</c:if>
    	<c:if test="${pagecondition.totalCount==0}">
    		<div style = "margin-top: 10px;font-size: 16px;font-weight: lighter;">暂无任何课程信息</div>
    	</c:if>
	</div>
	<!-- 课程内容结束-->