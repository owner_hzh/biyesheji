<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
  <head>
    <base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/courseware.css'/>">
	<style type="text/css">
	</style>
    <script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>
    <script type="text/javascript">	 
		function setQuestion(curriculum_code,curriculum_name,id){
			var pp = window.parent.document.getElementById("iframe");//获得父级iframe
			curriculum_name = encodeURIComponent(encodeURIComponent(curriculum_name));
			$(pp).attr("src" , "<c:url value='/online/setquestion?curriculum_code="+curriculum_code+"&curriculum_name="+curriculum_name+"&id="+id+"'/>");//改变父级iframe的src属性
		}
		
		function showpaper(title,number,curriculum_code,start,end){
			var pp = window.parent.document.getElementById("iframe");//获得父级iframe
			title = encodeURIComponent(encodeURIComponent(title));
			$(pp).attr("src" , "<c:url value='/online/showpaper?title="+title+"&curriculum_code="+curriculum_code+"&number="+number+"&start="+start+"&end="+end+"'/>");//改变父级iframe的src属性
		}
	</script>
  </head>	
	<!-- 该课程的试卷开始-->
	<div class="content" align="center"> 	
		<div class="title">
			<span style="margin-left: 14px;font-size: 16px;">${curriculum_name}&nbsp;&nbsp;试卷集</span>
			<a onclick="javascript:history.back(-1)" style="float: right;margin-right: 10px;cursor:pointer;">返&nbsp;回</a>
		</div>	
    	<c:if test="${testCodes!=null }">
    		<table class="table">
    			<tr>
    				<th>试卷编码</th>
    				<th>试卷标题</th>
    				<th>出题日期</th>
    				<th>出题教师</th>
    				<th>考试范围</th>
    				<th>操作</th>
    			</tr>
    			<c:forEach items="${testCodes}" var="tc">
	    			<tr>
	    				<td>${tc.number}</td>
	    				<c:choose>
							<c:when test="${tc.title.length() >7}">
								<td title="${tc.title}">${tc.title.substring(0,7)}..</td>
							</c:when>
							<c:otherwise>
								<td>${tc.title}</td>
							</c:otherwise>
						</c:choose>
	    				<td>${tc.create_date}</td>
	    				<td>${tc.user_name}</td>
	    				<td>${tc.start}章到${tc.end}章</td>
	    				<td><a href="javascript:void(0);" onclick="showpaper('${tc.title}','${tc.number}','${tc.curriculum_code}','${tc.start}','${tc.end}')">进入考试</a></td>
	    			</tr>
    			</c:forEach>
    		</table>
    	</c:if>
    	<c:if test="${testCodes==null }">
    		<div style="margin-top:20px;">暂无试卷信息</div>
    	</c:if>
	</div>
	<!-- 该课程的试卷结束-->