<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    <title>My JSP 'content.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<style type="text/css">
	   html body{ height:100%; width:100%;overflow:hidden;}
	   .menubox{width:15%; height: 100%; 
	   		float: left; background-color: #e8e8e8; overflow: auto;
	   		border-top-left-radius:15px; 
	   		border-bottom-left-radius:15px; 
	   		color: #AA531E;
	   } 
	   .activetwo{color: blue;}
	   .menutwo{ cursor: pointer;
	   			  border-bottom: 1px dashed gray;
	   			  width:100%;
	   			  font-size: 19px;
	   			  font-family: '幼圆';
	   			  font-weight: normal;
	   			  line-height: 40px;
	   			  text-align: center;
	   	}
	   .menutwo:HOVER{color: blue;}/* 909EDA  F2F2F2 cccccc*/
	   .menuthreebox{
	   				display: none;
	   				background-color: #B5B5B5;
	   				}
	   .menuthree{color:#ffffff;
	   			  font-size: 16px;
	   			  border-top: 1px solid #ffffff;
	   			  font-weight: lighter;
	   			  }
	   .menuthree:HOVER{background-color:#CCCCCC;color:#0066CC;}
	   .threeaction{background-color:#CCCCCC;color:#0066CC;}
	   .content { width:85%; height:100%; float: right; background-color: #CCCCCC;
	   	   			  border-top-right-radius:15px; 
	   			  border-bottom-right-radius:15px;}	  
	   .iframe_content{ width: 100%; height: 100%; overflow: auto;border-top-right-radius:15px; 
	   			        border-bottom-right-radius:15px;}
	</style>
	<link rel="stylesheet" href="<c:url value='resources/css/jquery.treeview.css'/>" />
    <script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>
	<script src=' <c:url value='/resources/js/jquery.cookie.js'/>' type="text/javascript"></script>
	<script src='<c:url value='/resources/js/jquery.treeview.js'/>' type="text/javascript"></script>
  	<script type="text/javascript">	 
		$(function(){
			$("#menutwobox").children().eq(0).addClass("activetwo");//初始化第一个菜单选项被选中
			$("#menutwobox").children().eq(0).children(".menuthreebox").show(800);
			$("#menutwobox").children().eq(0).children(".menuthreebox").children().eq(0).addClass("threeaction");
			var action = $("#menutwobox").children().eq(0).children(".menuthreebox").children().eq(0).attr("urldate");
			//alert(action)
			$("#iframe").attr("src" , action);
		}); 
		function getContent(obj){
			var prior = $(".activetwo").children().eq(0).text();
			$(".activetwo").children(".menuthreebox").hide(800);
			$(".activetwo").removeClass("activetwo");
			if(prior!=$(obj).text()){
				$(obj).parent().addClass("activetwo");
				$(obj).parent().children(".menuthreebox").show(800);
			}
		}
		function gotochoose(obj){
			$(".threeaction").removeClass("threeaction");
			$(obj).addClass("threeaction");
			//alert($(obj).attr("urldate"))
			var action = $(obj).attr("urldate");
			$("#iframe").attr("src" , action);
		}
		
		//教师选择的返回事件
		function teachergoback(id)
		{
		   $("#te_"+id).click();
		}
	</script>
  </head>
  
  <body>
    <div id="menutwobox" class="menubox">
	<c:forEach items="${menulist}" var="menutwo">
   		<div class="menutwo">
   			<span onclick="getContent(this)">${menutwo.menu_level_two_name}</span>
   			<div class="menuthreebox">
   			<c:forEach items="${menutwo.menuLevelThreeList}" var="menuthree">
	   			<div class="menuthree" id="te_${menuthree.menu_level_three_id}" onclick="gotochoose(this)" urldate="${menuthree.mapping_page}&id=${menuthree.menu_level_three_id}">
	   				<span>${menuthree.menu_level_three_name }</span>
	   			</div>
   			</c:forEach>
   			</div>
   		</div>
	</c:forEach>
	</div>
	<div class="content"> 
	    <iframe id="iframe" class="iframe_content" marginheight="0" marginwidth="0" hspace="0" vspace="0" frameborder="0"></iframe> 
	</div>
  </body>
</html>
