<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
  <head>
    <base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/courseware.css'/>">
	<style type="text/css">
	</style>
    <script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>
    <script type="text/javascript">	 
		function setQuestion(curriculum_code,curriculum_name,id){
			var pp = window.parent.document.getElementById("iframe");//获得父级iframe
			curriculum_name = encodeURIComponent(encodeURIComponent(curriculum_name));
			$(pp).attr("src" , "<c:url value='/online/setquestion?curriculum_code="+curriculum_code+"&curriculum_name="+curriculum_name+"&id="+id+"'/>");//改变父级iframe的src属性
		}
		
		function custompaper(curriculum_code){
			var pp = window.parent.document.getElementById("iframe");//获得父级iframe
			//curriculum_name = encodeURIComponent(encodeURIComponent(curriculum_name));
			$(pp).attr("src" , "<c:url value='/online/custompaper?curriculum_code="+curriculum_code+"'/>");//改变父级iframe的src属性
		}
	</script>
  </head>	
	<!-- 课程内容开始-->
	<div class="content" align="center"> 
    	<c:if test="${testCodes!=null }">
    		<table class="table">
    			<tr>
    				<th>课程</th>
    				<th>考试卷编码</th>
    				<th>试卷编码</th>
    				<th>试卷名</th>
    				<th>考试时间</th>
    				<th>自评总成绩</th>
    				<th>审核人</th>
    				<th>审核总成绩</th>
    				<th>审核状态</th>
    				<th>审核时间</th>
    				<%--<th>操作</th>
    			--%></tr>
    			<c:forEach items="${testCodes}" var="tc">
    				<c:forEach items="${tc.testRecords}" var="tc_tr">
		    			<tr>
		    				<c:choose>
								<c:when test="${curriculum_name.length() >5}">
									<td title="${curriculum_name}">${curriculum_name.substring(0,5)}..</td>
								</c:when>
								<c:otherwise>
									<td>${curriculum_name}</td>
								</c:otherwise>
							</c:choose>
		    				<td>${tc_tr.test_code}</td>
		    				<td>${tc_tr.number}</td>
		    				<c:choose>
								<c:when test="${tc.title.length() >5}">
									<td title="${tc.title}">${tc.title.substring(0,5)}..</td>
								</c:when>
								<c:otherwise>
									<td>${tc.title}</td>
								</c:otherwise>
							</c:choose>
		    				<td><fmt:formatDate value="${tc_tr.test_date}" pattern="yyyy-MM-dd HH:mm"/></td>
		    				<td>${tc_tr.selfallscore}</td>
		    				<c:if test="${tc_tr.is_check==0}">
		    					<td>未审核</td>
		    					<td>未审核</td>
		    					<td>未审核</td>
		    					<td>未审核</td>
		                    </c:if>
		    				<c:if test="${tc_tr.is_check!=0}">
		    					<td>${tc_tr.tea_user_name}</td>
			    				<td>${tc_tr.teaallscore}</td>
			    				<td>已审核</td>
			    				<%-- <td>${tc_tr.check_time}</td> --%>
			    				<td><fmt:formatDate value="${tc_tr.check_time}" pattern="yyyy-MM-dd HH:mm"/></td>
		                    </c:if>
		    			</tr>
	    			</c:forEach>
    			</c:forEach>
    		</table>
    	</c:if>
    	<c:if test="${testCodes==null }">
    		<div style="margin-top:20px;">暂无考试信息</div>
    	</c:if>
	</div>
	<!-- 课程内容结束-->