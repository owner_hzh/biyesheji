<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
  <head>
    <base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/courseware.css'/>">

	   <style type="text/css">
		.title{
			height:4%;
			width:95%;
			margin-top: 20px;
			background-color: blue;
			line-height:30px;
			color:#FFFFFF;
			font-size:17px;
			border-radius:15px; 
			text-align: left;
			font-weight: bold;
		}
		input,select{
			font-size:18px;
			width:250px;
			margin-top: 30px;
		}
		.commit{
			cursor: pointer;
			background-color: #FFCC66;
			margin-top:20px;
			margin-left:33%;
			height:40px;
			font-size:20px;
			font-family:"幼圆";
			border-radius:10px;
			border: 0px;
		}
		.commit:hover{
			background-color: #0033CC;
			color:#ffffff;
		}
		.spann{
		   margin-left: 20px;
		}
		.editTextarea{
       border:1px solid #999;
       font-size:12px;
       font-family:"幼圆";
       padding:1px;
       height:140px;
       overflow:auto;
       width:70%;
       text-align:left;
       padding:5px;
       }
	</style>

    <script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>
	<!-- 下面这两个js文件位置不能交换 -->
    <script type="text/javascript" src="<c:url value='resources/ckeditor/ckeditor.js'/>"></script> 
    <script type="text/javascript" src="<c:url value='resources/ckeditor/adapters/jquery.js'/>"></script> 
    <script type="text/javascript">	 
        var editor;
        $(document).ready(function(){
		    $("#editor1").ckeditor({
				toolbar: [
					[ 'Bold', 'Italic', '-', 'NumberedList', 'BulletedList','Font'],
					[ 'FontSize', 'TextColor', 'BGColor' ,'-','Image']
				],
				height:300
			});	
			
			$("#commit").click(function(){
			   var questionTypes=$("#questionTypes").val();
			   var chapter=$("#chapter").val();
			   var answer=null;
			   if(questionTypes==1||questionTypes==2)
			   {
			      answer=$("#answerinput").val();
			   }else if(questionTypes==3)
			   {
			      answer=$("input[name='answerradio']:checked").val();
			   }else
			   {
			      answer=$("#answeredit").val();
			   }
			   
			   var editor = CKEDITOR.instances.editor1;
			   //var text=editor.document.getBody().getText(); //取得纯文本
               var html=editor.document.getBody().getHtml(); //取得html文本
               
               $.post("<c:url value='/online/timucommit'/>",
                   {
                      qt_code:questionTypes,
                      curriculum_code:'${curriculum_code}',
                      introduction:html,
                      chapter:chapter,
                      answer:answer
                   },
                   function(date){
						if(date=="true")
						{
							alert("提交成功！");
							var editor = CKEDITOR.instances.editor1;
							editor.setData("");
							$("#answerinput").val("");
							$("#answeredit").val("");
						}else
						{
						   alert("提交失败！");
						}
			   },"text").error(function() { alert("服务器出错！"); });
               
			}); 
			
			//题型选择事件
			$("#questionTypes").change(function()
				{
				   var questionTypes=$("#questionTypes").val();
				   if(questionTypes==1||questionTypes==2)	
				   {
				      $("#content").empty();
				      var ju='<span class="spann">题目的参考答案:</span><input type="text" id="answerinput" name="answerinput" placeholder="题目的答案  例:ABC"/>';
				      $("#content").append(ju);
				      
				   }else if(questionTypes==3)
				   {
				      $("#content").empty();
				      var ju='<div id="judge"><span class="spann">题目的参考答案:</span><input type="radio" style="width: 20px;" name="answerradio" id="answerradio" value="1"/>对 <input type="radio" style="width: 20px;" name="answerradio" id="answerradio" value="0"/>错</div>'
				      $("#content").append(ju);
				   }else
				   {
				      $("#content").empty();
				      var ju='<br/><span class="spann">题目的参考答案:</span><br/><textarea style="margin-left: 20px;" name="answeredit" id="answeredit" class="editTextarea"></textarea>';
				      $("#content").append(ju);
				   }			   
				
				});   			
		});
        
		function showCourseWare(curriculum_code,curriculum_name){
			var pp = window.parent.document.getElementById("iframe");//获得父级iframe
			curriculum_name = encodeURIComponent(encodeURIComponent(curriculum_name));
			$(pp).attr("src" , "<c:url value='/courseware/show?curriculum_code="+curriculum_code+"&curriculum_name="+curriculum_name+"'/>");//改变父级iframe的src属性
		}
		//返回按钮
		function goback()
		{ 
		   parent.teachergoback("${id}");
		}
		
	</script>
  </head>	
    
	<!-- 出题开始-->
	<div class="content"> 
	<center><div class="title"><span style="margin-left: 14px;">${curriculum_name}课程出题</span><a onclick="goback()" style="float: right;margin-right: 10px;cursor:pointer;">返&nbsp;回</a></div></center>
		<div><span class="spann">选择题型:</span> 
		   <select name="questionTypes" id="questionTypes" >
				<c:forEach items="${questionTypes}" var="qt">
				     <option value="${qt.qt_code}" >${qt.qt_name}</option>
				</c:forEach>
			</select>
		</div>
		<div><span class="spann">所属章节:</span> 
		   <select name="chapter" id="chapter" >
				<c:forEach items="${chapters}" var="chapter">
				     <option value="${chapter}" >第${chapter}章</option>
				</c:forEach>
			</select>
		</div>
		<div style="margin-top: 30px;"><span class="spann">题干和答案选项:</span>
		 <textarea cols="100" id="editor1" name="editor1" rows="10" ></textarea> 
		</div>
		<div id="content"><span class="spann">题目的参考答案:</span>
		    <input type="text" id="answerinput" name="answerinput" placeholder="题目的答案  例:ABC"/>  		 
		</div>
		<div>
		 <input type="button" class="commit" id="commit" name="commit" value="提交"/> 
		</div>
	</div>
	<!-- 出题结束-->