<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML>
  <head>
    <base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/courseware.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/js/uploadify/uploadify.css'/>">
	<style type="text/css">
		.title{
			height:4%;
			width:95%;
			margin-top: 20px;
			background-color: blue;
			line-height:30px;
			color:#FFFFFF;
			font-size:17px;
			border-radius:15px; 
			text-align: left;
			font-weight: bold;
		}
	</style>
	
	<script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>  
    <script type="text/javascript" src="<c:url value='resources/js/uploadify/jquery.uploadify.min.js'/>"></script>
    <script type="text/javascript">	 
    	//下载教学录像
		function downAssignment(code,url,name,obj){
			$.post("<c:url value='/teachingvideotape/downTeachingvideotape'/>",{teachingvideotape_code:code},function(date){
				if(date=="success")
				{
					$(obj).attr("href",url).on("click",function(){
					});
				}
			});

		}
    	//在线播放
    	function videoPlay(curriculum_code,curriculum_name,teachingvideotape_code,teachingvideotape_filename)
    	{
    		var pp = window.parent.document.getElementById("iframe");//获得父级iframe
    		curriculum_name = encodeURIComponent(encodeURIComponent(curriculum_name));
    		teachingvideotape_filename = encodeURIComponent(encodeURIComponent(teachingvideotape_filename));
    		window.location.href="<c:url value='/teachingvideotape/videoPlay'/>"
					           +"?curriculum_code="+curriculum_code+"&curriculum_name="
					           +curriculum_name+"&teachingvideotape_code="+teachingvideotape_code
					           +"&teachingvideotape_filename="+teachingvideotape_filename;//改变父级iframe的src属性
    	}

		
		//返回专业页面
		function goback(){
			var pp = window.parent.document.getElementById("menutwo");//获得父级iframe
			$(pp).children(".activetwo").click();
		}
		<c:if test="${loginuserRole==3||loginuserRole==6 }">
		//初始化
		$(function() {
			//上传教学录像文件
			 $("#upload").uploadify({
				 'fileSizeLimit' : '2GB',
				 'height'         : 30,//表示按钮的高度，默认30PX。若要改为50PX，如下设置：'height' : 50，  
		         'width'          : 120,//表示按钮的宽度  
			     'swf'            : "<c:url value='/resources/js/uploadify/uploadify.swf' />",  
			     'uploader'       : "<c:url value='/teachingvideotape/uploadTeachingvideotape/?dir=media&curriculum_code="+"${curriculum_code}"+" '/>",
			     'cancelImg'      : "<c:url value='/resources/js/uploadify/uploadify-cancel.png' />",
			     'buttonText'     :'选择上传录像' ,
			     'formData'       : {'folder' : 'media','curriculum_code':"${curriculum_code}"},
			     'auto'           : true,  
			     'multi'          : false, 
			     'wmode'          : 'transparent', 
			     'fileObjName'    : 'file',//文件对象名称。用于在服务器端获取文件。
			     'simUploadLimit' : 1,  
			     'fileTypeExts'   : '*.mp4;',  
			     'fileTypeDesc'   : '文件(*.mp4;)', 
			     'onUploadSuccess': function(file, data) {
			    	 //alert(data)
			     	var message=eval("("+data+")");
			         if(message.error==0){//上传
			        	 location.reload();
			         }else{
			        	 alert(message.message);
			         }
			     }  
			 }); 
			
			
		});
		//删除教学录像
		function deleteAssignment(teachingvideotape_code){
			if(confirm("是否删除？")){
				$.get("<c:url value='/teachingvideotape/delTeachingvideotape'/>",{teachingvideotape_code:teachingvideotape_code},function(date){
					if(date=="success"){
						location.reload();
					}
				});
			}
		}
		</c:if>
  	</script>
  </head>	
	<!-- 作业内容开始-->
	<div class="content" align="center">
		<div class="title"><span style="margin-left: 14px;">${curriculum_name }</span><a onclick="goback()" style="float: right;margin-right: 10px;cursor:pointer;">返&nbsp;回</a></div>
    	<c:if test="${TeachingvideotapeList!=null }">
    		<table class="table" style="margin-top: 10px;">
    			<tr>
    				<th>教学录像名称</th>
    				<th>下载次数</th>
    				<th>上传时间</th>
    				<th>操作</th>
    			</tr>
    			<c:forEach items="${TeachingvideotapeList}" var="teachingvideotape">
	    			<tr>
	    				<td style="text-align: middle;">${teachingvideotape.filename}</td>
	    				<td style="text-align: middle;">${teachingvideotape.down_count}</td>
	    				<td style="text-align: middle;">${teachingvideotape.uploaddate}</td>
	    				<td>
	    					<a onclick="downAssignment('${teachingvideotape.teachingvideotape_code}','${teachingvideotape.location }','${teachingvideotape.filename}')" href="${teachingvideotape.location }">下载</a>
	    					<a onclick="videoPlay('${curriculum_code}','${curriculum_name}','${teachingvideotape.teachingvideotape_code}','${teachingvideotape.filename}')">在线播放</a>  
	    					<c:if test="${loginuserRole==3||loginuserRole==6 }"><a onclick="deleteAssignment(${teachingvideotape.teachingvideotape_code})">删除</a></c:if>
	    				</td>
	    			</tr>
    			</c:forEach>
    		</table>
    	</c:if>
    	<c:if test="${TeachingvideotapeList==null }">
    		<div style = "margin-top: 10px;font-size: 16px;font-weight: lighter;">暂无任何教学录像</div>
    	</c:if>
    	<c:if test="${loginuserRole==3||loginuserRole==6 }"><div style="float: right;width:122px;margin-top: 10px;margin-right: 50px;"><input type="file" id="upload" width="120px" height="30px"/></div></c:if>
	</div>
	<!-- 课件内容结束-->