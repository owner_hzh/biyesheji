<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
  <head>
    <base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/courseware.css'/>">
	<style type="text/css">
		.title{
			height:4%;
			width:95%;
			margin-top: 20px;
			background-color: blue;
			line-height:30px;
			color:#FFFFFF;
			font-size:17px;
			border-radius:15px; 
			text-align: left;
			font-weight: bold;
		}
	</style>
    <script src="<c:url value='/resources/js/johndyer-mediaelement/build/jquery.js'/>"></script>	
	<script src="<c:url value='/resources/js/johndyer-mediaelement/build/mediaelement.js'/>"></script>
	<link rel="stylesheet" href="<c:url value='/resources/js/johndyer-mediaelement/build/mediaelementplayer.min.css'/>" />
    <script type="text/javascript">	 
	//返回录像页面
	function goback()
	{
		//var pp = window.parent.document.getElementById("iframe");//获得父级iframe
		var curriculum_name = encodeURIComponent(encodeURIComponent("${curriculum_name}"));
		window.location.href="<c:url value='/teachingvideotape/showTeachingvideotape?curriculum_code=${curriculum_code}&curriculum_name="+curriculum_name+"'/>";//改变父级iframe的src属性
	}
	</script>
  </head>	
  <body>
    <div class="content" align="center">
        <div class="title"><span style="margin-left: 14px;">${curriculum_name}-${teachingvideotape_filename}</span><a onclick="goback()" style="float: right;margin-right: 10px;cursor:pointer;">返&nbsp;回</a></div>
        <div  style="float:middle;margin-top: 30px;cursor:pointer;">
            <video width="800" height="600" id="player1" src="<c:url value='${teachingvideotape_location}'/>" type="video/mp4" controls="controls"></video>
        </div>
    </div>
  </body>