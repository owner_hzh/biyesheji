<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'show-practicing.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/courseware.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/js/uploadify/uploadify.css'/>">
	<style type="text/css">
		.title{
			height:4%;
			width:95%;
			margin-top: 20px;
			background-color: blue;
			line-height:30px;
			color:#FFFFFF;
			font-size:17px;
			border-radius:15px; 
			text-align: left;
			font-weight: bold;
		}
		.pagechange:hover
		{
		    color:blue;
			text-decoration: underline;
		}
		.pagechange
		{
		    font-size: 20px;
		    font-weight:lighter;
		    font-family:'幼圆';
		    cursor:pointer;
		}
	</style>
	
	<script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>  
    <script type="text/javascript" src="<c:url value='resources/js/uploadify/jquery.uploadify.min.js'/>"></script>
    <script type="text/javascript">
	//返回上级页面
	function goback()
	{
		window.location.href="<c:url value='practicing/basicpracticing/search'/>";//改变父级iframe的src属性
	}
	//下载文档
	function downDocument(code,url,obj)
	{
		$.post("<c:url value='/practicingDocument/downPracticingDocument'/>",{ppd_code:code},function(date)
		{
			if(date=="success")
			{
				$(obj).attr("href",url).on("click",function(){
				});
			}
		});
	}
	<c:if test="${loginuserRole==3||loginuserRole==6 }">
	//删除文档
	function deleteDocument(ppd_code,ppd_link)
	{
		$.get("<c:url value='/practicingDocument/deletePracticingDocument'/>",{ppd_code:ppd_code,ppd_link:ppd_link},function(date)
				{
					if(date=="success")
					{
						 location.reload();
					}
					else
					{
						alert(date);
					}
				});
	}
	
	//上传控件初始化
	$(function() {
		//上传文档
		 $("#upload").uploadify({
			 'height'         : 30,//表示按钮的高度，默认30PX。若要改为50PX，如下设置：'height' : 50，  
	         'width'          : 120,//表示按钮的宽度  
		     'swf'            : "<c:url value='/resources/js/uploadify/uploadify.swf' />",  
		     'uploader'       : "<c:url value='/practicingDocument/uploadPracticingDocument?dir=file&ppcode="+"${ppcode}"+"&ppnumber="+"${ppnumber}"+"'/>",
		     'cancelImg'      : "<c:url value='/resources/js/uploadify/uploadify-cancel.png' />",
		     'buttonText'     :'选择上传文档' ,
		     'formData'       : {'folder' : 'file'},
		     'auto'           : true,  
		     'multi'          : false, 
		     'wmode'          : 'transparent', 
		     'fileObjName'    : 'file',//文件对象名称。用于在服务器端获取文件。
		     'simUploadLimit' : 1,  
		     'fileExt'        : '*.zip,*.rar,*.doc,*.docx,*.xls,*.xlsx,*.ppt,*.txt;',  
		     'fileDesc'       : '文件(*.zip,*.rar,*.doc,*.docx,*.xls,*.xlsx,*.ppt,*.txt;)', 
		     'onUploadSuccess': function(file, data) {
		    	 //alert(data)
		     	var message=eval("("+data+")");
		         if(message.error==0){//上传
		        	 location.reload();
		         }else{
		        	 alert(message.message);
		         }
		     }  
		 }); 
	});
	</c:if>
    </script>
</head>
<body>
    <div class="content" align="center">
        <div class="title"><span style="margin-left: 14px;">${pp_name}-文档</span><a onclick="goback()" style="float: right;margin-right: 10px;cursor:pointer;">返&nbsp;回</a></div>
        <c:if test="${practicalProjectDocumentList!=null }">
        <table class="table" style="margin-top: 10px;">
           <tr>
    			<th>序号</th>
    			<th>文档名称</th>
    			<th>上传时间</th>
    			<th>下载次数</th>
    			<th>操作</th>
    		</tr>
    		<c:set value="1" var="num" scope="page"/>
    	    <c:forEach items="${practicalProjectDocumentList}" var="practicalProjectDocument">
    		<tr>
	    		<td style="text-align: middle;">${num}</td>
	    		<c:set value="${num+1}" var="num" scope="page"/>
	    		<td style="text-align: middle;">${practicalProjectDocument.ppd_name}</td>
	    		<td style="text-align: middle;">${practicalProjectDocument.upload_time}</td>
	    		<td style="text-align: middle;">${practicalProjectDocument.down_count}</td>
	    		<td>
	    			<a onclick="downDocument('${practicalProjectDocument.ppd_code}','${practicalProjectDocument.ppd_link}')" href="${practicalProjectDocument.ppd_link}">下载</a>
	    			<c:if test="${loginuserRole==3||loginuserRole==6 }"><a onclick="deleteDocument('${practicalProjectDocument.ppd_code}','${practicalProjectDocument.ppd_link}')">删除</a></c:if>
	    		</td>
	    	</tr>
	    	</c:forEach>
        </table>
        </c:if>
        <c:if test="${practicalProjectDocumentList==null }">
    		<div style = "margin-top: 10px;font-size: 16px;font-weight: lighter;">暂无任何文档</div>
    	</c:if>
    	<c:if test="${loginuserRole==3||loginuserRole==6 }"><div style="float: right;width:122px;margin-top: 10px;margin-right: 50px;"><input type="file" id="upload" width="120px" height="30px"/></div></c:if>
    </div>
</body>
</html>