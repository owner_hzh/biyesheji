<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'review.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/courseware.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/general.css'/>">
    <script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>
    <script type="text/javascript" src="<c:url value='resources/js/popup.js'/>"></script>
    <script type="text/javascript">
       function sseedetail(xt)
       {
          //显示弹出框
	       centerPopup();
	       loadPopup();
	       $("#contactArea").empty();
	       $("#contactArea").append(xt);
       }
    </script>
  </head>
  
  <body>
    <!-- 要提交答案的题目列表 -->
	<div class="content" align="center">
    	<c:if test="${sList!=null && fn:length(sList) > 0}">
    		<table class="table" style="margin-top: 10px;">
    		    <tr>
    				<th>题目类型</th>
    				<th>老师名称</th>
    				<th>开课学期</th>
    				<th>实战题目</th>
    				<th>上传时间</th>
    				<th>自测结果</th>
    				<th>老师评分</th>
    				<th>老师评语</th>
    			</tr>
    			<c:forEach items="${sList}" var="cwl">
	    			<tr>
	    				<td>${cwl.ppcode_name}</td>
	    				<td>${cwl.tea_user_name}</td>
	    				<td>${cwl.practicalProject.semester_code}</td>
	    				<td>${cwl.practicalProject.pp_name}</td>
	    				<td><fmt:formatDate value="${cwl.upload_date}" type="both"/></td>
	    				<td>${cwl.self_score}</td>
	    				<td>${cwl.teacher_score}</td>
	    				<td><a onclick="sseedetail('${cwl.comment}')">查看</a></td>
	    			</tr>
    			</c:forEach>
    		</table>
    	</c:if>
    	<c:if test="${fn:length(sList) <= 0||sList==null }">
    		<div style = "margin-top: 10px;font-size: 16px;font-weight: lighter;">暂无任何题目</div>
    	</c:if>
	</div>
	<!-- 要提交答案的题目列表结束-->
	
	<!-- 弹出框代码 -->
	<div id="popupContact">
		<a id="popupContactClose">x</a>
		<h1>查看评语</h1>
		<p id="contactArea">
					
		</p>
	</div>
	<div id="backgroundPopup"></div>
	<!-- 弹窗结束 -->
	
  </body>
</html>
