<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>  
    <script type="text/javascript">
    </script>
</head>
<body >
<div align="center" style=" font-family:'幼圆';">
<div align="center"><span style=" font-size:36px">${practicalProject.getPp_name()}</span></div>
<div align="center" style="widows:600px">
    <div style="margin-top:30px;">
	    <span style="margin-right:30px">编号:${practicalProject.getProject_stringcode()}</span>
	    <span  style="margin-left:30px">题目:${practicalProject.getPp_name()}</span>
	</div>
	<div style="margin-top:30px;">
	    <span style="margin-right:30px">开题学期:${practicalProject.getSemester_code()}</span>
	    <span  style="margin-left:30px">项目来源:${practicalProject.getSource_CN()}</span>
	</div>
	<div style="margin-top:30px;">
	    <span style="margin-right:30px">项目发布人:<c:if test="${student_name!=null}">${student_name}</c:if><c:if test="${student_name==null}">${teacher_name}</c:if></span>
	    <span  style="margin-left:30px">项目审核人:${teacher_name}</span>
	</div>
	<div style=" width:600px; margin-top:30px;">
	    <div align="left"><span style="font-size:24px">具体要求:</span></div>
	    <textarea readonly=“true" rows="15" cols="50"  style=" width:600px;border:solid 1 black;overflow:auto;resize:none;">${practicalProject.getRequirement()}</textarea>
	</div>
</div>
</div>
</body>
</html>