<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'review.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/js/uploadify/uploadify.css'/>">    
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/courseware.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/general.css'/>">
    <script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>
    <script type="text/javascript" src="<c:url value='resources/js/popup.js'/>"></script>
    <script type="text/javascript" src="<c:url value='resources/js/uploadify/jquery.uploadify.min.js'/>"></script>
    <script type="text/javascript">      
    
    function sendFile(record_code)
    {
       //显示弹出框
       centerPopup();
       loadPopup();
       
       $("#multiple_file_upload").uploadify({  
            'uploader' : "<c:url value='/practicing/uploaddoc?dir=file&ppcode="+record_code+"'/>",//************ action **************  
            'height' : 27,//表示按钮的高度，默认30PX。若要改为50PX，如下设置：'height' : 50，  
            'width' : 50,//表示按钮的宽度  
            'buttonText' : '浏 览',//按钮上显示的文字，默认”SELECT FILES”  
            'buttonCursor' : 'hand',//上传按钮Hover时的鼠标形状，默认值是’hand’  
            'auto' : true, //是否自动开始     
            'multi' : true, //是否支持多文件上传，默认为true  
            'method' : 'post',//默认是’post’,也可以设置为’get’  
            'swf' : "<c:url value='resources/js/uploadify/uploadify.swf'/>",//进度条显示文件    resources/js/uploadify/uploadify.swf          
            'fileTypeDesc' : 'doc,docx',//允许上传的文件类型的描述，在弹出的文件选择框里会显示  
            'fileTypeExts' : '*.doc;*.docx',//指定文件格式  
            'fileSizeLimit' : '50MB',//上传文件大小限制，默认单位是KB，若需要限制大小在100KB以内，可设置该属性为：'100KB'  
            'fileObjName' : 'file',//文件对象名称。用于在服务器端获取文件。  
            'formData' : {  
                'ppcode' : record_code
            },  
            'progressData' : 'all', // 'percentage''speed''all'//队列中显示文件上传进度的方式：all-上传速度+百分比，percentage-百分比，speed-上传速度  
            'preventCaching' : true,//若设置为true，一个随机数将被加载swf文件URL的后面，防止浏览器缓存。默认值为true  
            'timeoutuploadLimit' : 5,//能同时上传的文件数目  
            'removeCompleted' : true,//默认是True，即上传完成后就看不到上传文件进度条了。  
            'removeTimeout' : 3,//上传完成后多久删除队列中的进度条，默认为3，即3秒。  
            'requeueErrors' : true,//若设置为True，那么在上传过程中因为出错导致上传失败的文件将被重新加入队列。  
            'successTimeout' : 30,//表示文件上传完成后等待服务器响应的时间。不超过该时间，那么将认为上传成功。默认是30，表示30秒。  
            'uploadLimit' : 999,//最多上传文件数量，默认999  
            'onUploadStart' : function(file) {  
                //$("#file_upload").uploadify("settings", "formData", {'userName':name,'qq':qq});    
                //$("#file_upload").uploadify("settings", "buttonText", "aaa");  
                //alert(file.name + " is ready to go!")  
                //$("#stopUpload").removeAttr("hidden");  
            },  
            'onUploadSuccess' : function(file, data, response) {   
                ///$("#stopUpload").attr("hidden",true);
                $("#pinfen").css('display','block'); 
                $("#sendfenshu").click(function(){
                   sendpass(record_code);
                });   
            }  
  
        });
    }
    
    //选择类型后发送
       function sendpass(record_code)
       {
          var val=$('input:radio[name="ri"]:checked').val();
          if(val==null)
          {
             alert("你什么也没有选择！");
          }else
          {            
              $.ajax({
               type: "GET",
               dataType: "text",//返回text格式的数据
			   url: "practicing/practicing/zijidafen"+"?noCache="+Math.floor(Math.random()*100000),
			   data: {
			            ppcode:record_code,
			            val:val
			         },
			   success:function(data){
                    if(data=="true")
                    {
                       //弹框消失
                       disablePopup();
                       parent.menuclick("${id}");
                    }else
                    {
                      $("#contactArea").append("<center><span style=\"color:red\">数据提交失败！</span></center>");
                    }
			   },
			   error:function(XMLHttpRequest, textStatus, errorThrown){
			         $("#contactArea").append("<center><span style=\"color:red\">数据提交失败！</span></center>");
			   }
           });  
          }        
       }
    </script>
  </head>
  
  <body>
    <!-- 要提交答案的题目列表 -->
	<div class="content" align="center">
    	<c:if test="${sList!=null && fn:length(sList) > 0}">
    		<table class="table" style="margin-top: 10px;">
    		    <tr>
    				<th>题目类型</th>
    				<th>老师名称</th>
    				<th>开课学期</th>
    				<th>下载时间</th>
    				<th>实战题目</th>
    				<th>上传答案</th>
    			</tr>
    			<c:forEach items="${sList}" var="cwl">
	    			<tr>
	    				<td>${cwl.ppcode_name}</td>
	    				<td>${cwl.tea_user_name}</td>
	    				<td>${cwl.practicalProject.semester_code}</td>
	    				<td><fmt:formatDate value="${cwl.download_date}" type="both"/></td>
	    				<td>${cwl.practicalProject.pp_name}</td>
	    				
	    				<c:if test="${cwl.self_score==' ' || cwl.self_score==null}">
	    				   <td><a onclick="sendFile(${cwl.record_code})">上传</a></td>
	    				</c:if>
	    				<c:if test="${cwl.self_score!=null && cwl.self_score!=' '}">
	    		           <td><a onclick="sendFile(${cwl.record_code})">上传最新</a></td>
	    				</c:if>
	    			</tr>
    			</c:forEach>
    		</table>
    	</c:if>
    	<c:if test="${fn:length(sList) <= 0||sList==null }">
    		<div style = "margin-top: 10px;font-size: 16px;font-weight: lighter;">暂无任何题目</div>
    	</c:if>
	</div>
	<!-- 要提交答案的题目列表结束-->
	
	<!-- 弹出框代码 -->
	<div id="popupContact">
		<a id="popupContactClose">x</a>
		<h1>上传实战答案文件</h1>
		<p id="contactArea">
		  <center><input type="file" name="uploadify" id="multiple_file_upload" /></center>	
		  <p id="pinfen" style="display: none;">为自己评分：<br>
		     <input value="A" type="radio" name="ri"/>优<br>
		     <input value="B" type="radio" name="ri"/>良<br>
		     <input value="C" type="radio" name="ri"/>中<br>
		     <input value="D" type="radio" name="ri"/>合格<br>
		     <input value="E" type="radio" name="ri"/>不合格<br>
		     <center><input type="button" value="确认" id="sendfenshu" /></center>
		  </p>    	
		</p>
	</div>
	<div id="backgroundPopup"></div>
	<!-- 弹窗结束 -->
	
  </body>
</html>
