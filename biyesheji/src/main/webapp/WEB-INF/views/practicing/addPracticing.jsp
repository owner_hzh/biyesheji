<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/js/uploadify/uploadify.css'/>">
	<script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>
	<script type="text/javascript" src="<c:url value='resources/js/uploadify/jquery.uploadify.min.js'/>"></script>
	<script type="text/javascript">
	var isUploadFile=false;//全局变量，判断是否有文件上传
	function definebutton()
	{
		if($.trim($("#pp_name").val())==""||$.trim($("#requirement").val())=="")
			{
			   alert("项目名称或具体要求不能为空!");   
			}
		else if($.trim($("#pp_name").val()).length>28)
			{
			    alert("项目名称不能超过28个字!");  
			}
		else if($.trim($("#requirement").val()).length>500)
			{
			alert("项目要求不能超过500个字!");  
			}
		else
			{
			    //alert("判断");
			    if(isUploadFile==true)//有文件时选择用控件的上传
			    {
			         var role=${role};
			         var user_id='${user_id}';
					 var pp_name=encodeURIComponent(encodeURIComponent($("#pp_name").val()));
			         var ppcode=encodeURIComponent(encodeURIComponent($("#ppcode").val()));
			         var semester_code=encodeURIComponent(encodeURIComponent($("#semester_code").val()));
			         var requirement=encodeURIComponent(encodeURIComponent($("#requirement").val()));
			        //alert("<c:url value='/practicing/AddPracticing?dir=file&pp_name="+pp_name+"&ppcode="+ppcode+"&semester_code="+semester_code+"&requirement="+requirement+";jsessionid="+jsessionid+"'/>");
			         //$('#upload').uploadify('settings','uploader',"<c:url value='/practicing/AddPracticing?dir=file&pp_name="+pp_name+"&ppcode="+ppcode+"&semester_code="+semester_code+"&requirement="+requirement+";jsessionid="+jsessionid+"'/>");
			          $('#upload').uploadify('settings','uploader',"<c:url value='/practicing/AddPracticing?dir=file&pp_name="+pp_name+"&ppcode="+ppcode+"&semester_code="+semester_code+"&requirement="+requirement+"&role="+role+"&user_id="+user_id+"'/>");
			         //$('#upload').uploadify(('settings','formData',{'dir' : 'file','pp_name':pp_name,'ppcode':ppcode,'semester_code':semester_code,'requirement':requirement}));

			    	$('#upload').uploadify('upload','*');
			    }
			    else//无文件时用ajax上传  
			    {
			    	//alert("无文件");
			    	addPracticing();	
			    }

			}
	}
	function addPracticing()
	{
	     var role=${role};
		 var user_id='${user_id}';
		 $.ajax({
             type: "POST",
             dataType: "text",//返回text格式的数据
			   url: "practicing/AddPracticing"+"?noCache="+Math.floor(Math.random()*100000),
			   data: {
			            pp_name:$("#pp_name").val(),
			            ppcode:$("#ppcode").val(),
			            semester_code:$("#semester_code").val(),
			            requirement:$("#requirement").val(),
			            role:role,
			            user_id:user_id
			         },
			   success:function(data){
				   //alert(data);
                  if(data=="success")
                  {
                	  alert("添加成功！");  
                	 // $("#clean").trigger("click");
                   var pp = window.parent.parent.document.getElementById("menutwo");//获得父级iframe
			       $(pp).children(".activetwo").click();
                  }
                  else
                  {
                	  alert("添加失败！");  
                  }
			   },
			   error:function(XMLHttpRequest, textStatus, errorThrown)
			   {
				   alert("添加失败!");  
			   }
		 });
	}
	//文件上传控件初始化
	$(function() 
	{
		// var pp_name=$("#pp_name").val();
        // var ppcode=$("#ppcode").val();
         //var semester_code=$("#semester_code").val();
         //var requirement=$("#requirement").val();
		 //上传作业文件
		 $("#upload").uploadify({
			 'height'         : 30,//表示按钮的高度，默认30PX。若要改为50PX，如下设置：'height' : 50，  
	         'width'          : 120,//表示按钮的宽度  
	         'method'         : 'post',
		     'swf'            : "<c:url value='resources/js/uploadify/uploadify.swf' />",  
		     //'uploader'       : "<c:url value='/practicing/AddPracticing' />",
		     'cancelImg'      : "<c:url value='/resources/js/uploadify/uploadify-cancel.png' />",
		     'buttonText'     :'选择上传文档' ,
		     //'formData'       : {'folder' : 'file','pp_name':asd,'ppcode':asd,'semester_code':asd,'requirement':asd},
		     'auto'           : false,  
		     'multi'          : false, 
		     'wmode'          : 'transparent', 
		     'fileObjName'    : 'file',//文件对象名称,用于在服务器端获取文件。
		     'simUploadLimit' : 1,  
		     'fileTypeExts'        : '*.zip;*.rar;*.doc;*.docx;*.xls;*.xlsx;*.ppt;*.txt;',  

		     'fileTypeDesc'       : '文件(*.zip;*.rar;*.doc;*.docx;*.xls;*.xlsx;*.ppt;*.txt;)', 

		     'onUploadSuccess': function(file, data) 
		     {
		    	 alert("成功上传!");
		    	 isUploadFile=false;
		    	 $("#clean").trigger("click");
		     	var message=eval(data);
		         if(message.error==0){//上传
		        	 location.reload();
		         }
		         else
		         {
		        	 alert(message.message);
		         }
		     },
		     'onSelect': function(e, queueId, fileObj)//选择上传文件触发的事件(将全局变量isUploadFile设为true)
		     {
		    	 isUploadFile=true;
		     },
		     'onCancel': function(event,queueId,fileObj,data)//取消上传文件触发事件(由于设成只能上传1个文档，所以当点击取消时将全局变量isUploadFile设为fasle)
		     {
		    	 isUploadFile=false;
		     }
		 }); 
		
	}); 
	</script>
</head>
<body>
<div align="center" style=" font-family:'幼圆';" >
<form method="post" id="form" action="<c:url value='practicing/AddPracticing'/>">
<div align="center" > 
  <div>添加实战</div>
  <div align="center" style=" width:500px;">
  <div  align="center">
    <div align="left" style="left:120px;position:relative; margin-top:10px;">
	    <span>项目名称:</span>
		<input type="text" name="pp_name" id="pp_name" maxlength="20" class="pp_name"/>
	</div>
	 <div align="left" style="left:120px;position:relative;margin-top:10px;">
	    <span >项目类型:</span>
		<select class="ppcode" name="ppcode" id="ppcode">
		    <c:forEach items="${practicalProjectCodeList}" var="practicalProjectCode">
		    <option value="${practicalProjectCode.getPpcode()}">${practicalProjectCode.getPptype_name()}</option>
			</c:forEach>
		</select>
		<span>学期:</span>
		<select class="semester_code" id="semester_code" name="semester_code">
		    <option value="0" selected="selected">无要求</option>
		    <option value="1">第一学期</option>
		    <option value="2">第二学期</option>
		    <option value="3">第三学期</option>
		    <option value="4">第四学期</option>
		    <option value="5">第五学期</option>
		    <option value="6">第六学期</option>
		    <option value="7">第七学期</option>
		    <option value="8">第八学期</option>
		</select>
	</div>
  </div>
  <div  align="center" style="margin-top:10px;">
       <div align="left"  style=" margin-top:20px;"><span>具体要求:</span></div>
	   <textarea  rows="15" cols="50" id="requirement" name="requirement" class="requirement" style=" width:600px;border:solid 1 black;overflow:auto;resize:none;"></textarea>
  </div>
  <div align="left" style=" margin-top:20px;"><input onclick="asd()" type="file" id="upload" width="120px" height="30px"/></div>
  </div>
</div>
    <input type="button" id="determine" onclick="definebutton()" value="确定">
    <input type="reset" id="clean" value="清除">
</form>
</div>
</body>
</html>