<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/courseware.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/popwindow.css'/>">
	<style type="text/css">
		.title{
			height:4%;
			width:95%;
			margin-top: 20px;
			background-color: blue;
			line-height:30px;
			color:#FFFFFF;
			font-size:17px;
			border-radius:15px; 
			text-align: left;
			font-weight: bold;
		}
		.pagechange:hover
		{
		    color:blue;
			text-decoration: underline;
		}
		.pagechange
		{
		    font-size: 20px;
		    font-weight:lighter;
		    font-family:'幼圆';
		    cursor:pointer;
		}
	</style>
	
	<script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>  
    <script type="text/javascript" src="<c:url value='resources/js/uploadify/jquery.uploadify.min.js'/>"></script>
    <script type="text/javascript" src="<c:url value='resources/js/popwindow/popwindow.js'/>"></script>
    <script type="text/javascript">
    <c:if test="${loginuserRole==3||loginuserRole==6 }">
    //删除实战项目
    function deletePracticing(ppcode,ppnumber)
    {
   	 if(confirm("是否删除该实战？"))
	 {
					   $.ajax({
					 	   type: "POST",
					 	   url: "practicing/deletePracticing",
					 	   data:{ppcode:ppcode,ppnumber:ppnumber},
					 	   success: function(msg)
					 	   {
					 		  location.reload();
					 	   }
					 	   });
	 }
    }
    </c:if>
    //查看详情
    function showPracticingDetial(ppcode,ppnumber)
    {
    	 $(window).ShowDialog({ width: 800, height: 600, title: "详细要求", src: "<c:url value='practicing/basicpracticing/getOnePracticing?ppcode="+ppcode
             +"&ppnumber="+ppnumber+"'/>" });
    }
    //添加实战
    function addPracticing()
    {
    	 $(window).ShowDialog({ width: 800, height: 600, title: "添加实战", src: "<c:url value='practicing/showAddPracticingFrame'/>" });
    }
    //查看文档
     function showDocuments( ppcode, ppnumber,pp_name)
     {
    	 pp_name=encodeURIComponent(encodeURIComponent(pp_name));
    	 window.location.href="<c:url value='/practicing/basicpracticing/getDocuments?ppcode="+ppcode+"&ppnumber="+ppnumber+"&pp_name="+pp_name+"'/>";
     }
    //页码切换
    function pagechange(goalpage)
    {
    	 var sort_select="";
    	 var semester_select="";
    	 var search_type="";
    	 var keyword="";
    	 var ppcode="";
    	 <c:if test="${sort_select!=null}">sort_select="&sort_select=${sort_select}";</c:if>
    	 <c:if test="${semester_select!=null}">semester_select="&semester_select=${semester_select}";</c:if>
    	 <c:if test="${search_type!=null}">search_type="&search_type=${search_type}";</c:if>
    	 <c:if test="${keyword!=null}">keyword="&keyword=${keyword}";</c:if>
    	 <c:if test="${ppcode!=null}">ppcode="&ppcode=${ppcode}";</c:if>
    	 //alert(sort_select);
     	 window.location.href="<c:url value='/practicing/basicpracticing/pageChange?page="+goalpage+sort_select+semester_select+search_type+keyword+ppcode+"'/>";  
        }
    //下一页
     function nextpage()
     {
    	 var goalpage=Number($("select.pageSelect option:selected").val())+1;
    	 //alert(goalpage);
    	 pagechange(goalpage);
     }
    //页数选择select的改变事件
     function selectchange(select)
     {
    	 var goalpage=$("select.pageSelect option:selected").val();
    	 pagechange(goalpage);
     }
     //上一页
     function forwardpage()
     {
    	 var goalpage=Number($("select.pageSelect option:selected").val())-1;
    	 //alert(goalpage);
    	 pagechange(goalpage);
     }
     <c:if test="${loginuserRole==2 }">
     //学生参加项目按钮
     function join(ppcode,ppnumber,tea_user_id)
     {
    	 if(confirm("是否参加该实战？"))
    	 {
    			$.get("<c:url value='practicing/joinPracticing'/>",{ppcode:ppcode,ppnumber:ppnumber,tea_user_id:tea_user_id},function(date)
    					{
    						if(date=="success")
    						{
    							 alert("成功加入!");  
    							 //location.reload();
    						}
    						else
    						{
    							alert(date);
    						}
    					});
    	 }
     }
     </c:if>
    </script>
  </head>
  
  <body>
  <div class="content" align="center">
		<div class="title"><span style="margin-left: 14px;">基础实战</span></div>
		<div style="margin-top:5px">
		    <form method="post" action="<c:url value='/practicing/basicpracticing/pageChange'/>">
		    <input  type="button" value="添加实战" onclick="addPracticing()">
		    <span>实战类型:</span>
		    <select id="ppcode" name="ppcode">
		         <option value="-1">所有类型</option>
		        <c:forEach items="${practicalProjectCodeList}" var="practicalProject">
		        <option value="${practicalProject.ppcode}" <c:if test="${ppcode==practicalProject.ppcode}">selected="selected"</c:if>>${practicalProject.pptype_name}</option>
		        </c:forEach>
		    </select>
		    <span>学期:</span>
		    <select id="semester_select" name="semester_select">
		         <option value="-1" <c:if test="${semester_select!=null&&semester_select=='-1'}">selected="selected"</c:if>>所有</option>
		         <option value="0" <c:if test="${semester_select!=null&&semester_select=='0'}">selected="selected"</c:if>>无限制</option>
		         <option value="1" <c:if test="${semester_select!=null&&semester_select=='1'}">selected="selected"</c:if>>第一学期</option>
		         <option value="2" <c:if test="${semester_select!=null&&semester_select=='2'}">selected="selected"</c:if>>第二学期</option>
		         <option value="3" <c:if test="${semester_select!=null&&semester_select=='3'}">selected="selected"</c:if>>第三学期</option>
		         <option value="4" <c:if test="${semester_select!=null&&semester_select=='4'}">selected="selected"</c:if>>第四学期</option>
		         <option value="5" <c:if test="${semester_select!=null&&semester_select=='5'}">selected="selected"</c:if>>第五学期</option>
		         <option value="6" <c:if test="${semester_select!=null&&semester_select=='6'}">selected="selected"</c:if>>第六学期</option>
		         <option value="7" <c:if test="${semester_select!=null&&semester_select=='7'}">selected="selected"</c:if>>第七学期</option>
		         <option value="8" <c:if test="${semester_select!=null&&semester_select=='8'}">selected="selected"</c:if>>第八学期</option>
		     </select>
		    <span>排序方式:</span>
		    <select id="sort_select" name="sort_select">
		         <option value="ppnumber" <c:if test="${sort_select!=null&&sort_select=='ppnumber'}">selected="selected"</c:if>>内置序号</option>
		         <option value="project_stringcode" <c:if test="${sort_select!=null&&sort_select=='project_stringcode'}">selected="selected"</c:if>>实战编码</option>
		         <option value="semester_code" <c:if test="${sort_select!=null&&sort_select=='semester_code'}">selected="selected"</c:if>>学期</option>
		    </select>
		    <span>搜索:</span><input type="text" id="keyword" name="keyword" value="${keyword}" />
		    <span>搜索方式:</span>
		    <select id="search_type" name="search_type">
		        <option value="project_stringcode" <c:if test="${search_type!=null&&search_type=='project_stringcode'}">selected="selected"</c:if>>实战编号</option>
		        <option value="pp_name" <c:if test="${search_type!=null&&search_type=='pp_name'}">selected="selected"</c:if>>实战题目</option>
		    </select>
		    <input type="submit" value="搜索" />
		    </form>
		</div>
    	<c:if test="${practicalProjectList!=null }">
    		<table class="table" style="margin-top: 10px;">
    			<tr>
    				<th>实战编码</th>
    				<th>实战类型</th>
    				<th>实战题目</th>
    				<th>开课学期</th>
    				<th>项目来源</th>
    				<th>项目来源人</th>
    				<th>操作</th>
    			</tr>
    			<c:forEach items="${practicalProjectList}" var="practicalProject">
	    	    <tr>
	    			<td style="text-align: middle;">${practicalProject.project_stringcode }</td>
	    			<td style="text-align: middle;">${practicalProject.pptype_name }</td>
	    			<td style="text-align: middle;">${practicalProject.pp_name }</td>
	    			<td style="text-align: middle;">${practicalProject.semester_code }</td>
	    			<td style="text-align: middle;">${practicalProject.source_CN }</td>
	    			<td style="text-align: middle;">${practicalProject.user_name }</td>
	    			<td>
	    			    <a onclick="showPracticingDetial(${practicalProject.ppcode},${practicalProject.ppnumber})">查看详情</a>
	    			   <c:if test="${loginuserRole==2 }"> <a onclick="join('${practicalProject.ppcode}','${practicalProject.ppnumber}','${practicalProject.tea_user_id}')">参加</a></c:if>
	    				<a onclick="showDocuments('${practicalProject.ppcode}','${practicalProject.ppnumber}','${practicalProject.pp_name }')">文档</a>
	    				<c:if test="${loginuserRole==3||loginuserRole==6 }"><a onclick="deletePracticing('${practicalProject.ppcode}','${practicalProject.ppnumber}')">删除</a></c:if>
	    			</td>
	    		</tr>
	    		</c:forEach>
    		</table>
            <a class="pagechange" onclick="forwardpage()">上一页</a>
            <select class="pageSelect" onchange="selectchange(this)">
            <c:forEach items="${num}" var="num">
            <option value="${num}" <c:if test="${page!=null&&page==num}">selected="selected"</c:if>>${num}</option>
            </c:forEach>
            </select>
            <a class="pagechange" onclick="nextpage()">下一页</a>
    </c:if>
    <c:if test="${practicalProjectList==null }">
    		<div style = "margin-top: 10px;font-size: 16px;font-weight: lighter;">暂无任何实战</div>
    	</c:if>
	</div>
	
	<div id="asd" class="asd">
	</div>
  </body>
</html>
