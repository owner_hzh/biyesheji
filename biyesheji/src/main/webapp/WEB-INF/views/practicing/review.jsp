<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'review.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/courseware.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/general.css'/>">
    <script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>
    <script type="text/javascript" src="<c:url value='resources/js/popup.js'/>"></script>
    <script type="text/javascript">
        var n=null;
       //通过审核按钮点击事件
       function pass(number)
       {
           //显示弹出框
           centerPopup();
		   loadPopup();  
		   n=number;        
       }
       //选择类型后发送
       function sendpass()
       {
          var val=$('input:radio[name="ri"]:checked').val();
          if(val==null)
          {
             alert("你什么也没有选择！");
          }else
          {            
              $.ajax({
               type: "GET",
               dataType: "text",//返回text格式的数据
			   url: "practicing/practicing/pass"+"?noCache="+Math.floor(Math.random()*100000),
			   data: {
			            number:n,
			            code:val
			         },
			   success:function(data){
                    if(data=="true")
                    {
                       //弹框消失
                       disablePopup();
                       parent.menuclick("${id}");
                    }else
                    {
                      $("#contactArea").append("<center><span style=\"color:red\">数据提交失败！</span></center>");
                    }
			   },
			   error:function(XMLHttpRequest, textStatus, errorThrown){
 
			         $("#contactArea").append("<center><span style=\"color:red\">数据提交失败！</span></center>");
			   }
           });  
          }        
       }
       //删除待审核的题目
       function ddelete(number)
       {
           $.ajax({
               type: "GET",
               dataType: "text",//返回text格式的数据
			   url: "practicing/practicing/delete"+"?noCache="+Math.floor(Math.random()*100000),
			   data: {
			            number:number
			         },
			   success:function(data){
                    if(data=="true")
                    {
                       parent.menuclick("${id}");
                    }else
                    {
                      alert("删除失败，请重试！");
                    }
			   },
			   error:function(XMLHttpRequest, textStatus, errorThrown){
			         //alert("error-->文档删除失败！");
			   }
           });
       }
       //在线预览待审核的题目
       function show(number)
       {
           $.ajax({
               type: "GET",
               dataType: "text",//返回text格式的数据
			   url: "practicing/practicing/show"+"?noCache="+Math.floor(Math.random()*100000),
			   data: {
			            number:number
			         },
			   success:function(data){
                   parent.showPage(data);
			   },
			   error:function(XMLHttpRequest, textStatus, errorThrown){
			         //alert("error-->文档删除失败！");
			   }
           });
       }
    </script>
  </head>
  
  <body>
    <!-- 要审的题目列表 -->
	<div class="content" align="center">
    	<c:if test="${aList!=null && fn:length(aList) > 0}">
    		<table class="table" style="margin-top: 10px;">
    		    <tr>
    				<th>实战题目</th>
    				<th>上传者</th>
    				<th>操作</th>
    				<th>确认通过</th>
    			</tr>
    			<c:forEach items="${aList}" var="cwl">
	    			<tr>
	    				<td>${cwl.pp_name}</td>
	    				<td>${cwl.requirement}</td>
	    				<td>
	    					<c:if test="${cwl.ppd_link!=null}"><a href="<c:url value='${cwl.ppd_link}'/>">下载</a></c:if>
	    					<c:if test="${ccwl.ppd_link!=null }"><a onclick="show(${cwl.equence_number})">在线预览</a></c:if>
	    					<a onclick="ddelete(${cwl.equence_number})">删除</a>	    					
	    				</td>
	    				<td>
	    				    <a onclick="pass(${cwl.equence_number})">通过审核</a>
	    				</td>
	    			</tr>
    			</c:forEach>
    		</table>
    	</c:if>
    	<c:if test="${fn:length(aList) <= 0||aList==null }">
    		<div style = "margin-top: 10px;font-size: 16px;font-weight: lighter;">暂无任何待审核的题目</div>
    	</c:if>
	</div>
	<!-- 要审的题目列表结束-->
	
	<!-- 弹出框代码 -->
	<div id="popupContact">
		<a id="popupContactClose">x</a>
		<h1>选择题目类型</h1>
		<p id="contactArea">
			<c:if test="${pList!=null && fn:length(pList) > 0}">
    			<c:forEach items="${pList}" var="pr">
	    			<input value="${pr.ppcode}" type="radio" name="ri"/>${pr.pptype_name}<br>
    			</c:forEach>
    			<center><input type="button" onclick="sendpass()"  value="确认"/></center>
    	     </c:if>
	    	 <c:if test="${fn:length(pList) <= 0||pList==null }">
	    		<div style = "margin-top: 10px;font-size: 16px;font-weight: lighter;">暂无类型数据</div>
	    	 </c:if>		
		</p>
	</div>
	<div id="backgroundPopup"></div>
	<!-- 弹窗结束 -->
	
  </body>
</html>
