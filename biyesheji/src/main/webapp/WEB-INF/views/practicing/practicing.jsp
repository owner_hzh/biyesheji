<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'practicing.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
    <style type="text/css">
	   html body{ height:100%; width:100%;overflow:hidden;}
	   .menu{width:15%; height: 100%; 
	   		float: left; background-color: #e8e8e8; overflow: auto;
	   		border-top-left-radius:15px; 
	   		border-bottom-left-radius:15px; 
	   		color: #AA531E;
	   } 
	   .activetwo{background-color: #CCCCCC;color: blue;}
	   .menu div{ cursor: pointer;
	   			  border-bottom: 1px dashed gray;
	   			  height:40px;width:100%;
	   			  font-size: 15px;
	   			  font-family: '幼圆';
	   			  font-weight: normal;
	   			  line-height: 40px;
	   			  text-align: center;
	   	}
	   .menu div:HOVER{background-color: #CCCCCC;color: blue;}/* 909EDA  F2F2F2*/
	   .content { width:85%; height:100%; 
	   			  float: right;
	   			  background-color:#EDEDED;
	   			  border-top-right-radius:15px; 
	   			  border-bottom-right-radius:15px; 
	   }	  
	   .iframe_content{ width: 100%; height: 100%; overflow: auto;background-color:#CCCCCC;
	   					border-top-right-radius:15px; 
	   			        border-bottom-right-radius:15px;  }
	</style>
	<link rel="stylesheet" href="<c:url value='resources/css/jquery.treeview.css'/>" />	
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/general2.css'/>">   
    <script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>
    <script type="text/javascript" src="<c:url value='resources/js/popuptwo.js'/>"></script>
	<script src=' <c:url value='/resources/js/jquery.cookie.js'/>' type="text/javascript"></script>
	<script src='<c:url value='/resources/js/jquery.treeview.js'/>' type="text/javascript"></script>
  	<script type="text/javascript">	 
 		$(function(){
			$("#menutwo").children().eq(0).addClass("activetwo");
			var obj = $("#menutwo").children().eq(0);
			obj.click();
		}); 
		function getContent(obj,id){
			$(".activetwo").removeClass("activetwo");
			$(obj).addClass("activetwo");
			var name = $(obj).text();
			name = encodeURIComponent(encodeURIComponent(name));
			$("#iframe").attr("src" , "<c:url value='/practicing/practicing/showpracticing?name="+name+"&id="+id+"'/>");
		}
		
		//根据菜单id触发菜单点击事件
		function menuclick(id)
		{
		   $("#mdiv"+id).click();
		}
		
		//显示弹出框
		function showPage(html)
		{
		   //显示弹出框
           centerPopup2();
		   loadPopup2();
		   $("#contactArea").empty();
		   $("#contactArea").append(html);
		}
		
	</script>
  </head>
  
  <body>
      <!-- 实战演练菜单开始 -->
    <div id="menutwo" class="menu">
   		<c:forEach items="${menuLevelTwoList}" var="menu">
   			<div id="mdiv${menu.menu_level_two_id}" onclick="getContent(this,${menu.menu_level_two_id})"><span>${menu.menu_level_two_name }</span></div>
   		</c:forEach>
	</div>
	<!-- 实战演练菜单结束-->
	
	<!-- 实战演练内容开始-->
	<div class="content" align="center"> 
      	<iframe id="iframe" class="iframe_content" marginheight="0" marginwidth="0" hspace="0" vspace="0" frameborder="0">
    	</iframe>
	</div>
	<!-- 实战演练内容结束-->
	
	<!-- 弹出框代码 -->
	<div id="popupContact">
		<a id="popupContactClose">x</a>
		<h1>文档信息</h1>
		<p id="contactArea">
				
		</p>
	</div>
	<div id="backgroundPopup"></div>
	<!-- 弹窗结束 -->
	
  </body>
</html>
