<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'score-practicing.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<style type="text/css">
	  textarea{
       border:1px solid #999;
       font-size:12px;
       font-family:"幼圆";
       padding:1px;
       height:140px;
       overflow:auto;
       width:80%;
       text-align:left;
       padding:5px;
       }
	</style>
    <link rel="stylesheet" type="text/css" href="<c:url value='resources/css/courseware.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/general.css'/>">
    <script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>
    <script type="text/javascript" src="<c:url value='resources/js/popup.js'/>"></script>
    <script type="text/javascript">
       var ppcode=null;
       function showDialog(code)
       {
          //显示弹出框
           centerPopup();
		   loadPopup();
		   ppcode=code;
       }
       function sendscore()
       {
         $("#ts").empty();
         var score=$("#score").val();
         var words=$("#score_word").val();
         //words = encodeURIComponent(encodeURIComponent(words));
         var re = /^[0-9]+.?[0-9]*$/;   //判断字符串是否为数字     //判断正整数 /^[1-9]+[0-9]*]*$/   
         if (!re.test(score))
         { 
           $("#score").val("");  
           $("#ts").append("<span style=\"color:red\">请给予得分</span>");
           //$("#contactArea").append("<center><span style=\"color:red\">请输入数字</span></center>");
         }else
         {
           $.ajax({
               type: "POST",
               dataType: "text",//返回text格式的数据
			   url: "practicing/practicing/score"+"?noCache="+Math.floor(Math.random()*100000),
			   data: {
			            words:words,
			            score:score,
			            ppcode:ppcode
			            
			         },
			   success:function(data){
                    if(data=="true")
                    {
                       //弹框消失
                       disablePopup();
                       parent.menuclick("${id}");
                    }else
                    {
                      $("#contactArea").append("<center><span style=\"color:red\">数据提交失败！</span></center>");
                    }
			   },
			   error:function(XMLHttpRequest, textStatus, errorThrown){
			         $("#contactArea").append("<center><span style=\"color:red\">数据提交失败！</span></center>");
			   }
           }); 
         }  
         
       }
       $(function(){
         $("#score").focus(function(){
            $("#ts").empty();
         });
       });
    </script>
  </head>
  
  <body>
    <!-- 要评分的题目列表 -->
	<div class="content" align="center">
    	<c:if test="${sList!=null && fn:length(sList) > 0}">
    		<table class="table" style="margin-top: 10px;">
    		    <tr>
    				<th>实战题目</th>
    				<th>题目类型</th>
    				<th>上传者</th>
    				<th>下载题目时间</th>
    				<th>上传答案时间</th>
    				<th>下载</th>
    				<th>评分</th>
    			</tr>
    			<c:forEach items="${sList}" var="cwl">
	    			<tr>
	    				<td>${cwl.pp_name}</td>
	    				<td>${cwl.ppcode_name}</td>
	    				<td>${cwl.user_name}</td>
	    				<td><fmt:formatDate value="${cwl.download_date}" type="both"/></td>
	    				<td><fmt:formatDate value="${cwl.upload_date}" type="both"/></td>
	    				<td>
	    				    <a href="${cwl.link}">下载</a>
	    				</td>
	    				<td>
	    				    <a onclick="showDialog(${cwl.record_code})">评分</a>
	    				</td>
	    			</tr>
    			</c:forEach>
    		</table>
    	</c:if>
    	<c:if test="${fn:length(sList) <= 0||sList==null }">
    		<div style = "margin-top: 10px;font-size: 16px;font-weight: lighter;">暂无任何学生提交记录</div>
    	</c:if>
	</div>
	<!-- 要评分的题目列表结束-->
	
	<!-- 弹出框代码 -->
	<div id="popupContact">
		<a id="popupContactClose">x</a>
		<h1>评分</h1>
		<p id="contactArea">
			<center>得分：<input type="text" id="score" />	</center><br>
			<div style="margin-left: 26%;">简单评价：<br>
			   <textarea rows="3" cols="10" id="score_word"></textarea>
			</div><br>
			<center><div id="ts"></div></center><br>
			<center><input type="button" onclick="sendscore()" value="确认" /></center>
		</p>
	</div>
	<div id="backgroundPopup"></div>
	<!-- 弹窗结束 -->
  </body>
</html>
