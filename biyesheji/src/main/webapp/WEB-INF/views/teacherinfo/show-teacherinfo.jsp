<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
  <head>
    <base href="<%=basePath%>">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/courseware.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/js/uploadify/uploadify.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/css/popwindow.css'/>">
	<style type="text/css">
		.title{
			height:4%;
			width:95%;
			margin-top: 20px;
			background-color: gray;
			line-height:30px;
			color:#FFFFFF;
			font-size:17px;
			border-radius:15px; 
			text-align: left;
			font-weight: bold;
		}
	</style>
	
	<script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>  
    <script type="text/javascript" src="<c:url value='resources/js/uploadify/jquery.uploadify.min.js'/>"></script>
    <script type="text/javascript" src="<c:url value='resources/js/popwindow/popwindow.js'/>"></script>
    <script type="text/javascript">
       //查看老师详细信息
           function showTeacherDetial(user_id)
    {
    	 $(window).ShowDialog({ width: 800, height: 600, title: "教师详细信息", src: "<c:url value='/teacherinfo/showTeacherDetail?user_id="+user_id+"'/>" });
    }
     // 修改教师信息
           function showTeacherModify(user_id)
    {
    	 $(window).ShowDialog({ width: 800, height: 600, title: "教师信息修改", src: "<c:url value='/teacherinfo/displayTeacherModify?user_id="+user_id+"'/>" });
    }
        // 显示查看信息弹窗	 拼接html字符串
    	/* function checkout(teacher_name,teacher_sex,technical_name,tc_name)
    	{
    	     if(teacher_sex==1||teacher_sex=="1")
    	     {
    	         teacher_sex="男";
    	     }else
    	     {
    	        teacher_sex="女";
    	     }
    	     var html="<center>";
    	     <p style="color:blue margin-left: 20px"></p> 
    	     html+="姓名："+teacher_name+"    性别："+teacher_sex;
    	     
    	     html+="职称："+technical_name+"    性质："+tc_name; 
 //   	     html+="</center>";
 //   	     </font>
    	     //显示弹出框
    	     parent.showPage(html);
    	}
    	*/
    	// 修改信息弹窗  自己拼接html字符串
/*     	function changeinfo(teacher_name,teacher_sex,technical_name,tc_name)
    	{
    	     if(teacher_sex==1||teacher_sex=="1")
    	     {
    	         teacher_sex="男";
    	     }else
    	     {
    	        teacher_sex="女";
    	     }
    	     var html="<center>";
    	     html+="<p style='color:blue '>姓名：<input type='text' value='"+teacher_name+"' /></p>";
    	     html+="<p style='color:blue '>职称：<input type='text' value='"+technical_name+"' /></p>";
    	     html+="<p style='color:blue '>学历：<input type='text' value='"+"' /></p>";
    	     html+="<p style='color:blue '>性别：<input type='text' value='"+teacher_sex+"' /></p>";
    	     html+="<p style='color:blue '>生日：<input type='text' value='"+teacher_date+"' /></p>";
    	     html+="<p style='color:blue '>教师性质：<input type='text' value='"+tc_name+"' /></p>";
    	     html+="<p style='color:blue '>民族：<input type='text' value='"+"' /></p>";
    	     html+="<p style='color:blue '>政治面貌：<input type='text' value='"+"' /></p>";
    	     html+="<p><input  type='button' value='确认' /></p>";
    	     html+="</center>";
    	     //显示弹出框
    	     parent.showPage(html);
    	} */
  	</script>
  </head>	
<!-- 课件内容开始-->
	<div class="content" align="center">
		<div class="title"><span style="margin-left: 14px;">师资情况</span></div>
    	
    		<table class="table" style="margin-top: 10px;">
    			<tr>
    				<th>姓名</th>
    				<th>性别</th>
    				<th>职称</th>
    				<th>性质</th>
    				<th>操作</th>
 
    			</tr>
    			<c:forEach items="${TeacherList}" var="cwl">
	    			<tr>
	    				<td>${cwl.teacher_name}</td>
	    				
	    				<c:if test="${cwl.teacher_sex==1}">
	    				     <td>男</td>
	    				</c:if>
	    				<c:if test="${cwl.teacher_sex==0}">
	    				     <td>女</td>
	    				</c:if>
	    				
	    				<td>${cwl.technical_name}</td>
	    				<td>${cwl.tc_name}</td>
	    				<td>
	    				<c:if test="${sessionScope.loginuser.role_code==6}">
	    					<a onclick="showTeacherModify( ' ${cwl.user_id}' )">修改</a>
	    				</c:if>
	    					<a onclick="showTeacherDetial(' ${cwl.user_id}')">查看</a>
	    				</td>
	    			</tr>
    			</c:forEach>
    		</table>
    		
    	
    		<div style = "margin-top: 10px;font-size: 16px;font-weight: lighter;"></div>
    	
    	<div style="float: right;width:122px;margin-top: 10px;margin-right: 50px;"></div>
	</div>
	<!-- 课件内容结束--> 