<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.cqjt.pojo.Teacher" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
    <script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>  
    <script type="text/javascript">
    </script>
  </head>
 <body>
 <center>
<table width="512" border="3" bordercolor="#0099FF">
	<caption><P style="color:#03C" >个人简介</P></caption>
    <strong>
		<tr>
  			<td width="51" height="26" bgcolor="#3399FF" style="color:#00F">姓名</td>
	  		<td width="86" bgcolor="#3399FF">${teacher.teacher_name }</td>
	  		<td width="73" bgcolor="#3399FF" style="color:#00F">职称</td>
	  		<td width="123" bgcolor="#3399FF">${teacher.technical_name }</td>
	  		<td width="155" rowspan="6"><img src="${teacher.iconpath }"  height="100"  width="102"></td>
		</tr>
	<tr>
  		<td width="51" height="26"  style="color:#00F">性别</td>
	  	<td width="86" ><c:if test="${teacher.teacher_sex eq 1 }">男</c:if><c:if test="${teacher.teacher_sex eq 0 }">女</c:if></td>
	  	<td width="73"  style="color:#00F">学历</td>
	  	<td width="123" > ${teacher.teacher_degree }</td>
  	</tr>
    <tr>
  		<td height="28" bgcolor="#3399FF" style="color:#00F">生日</td>
	  	<td bgcolor="#3399FF"> ${teacher.teacher_date}</td>
	  	<td bgcolor="#3399FF" style="color:#00F">教师性质</td>
	  	<td bgcolor="#3399FF">${teacher.tc_name }</td>
  	</tr>
    <tr>
  		<td height="28" style="color:#00F">民族</td>
	  	<td> ${teacher.teacher_nation}</td>
	  	<td style="color:#00F">政治面貌</td>
	  	<td>${teacher.teacher_features}</td>
  	</tr>
    </table>
    </strong>

    </center>
</body>
</html>
