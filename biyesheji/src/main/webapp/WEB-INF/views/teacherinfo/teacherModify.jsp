<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'teacherModify.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<c:url value='resources/js/uploadify/uploadify.css'/>">
	<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/datepicker.css" />">
	<link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.min.css"/>" />
	<style type="text/css">
		.commit{
			cursor: pointer;
			background-color: #FFCC66;
			margin-top:20px;
			height:30px;
			widht:80px;
			font-size:20px;
			font-family:"幼圆";
			margin-left: 35%;
			border-radius:10px;
			border: 0px;
		}
		.commit:hover{
			background-color: #0033CC;
			color:#ffffff;
		}
	</style>
	<script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>  
    <script type="text/javascript" src="<c:url value='resources/js/uploadify/jquery.uploadify.min.js'/>"></script>
    <script type="text/javascript" src='<c:url value="/resources/js/bootstrap-datepicker.js" />'></script>
	<script type="text/javascript">
	  $(function() {  
	    $("#multiple_file_upload").uploadify({  
	            'uploader' : "<c:url value='/teacherinfo/uploadIco?userid=${teacher.user_id }'/>",//************ action **************  
	            'height' : 27,//表示按钮的高度，默认30PX。若要改为50PX，如下设置：'height' : 50，  
	            'width' : 100,//表示按钮的宽度  
	            'buttonText' : '浏 览',//按钮上显示的文字，默认”SELECT FILES”  
	            'buttonCursor' : 'hand',//上传按钮Hover时的鼠标形状，默认值是’hand’  
	            'auto' : true, //是否自动开始     
	            'multi' : false, //是否支持多文件上传，默认为true  
	            'method' : 'post',//默认是’post’,也可以设置为’get’  
	            'swf' : "<c:url value='resources/js/uploadify/uploadify.swf'/>",//进度条显示文件    resources/js/uploadify/uploadify.swf          
	            'fileTypeDesc' : 'doc,docx',//允许上传的文件类型的描述，在弹出的文件选择框里会显示  
	            'fileTypeExts' : '*.png;*.jpg;*jpeg;*gif',//指定文件格式  
	            'fileSizeLimit' : '50MB',//上传文件大小限制，默认单位是KB，若需要限制大小在100KB以内，可设置该属性为：'100KB'  
	            'fileObjName' : 'file',//文件对象名称。用于在服务器端获取文件。  
	            'formData' : {  
	                 "userid":"${teacher.user_id }"
	            },  
	            'progressData' : 'all', // 'percentage''speed''all'//队列中显示文件上传进度的方式：all-上传速度+百分比，percentage-百分比，speed-上传速度  
	            'preventCaching' : true,//若设置为true，一个随机数将被加载swf文件URL的后面，防止浏览器缓存。默认值为true  
	            'timeoutuploadLimit' : 5,//能同时上传的文件数目  
	            'removeCompleted' : true,//默认是True，即上传完成后就看不到上传文件进度条了。  
	            'removeTimeout' : 3,//上传完成后多久删除队列中的进度条，默认为3，即3秒。  
	            'requeueErrors' : true,//若设置为True，那么在上传过程中因为出错导致上传失败的文件将被重新加入队列。  
	            'successTimeout' : 30,//表示文件上传完成后等待服务器响应的时间。不超过该时间，那么将认为上传成功。默认是30，表示30秒。  
	            'uploadLimit' : 999,//最多上传文件数量，默认999  
	            'onUploadStart' : function(file) {  
	                //$("#file_upload").uploadify("settings", "formData", {'userName':name,'qq':qq});    
	                //$("#file_upload").uploadify("settings", "buttonText", "aaa");  
	                //alert(file.name + " is ready to go!")  
	            },  
	            'onUploadSuccess' : function(file, data, response) {  
	                /* alert(file.name + " upload success !");  
	                alert(data + "----" + response); */    
	            }  
	  
	     }); 
	     
	    $(document).ready(function() {
				//输入时间
				$("#teacher_date").datepicker({ dateFormat: "yyyy-MM-dd HH:mm:ss"});
				$('.evenchilds tr:odd').css({
						background : "#fff"
				});
		});
	   }); 
	</script>
  </head>

<body>
	<form action="teacherinfo/teacherModify" method="post">
	<div align="left" style="margin-left:160px;margin-top:40px;">
		<p style="color:#09F">
			工号: <input type="text" name="user_id" readonly value="${teacher.user_id }" />
			<span style="margin-left: 60px;">姓名: <input type="text " name="teacher_name" value="${teacher.teacher_name }" /></span>
		</p>

		<p style="color:#09F;margin-top: 30px;">
			教师性质: <select class="pageSelect" name="tc_code">
				<c:forEach items="${teacherCharacters}" var="teac">
					<option value='${ teac.tc_code}'
						<c:if test="${ teac.tc_code==teacher.tc_code}">selected="selected"</c:if>>${
						teac.tc_name}</option>
				</c:forEach>
			</select>
			<span style="margin-left: 140px;">
			职称: <select class="pageSelect" name="technical_code">
				<c:forEach items="${technicalPost}" var="teapost">
					<option value='${ teapost.technical_code}'
						<c:if test="${ teapost.technical_code==teacher.technical_code}">selected="selected"</c:if>>${
						teapost.technical_name}</option>
				</c:forEach>
			</select>
			</span>
		</p>
		<p style="color:#09F;margin-top: 30px;">
			性别: <input type="radio" name="teacher_sex" value="1"
				 <c:if test="${teacher.teacher_sex==1}">checked="checked"</c:if> /> 男
			    <input type="radio" name="teacher_sex" value="0" style="margin-left:20px;"
				<c:if test="${teacher.teacher_sex==0}">checked="checked"</c:if> /> 女
			<span style="margin-left: 128px;">
			学历: <input type="text" name="teacher_degree" value="${teacher.teacher_degree }" />
			</span>
		</p>

		<p style="color:#09F; margin-top: 30px;">
			生日: <input type="text" name="teacher_date" id="teacher_date" value="${teacher.teacher_date}" />
			<span style="margin-left: 61px;">
			民族: <input type="text" name="teacher_nation" value="${teacher.teacher_nation}" />
			</span>
		</p>

		<p style="color:#09F; margin-top: 30px;">
			政治面貌: <input type="text" name="teacher_features"
				value="${teacher.teacher_features}" />
			<span style="margin-left: 28px;">
			教师寸照:<div style="top:250px;right:120px;position:absolute;"><input type="file" name="uploadify" onclick="sc()" id="multiple_file_upload" /></div>
			</span>
		</p>
		<input type="submit" value="提交" class="commit"/>
	</div>
	</form>
</body>
</html>
