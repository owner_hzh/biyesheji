<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<title>修改密码</title>
<script type="text/javascript" src="<c:url value='resources/js/jquery.js'/>"></script>
<script type="text/javascript">
//判断是否为空
function isempty()
{
	if($.trim($('#oldpassword').val())==""||$.trim($('#newpassword').val())==""||$.trim($('#re_newpassword').val())=="")
	{
		alert("不能存在空的输入!");
		return false;
	}
	else
	{
		return true;
	}
}
//判断字符串中是否含有空格
function is_hasblank()
{
    var newpassword=$('#newpassword').val();
    var re_newpassword=$('#re_newpassword').val();
    var reg=/^[\w\u4e00-\u9fa5\uf900-\ufa2d()（）-]*$/;//检查是否存在全角字符
    if(newpassword.indexOf(" ")>-1||re_newpassword.indexOf(" ")>-1||!reg.test(newpassword))
    {
    	alert("存在不合法的字符！");
    	return false;
    }
    else
    	return true;
}
//判断新密码和确认密码是否相同
function is_same()
{
	if($('#newpassword').val()==$('#re_newpassword').val())
	{
		return true;
	}
	else
	{
		alert("新密码和确认密码不相同!");
		return false;
	}
}
function send2server()
{
	if(isempty()&&is_hasblank()&&is_same())
    {
	   $.ajax({
 	   type: "POST",
 	   url: "changepassword",
 	   data:{'oldpassword':$('#oldpassword').val(),'newpassword':$('#newpassword').val(),'re_newpassword':$('#re_newpassword').val()},
 	   success: function(msg)
 	   {
 	     if(msg=="fail")
 	    	 alert("密码不正确！");
 	     else if(msg=="success")
 	     {
 	    	 alert("修改成功！");
 	    	location.reload();
 	     }
 	   }
 	   });
    }
}
</script>
</head>
<body>
<div align="center" style="font-family:'幼圆';">
  <div style="margin-top:10%;">
    <div style="width:300px;">注意！密码最多包含12半角字符（数字、字母或符号）！</div>
    <div style="margin-top:10px;">
    <span>旧密码:</span><input id="oldpassword" type="password" maxlength="12"/>*
    </div>
    <div style="margin-top:10px;">
    <span>新密码:</span><input id="newpassword"  type="password" maxlength="12"/>*
    </div>
    <div style="margin-top:10px;">
    <span>确认密码:</span><input id="re_newpassword"  type="password" maxlength="12">*
    </div>
  </div>
  <div style="margin-top:10px;"><input type="button" onclick="send2server()" value="确定"></div>
</div>
</body>
</html>