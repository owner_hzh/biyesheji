package com.cqjt.interceptor;

import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.cqjt.pojo.User;
import com.cqjt.util.AuthEnum;

public class TestInterceptor implements HandlerInterceptor {

	public void afterCompletion(HttpServletRequest arg0,
			HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		// TODO Auto-generated method stub
		System.out.println("after completion");
	}

	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1,
			Object request, ModelAndView arg3) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("post handle");
	}

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
			Object obj) throws Exception {
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		String url=request.getRequestURI();                //获取访问的url
		System.out.println("===============访问的路径："+url+"================");
		HttpSession session = request.getSession();
		User logoUser=(User) session.getAttribute("loginuser");
		//String path = request.getContextPath();  
		//String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";  
		if(logoUser!=null){
			String string = String.valueOf(logoUser.getRole_code());
			System.out.println("=============="+string+"==============="+AuthEnum.getAction(url));
			//String string  = (String) request.getSession().getAttribute("roleString");//拿到用户的权限
			if(AuthEnum.getAction(url)!=null){
				//if(string.indexOf(AuthEnum.getAction(url))!=-1){//有权限，放行
				if(AuthEnum.getAction(url).indexOf(string)!=-1){//有权限，放行
					return true;
				}else{//无权限，跳转到无权限页面response.sendRedirect("/noauth");
					System.out.println("===============无权限================");
					//out.print("noauth");
					//response.sendRedirect("/biyesheji/login/noauth");
					request.getRequestDispatcher("/login/noauth").forward(request, response);
					return false;
				}
			}else{
				System.out.println("===============无对应的action================");
				return true;
			}
		}else{//未登录
			System.out.println("===============未登录================");
			response.sendRedirect("/biyesheji");
			return false;
		}
	}
}
