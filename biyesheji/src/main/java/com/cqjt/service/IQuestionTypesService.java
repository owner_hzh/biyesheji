package com.cqjt.service;

import java.util.List;
import java.util.Map;

import com.cqjt.pojo.QuestionTypes;
/*
 *	author: xiaoshujun
 * date: 2014-4-20
 * 试题类型接口
 */
public interface IQuestionTypesService {
	   /**
     * 获取所有的题型类型列表
     * @return 题型类型list
     */
    public List<QuestionTypes> getAllQuestionTypes(Map<String, Object> params);
    
    /**
     * 根据题型类型qt_code获取一个题型类型实体
     * @param 题型类型qt_code
     * @return 题型类型实体
     */
    public QuestionTypes getQuestionTypesByCode(int qt_code);
    
    /**
     * 添加题型类型
     * @param QuestionTypes对象
     * @return true  or false
     */
    public boolean addQuestionTypes(QuestionTypes questionTypes);
    
    /**
     * 根据题型类型qt_code更新题型类型
     * @param QuestionTypes对象 这个实体只需要qt_code就可
     * @return true or false
     */
    public boolean updateQuestionTypes(QuestionTypes questionTypes);
    
    /**
     * 根据题型类型qt_code删除这条信息
     * @param QuestionTypes对象 这个实体只需要qt_code就可
     * @return true or false
     */
    public boolean deleteQuestionTypes(QuestionTypes questionTypes);
    
    /**
     * 根据课程curriculum_code获取题型
     * @param curriculum_code
     * @return 题型类型list
     */
    public List<QuestionTypes> getQusetionTypeListByCurriculum_code(int curriculum_code);
    /**
     * 根据课程curriculum_code,start_chapter,end_chapter获取题型
     * @param curriculum_code,start_chapter,end_chapter
     * @return 题型类型list
     */
    public List<QuestionTypes> getQusetionTypeListByCodeAndChapter(int curriculum_code,int start_chapter,int end_chapter);
} 
