package com.cqjt.service;

import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.cqjt.pojo.TechnicalPost;


public interface ITechnicalPostService {
	/**
	 * 把Excel数据导入到数据库 以数据库表字段为Excel表头格式
	 * @param file
	 * @param path
	 */
   public void uploadTechnicalPostExcel(MultipartFile file, String path);
   /**
    * 获取教师信息
    * @param params
    * @return
    */
   public List<TechnicalPost> getAllTechnicalPost(Map<String, Object> params);
   
   
   /**
    * 添加教师职称信息
    * @param 教师职称信息
    * @return true  or false
    */
   public boolean addTechnicalPost(TechnicalPost technicalpost);
   
   
   /**
    * 根据教师technical_code删除这条信息
    * @param TechnicalPost这个实体只需要technical_code就可
    * @return true or false
    */
   public boolean deleteTechnicalPost(TechnicalPost technicalpost);
   
   /**
    * 根据教师technical_code更新教师职称信息
    * @param TechnicalPost对象 这个实体只需要technical_code就可
    * @return true or false
    */
   public boolean updateTechnicalPost(TechnicalPost technicalpost);
   /**
    * 根据教师technical_code获取教师职称信息
    * @param TechnicalPost对象 这个实体只需要technical_code就可
    * @return true or false
    */
   public TechnicalPost getTechnicalPostByCode(int technical_code);
}

