package com.cqjt.service;

import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.cqjt.pojo.Courseware;
import com.cqjt.pojo.Teacher;
import com.cqjt.pojo.TeacherCharacter;

public interface ITeacherCharacterService {
	/**
	 * 把Excel数据导入到数据库 以数据库表字段为Excel表头格式
	 * @param file
	 * @param path
	 */
   public void uploadTeacherCharacterExcel(MultipartFile file, String path);
   /**
    * 获取教师信息
    * @param params
    * @return
    */
   public List<TeacherCharacter> getAllTeacherCharacter(Map<String, Object> params);
   
   
   /**
    * 添加教师性质
    * @param 教师性质
    * @return true  or false
    */
   public boolean addTeacherCharacter(TeacherCharacter teachercharacter);
   
   
   /**
    * 根据教师tc_code删除这条信息
    * @param tc_code对象 这个实体只需要code就可
    * @return true or false
    */
   public boolean deleteTeacherCharacter(TeacherCharacter teachercharacter);
   
   /**
    * 根据tc_code更新教师性质
    * @param teachercharacter对象 这个实体只需要tc_code就可
    * @return true or false
    */
   public boolean updateTeacherCharacter(TeacherCharacter teachercharacter);
TeacherCharacter getTeacherCharacterByCode(int tc_code);
List<TeacherCharacter> getAllTeacherCharacter1(Map<String, Object> params);
boolean addTeacherCharacter1(TeacherCharacter TeacherCharacter);
//boolean addTeacherCharacter11(TeacherCharacter teachercharacter);
}

