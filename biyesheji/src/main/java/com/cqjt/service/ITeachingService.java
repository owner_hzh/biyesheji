package com.cqjt.service;

import java.util.List;
import java.util.Map;

import com.cqjt.pojo.Teaching;

/*
 * xiaoshujun
 */

public interface ITeachingService {
	  /*
	   * 获取整个列表
	   */
	  public List<Teaching> getAllTeaching(Map<String, Object> params);
	  
	  /*
	   * 根据老师的id获取该老师要交的课程
	   */
	  public List<Teaching> getAllTeaching(String userid);
	  
	  /*
	   * 根据课程获取老师的list
	   */
	  public List<Teaching> getAllTeacher(long curriculum_code);
	  
	  public List<Teaching> getTeachingByUserid(String userid);
	 
	  public List<Teaching> getTeachingByCurriculumCode(long curriculum_code);
	  
	  public boolean addTeaching(Teaching teaching);
	  
	  public boolean delTeaching(Teaching teaching);
	 
	  public boolean updateTeaching(Teaching teaching);
}
