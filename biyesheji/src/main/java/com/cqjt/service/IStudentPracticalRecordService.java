package com.cqjt.service;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.cqjt.pojo.StudentPracticalRecord;

public interface IStudentPracticalRecordService {

	/**
	 * 添加信息
	 * @param record StudentPracticalRecord实体
	 * @throws SQLException
	 */
	boolean insert(StudentPracticalRecord record) throws SQLException;

    /**
     * 根据主键删除信息 包含ppcode和ppnumber
     * @param record 包含ppcode和ppnumber
     * @return
     * @throws SQLException
     */
    Boolean deleteByPrimaryKey(StudentPracticalRecord record) throws SQLException;
 
    /**
     * 根据主键动态进行更新  主键包含ppcode或ppnumber
     * @param record
     * @return
     * @throws SQLException
     */
    Boolean updateByPrimaryKeySelective(StudentPracticalRecord record) throws SQLException;

    /**
     * 根据教师id查询相关的信息
     * @param record
     * @return
     * @throws SQLException
     */
    List<StudentPracticalRecord> selectByTeaUserId(StudentPracticalRecord record) throws SQLException;
    
    /**
     * 根据主键查询记录 主键包含ppcode和ppnumber
     * @param record
     * @return
     * @throws SQLException
     */
    StudentPracticalRecord selectByPrimaryKey(StudentPracticalRecord record) throws SQLException;
    
    /**
     * 可以根据你传入的参数进行查找 不限参数个数
     * @param map
     * @return
     * @throws SQLException
     */
    List<StudentPracticalRecord> selectByAny(StudentPracticalRecord record) throws SQLException;
}