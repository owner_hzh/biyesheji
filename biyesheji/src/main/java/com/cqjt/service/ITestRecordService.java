package com.cqjt.service;

import java.util.List;
import java.util.Map;

import com.cqjt.pojo.TestRecord;

/**
 * 考试记录接口
 * @author LIJIN
 *
 */
public interface ITestRecordService {
    
    /**
     * 获取考试记录(单个或者多个)
     * @return 试卷list
     */
    public List<TestRecord> getAllTestRecord(Map<String, Object> params);
    
    
    /**
     * 添加考试记录
     * @param 考试记录对象
     * @return true  or false
     */
    public Integer addTestRecord(TestRecord testRecord);
    
    
    /**
     * 根据考试记录code删除这条信息
     * @param  这个实体只需要code就可
     * @return true or false
     */
    public boolean deleteTestRecord(TestRecord testRecord);
    
    /**
     * 根据考试记录code更新考试记录信息
     * @param这个实体只需要code就可
     * @return true or false
     */
    public boolean updateTestRecord(TestRecord testRecord);
}
