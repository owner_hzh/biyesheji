package com.cqjt.service;

import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;

import com.cqjt.pojo.User;
/**
 * 导出教师学生信息的接口
 * @author LIJIN
 *
 */
public interface ExportExcelService {
	public void inExportTeacher(String [] titles,ServletOutputStream outputStream,Map<String, Object> params); //导出教师
	public void inExportStudent(String [] titles,ServletOutputStream outputStream,Map<String, Object> params); //导出学生
}
