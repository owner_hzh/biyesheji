package com.cqjt.service;

import java.util.List;
import java.util.Map;

import com.cqjt.pojo.Major;


public interface IMajorService extends BaseService{
	/**
	 * 获取所有的专业名称
	 * @return
	 */
  public List<Major> getMajorList();
  
  /**
   * 添加专业
   * @param major
   */
  public boolean addMajor(Major major);
  
  /**
   * 获取单个专业
   * @param major
   */
  public Major getMajor(Map<String, Object> params);
  
  /**
   * 动态更新专业
   * @param major
   * @return
   */
  public boolean updateMajor(Major major);
  
  /**
   * 删除专业
   * @param major
   * @return
   */
  public boolean deleteMajor(Map<String, Object> params);
  
  /**
   * 只查询专业code和专业名称
   * @return
   */
  public List<Major> getMajorListOnlyCodeAndName();
}
