package com.cqjt.service;

import java.util.List;
import java.util.Map;

import com.cqjt.pojo.SelfTestScores;

/**
 * 自测考试成绩接口
 * @author LIJIN
 *
 */
public interface ISelfTestScoresService {
    
    /**
     * 获取自测考试成绩(单个或者多个)
     * @return 试卷list
     */
    public List<SelfTestScores> getAllSelfTestScores(Map<String, Object> params);
	
    /**
	 * 获取自测考试总成绩
	 * @return 自测考试成绩list
	 */
	public SelfTestScores getTotalScores(Map<String, Object> params);
    
    /**
     * 添加自测考试成绩
     * @param 自测考试成绩对象
     * @return true  or false
     */
    public Integer addSelfTestScores(SelfTestScores selfTestScores);
    
    
    /**
     * 根据自测考试成绩code删除这条信息
     * @param  这个实体只需要code就可
     * @return true or false
     */
    public boolean deleteSelfTestScores(SelfTestScores selfTestScores);
    
    /**
     * 根据自测考试成绩code更新自测考试成绩信息
     * @param这个实体只需要code就可
     * @return true or false
     */
    public boolean updateSelfTestScores(SelfTestScores selfTestScores);
    
    /**
     * 批量更新自测考试成绩
     * @param List<SelfTestScores selfTestScores
     * @return
     */
    public boolean updateSelfTestScores(List<SelfTestScores> selfTestScores);

}
