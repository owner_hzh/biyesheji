package com.cqjt.service;

import java.util.List;
import java.util.Map;

import com.cqjt.pojo.Curriculum;
import com.cqjt.pojo.User;

/**
 * 用户页面服务接口
 * @author Owner
 *
 */
public interface IUserService {
    
    /**
     * 获取所有的用户列表
     * @return 课程list
     */
    public List<User> getAllUser(Map<String, Object> params);
    
    /**
     * 根据用户id获取一个用户
     * @param 用户id
     * @return 用户实体
     */
    public User getUserid(String id);
    
    /**
     * 添加用户
     * @param user对象
     * @return true  or false
     */
    public boolean addUser(User user);
    
    /**
     * 根据用户id更新用户
     * @param user对象 这个实体只需要id就可
     * @return true or false
     */
    public boolean updateUser(User user);
    
    /**
     * 根据用户id删除这条信息
     * @param user对象 这个实体只需要id就可
     * @return true or false
     */
    public boolean deleteUser(User user);
}
