package com.cqjt.service;

import java.util.List;
import java.util.Map;

import com.cqjt.pojo.AlternativePracticalProject;

public interface IAlternativePracticalProjectService {
	/**
     * 获取所有的实战项目备选表列表
     * @return 实战项目备选表list
     */
    public List<AlternativePracticalProject> getAllAlternativePracticalProject(Map<String, Object> params);
    
    /**
     * 根据实战项目备选表equence_number获取一个实战项目备选表实体
     * @param 实战项目备选表equence_number
     * @return 实战项目备选表实体
     */
    public AlternativePracticalProject getAlternativePracticalProjectByEquence_number(int equence_number);
    
    /**
     * 添加实战项目备选表
     * @param AlternativePracticalProject对象
     * @return true  or false
     */
    public boolean addAlternativePracticalProject(AlternativePracticalProject alternativePracticalProject);
    
    /**
     * 根据实战项目备选表code更新实战项目备选表
     * @param AlternativePracticalProject对象 这个实体只需要equence_number就可
     * @return true or false
     */
    public boolean updateAlternativePracticalProject(AlternativePracticalProject alternativePracticalProject);
    
    /**
     * 根据实战项目备选表code删除这条信息
     * @param AlternativePracticalProject对象 这个实体只需要equence_number就可
     * @return true or false
     */
    public boolean deleteAlternativePracticalProject(AlternativePracticalProject alternativePracticalProject);
}
