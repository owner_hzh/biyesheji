package com.cqjt.service;

import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.cqjt.pojo.TeacherInfor;

public interface ITeacherInforServive {
	/**
	 * 把Excel数据导入到数据库 以数据库表字段为Excel表头格式
	 * @param file
	 * @param path
	 */
   public void uploadTeacherInforExcel(MultipartFile file, String path);
   /**
    * 获取教师信息
    * @param params
    * @return
    */
   public List<TeacherInfor> getAllTeacherInfor(Map<String, Object> params);
   
   
   /**
    * 添加教师信息
    * @param 教师信息
    * @return true  or false
    */
   public boolean addTeacherInfor(TeacherInfor teacherinfor);
   
   
   /**
    * 根据教师code删除这条信息
    * @param curriculum对象 这个实体只需要code就可
    * @return true or false
    */
   public boolean deleteTeacherInfor(TeacherInfor teacherinforr);
   
   /**
    * 根据user_id更新教师信息
    * @param teacherinfor对象 这个实体只需要user_id就可
    * @return true or false
    */
   public boolean updateTeacherInfor(TeacherInfor teacherinfor);
   
   /**
    * 根据user_id更新教师信息
    * @param teacherinfor对象 这个实体只需要user_id就可
    * @return true or false
    */
   public TeacherInfor getTeacherInforByCode(String user_id);
}

