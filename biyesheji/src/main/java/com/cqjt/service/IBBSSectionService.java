package com.cqjt.service;

import java.util.List;
import java.util.Map;

import com.cqjt.pojo.BBSSection;

public interface IBBSSectionService {
	 /**
     * 获取所有的BBS板块列表
     * @return BBS板块list
     */
    public List<BBSSection> getAllBBSSection(Map<String, Object> params);
    
    /**
     * 根据BBS板块code获取一个BBS板块实体
     * @param BBS板块code
     * @return BBS板块实体
     */
    public BBSSection getBBSSectionByCode(int code);
    
    /**
     * 添加BBS板块
     * @param BBSSection对象
     * @return true  or false
     */
    public boolean addBBSSection(BBSSection bBSSection);
    
    /**
     * 根据BBS板块code更新BBS板块
     * @param BBSSection对象 这个实体只需要code就可
     * @return true or false
     */
    public boolean updateBBSSection(BBSSection bBSSection);
    
    /**
     * 根据BBS板块code删除这条信息
     * @param BBSSection对象 这个实体只需要code就可
     * @return true or false
     */
    public boolean deleteBBSSection(BBSSection bBSSection);
}
