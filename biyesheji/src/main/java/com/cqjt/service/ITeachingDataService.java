package com.cqjt.service;

import java.util.List;
import java.util.Map;

import com.cqjt.pojo.TeachingData;

public interface ITeachingDataService {
	  /**
	   * 根据课程curriculum_code获取该课程的所有的教辅资料列表
	   * @return 教辅资料list
	   */
	  public List<TeachingData> getAllTeachingData(Map<String, Object> params);
	  
	  /**
	   * 根据课程teachingdata_code获取当前教辅资料对象
	   * @return 教辅资料list
	   */
	  public TeachingData getTeachingDataByCode(int teachingdata_code);
	  /**
	   * 根据教辅资料添加一个教辅资料记录
	   * @param TeachingData对象，要完整的
	   * @return 成功(true) or 失败(false)
	   */
	  public boolean addTeachingData(TeachingData teachingData);
	  /**
	   * 根据教辅资料删除一个教辅资料记录
	   * @param TeachingData对象，要完整的
	   * @return 成功(true) or 失败(false)
	   */
	  public boolean delTeachingData(TeachingData teachingData);
	  /**
	   * 根据教辅资料给一个教辅资料的下载次数加1
	   * @param TeachingData对象
	   * @return 成功(true) or 失败(false)
	   */
	  public boolean updateTeachingData(TeachingData teachingData);
}
