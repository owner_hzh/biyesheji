package com.cqjt.service;

import java.util.List;
import java.util.Map;

import com.cqjt.pojo.Role;
/**
 * 
 * @author LIJIN
 *
 */
public interface IRoleService {
	/**
	 * 添加角色
	 * @param role
	 * @return
	 */
   public Boolean addRole(Role role);
   
   /**
    * 修改
    * @param role
    * @return
    */
   public Boolean updateRole(Role role);
   
   /**
    * 删除
    * @param role
    * @return
    */
   public Boolean deleteRole(Role role);
   
   /**
    * 查找
    * @param role
    * @return
    */
   public List<Role> getRole(Map<String, Object> params);
   
}
