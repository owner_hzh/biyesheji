package com.cqjt.service;

import java.util.List;
import java.util.Map;

import com.cqjt.pojo.Assignment;
import com.cqjt.pojo.Courseware;

/**
 * 作业页面服务接口
 * @author 见鸣
 *
 */
public interface IAssignmentService extends BaseService
{
	  /**
     * 根据课程curriculum_code获取该课程的所有的作业列表
     * @return 作业list
     */
    public List<Assignment> getAllAssignment(Map<String, Object> param);
    /**
     * 根据作业添加一个作业记录
     * @param Assignment对象，要完整的
     * @return 成功(true) or 失败(false)
     */
    public boolean addAssignment(Assignment assignment);
    /**
     * 根据作业删除一个作业记录
     * @param Assignment对象，要完整的
     * @return 成功(true) or 失败(false)
     */
    public boolean delAssignment(Assignment assignment);
    /**
     * 根据作业给一个作业的下载次数加1
     * @param Assignment对象
     * @return 成功(true) or 失败(false)
     */
    public boolean updateAssignmentDowncount(Assignment assignment);
}
