package com.cqjt.service;

import java.util.List;
import java.util.Map;

import com.cqjt.pojo.Teachingvideotape;;

/**
 * 教学录像页面服务接口
 * @author 见鸣
 *
 */
public interface ITeachingvideotapeService 
{

	  /**
   * 根据课程curriculum_code获取该课程的所有的教学录像列表
   * @return 教学录像list
   */
  public List<Teachingvideotape> getAllTeachingvideotape(Map<String, Object> param);
  /**
   * 根据教学录像添加一个教学录像记录
   * @param Teachingvideotape对象，要完整的
   * @return 成功(true) or 失败(false)
   */
  public boolean addTeachingvideotape(Teachingvideotape teachingvideotape);
  /**
   * 根据教学录像删除一个教学录像记录
   * @param Teachingvideotape对象，要完整的
   * @return 成功(true) or 失败(false)
   */
  public boolean delTeachingvideotape(Teachingvideotape teachingvideotape);
  /**
   * 根据教学录像给一个教学录像的下载次数加1
   * @param Teachingvideotape对象
   * @return 成功(true) or 失败(false)
   */
  public boolean updateTeachingvidotapeDowncount(Teachingvideotape teachingvideotape);

  /**
   * 根据教学录像代码（teachingvideotape_code）获得一个教学录像纪录
   * @param int teachingvideotape_code
   * @return Teachingvideotape
   */
  public Teachingvideotape getTeachingvideotape(int teachingvideotape_code);

}
