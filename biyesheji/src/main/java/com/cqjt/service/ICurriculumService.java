package com.cqjt.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.cqjt.pojo.BBSTopic;
import com.cqjt.pojo.Curriculum;
import com.cqjt.util.PageCondition;

/**
 * 课程页面服务接口
 * @author Owner
 *
 */
public interface ICurriculumService extends BaseService {
    
    /**
     * 获取所有的课程列表
     * @return 课程list
     */
    public List<Curriculum> getAllCurriculum(Map<String, Object> params);

	/**
	 * 分页
	 * 
	 * @param pageSize
	 *            每页显示条数
	 * @param pageNo
	 *            当前页数
	 * @param params
	 *            筛选条件
	 * @param request
	 * @return PageCondition
	 */
	public PageCondition getAllCurriculum(int pageSize, int pageNo,
			Map<String, Object> params, HttpServletRequest request);
    
    /**
     * 分页显示
     * @return 课程list
     */
    public List<Curriculum> getCurriculumByPages(Map<String, Object> params);
    
    /**
     * 根据课程code获取一个课程实体
     * @param 课程code
     * @return 课程实体
     */
    public Curriculum getCurriculumByCode(int code);
    
    /**
     * 根据课程curriculum_name获取一个课程实体
     * @param 课程curriculum_name
     * @return 课程实体
     */
    public Curriculum getCurriculumByCurriculumName(String curriculum_name);
    
    /**
     * 添加课程
     * @param curriculum对象
     * @return true  or false
     */
    public boolean addCurriculum(Curriculum curriculum);
    
    /**
     * 根据课程code更新课程
     * @param curriculum对象 这个实体只需要code就可
     * @return true or false
     */
    public boolean updateCurriculum(Curriculum curriculum);
    
    /**
     * 根据课程code删除这条信息
     * @param curriculum对象 这个实体只需要code就可
     * @return true or false
     */
    public boolean deleteCurriculum(Curriculum curriculum);
    
    /**
     * 根据教师id获取这个老师所上的所有课程列表
     * @param userid
     * @return
     */
    public List<Curriculum> getTeacherCurriculumListByUserId(String userid);
    
    /**
     * 根据教师id获取这个老师所上的所有课程列表
     * 分页拉取
     * @param userid
     * @return
     */
    public List<Curriculum> getTeacherCurriculumListByUserIdPages(Map<String, Object> params);
    
    /**
     * 获取教师Id查询的所有的条数
     * @return
     */
    public int getTeacherCurriculumCount(String userid);
    
    /**
     * 获取所有的条数
     * @return
     */
    public int getCount();
    
    public Long getPageCounts(Map<String, Object> params);
    
	public List<Curriculum> pageList(Map<String,Object> params);
}
