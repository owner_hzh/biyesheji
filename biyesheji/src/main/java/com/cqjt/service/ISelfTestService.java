package com.cqjt.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cqjt.pojo.SelfTest;

/**
 * 自测试卷接口
 * @author LIJIN
 *
 */
public interface ISelfTestService {
    
    /**
     * 获取自测试卷(单个或者多个)
     * @return 试卷list
     */
    public List<SelfTest> getAllSelfTest(Map<String, Object> params);
    
    /**
     * 获取大题的自测试卷题目
     * @return 试卷list
     */
    public List<SelfTest> getAllSelfTestBigger(Map<String, Object> params);
    
    
    /**
     * 添加自测试卷
     * @param 自测试卷对象
     * @return true  or false
     */
    public Integer addSelfTest(SelfTest selfTest);
    
    
    /**
     * 批量添加自测试卷
     * @param 自测试卷对象
     * @return true  or false
     */
    public boolean addSelfTest(List<SelfTest> selfTestList);
    
    
    /**
     * 根据自测试卷code删除这条信息
     * @param  这个实体只需要code就可
     * @return true or false
     */
    public boolean deleteSelfTest(SelfTest selfTest);
    
    /**
     * 根据自测试卷code更新自测试卷信息
     * @param这个实体只需要code就可
     * @return true or false
     */
    public boolean updateSelfTest(SelfTest selfTest);
    
    /**
     * 获取正确题数
     * @param 自测试卷对象
     * @return true  or false
     */
    public Integer getRight(Map<String, Object> params);
}
