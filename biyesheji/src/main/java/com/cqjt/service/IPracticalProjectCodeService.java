package com.cqjt.service;

import java.util.List;
import java.util.Map;

import com.cqjt.pojo.PracticalProjectCode;

public interface IPracticalProjectCodeService {
	/**
     * 获取所有的实战项目编码列表
     * @return 实战项目编码list
     */
    public List<PracticalProjectCode> getAllPracticalProjectCode(Map<String, Object> params);
    
    /**
     * 根据实战项目编码code获取一个实战项目编码实体
     * @param 实战项目编码code
     * @return 实战项目编码实体
     */
    public PracticalProjectCode getPracticalProjectCodeByCode(int code);
    
    /**
     * 添加实战项目编码
     * @param PracticalProjectCode对象
     * @return true  or false
     */
    public boolean addPracticalProjectCode(PracticalProjectCode practicalProjectCode);
    
    /**
     * 根据实战项目编码code更新实战项目编码
     * @param PracticalProjectCode对象 这个实体只需要code就可
     * @return true or false
     */
    public boolean updatePracticalProjectCode(PracticalProjectCode practicalProjectCode);
    
    /**
     * 根据实战项目编码code删除这条信息
     * @param PracticalProjectCode对象 这个实体只需要code就可
     * @return true or false
     */
    public boolean deletePracticalProjectCode(PracticalProjectCode practicalProjectCode);
}
