package com.cqjt.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.cqjt.pojo.Courseware;
import com.cqjt.pojo.Curriculum;
import com.cqjt.util.PageCondition;

/**
 * 课件接口
 * @author LIJIN
 *
 */
public interface ICoursewareService {
    
    /**
     * 获取课件(单个或者多个)
     * @return 课程list
     */
    public List<Courseware> getAllCourseware(Map<String, Object> params);
    
    
	/**
	 * 分页
	 * 
	 * @param pageSize
	 *            每页显示条数
	 * @param pageNo
	 *            当前页数
	 * @param params
	 *            筛选条件
	 * @param request
	 * @return PageCondition
	 */
	public PageCondition getAllCourseware(int pageSize, int pageNo,
			Map<String, Object> params, HttpServletRequest request);
    
    /**
     * 添加课件
     * @param 课件对象
     * @return true  or false
     */
    public boolean addCourseware(Courseware courseware);
    
    
    /**
     * 根据课件code删除这条信息
     * @param curriculum对象 这个实体只需要code就可
     * @return true or false
     */
    public boolean deleteCourseware(Courseware courseware);
    
    /**
     * 根据课件code更新课件信息
     * @param curriculum对象 这个实体只需要code就可
     * @return true or false
     */
    public boolean updateCourseware(Courseware courseware);
}
