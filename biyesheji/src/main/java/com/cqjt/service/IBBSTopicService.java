package com.cqjt.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;

import com.cqjt.pojo.BBSTopic;

public interface IBBSTopicService extends BaseService{
	/**
     * 获取所有的BBS主贴列表
     * @return BBS主贴list
     */
    public List<BBSTopic> getAllBBSTopic(Map<String, Object> params);
    
    /**
     * 根据BBS主贴code获取一个BBS主贴实体
     * @param BBS主贴code
     * @return BBS主贴实体
     */
    public BBSTopic getBBSTopicByCode(int code);
    
    /**
     * 添加BBS主贴
     * @param BBSTopic对象
     * @return true  or false
     */
    public boolean addBBSTopic(BBSTopic bBSTopic);
    
    /**
     * 根据BBS主贴code更新BBS主贴
     * @param BBSTopic对象 这个实体只需要code就可
     * @return true or false
     */
    public boolean updateBBSTopic(BBSTopic bBSTopic);
    
    /**
     * 根据BBS主贴code删除这条信息
     * @param BBSTopic对象 这个实体只需要code就可
     * @return true or false
     */
    public boolean deleteBBSTopic(BBSTopic bBSTopic);

	public Long getPageCounts(Map<String, Object> params);
    
	public List<BBSTopic> pageList(Map<String,Object> params);
  
    
    
}
