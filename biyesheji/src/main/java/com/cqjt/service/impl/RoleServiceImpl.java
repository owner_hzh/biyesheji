package com.cqjt.service.impl;

import java.util.List;
import java.util.Map;

import javax.jws.WebService;

import com.cqjt.pojo.Role;
import com.cqjt.service.IRoleService;


//@Service(value="roleService")
@WebService(endpointInterface = "com.cqjt.service.IRoleService", serviceName = "roleService")
public class RoleServiceImpl extends BaseServiceImpl implements IRoleService {

	/**
	 * 添加角色
	 * @param role
	 * @return
	 */
	@Override
   public Boolean addRole(Role role){
		String sqlName = "addRole";
		Boolean flag=(Boolean) super.insert(sqlName, role);//roleDAO.addRole(role);
		return flag;
	}
   
   /**
    * 修改
    * @param role
    * @return
    */
	@Override
   public Boolean updateRole(Role role){
	return null;
		
	}
   
   /**
    * 删除
    * @param role
    * @return
    */
	@Override
   public Boolean deleteRole(Role role){
	return null;
		
	}
   
   /**
    * 查找
    * @param role
    * @return
    */
	@Override
   public List<Role> getRole(Map<String, Object> params){
		String sqlName = "getRole";
		List<Role> roleList = (List<Role>) super.select(sqlName, params);
	return roleList;
		
	}
}
