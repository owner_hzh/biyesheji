package com.cqjt.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cqjt.pojo.PracticalProjectDocument;
import com.cqjt.service.IPracticalProjectDocumentService;
import com.cqjt.util.FileOperation;

public class PracticalProjectDocumentServiceImpl extends BaseServiceImpl implements IPracticalProjectDocumentService{

	@Override
	public List<PracticalProjectDocument> getAllPracticalProjectDocumentByCodeAndNum
	(
			Map<String, Object> params) {
		// TODO Auto-generated method stub
		String sqlName = "getPracticalProjectDocumentByCodeAndNum" ;
		@SuppressWarnings("unchecked")
		List<PracticalProjectDocument> practicalProjects = (List<PracticalProjectDocument>) super.select(sqlName, params);
		return practicalProjects;
	}

	@Override
	public PracticalProjectDocument getPracticalProjectDocumentByCode(int code) {
		// TODO Auto-generated method stub
		String sqlName = "getPracticalProjectDocumentByCode";
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("ppcode", code);
		PracticalProjectDocument practicalProject = (PracticalProjectDocument) super.select(sqlName, params);
		return practicalProject;
	}

	@Override
	public boolean addPracticalProjectDocument(PracticalProjectDocument practicalProject) {
		// TODO Auto-generated method stub
		String sqlName = "addPracticalProjectDocument" ;
		Object o=super.insert(sqlName, practicalProject);
		return o==null?false:true;
	}

	@Override
	public boolean updatePracticalProjectDocument(PracticalProjectDocument practicalProject) {
		// TODO Auto-generated method stub
		String sqlName = "updatePracticalProjectDocument" ;
		Object o=super.update(sqlName, practicalProject);
		return o==null?false:true;
	}

	@Override
	public boolean deletePracticalProjectDocument(Map<String, Object> params) {
		// TODO Auto-generated method stub
		String sqlName = "deletePracticalProjectDocument" ;
		//HashMap<String, Object> params=new HashMap<String, Object>();
		//params.put("ppcode", practicalProject.getPpcode());
		Object o=super.delete(sqlName, params);
		return o==null?false:true;
	}

	@Override
	public boolean updatePracticalProjectDocumentDownCount(
			Map<String, Object> params) {
		String sqlName = "updatePracticalProjectDocumentDownCount" ;
		Object o=super.update(sqlName, params);
		return o==null?false:true;
	}

	@Override
	public boolean deleteRelatedDoc(String docPath) 
	{
		return FileOperation.DeleteFolder(docPath);	
	}

}
