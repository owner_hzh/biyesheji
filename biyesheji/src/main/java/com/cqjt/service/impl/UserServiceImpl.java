package com.cqjt.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.cqjt.pojo.Curriculum;
import com.cqjt.pojo.User;
import com.cqjt.service.ICurriculumService;
import com.cqjt.service.IUserService;
/**
 * 
 * @author LIJIN
 *
 */

@Service(value="userService")
public class UserServiceImpl extends BaseServiceImpl implements IUserService {

    /**
     * 获取所有的用户列表
     * @return 用户list
     */
	@Override
	public List<User> getAllUser(Map<String, Object> params) {
		// TODO Auto-generated method stub
		@SuppressWarnings("unchecked")
		List<User> uList = (List<User>) super.select("getUser", params);
		return uList;
	}

	
    /**
     * 根据用户id获取一个用户
     * @param 用户id
     * @return 用户实体
     */
	@Override
	public User getUserid(String id) {
		// TODO Auto-generated method stub
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("user_id", id);
		User user = (User) super.find("getUser", params);
		return user;
	}

	
    /**
     * 添加用户
     * @param user对象
     * @return true  or false
     */
	@Override
	public boolean addUser(User user) {
		// TODO Auto-generated method stub
		return false;
	}

	
    /**
     * 根据用户id更新用户
     * @param user对象 这个实体只需要id就可
     * @return true or false
     */
	@Override
	public boolean updateUser(User user) 
	{
		String sql="updateuser";
		Object o=super.update(sql, user);
		return o==null?false:true;
	}
	
    
    /**
     * 根据用户id删除这条信息
     * @param user对象 这个实体只需要id就可
     * @return true or false
     */

	@Override
	public boolean deleteUser(User user) {
		// TODO Auto-generated method stub
		return false;
	}

}
