package com.cqjt.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.cqjt.pojo.TestCode;
import com.cqjt.pojo.TestCustomize;
import com.cqjt.pojo.TestRecord;
import com.cqjt.service.ITestCodeService;
import com.cqjt.service.ITestCustomizeService;
import com.cqjt.service.ITestRecordService;

/**
 * 考试记录接口实现
 * @author LIJIN
 *
 */

@Service(value="testRecordService")
//@WebService(endpointInterface = "com.cqjt.service.ICurriculumService", serviceName = "curriculumService")
public class TestRecordServiceImpl extends BaseServiceImpl implements ITestRecordService {

    /**
     * 获取考试记录
     * @return 考试记录list
     */
	@Override
	public List<TestRecord> getAllTestRecord(Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		List<TestRecord> cwList=(List<TestRecord>) super.select("gettestrecord", params);
		return cwList;
	}

    
    /**
     * 添加考试记录
     * @param 考试记录对象
     * @return true  or false
     */
	@Override
	public Integer addTestRecord(TestRecord testRecord) {
		Integer o=(Integer) super.insert("addtestrecord", testRecord);
		return o;
	}



    /**
     * 根据考试记录code删除这条信息
     * @param TestRecord_code
     * @return true or false
     */
	@Override
	public boolean deleteTestRecord(TestRecord testRecord) {
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("test_code", testRecord.getTest_code());
		Integer o=super.delete("deletetestrecord", params);
		return o==0?false:true;
	}

	
    /**
     * 根据考试记录code更新考试记录信息
     * @param 这个实体只需要code就可
     * @return true or false
     */
    public boolean updateTestRecord(TestRecord testRecord){
    	return super.update("updatetestrecord", testRecord);
    }
}
