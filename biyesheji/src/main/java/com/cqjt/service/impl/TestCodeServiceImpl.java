package com.cqjt.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.cqjt.pojo.TestCode;
import com.cqjt.service.ITestCodeService;

/**
 * 定制试卷接口实现
 * @author LIJIN
 *
 */

@Service(value="testCodeService")
//@WebService(endpointInterface = "com.cqjt.service.ICurriculumService", serviceName = "curriculumService")
public class TestCodeServiceImpl extends BaseServiceImpl implements ITestCodeService {

    /**
     * 获取定制试卷
     * @return 定制试卷list
     */
	@Override
	public List<TestCode> getAllTestCode(Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		List<TestCode> cwList=(List<TestCode>) super.select("gettestcode", params);
		return cwList;
	}

    
    /**
     * 添加定制试卷
     * @param 定制试卷对象
     * @return true  or false
     */
	@Override
	public int addTestCode(TestCode testCode) {
		int test_code=(Integer) super.insert("addtestcode", testCode);
		return test_code;
	}



    /**
     * 根据定制试卷code删除这条信息
     * @param TestCode_code
     * @return true or false
     */
	@Override
	public boolean deleteTestCode(TestCode testCode) {
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("number", testCode.getNumber());
		Integer o=super.delete("deletetestcode", params);
		return o==0?false:true;
	}

	
    /**
     * 根据定制试卷code更新定制试卷信息
     * @param 这个实体只需要code就可
     * @return true or false
     */
    public boolean updateTestCode(TestCode testCode){
    	return super.update("updatetestcode", testCode);
    }
}
