package com.cqjt.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cqjt.pojo.BBSSection;
import com.cqjt.service.IBBSSectionService;

public class BBSSectionServiceImpl extends BaseServiceImpl implements IBBSSectionService{

	@Override
	public List<BBSSection> getAllBBSSection(Map<String, Object> params) {
		// TODO Auto-generated method stub
		@SuppressWarnings("unchecked")
		List<BBSSection> list=(List<BBSSection>) super.select("getAllBBSSection", params);
		return list;
	}

	@Override
	public BBSSection getBBSSectionByCode(int code) {
		// TODO Auto-generated method stub
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("tid", code);
		BBSSection b=(BBSSection) super.find("getBBSSectionByCode", params);
		return b;
	}

	@Override
	public boolean addBBSSection(BBSSection bBSSection) {
		// TODO Auto-generated method stub	
		Object o=super.insert("addBBSSection", bBSSection);		
		return o==null?false:true;
	}

	@Override
	public boolean updateBBSSection(BBSSection bBSSection) {
		// TODO Auto-generated method stub
		Object o=super.update("updateBBSSection", bBSSection);
		return o==null?false:true;
	}

	@Override
	public boolean deleteBBSSection(BBSSection bBSSection) {
		// TODO Auto-generated method stub
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("sid", bBSSection.getSid());
		Object o=super.delete("deleteBBSSection", params);
		return o==null?false:true;
	}
	
}
