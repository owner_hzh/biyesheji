package com.cqjt.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cqjt.pojo.QuestionTypes;
import com.cqjt.service.IQuestionTypesService;

public class QuestionTypesServiceImpl extends BaseServiceImpl implements IQuestionTypesService{

	@Override
	public List<QuestionTypes> getAllQuestionTypes(Map<String, Object> params) {
		// TODO Auto-generated method stub
		@SuppressWarnings("unchecked")
		List<QuestionTypes> list = (List<QuestionTypes>) super.select("getAllQuestionTypes", params); 
		return list;
	}

	@Override
	public QuestionTypes getQuestionTypesByCode(int qt_code) {
		// TODO Auto-generated method stub
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("qt_code", qt_code);
		QuestionTypes questionTypes = (QuestionTypes) super.find("getQuestionTypesByCode", params);
		return questionTypes;
	}

	@Override
	public boolean addQuestionTypes(QuestionTypes questionTypes) {
		// TODO Auto-generated method stub
		Object o=super.insert("addQuestionTypes",questionTypes);
		return o==null?false:true;
	}

	@Override
	public boolean updateQuestionTypes(QuestionTypes questionTypes) {
		// TODO Auto-generated method stub
		Object o=super.update("updateQuestionTypes",questionTypes);
		return o==null?false:true;
	}

	@Override
	public boolean deleteQuestionTypes(QuestionTypes questionTypes) {
		// TODO Auto-generated method stub
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("qt_code", questionTypes.getQt_code());
		Object o=super.delete("deleteQuestionTypes",params);
		return o==null?false:true;
	}

	@Override
	public List<QuestionTypes> getQusetionTypeListByCurriculum_code(
			int curriculum_code) 
	{
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("curriculum_code",curriculum_code);
		@SuppressWarnings("unchecked")
		List<QuestionTypes> list = (List<QuestionTypes>) super.select("getQusetionTypeListByCurriculum_code",params);
		return list;
	}

	@Override
	public List<QuestionTypes> getQusetionTypeListByCodeAndChapter(
			int curriculum_code, int start_chapter, int end_chapter) {
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("curriculum_code",curriculum_code);
		params.put("start_chapter",start_chapter);
		params.put("end_chapter",end_chapter);
		@SuppressWarnings("unchecked")
		List<QuestionTypes> list = (List<QuestionTypes>) super.select("getQusetionTypeListByCurriculum_code",params);
		return list;
	}
	
}
