package com.cqjt.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cqjt.pojo.AlternativePracticalProject;
import com.cqjt.service.IAlternativePracticalProjectService;

public class AlternativePracticalProjectServiceImpl extends BaseServiceImpl 
	implements IAlternativePracticalProjectService{

	@Override
	public List<AlternativePracticalProject> getAllAlternativePracticalProject(
			Map<String, Object> params) {
		// TODO Auto-generated method stub
		String sqlName = "getAllAlternativePracticalProject";
		@SuppressWarnings("unchecked")
		List<AlternativePracticalProject> alternativePracticalProjects =
			(List<AlternativePracticalProject>) super.select(sqlName, params);
		return alternativePracticalProjects;
		
	}

	@Override
	public AlternativePracticalProject getAlternativePracticalProjectByEquence_number(
			int equence_number) {
		// TODO Auto-generated method stub
		String sqlName = "getAlternativePracticalProjectByEquence_number";
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("equence_number", equence_number);
		AlternativePracticalProject alternativePracticalProject = (AlternativePracticalProject) super.find(sqlName, params);
		return alternativePracticalProject;
	}

	@Override
	public boolean addAlternativePracticalProject(
			AlternativePracticalProject alternativePracticalProject) {
		// TODO Auto-generated method stub
		Object o=super.insert("addAlternativePracticalProject", alternativePracticalProject);
		return o==null?false:true;
	}

	@Override
	public boolean updateAlternativePracticalProject(
			AlternativePracticalProject alternativePracticalProject) {
		// TODO Auto-generated method stub
		Object o=super.update("updateAlternativePracticalProject", alternativePracticalProject);
		return o==null?false:true;
	}

	@Override
	public boolean deleteAlternativePracticalProject(
			AlternativePracticalProject alternativePracticalProject) {
		// TODO Auto-generated method stub
		Object o=super.update("deleteAlternativePracticalProject", alternativePracticalProject);
		return o==null?false:true;
	}
}
