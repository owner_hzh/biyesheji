package com.cqjt.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cqjt.pojo.Teaching;
import com.cqjt.service.ITeachingService;

/*
 * xiaoshujun
 */

public class TeachingServiceImpl extends BaseServiceImpl implements ITeachingService{

	@Override
	public List<Teaching> getAllTeaching(Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		List<Teaching> list = (List<Teaching>) super.select("getAllTeaching", params);
		return list;
	}

	@Override
	public List<Teaching> getAllTeaching(String userid) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("userid", userid);
		@SuppressWarnings("unchecked")
		List<Teaching> list = (List<Teaching>) super.select("getAllTeaching", params);
		return list;
	}

	@Override
	public List<Teaching> getAllTeacher(long curriculum_code) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("curriculum_code", curriculum_code);
		@SuppressWarnings("unchecked")
		List<Teaching> list = (List<Teaching>) super.select("getAllTeaching", params);
		return list;
	}

	@Override
	public List<Teaching> getTeachingByUserid(String userid) {
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("user_id", userid);
		@SuppressWarnings("unchecked")
		List<Teaching> list=  (List<Teaching>) super.select("getAllTeachingByUserID", params);
		System.out.println("service:size  "+list.size());
		return list;
	}

	@Override
	public List<Teaching> getTeachingByCurriculumCode(long curriculum_code) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("curriculum_code", curriculum_code);
		@SuppressWarnings("unchecked")
		List<Teaching> list=  (List<Teaching>) super.select("getAllTeachingByCurriculumcode", params);
		return list;
	}

	@Override
	public boolean addTeaching(Teaching teaching) {
		Object o=super.insert("addTeaching", teaching);
		return o==null?false:true;
	}

	@Override
	public boolean delTeaching(Teaching teaching) {
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("userid", teaching.getUser_id());
		params.put("curriculum_code", teaching.getCurriculum_code());
		Object o=super.delete("deleteTeaching", params);
		return o==null?false:true;
	}

	@Override
	public boolean updateTeaching(Teaching teaching) {
		Object o=super.update("updateTeaching", teaching);
		return o==null?false:true;
	}

}
