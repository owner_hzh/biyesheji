package com.cqjt.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.cqjt.pojo.TeacherInfor;
import com.cqjt.service.ITeacherInforServive;

public class TeacherInforServiceImpl extends BaseServiceImpl implements ITeacherInforServive{

	@Override
	public List<TeacherInfor> getAllTeacherInfor(Map<String, Object> params) {
		// TODO Auto-generated method stub
		@SuppressWarnings("unchecked")
		List<TeacherInfor> list = (List<TeacherInfor>) super.select("getAllTeacherInfor", params); 
		return list;
	}

	@Override
	public TeacherInfor getTeacherInforByCode(String user_id) {
		// TODO Auto-generated method stub
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("user_id", user_id);
		TeacherInfor TeacherInfor = (TeacherInfor) super.find("getTeacherInforByCode", params);
		return TeacherInfor;
	}

	@Override
	public boolean addTeacherInfor(TeacherInfor teacherinfor) {
		// TODO Auto-generated method stub
		Object o=super.insert("addTeacherInfor",teacherinfor);
		return o==null?false:true;
	}

	@Override
	public boolean updateTeacherInfor(TeacherInfor teacherinfor) {
		// TODO Auto-generated method stub
		Object o=super.update("updateTeacherInfor",teacherinfor);
		return o==null?false:true;
	}

	@Override
	public boolean deleteTeacherInfor(TeacherInfor teacherinfor) {
		// TODO Auto-generated method stub
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("user_id", teacherinfor.getUser_id());
		Object o=super.delete("deleteTeacherInfor",params);
		return o==null?false:true;
	}

	@Override
	public void uploadTeacherInforExcel(MultipartFile file, String path) {
		// TODO Auto-generated method stub
		
	}
	
}
