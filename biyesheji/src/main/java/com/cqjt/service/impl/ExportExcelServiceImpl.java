package com.cqjt.service.impl;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jws.WebService;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.poi.xssf.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;

import com.cqjt.pojo.Student;
import com.cqjt.pojo.Teacher;
import com.cqjt.pojo.User;
import com.cqjt.service.ExportExcelService;
import com.cqjt.service.IStudentService;
import com.cqjt.service.ITeacherService;
import com.cqjt.service.IUserService;
import com.cqjt.util.ExportUtil;
/**
 * 导出教师学生信息的实现接口
 * @author LIJIN
 *
 */

@WebService(endpointInterface = "com.yapai.app.service.ExportExcelService", serviceName = "exportExcelService")
public class ExportExcelServiceImpl extends BaseServiceImpl implements ExportExcelService {
	// 注入用户的service
	@Autowired
	private IUserService userService;
	//注入教师的Service
	@Autowired
	private ITeacherService teacherService;
	//注入学生的Service
	@Autowired
	private IStudentService studentService;
	
	//处理收尾工作的公用方法
	public void outexcel(XSSFWorkbook workBook,ServletOutputStream outputStream){
        try  
        {  
        	//String outputFile="D:\\sd.xls";
            // 新建一输出文件流
           // FileOutputStream fOut = new FileOutputStream(outputFile);
            //workBook.write(fOut);  
            workBook.write(outputStream);  
            outputStream.flush();  
            outputStream.close();  
        }  
        catch (IOException e)  
        {  
            e.printStackTrace();  
        }  
        finally  
        {  
            try  
            {  
                outputStream.close();  
            }  
            catch (IOException e)  
            {  
                e.printStackTrace();  
            }  
        }  
	}
	
	
	
	/**
	 * 将教师写入Excel表
	 */
	@Override
    public void inExportTeacher(String[] titles, ServletOutputStream outputStream ,Map<String, Object> params)  
    {  
		List<Teacher> tList = teacherService.exportTeachers(params);
        // 创建一个workbook 对应一个excel应用文件  
        XSSFWorkbook workBook = new XSSFWorkbook();  
        // 在workbook中添加一个sheet,对应Excel文件中的sheet  
        XSSFSheet sheet = workBook.createSheet("导出excel");  
        ExportUtil exportUtil = new ExportUtil(workBook, sheet);  
        XSSFCellStyle headStyle = exportUtil.getHeadStyle();  
        XSSFCellStyle bodyStyle = exportUtil.getBodyStyle();  
        // 构建表头  
        XSSFRow headRow = sheet.createRow(0); 
        headRow.setHeight((short) 800);
        XSSFCell cell = null;  
        for (int i = 0; i < titles.length; i++)  
        {  
            cell = headRow.createCell(i);  
            cell.setCellStyle(headStyle);  
            cell.setCellValue(titles[i]);  
        }  
        // 构建表体数据  
        if (tList != null && tList.size() > 0)  
        {  
            for (int j = 0; j < tList.size(); j++)  
            {  
                XSSFRow bodyRow = sheet.createRow(j + 1); 
                bodyRow.setHeight((short) 700);
                Teacher teacher = tList.get(j);  
  
                sheet.setColumnWidth(0, 5500);
                cell = bodyRow.createCell(0); 
                cell.setCellStyle(bodyStyle);  
                cell.setCellValue(teacher.getUser_id().toString());//教师id 
                
                sheet.setColumnWidth(1, 5500);
                cell = bodyRow.createCell(1);  
                cell.setCellStyle(bodyStyle);  
                cell.setCellValue(teacher.getTeacher_name());  //教师姓名
                
                sheet.setColumnWidth(2, 5500);
                cell = bodyRow.createCell(2);  
                cell.setCellStyle(bodyStyle);  
                cell.setCellValue(teacher.getRole_code());  //角色
  
                sheet.setColumnWidth(3, 5500);
                cell = bodyRow.createCell(3);  
                cell.setCellStyle(bodyStyle);  
                cell.setCellValue(teacher.getTechnical_code());  //教师职称
                
                sheet.setColumnWidth(4, 5500);
                cell = bodyRow.createCell(4);  
                cell.setCellStyle(bodyStyle);  
                cell.setCellValue(teacher.getTc_code());  //教师性质
                
                sheet.setColumnWidth(5, 5500);
                cell = bodyRow.createCell(5);  
                cell.setCellStyle(bodyStyle);  
                cell.setCellValue(teacher.getTeacher_sex());  //教师性别
                
                sheet.setColumnWidth(6, 5500);
                cell = bodyRow.createCell(6);  
                cell.setCellStyle(bodyStyle);
                if(teacher.getTeacher_date()!=null){//出生日期
                	cell.setCellValue(teacher.getTeacher_date().toString());  
                }else{
                	cell.setCellValue("暂无");
                }
            }  
        }
        
        //处理收尾工作
        outexcel(workBook,outputStream);
    }
	
	/**
	 * 将学生表写入Excel表
	 */
	@Override
	public void inExportStudent(String[] titles, ServletOutputStream outputStream ,Map<String, Object> params) {
		// TODO Auto-generated method stub
		List<Student> list =studentService.getAllStudents(params);
		// 创建一个workbook 对应一个excel应用文件  
		XSSFWorkbook workBook = new XSSFWorkbook();  
		// 在workbook中添加一个sheet,对应Excel文件中的sheet  
		XSSFSheet sheet = workBook.createSheet("导出excel");  
		ExportUtil exportUtil = new ExportUtil(workBook, sheet);  
		XSSFCellStyle headStyle = exportUtil.getHeadStyle();  
		XSSFCellStyle bodyStyle = exportUtil.getBodyStyle();  
		// 构建表头  
		XSSFRow headRow = sheet.createRow(0); 
		headRow.setHeight((short) 800);
		XSSFCell cell = null;  
		for (int i = 0; i < titles.length; i++)  
		{  
			cell = headRow.createCell(i);  
			cell.setCellStyle(headStyle);  
			cell.setCellValue(titles[i]);  
		}  
		// 构建表体数据  
		if (list != null && list.size() > 0)  
		{  
			for (int j = 0; j < list.size(); j++)  
			{  
				XSSFRow bodyRow = sheet.createRow(j + 1);
				bodyRow.setHeight((short) 700);
				Student student = list.get(j); 
				//学号
				sheet.setColumnWidth(0, 2500);
				cell = bodyRow.createCell(0);
				cell.setCellStyle(bodyStyle);
				cell.setCellValue(student.getUser_id().toString());
				
				//学生姓名
				sheet.setColumnWidth(1, 5500);
				cell = bodyRow.createCell(1);  
				cell.setCellStyle(bodyStyle);  
				cell.setCellValue(student.getStudent_name());  
				
				//学生角色
				sheet.setColumnWidth(2, 5500);
				cell = bodyRow.createCell(2);  
				cell.setCellStyle(bodyStyle);  
				cell.setCellValue(student.getRole_code());  
				
				//所属专业
				sheet.setColumnWidth(3, 2500);
				cell = bodyRow.createCell(3);  
				cell.setCellStyle(bodyStyle); 
				cell.setCellValue(student.getMajor_code());  
				
				//所属年级
				sheet.setColumnWidth(4, 2500);
				cell = bodyRow.createCell(4);  
				cell.setCellStyle(bodyStyle);  
				cell.setCellValue(student.getGrade()); 
				
				//学生性别
				sheet.setColumnWidth(5, 5500);
				cell = bodyRow.createCell(5);  
				cell.setCellStyle(bodyStyle);  
				cell.setCellValue(student.getStudent_sex());  
				
				//毕业时间
				sheet.setColumnWidth(6, 5500);
				cell = bodyRow.createCell(6);  
				cell.setCellStyle(bodyStyle);  
				cell.setCellValue(student.getGradute_date());  
			} 
		}
		System.out.println("导出完毕");
		//处理收尾工作
		outexcel(workBook,outputStream);
	} 
	
}
