package com.cqjt.service.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cqjt.pojo.StudentPracticalRecord;
import com.cqjt.service.IStudentPracticalRecordService;

public class StudentPracticalRecordServiceImpl extends BaseServiceImpl implements IStudentPracticalRecordService{

	@Override
	public boolean insert(StudentPracticalRecord record) throws SQLException {
		Object success=super.insert("studentpracticalrecord.abatorgenerated_insert", record);
		return success==null?false:true;
	}

	@Override
	public Boolean updateByPrimaryKeySelective(StudentPracticalRecord record)
			throws SQLException {
		boolean f=super.update("studentpracticalrecord.abatorgenerated_updateByPrimaryKeySelective", record);
		return f;
	}

	@Override
	public Boolean deleteByPrimaryKey(StudentPracticalRecord record)
			throws SQLException {
		Map<String, Object> params=new HashMap<String, Object>();
		params.put("ppcode", record.getPpcode());
		params.put("ppnumber", record.getPpnumber());
		params.put("record_code", record.getRecord_code());
		int i=super.delete("studentpracticalrecord.abatorgenerated_deleteByPrimaryKey", params);
		return i==1?true:false;
	}

	@Override
	public List<StudentPracticalRecord> selectByTeaUserId(
			StudentPracticalRecord record) throws SQLException {
		Map<String, Object> params=new HashMap<String, Object>();
		params.put("tea_user_id", record.getTea_user_id());
		 List<StudentPracticalRecord> l=(List<StudentPracticalRecord>) super.select("studentpracticalrecord.selectByTeaUserId", params);
		return l;
	}

	@Override
	public StudentPracticalRecord selectByPrimaryKey(
			StudentPracticalRecord record) throws SQLException {
		Map<String, Object> params=new HashMap<String, Object>();
		params.put("ppcode", record.getPpcode());
		params.put("ppnumber", record.getPpnumber());
		params.put("record_code", record.getRecord_code());
		StudentPracticalRecord s=(StudentPracticalRecord) super.find("studentpracticalrecord.abatorgenerated_selectByPrimaryKey", params);
		return s;
	}

	@Override
	public List<StudentPracticalRecord> selectByAny(StudentPracticalRecord record)
			throws SQLException {
		Map<String, Object> params=new HashMap<String, Object>();
		/*if(record.getAnswer_name()!=null)
		{
			params.put("answer_name", record.getAnswer_name());
		}
		if(record.getLink()!=null)
		{
			params.put("link", record.getLink());
		}
		if(record.getUser_id()!=null)
		{
			params.put("user_id", record.getUser_id());
		}
		if(record.getTea_user_id()!=null)
		{
			params.put("tea_user_id", record.getTea_user_id());
		}
		if(record.getDownload_date()!=null)
		{
			params.put("download_date", record.getDownload_date());
		}
		if(record.getUpload_date()!=null)
		{
			params.put("upload_date", record.getUpload_date());
		}
		if(record.getSelf_score()!=null)
		{
			params.put("self_score", record.getSelf_score());
		}
		params.put("teacher_score", record.getTeacher_score());
		if(record.getComment()!=null)
		{
			params.put("comment", record.getComment());
		}*/
		params.put("user_id", record.getUser_id());
		params.put("is_audit", record.getIs_audit());
		List<StudentPracticalRecord> l=(List<StudentPracticalRecord>) super.select("studentpracticalrecord.selectByAny", params);
		return l;
	}

}
