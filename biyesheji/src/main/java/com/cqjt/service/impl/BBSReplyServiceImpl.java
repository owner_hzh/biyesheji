package com.cqjt.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cqjt.pojo.BBSReply;
import com.cqjt.pojo.BBSReply;
import com.cqjt.service.IBBSReplyService;

public class BBSReplyServiceImpl extends BaseServiceImpl implements IBBSReplyService{

	@Override
	public List<BBSReply> getAllBBSReply(Map<String, Object> params) {
		// TODO Auto-generated method stub
		@SuppressWarnings("unchecked")
		List<BBSReply> list=(List<BBSReply>) super.select("getAllBBSReply", params);
		return list;
	}

	@Override
	public BBSReply getBBSReplyByCode(int code) {
		// TODO Auto-generated method stub
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("sid", code);
		BBSReply b=(BBSReply) super.find("getBBSReplyByCode", params);
		return b;
	}

	@Override
	public boolean addBBSReply(BBSReply bBSReply) {
		// TODO Auto-generated method stub
		Object o=super.insert("addBBSReply", bBSReply);		
		return o==null?false:true;
	}

	@Override
	public boolean updateBBSReply(BBSReply bBSReply) {
		Object o=super.update("updateBBSReply", bBSReply);
		return o==null?false:true;
	}

	@Override
	public boolean deleteBBSReply(BBSReply bBSReply) {
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("tid", bBSReply);
		Object o=super.delete("deleteBBSReply", params);
		return o==null?false:true;
	}

	@Override
	public Long getPageCounts(Map<String, Object> params) {
		Long count=(long) super.getCount("bbsreply_pageCounts", params);
		return count;
	}

	@Override
	public List<BBSReply> pageList(Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		List<BBSReply> list=(List<BBSReply>) super.select("bbsreply_pageList", params);
		return list;
	}
	

}
