package com.cqjt.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Service;
import com.cqjt.pojo.BBSTopic;
import com.cqjt.service.IBBSTopicService;

public class BBSTopicServiceImpl extends BaseServiceImpl implements IBBSTopicService{
	
	@Override
	public List<BBSTopic> getAllBBSTopic(Map<String, Object> params) {
		// TODO Auto-generated method stub
		@SuppressWarnings("unchecked")
		List<BBSTopic> list=(List<BBSTopic>) super.select("getAllBBSTopic", params);
		return list;
	}

	@Override
	public BBSTopic getBBSTopicByCode(int code) {
		// TODO Auto-generated method stub
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("tid", code);
		BBSTopic b=(BBSTopic) super.find("getBBSTopicByCode", params);
		return b;
	}

	@Override
	public boolean addBBSTopic(BBSTopic bBSTopic) {
		// TODO Auto-generated method stub
		Object o=super.insert("addBBSTopic", bBSTopic);		
		return o==null?false:true;
	}

	@Override
	public boolean updateBBSTopic(BBSTopic bBSTopic) {
		// TODO Auto-generated method stub
		Object o=super.update("updateBBSTopic", bBSTopic);
		return o==null?false:true;
	}

	@Override
	public boolean deleteBBSTopic(BBSTopic bBSTopic) {
		// TODO Auto-generated method stub
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("tid", bBSTopic.getTid());
		Object o=super.delete("deleteBBSTopic", params);
		return o==null?false:true;
	}
	@Override
	public Long getPageCounts(Map<String, Object> params) {
		Long count=(long) super.getCount("bbstopic_pageCounts", params);
		return count;
	}

	@Override
	public List<BBSTopic> pageList(Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		List<BBSTopic> list=(List<BBSTopic>) super.select("bbstopic_pageList", params);
		return list;
	}
}
