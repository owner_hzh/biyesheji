package com.cqjt.service.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.cqjt.pojo.Curriculum;
import com.cqjt.service.ICurriculumService;
import com.cqjt.util.PageCondition;
import com.cqjt.util.PageNameEnum;
import com.cqjt.util.PageUtil;


/**
 * 大纲服务层
 * @author Owner
 *
 */

@Service(value="curriculumService")
//@WebService(endpointInterface = "com.cqjt.service.ICurriculumService", serviceName = "curriculumService")
public class CurriculumServiceImpl extends BaseServiceImpl implements ICurriculumService {

	@Override
	public List<Curriculum> getAllCurriculum(Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		List<Curriculum> coList=(List<Curriculum>) super.select("getAllCurriculum", params);
		return coList;
	}
	
	/**
	 * 分页
	 * 
	 * @param pageSize
	 *            每页显示条数
	 * @param pageNo
	 *            当前页数
	 * @param params
	 *            筛选条件
	 * @param request
	 * @return PageCondition
	 */
	public PageCondition getAllCurriculum(int pageSize, int pageNo,
			Map<String, Object> params, HttpServletRequest request){
		try {
			int total = super.getCount("countcurriculum", params);

			params.put("startNo", pageSize * (pageNo - 1));
			params.put("pageSize", pageSize);
			List dataList = super.select("getcurriculumforpage", params);

			PageCondition pagecondition = new PageCondition();
			String path = request.getContextPath();
			String basePath = request.getScheme() + "://"
					+ request.getServerName() + ":" + request.getServerPort()
					+ path + "/";
			//处理查询条件
			Object major_name = params.get("major_name");
			//拼接链接
			if(major_name!=null&&!(major_name+"").equals("")){
				try {
					major_name = URLEncoder.encode(URLEncoder.encode(major_name+"","utf-8"),"utf-8");
					pagecondition.setShow(PageUtil.setPageHtmlStr(total, PageNameEnum.PAGESIZE, pageNo,
							basePath + "curriculum/show?major_name="+major_name,PageNameEnum.SHOWA, true));
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}else{//在线测试部分
				Object major_code = params.get("major_code");
				Object type = params.get("type");
				Object id = params.get("id");
				pagecondition.setShow(PageUtil.setPageHtmlStr(total, PageNameEnum.PAGESIZE, pageNo,
						basePath + "online/majorcurriculum?type="+type+"&id="+id+"&major_code="+major_code,PageNameEnum.SHOWA, true));

			}
			pagecondition.setDataList(dataList);
			pagecondition.settotalCount(total);
			return pagecondition;
		} catch (DataAccessException datae) {
			datae.printStackTrace();
			return null;
		}
	}


	@Override
	public Curriculum getCurriculumByCode(int code) {
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("curriculum_code", code);
		Curriculum c=(Curriculum) super.find("getCurriculumByCode", params);
		return c;
	}


	@Override
	public boolean addCurriculum(Curriculum curriculum) {
		Object o=super.insert("addCurriculum", curriculum);
		return o==null?false:true;
	}


	@Override
	public boolean updateCurriculum(Curriculum curriculum) {
		Object o=super.update("updateCurriculum", curriculum);
		return o==null?false:true;
	}


	@Override
	public boolean deleteCurriculum(Curriculum curriculum) {
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("curriculum_code", curriculum.getCurriculum_code());
		Object o=super.delete("deleteCurriculum", params);
		return o==null?false:true;
	}


	@Override
	public Curriculum getCurriculumByCurriculumName(String curriculum_name) {
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("curriculum_name", curriculum_name);
		Curriculum c=(Curriculum) super.find("getCurriculumByCurriculumName", params);
		return c;
	}


	@Override
	public List<Curriculum> getTeacherCurriculumListByUserId(String userid) {
		Map<String, Object> params=new HashMap<String, Object>();
		params.put("user_id", userid);
		@SuppressWarnings("unchecked")
		List<Curriculum> curriculums=(List<Curriculum>) super.select("getTeacherCurriculumListByUserId", params);
		return curriculums;
	}


	@Override
	public List<Curriculum> getCurriculumByPages(Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		List<Curriculum> coList=(List<Curriculum>) super.select("getCurriculumByPages", params);
		return coList;
	}


	@Override
	public int getCount() {
		Object o=super.find("numbers", null);
		return Integer.parseInt(o.toString());
	}


	@Override
	public List<Curriculum> getTeacherCurriculumListByUserIdPages(
			Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		List<Curriculum> curriculums=(List<Curriculum>) super.select("getTeacherCurriculumListByUserIdPages", params);
		return curriculums;
		}

	public Long getPageCounts(Map<String, Object> params) {
		return (long)getCount();
	}


	@Override
	public int getTeacherCurriculumCount(String userid ) {
		Map<String, Object> params=new HashMap<String, Object>();
		params.put("user_id", userid);
		Object o=super.find("getTeacherCurriculumListByUserIdCount", params);
		return Integer.parseInt(o.toString());
	}

	public List<Curriculum> pageList(Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		List<Curriculum> coList=(List<Curriculum>) super.select("curriculum_pageList", params);
		return coList;
	}
}
