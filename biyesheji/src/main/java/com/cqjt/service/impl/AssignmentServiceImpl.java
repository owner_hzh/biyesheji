package com.cqjt.service.impl;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jws.WebService;

import com.cqjt.pojo.Assignment;
import com.cqjt.service.IAssignmentService;
/**
 * 作业接口实现
 * @author 见鸣
 *
 */
//@Sservice(value="assignmentService")
@WebService(endpointInterface = "com.cqjt.service.IAssignmentService", serviceName = "assignmentService")
public class AssignmentServiceImpl extends BaseServiceImpl 
                            implements IAssignmentService 
{
	/**
	 * 获取某一课程的所有作业(通过课程的代码curriculum_code)
	 */
	@Override
	public List<Assignment> getAllAssignment(Map<String, Object> param) 
	{
		@SuppressWarnings("unchecked")
		List<Assignment> assignments=(List<Assignment>) super.select("getAllAssignment", param);
		return assignments;
	}
	/**
	 * 添加一个作业(通过一个作业实体)
	 */
	@Override
	public boolean addAssignment(Assignment assignment) 
	{
		Object success=super.insert("addAssignment", assignment);
		return success==null?false:true;
	}
	/**
	 * 删除一个作业(通过一个作业实体)
	 */
	@Override
	public boolean delAssignment(Assignment assignment) 
	{
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("assignment_code", assignment.getAssignment_code());
		Integer success=super.delete("delAssignment", params);
		return success==0?false:true;
	}
	/**
	 * 给一个作业的下载次数加1(通过一个作业实体)
	 */
	@Override
	public boolean updateAssignmentDowncount(Assignment assignment) 
	{
		return super.update("updatedown_count", assignment);
	}
}
