package com.cqjt.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.cqjt.pojo.CurriculumOutline;
import com.cqjt.pojo.MenuLevelOne;
import com.cqjt.pojo.MenuLevelThree;
import com.cqjt.pojo.MenuLevelTwo;
import com.cqjt.service.IMenuService;
import com.cqjt.service.IOutLineService;
import com.cqjt.util.FileOperation;
import com.cqjt.util.Hanzi2Pinyin;
import com.cqjt.util.RandomNumber;
import com.cqjt.util.WordToHtml;

/**
 * 大纲服务层
 * @author Owner
 *
 */

@Service(value="outLineService")
//@WebService(endpointInterface = "com.cqjt.service.IOutLineService", serviceName = "outLineService")
public class OutLineServiceImpl extends BaseServiceImpl implements IOutLineService {
	
	/*@Override
	public void openPageOffice(HttpServletRequest request,String docFile) {
		PageOfficeCtrl poCtrl=new PageOfficeCtrl(request);
		//设置服务器页面
		poCtrl.setServerPage(request.getContextPath()+"/poserver.zz");
		poCtrl.setTitlebar(false); //隐藏标题栏
		poCtrl.setMenubar(false); //隐藏菜单栏
		poCtrl.setOfficeToolbars(false);//隐藏Office工具条
		//poCtrl.setCustomToolbar(false);//隐藏自定义工具栏
		//添加自定义按钮
		//poCtrl.addCustomToolButton("保存","Save",1);
		poCtrl.addCustomToolButton("全屏", "SetFullScreen()", 4);
		//设置保存页面
		//poCtrl.setSaveFilePage("SaveFile");
		//打开Word文档
		poCtrl.webOpen(docFile,OpenModeType.docReadOnly,"owner");//"resources/doc/test.doc"
		poCtrl.setTagId("PageOfficeCtrl1");//此行必需
	}*/
	
	/*@Override
	public void editPageOffice(HttpServletRequest request, String docFile) {
		PageOfficeCtrl poCtrl=new PageOfficeCtrl(request);
		//设置服务器页面
		poCtrl.setServerPage(request.getContextPath()+"/poserver.zz");
		poCtrl.setTitlebar(false); //隐藏标题栏
		poCtrl.setMenubar(false); //隐藏菜单栏
		//poCtrl.setOfficeToolbars(false);//隐藏Office工具条
		//poCtrl.setCustomToolbar(false);//隐藏自定义工具栏
		//添加自定义按钮
		poCtrl.addCustomToolButton("保存","Save()",1);
		poCtrl.addCustomToolButton("全屏", "SetFullScreen()", 4);
		//设置保存页面
		poCtrl.setSaveFilePage("SaveFile?file="+docFile);
		//打开Word文档
		poCtrl.webOpen(docFile,OpenModeType.docNormalEdit,"owner");//"resources/doc/test.doc"
		poCtrl.setTagId("PageOfficeCtrl1");//此行必需
	}*/


	/*@Override
	public void saveFile(HttpServletRequest request, HttpServletResponse response,String docPath) {
		FileSaver fs=new FileSaver(request,response);
		fs.saveToFile(request.getSession().getServletContext()
				.getRealPath(docPath)+"/"+fs.getFileName());//"resources/doc/"
		fs.close();
	}*/


	@Override
	public List<CurriculumOutline> getAllOutline() {
		@SuppressWarnings("unchecked")
		List<CurriculumOutline> coList=(List<CurriculumOutline>) super.select("getAllOutline", null);
		return coList;
	}


	@Override
	public CurriculumOutline getOutlineByCode(int code,int ocode) {
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("curriculum_code", code);
		params.put("curriculumoutline_code", ocode);
		CurriculumOutline c=(CurriculumOutline) super.find("getOutlineByCode", params);
		return c;
	}


	@Override
	public boolean addOutline(CurriculumOutline curriculumOutline) throws SQLException {
		Object o=null;
		o = super.insert("addOutline", curriculumOutline);	
		return o==null?false:true;
	}


	@Override
	public boolean updateOutline(CurriculumOutline curriculumOutline) {
		Object o=super.update("updateOutline", curriculumOutline);
		return o==null?false:true;
	}


	@Override
	public boolean deleteOutline(CurriculumOutline curriculumOutline) {
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("curriculum_code", curriculumOutline.getCurriculum_code());
		params.put("curriculumoutline_code", curriculumOutline.getCurriculumoutline_code());
		Integer o=super.delete("deleteOutline", params);
		Boolean.valueOf(String.valueOf(o));
		return o==0?false:true;
	}
	
	@Override
	public boolean deleteRelatedFiles(String doc) 
	{	
			return FileOperation.DeleteFolder(doc.substring(0, doc.lastIndexOf("/")));	
	}

	@Override
	public String saveDocToFolder(MultipartFile file, String path) {
		//以文件名加六位随机数字创建文件夹
		//SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");  
        //String foldername = sdf.format(new Date())+"_"+RandomNumber.getRandomNumber();
		String filename=Hanzi2Pinyin.getFullSpell(file.getOriginalFilename());
        String foldername=filename.substring(0, filename.lastIndexOf("."))+"_"+RandomNumber.getRandomNumber();
        //保存文件
        String f = FileOperation.saveFileReturnPath(file, path+foldername+"/");
        return f;
	}

    
	@Override
	public String word2HtmlConverter(String docPath,String netPicPath) throws NullPointerException, TransformerException, IOException, ParserConfigurationException{
		//basePath              D:\Java\MyEclipse\.metadata\.me_tcat\webapps\biyesheji\
		//outline.getLocation() D:\Java\MyEclipse\.metadata\.me_tcat\webapps\biyesheji\resources\doc\testdocx_283385\
	    //outline.getFilename() testdocx.docx
		//netPic                http://localhost:8080/biyesheji/
		String fname=netPicPath.substring(netPicPath.lastIndexOf("/"), netPicPath.lastIndexOf(".")+1)+"html";
		String newFile=docPath.substring(0, docPath.lastIndexOf("."))+fname;
		File f=new File(newFile);
		if(!f.exists())
		{
			
			String picPath=docPath.substring(0, docPath.lastIndexOf("."))+"/";
			
			//String picPath="http:\\localhost:8080\\biyesheji\\resources/doc/docPath/test/";
			WordToHtml.convert2Html(docPath,newFile, picPath,netPicPath.substring(0, netPicPath.lastIndexOf("."))+"/");
			return newFile;
		}else
		{
			return newFile;
		}		
	}


	@Override
	public List<CurriculumOutline> getOutlineListByCode(int code) {
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("curriculum_code", code);
		@SuppressWarnings("unchecked")
		List<CurriculumOutline> c=(List<CurriculumOutline>) super.select("getOutlineListByCode", params);
		return c;
	}


	

}
