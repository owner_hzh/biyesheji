package com.cqjt.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cqjt.pojo.TeachingData;
import com.cqjt.service.ITeachingDataService;

public class TeachingDataServiceImpl extends BaseServiceImpl implements ITeachingDataService{

	@Override
	public List<TeachingData> getAllTeachingData(Map<String, Object> params) {
		// TODO Auto-generated method stub
		String sqlName = "getAllTeachingData";
		@SuppressWarnings("unchecked")
		List<TeachingData> teachingDatas = (List<TeachingData>) super.select(sqlName, params);
		
		return teachingDatas;
	}

	@Override
	public TeachingData getTeachingDataByCode(int teachingdata_code) {
		// TODO Auto-generated method stub
		String sqlName = "getTeachingDataByCode";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("teachingdata_code",teachingdata_code );
		TeachingData teachingData = (TeachingData) super.find(sqlName, params);
		return teachingData;
	}

	@Override
	public boolean addTeachingData(TeachingData teachingData) {
		// TODO Auto-generated method stub
		Object o=super.insert("addTeachingData",teachingData);
		return o==null?false:true;
	}

	@Override
	public boolean delTeachingData(TeachingData teachingData) {
		// TODO Auto-generated method stub
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("teachingdata_code", teachingData.getTeachingdata_code());
		Object o=super.delete("deleteTeachingData", params);
		return o==null?false:true;
	}

	@Override
	public boolean updateTeachingData(TeachingData teachingData) {
		// TODO Auto-generated method stub
		Object o=super.update("updateTeachingData", teachingData);
		return o==null?false:true;
	}

	
	
}
