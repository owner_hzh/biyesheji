package com.cqjt.service.impl;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;

import org.springframework.dao.DataAccessException;
import org.springframework.web.multipart.MultipartFile;

import com.cqjt.pojo.Teacher;
import com.cqjt.pojo.TeacherInfor;
import com.cqjt.pojo.User;
import com.cqjt.service.ITeacherService;
import com.cqjt.util.ExcelToDB;
import com.cqjt.util.FileOperation;
import com.cqjt.util.PageCondition;
import com.cqjt.util.PageNameEnum;
import com.cqjt.util.PageUtil;


//@Service(value="teacherService")
@WebService(endpointInterface = "com.cqjt.service.ITeacherService", serviceName = "teacherService")
public class TeacherServiceImpl extends BaseServiceImpl implements ITeacherService {

/*	@Resource(name = "teacherDAO")
	private ITeacherDAO teacherDAO;*/
	
	@Override
	public void uploadTeachersExcel(MultipartFile file, String path) {
		//以日期创建文件夹
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");  
        String ymd = sdf.format(new Date());
        
        //保存文件
        File targetFile = FileOperation.saveFile(file, path+ymd+"/");
        
        //保存数据
		ExcelToDB e=new ExcelToDB(targetFile,"teacher");

		@SuppressWarnings("unchecked")
		List<Teacher> teachers=(List<Teacher>) e.getList();
		if(teachers!=null)
		{
			for (Teacher t : teachers) {
				Map<String, Object> tmp = new HashMap<String, Object>();
				tmp.put("teacher", t);
				super.insert("addTeacher", tmp);
			}
		}
	}

	
	/**
	 * 根据用户名读取教师的信息
	 * @param Map<String, Object> params
	 * return Teacher
	 */
	@Override
	public Teacher getTeacherByUserid(Map<String, Object> params) {

		//String sqlName = "teacher.teacherinfo" ;

		String sqlName = "teacher.teacher.abatorgenerated_selectByPrimaryKey" ;

		Teacher teacher=(Teacher) super.find(sqlName, params);
		return teacher;
	}
	
	/**
	 * 插入教师有关的信息
	 * @param teacher
	 * @param teacherInfor
	 * @param user
	 */
	@Override
	public void insert(Teacher teacher, TeacherInfor teacherInfor, User user) {
		super.insert("adduser", user);
		super.insert("teacher.abatorgenerated_insert", teacher);
		super.insert("teacherinfo.insert", teacherInfor);
	}
	
	/**
	 * 修改教师有关的信息
	 * @param teacher
	 * @param teacherInfor
	 */
	@Override
	public void update(Teacher teacher, TeacherInfor teacherInfor,User user) {
		super.update("updateuser", user);
		super.update("updateteacher.teacher", teacher);
		super.update("updateteacherinfo.teacherinfo", teacherInfor);
	}

	/**
	 * 获取教师(单个或者多个)
	 * 
	 * @return 教师list
	 */
	@Override
	public List<Teacher> getAllTeachers(Map<String, Object> params) {
		return (List<Teacher>) super.select("getteacher", params);
	}
	
	/**
	 * 导出教师
	 * 
	 * @return 教师list
	 */
	@Override
	public List<Teacher> exportTeachers(Map<String, Object> params) {
		return (List<Teacher>) super.select("exportteacher", params);
	}

	/**
	 * 分页
	 * 
	 * @param pageSize
	 *            每页显示条数
	 * @param pageNo
	 *            当前页数
	 * @param params
	 *            筛选条件
	 * @param request
	 * @return PageCondition
	 */
	@Override
	public PageCondition getAllTeachers(int pageSize, int pageNo,
			Map<String, Object> params, HttpServletRequest request) {
		try {
			int total = super.getCount("getteachercount", params);

			params.put("startNo", pageSize * (pageNo - 1));
			params.put("pageSize", pageSize);
			List dataList = super.select("getteacher", params);

			PageCondition pagecondition = new PageCondition();
			String path = request.getContextPath();
			String basePath = request.getScheme() + "://"
					+ request.getServerName() + ":" + request.getServerPort()
					+ path + "/";
			String selectPath="";//链接中的查询条件
			Object teacher_name = params.get("teacher_name");
			Object teacher_sex = params.get("teacher_sex");
			//处理查询条件
			if(teacher_name!=null&&!(teacher_name+"").equals("")){
				try {
					teacher_name = URLEncoder.encode(URLEncoder.encode(teacher_name+"","utf-8"),"utf-8");
					selectPath = selectPath + "&teacher_name="+teacher_name;
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}
			if(!"".equals(teacher_sex) && teacher_sex != null){
				selectPath = selectPath + "&teacher_sex="+teacher_sex;
			}
			System.out.println(selectPath+"-------------------");
			//拼接链接
			if("".equals(selectPath) || selectPath==null){
				pagecondition.setShow(PageUtil.setPageHtmlStr(total, PageNameEnum.PAGESIZE, pageNo,
						basePath + "teacher/menu-content-show-teachers", PageNameEnum.SHOWA, true));
			}else{
				pagecondition.setShow(PageUtil.setPageHtmlStr(total, PageNameEnum.PAGESIZE, pageNo,
						basePath + "teacher/menu-content-show-teachers?"+selectPath,PageNameEnum.SHOWA, true));
			}
			pagecondition.setDataList(dataList);
			pagecondition.settotalCount(total);
			return pagecondition;
		} catch (DataAccessException datae) {
			datae.printStackTrace();
			return null;
		}
	}

	@Override
	public int updateByPrimaryKeySelective(Teacher record) throws SQLException {
		boolean f = super.update(
				"updateteacher.teacher", record);
		return f == true ? 1 : 0;
	}

	@Override
	public Teacher selectByPrimaryKey(String userId){
		Map<String, Object> params=new HashMap<String, Object>();
		params.put("user_id", userId);
		Teacher t=(Teacher) super.find("teacher.abatorgenerated_selectByPrimaryKey", params);
		return t;
	}

	//删除教师
	@Override
	public int deleteByPrimaryKey(String userId){
		Map<String, Object> params=new HashMap<String, Object>();
		params.put("user_id", userId);
		super.delete("deleteTeacherInfor", params);
		super.delete("deleteuser", params);
		int i=super.delete("teacher.abatorgenerated_deleteByPrimaryKey", params);
		return i;
	}


	@Override
	public List<Teacher> getTeacherFullInfoByUserid(Map<String, Object> params) {
		 List<Teacher> ts= (List<Teacher>) super.select("teacher.teacherinfo", params);
		return ts;
	}


	@Override
	public Teacher getTeacherByTeacherName(String name) {
		Map<String, Object> teacher_params=new HashMap<String, Object>();
		teacher_params.put("teacher_name", name);
		Teacher teacher = (Teacher) super.find("teacher.abatorgenerated_selectByTeacherName", teacher_params);
		return teacher;
	}


}
