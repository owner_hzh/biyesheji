package com.cqjt.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cqjt.pojo.TestQuestion;
import com.cqjt.pojo.TestQuestion;
import com.cqjt.service.ITestQuestionService;

public class TestQuestionServiceImpl extends BaseServiceImpl implements ITestQuestionService{

	@Override
	public List<TestQuestion> getAllTestQuestion(Map<String, Object> params) {
		// TODO Auto-generated method stub
		@SuppressWarnings("unchecked")
		List<TestQuestion> list = (List<TestQuestion>) super.select("getAllTestQuestion", params); 
		return list;
	}

	@Override
	public TestQuestion getTestQuestionByCode(int code) {
		// TODO Auto-generated method stub
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("tq_id", code);
		TestQuestion testQuestion = (TestQuestion) super.find("getTestQuestionByCode", params);
		return testQuestion;
	}

	@Override
	public boolean addTestQuestion(TestQuestion testQuestion) {
		// TODO Auto-generated method stub
		String o=super.insert("addTestQuestion",testQuestion).toString();
		int r=Integer.parseInt(o);
		return r>0?true:false;
	}

	@Override
	public boolean updateTestQuestion(TestQuestion testQuestion) {
		// TODO Auto-generated method stub
		Object o=super.update("updateTestQuestion",testQuestion);
		return o==null?false:true;
	}

	@Override
	public boolean deleteTestQuestion(TestQuestion testQuestion) {
		// TODO Auto-generated method stub
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("code", testQuestion.getQt_code());
		Object o=super.delete("deleteTestQuestion",params);
		return o==null?false:true;
	}
	
    /**
     * 随机获取题
     * @return 题型list
     */
	@Override
    public List<TestQuestion> getRandQuestion(Map<String, Object> params){
		List<TestQuestion> list = (List<TestQuestion>) super.select("getRandQuestion", params); 
		return list;
	}
}
