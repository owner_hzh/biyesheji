/**
 * 
 */
package com.cqjt.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.text.StyledEditorKit.BoldAction;

import com.cqjt.pojo.CurriculumCharacter;
import com.cqjt.service.ICurriculumCharacterService;

/**
 * <p>Title: CurriculumCharacterServiceImpl.java</p>
 * <p>Description: MyEclipse 平台软件</p>
 * <p>Copyright: Copyright (c) 毕业设计小分队</p>
 * @author xiaoshujun
 * @version 1.0 创建时间：2014-3-21 下午5:07:18
 */

public class CurriculumCharacterServiceImpl extends BaseServiceImpl implements ICurriculumCharacterService{

	/* (non-Javadoc)
	 * @see com.cqjt.service.ICurriculumCharacterService#getCurriculumCharacterList()
	 */
	@Override
	public List<CurriculumCharacter> getAllCurriculumCharacter() {
		// TODO Auto-generated method stub
		String sqlName = "getAllCurriculumCharacter";
		Map<String, Object> params = new HashMap<String, Object>();
		/*params.put("cc_code","01" );*/
		
		@SuppressWarnings("unchecked")
		List<CurriculumCharacter> curriculumCharacterList = (List<CurriculumCharacter>) super.select(sqlName, params);
		return curriculumCharacterList;
	}

	/* (non-Javadoc)
	 * @see com.cqjt.service.ICurriculumCharacterService#addCurriculumCharacter(com.cqjt.pojo.CurriculumCharacter)
	 */
	@Override
	public boolean addCurriculumCharacter(
			CurriculumCharacter curriculumCharacter) {
		// TODO Auto-generated method stub
		String sqlName = "addCurriculumCharacter";
		boolean success = (Boolean) super.insert(sqlName, curriculumCharacter);
		return success;
	}

	/* (non-Javadoc)
	 * @see com.cqjt.service.ICurriculumCharacterService#updateCurriculumCharacter(com.cqjt.pojo.CurriculumCharacter)
	 */
	@Override
	public boolean updateCurriculumCharacter(
			CurriculumCharacter curriculumCharacter) {
		// TODO Auto-generated method stub
		String sqlName = "updateCurriculumCharacter";
		boolean success = super.update(sqlName, curriculumCharacter);
		return success;
	}

	/* (non-Javadoc)
	 * @see com.cqjt.service.ICurriculumCharacterService#deleteCurriculumCharacter(com.cqjt.pojo.CurriculumCharacter)
	 */
	@Override
	public Integer deleteCurriculumCharacter(
			CurriculumCharacter curriculumCharacter) {
		// TODO Auto-generated method stub
		String sqlName = "deleteCurriculumCharacter";
		Map<String, Object> params = new HashMap<String, Object>();
		Integer success = super.delete(sqlName, params);
		return success;
	}

	@Override
	public CurriculumCharacter getCurriculumCharacterByCode(int code) {
		Map<String, Object> params=new HashMap<String, Object>();
		params.put("cc_code", code);
		return (CurriculumCharacter) super.find("getCurriculumCharacterById", params);
	}
	
}
