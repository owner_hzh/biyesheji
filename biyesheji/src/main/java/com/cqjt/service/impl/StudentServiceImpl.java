package com.cqjt.service.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.dao.DataAccessException;

import com.cqjt.pojo.Student;
import com.cqjt.pojo.Teacher;
import com.cqjt.pojo.User;
import com.cqjt.service.IStudentService;
import com.cqjt.util.PageCondition;
import com.cqjt.util.PageNameEnum;
import com.cqjt.util.PageUtil;

public class StudentServiceImpl extends BaseServiceImpl implements IStudentService{

	/**
	 * 获取学生(单个或者多个)
	 * 
	 * @return 教师list
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Student> getAllStudents(Map<String, Object> params) {
		return (List<Student>) super.select("exportstudent", params);
	}
	
	/**
	 * 分页
	 * 
	 * @param pageSize
	 *            每页显示条数
	 * @param pageNo
	 *            当前页数
	 * @param params
	 *            筛选条件
	 * @param request
	 * @return PageCondition
	 */
	@Override
	public PageCondition getAllStudents(int pageSize, int pageNo,
			Map<String, Object> params, HttpServletRequest request) {
		try {
			int total = super.getCount("countstudent", params);

			params.put("startNo", pageSize * (pageNo - 1));
			params.put("pageSize", pageSize);
			List dataList = super.select("getstudent", params);

			PageCondition pagecondition = new PageCondition();
			String path = request.getContextPath();
			String basePath = request.getScheme() + "://"
					+ request.getServerName() + ":" + request.getServerPort()
					+ path + "/";
			String selectPath="";//链接中的查询条件
			Object student_name = params.get("student_name");
			Object student_sex = params.get("student_sex");
			Object user_id = params.get("user_id");
			Object grade = params.get("grade");
			//处理查询条件
			if(student_name!=null&&!(student_name+"").equals("")){
				try {
					student_name = URLEncoder.encode(URLEncoder.encode(student_name+"","utf-8"),"utf-8");
					selectPath = selectPath + "&student_name="+student_name;
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}
			if(!"".equals(student_sex) && student_sex != null){
				selectPath = selectPath + "&student_sex="+student_sex;
			}
			if(!"".equals(user_id) && user_id != null){
				selectPath = selectPath + "&user_id="+user_id;
			}
			if(!"".equals(grade) && grade != null){
				selectPath = selectPath + "&grade="+grade;
			}
			System.out.println(selectPath+"-------------------");
			//拼接链接
			if("".equals(selectPath) || selectPath==null){
				pagecondition.setShow(PageUtil.setPageHtmlStr(total, PageNameEnum.PAGESIZE, pageNo,
						basePath + "student/menu-content-show-students",PageNameEnum.SHOWA, true));
			}else{
				pagecondition.setShow(PageUtil.setPageHtmlStr(total, PageNameEnum.PAGESIZE, pageNo,
						basePath + "student/menu-content-show-students?"+selectPath,PageNameEnum.SHOWA, true));
			}
			pagecondition.setDataList(dataList);
			pagecondition.settotalCount(total);
			return pagecondition;
		} catch (DataAccessException datae) {
			datae.printStackTrace();
			return null;
		}
	}
	
	
	/**
	 * 添加学生
	 */
	@Override
	public void insert(Student student,User user){
		super.insert("adduser", user);
		super.insert("student.abatorgenerated_insert", student);
	}

	@Override
	public int updateByPrimaryKey(Student record) throws SQLException {
		boolean rows = super.update("student.abatorgenerated_updateByPrimaryKey", record);
        return rows?1:0;
	}

	@Override
	public int updateByPrimaryKeySelective(Student record) throws SQLException {
		boolean rows = super.update("student.abatorgenerated_updateByPrimaryKey", record);
        return rows?1:0;
	}

	@Override
	public Student selectByPrimaryKey(String userId){
        Map<String, Object> params=new HashMap<String, Object>();
        params.put("user_id", userId);
        Student record = (Student) super.find("student.abatorgenerated_selectByPrimaryKey", params);
        return record;
	}

	//删除学生
	@Override
	public int deleteByPrimaryKey(String userId){
        Map<String, Object> params=new HashMap<String, Object>();
        params.put("user_id", userId);
        super.delete("deleteuser", params);
        int rows=super.delete("student.abatorgenerated_deleteByPrimaryKey", params);
        return rows;
	}

}
