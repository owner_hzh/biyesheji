package com.cqjt.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.cqjt.pojo.SelfTest;
import com.cqjt.pojo.TestCode;
import com.cqjt.pojo.TestCustomize;
import com.cqjt.pojo.TestRecord;
import com.cqjt.service.ISelfTestService;
import com.cqjt.service.ITestCodeService;
import com.cqjt.service.ITestCustomizeService;
import com.cqjt.service.ITestRecordService;

/**
 * 自测试卷接口实现
 * @author LIJIN
 *
 */

@Service(value="selfTestService")
//@WebService(endpointInterface = "com.cqjt.service.ICurriculumService", serviceName = "curriculumService")
public class SelfTestServiceImpl extends BaseServiceImpl implements ISelfTestService {

    /**
     * 获取自测试卷
     * @return 自测试卷list
     */
	@Override
	public List<SelfTest> getAllSelfTest(Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		List<SelfTest> cwList=(List<SelfTest>) super.select("getselftest", params);
		return cwList;
	}

    
    /**
     * 添加自测试卷
     * @param 自测试卷对象
     * @return true  or false
     */
	@Override
	public Integer addSelfTest(SelfTest selfTest) {
		Integer o=(Integer) super.insert("addselftest", selfTest);
		return o;
	}


    /**
     * 批量添加自测试卷
     * @param 自测试卷对象
     * @return true  or false
     */
    public boolean addSelfTest(List<SelfTest> selfTestList){
		try {
			super.insert("addselftest", selfTestList);
		} catch (Exception e) {
			
			e.printStackTrace();
			return false;
		}
		return true;
    }
	

    /**
     * 根据自测试卷code删除这条信息
     * @param SelfTest_code
     * @return true or false
     */
	@Override
	public boolean deleteSelfTest(SelfTest selfTest) {
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("curriculum_code", selfTest.getId());
		Integer o=super.delete("deleteselftest", params);
		return o==0?false:true;
	}

	
    /**
     * 根据自测试卷code更新自测试卷信息
     * @param 这个实体只需要code就可
     * @return true or false
     */
    public boolean updateSelfTest(SelfTest selfTest){
    	return super.update("updateselftest", selfTest);
    }
    
    /**
     * 获取正确题数
     * @param 自测试卷对象
     * @return true  or false
     */
    public Integer getRight(Map<String, Object> params){
    	Integer o=super.getCount("getrightcount", params);
		return o;
    }


	@Override
	public List<SelfTest> getAllSelfTestBigger(Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		List<SelfTest> cwList=(List<SelfTest>) super.select("getselftestbigger", params);
		return cwList;
	}
}
