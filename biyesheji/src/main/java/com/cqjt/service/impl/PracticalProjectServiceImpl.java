package com.cqjt.service.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cqjt.pojo.AlternativePracticalProject;
import com.cqjt.pojo.PracticalProject;
import com.cqjt.pojo.PracticalProjectDocument;
import com.cqjt.service.IPracticalProjectService;

public class PracticalProjectServiceImpl extends BaseServiceImpl implements IPracticalProjectService{

	@Override
	public List<PracticalProject> getAllPracticalProject(
			Map<String, Object> params) {
		// TODO Auto-generated method stub
		String sqlName = "getAllPracticalProject" ;
		@SuppressWarnings("unchecked")
		List<PracticalProject> practicalProjects = (List<PracticalProject>) super.select(sqlName, params);
		return practicalProjects;
	}

	@Override
	public PracticalProject getPracticalProjectByCode(Map<String, Object> params) {
		// TODO Auto-generated method stub
		String sqlName = "getPracticalProjectByCode";
		PracticalProject practicalProject = (PracticalProject) super.find(sqlName, params);
		return practicalProject;
	}

	@Override
	public boolean addPracticalProject(PracticalProject practicalProject) {
		// TODO Auto-generated method stub
		String sqlName = "addPracticalProject";
		int intppnumber= (Integer) super.insert(sqlName, practicalProject);//插入实战项目
		String ppnumber=Integer.toString(intppnumber);
		practicalProject.setPpnumber(ppnumber);
		int i=ppnumber.length();
		System.out.println("长度为="+i);
		while(i<3)
		{
			ppnumber="0"+ppnumber;
			i++;
		}
		practicalProject.setProject_stringcode(Integer.toString(practicalProject.getPpcode())+""+Integer.toString(practicalProject.getSemester_code())+ppnumber);//构造编号
		System.out.println("编号为="+practicalProject.getProject_stringcode());
		sqlName="updatePracticalProject";
		return super.update(sqlName, practicalProject);//更新编号
		
	}

	@Override
	public boolean updatePracticalProject(PracticalProject practicalProject) {
		// TODO Auto-generated method stub
		String sqlName = "updatePracticalProject" ;
		Object o=super.update(sqlName, practicalProject);
		return o==null?false:true;
	}

	@Override
	public boolean deletePracticalProject(PracticalProject practicalProject) {
		// TODO Auto-generated method stub
		String sqlName = "deletePracticalProject" ;
		Object o=super.update(sqlName, practicalProject);
		return o==null?false:true;
	}

	@Override
	public List<PracticalProject> getPageBasicPracticalProjects(Map<String, Object> params) 
	{
		String sqlName = "getPagePracticalProject" ;
		List<PracticalProject> practicalProjectList=(List<PracticalProject>) super.select(sqlName, params);
		return practicalProjectList;
	}

	@Override
	public int[] getBasicPracticalProjectsPagenum(Map<String, Object> params) 
	{
		String sqlName="getPageNum";
		double allnumber=super.getCount(sqlName, params);
		double doubleoflastpage= (allnumber/15);
		int lastpage=(int) Math.ceil(doubleoflastpage);
		int[] page = new int[lastpage];
		for(int i=0;i<lastpage;i++)
		{
			page[i]=i+1;
			System.out.println("页码："+page[i]);
		}
		return page;
	}

	public PracticalProject getNewerPracticalProjectByCode() {
		String sqlName = "getNewerPracticalProject";
		PracticalProject practicalProject = (PracticalProject) super.find(sqlName, null);
		return practicalProject;

	}

	@Override
	public Boolean shenTiTransationMethod(int number, int code,int isTeaOrStu,String loginID) {
		Boolean f1 = false,f2 = false,f3 = false;
			HashMap<String, Object> params=new HashMap<String, Object>();
			params.put("equence_number", number);
			AlternativePracticalProject a=(AlternativePracticalProject) super.find("getAlternativePracticalProjectByEquence_number", params);
			
			//获取最新的一条信息
			PracticalProject newer=getNewerPracticalProjectByCode();
			
			//拼接project_stringcode
			String newertring="";
			String twoString=Integer.toString(code)+Integer.toString(a.getSemester_code());
			if(newer==null||newer.equals(null))
			{
				newertring=twoString+"001";
			}else
			{
				newertring=newer.getProject_stringcode();
				newertring=Integer.parseInt(newertring.substring(2))+1+"";
				if(newertring.length()==1)
				{
					newertring=twoString+"00"+newertring;
				}else if(newertring.length()==2)
				{
					newertring=twoString+"0"+newertring;
				}
			}

			//插入两张表
			PracticalProject practicalProject=new PracticalProject();
			practicalProject.setPp_name(a.getPp_name());
			practicalProject.setPpcode(code);
			practicalProject.setRequirement(a.getRequirement());
			practicalProject.setSemester_code(a.getSemester_code());
			practicalProject.setSource(isTeaOrStu);                            //1 代表老师  0代表学生  
			practicalProject.setTea_user_id(loginID);                    //登录者的id号  "11"
			practicalProject.setUser_id(a.getUser_id());		
			practicalProject.setProject_stringcode(newertring);
			f1=addPracticalProject(practicalProject);
			
			PracticalProject  p=getNewerPracticalProjectByCode();
			
			PracticalProjectDocument practicalProjectDocument=new PracticalProjectDocument();
			practicalProjectDocument.setPpcode(code);
			practicalProjectDocument.setPpd_link(a.getPpd_link());
			practicalProjectDocument.setPpd_name(a.getPpd_name());
			practicalProjectDocument.setPpnumber(p.getPpnumber());
			
			Object o=super.insert("addPracticalProjectDocument", practicalProjectDocument);
			f2= (o==null?false:true);
			
			o=super.update("deleteAlternativePracticalProject",a);
			f3= (o==null?false:true);
            System.out.println("--f1--"+f1.toString()+"--f2--"+f2.toString()+"--f3--"+f3.toString());
			if(f1&&!f2&&f3)
			{
				return true;
			}
		    return false;
	}

	@Override
	public PracticalProject getPracticalProjectByCodeAndNumber(int ppcode,
			String ppnumber) {
		String sqlName = "getPracticalProjectByCode";
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("ppcode", ppcode);
		params.put("ppnumber", ppnumber);
		PracticalProject practicalProject = (PracticalProject) super.find(sqlName, params);
		return practicalProject;
	}

	@Override
	public Boolean teacherAddPracticingWithDoc(
			PracticalProject practicalProject, String ppd_name, String ppd_link) 
	{
		String sqlName = "addPracticalProject";
		int intppnumber= (Integer) super.insert(sqlName, practicalProject);//插入实战项目
		String ppnumber=Integer.toString(intppnumber);
		practicalProject.setPpnumber(ppnumber);
		int i=ppnumber.length();
		System.out.println("长度为="+i);
		while(i<3)
		{
			ppnumber="0"+ppnumber;
			i++;
		}
		practicalProject.setProject_stringcode(Integer.toString(practicalProject.getPpcode())+""+Integer.toString(practicalProject.getSemester_code())+ppnumber);//构造编号
		System.out.println("编号为="+practicalProject.getProject_stringcode());
		sqlName="updatePracticalProject";
		super.update(sqlName, practicalProject);//更新编号
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("ppcode", practicalProject.getPpcode());
		params.put("ppnumber", practicalProject.getPpnumber());
		params.put("ppd_name", ppd_name);
		params.put("ppd_link", ppd_link);
		Object success=super.insert("addDocumentInPracticing",params);//插入文档
		return success==null?false:true;
	}

}
