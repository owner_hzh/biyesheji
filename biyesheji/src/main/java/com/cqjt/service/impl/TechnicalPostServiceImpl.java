package com.cqjt.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.cqjt.pojo.TeacherCharacter;
import com.cqjt.pojo.TechnicalPost;
import com.cqjt.service.ITechnicalPostService;

public class TechnicalPostServiceImpl extends BaseServiceImpl implements ITechnicalPostService{

	@Override
	public List<TechnicalPost> getAllTechnicalPost(Map<String, Object> params) {
		// TODO Auto-generated method stub
		@SuppressWarnings("unchecked")
		List<TechnicalPost> list = (List<TechnicalPost>) super.select("getAllTechnicalPost", params); 
		return list;
	}

	@Override
	public TechnicalPost getTechnicalPostByCode(int technical_code) {
		// TODO Auto-generated method stub
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("technical_code", technical_code);
		TechnicalPost TechnicalPost = (TechnicalPost) super.find("getTechnicalPostByCode", params);
		return TechnicalPost;
	}

	@Override
	public boolean addTechnicalPost(TechnicalPost TechnicalPost) {
		// TODO Auto-generated method stub
		Object o=super.insert("addTechnicalPost",TechnicalPost);
		return o==null?false:true;
	}

	@Override
	public boolean updateTechnicalPost(TechnicalPost TechnicalPost) {
		// TODO Auto-generated method stub
		Object o=super.update("updateTechnicalPost",TechnicalPost);
		return o==null?false:true;
	}

	@Override
	public boolean deleteTechnicalPost(TechnicalPost TechnicalPost) {
		// TODO Auto-generated method stub
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("technical_code", TechnicalPost.getTechnical_code());
		Object o=super.delete("deleteTechnicalPost",params);
		return o==null?false:true;
	}

/*	@Override
	public TeacherCharacter getTeacherCharacterByCode(int tc_code) {
		// TODO Auto-generated method stub
		return null;
	}*/

	@Override
	public void uploadTechnicalPostExcel(MultipartFile file, String path) {
		// TODO Auto-generated method stub
		
	}
	
}
