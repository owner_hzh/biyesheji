package com.cqjt.service.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.cqjt.pojo.Courseware;
import com.cqjt.pojo.Curriculum;
import com.cqjt.service.ICoursewareService;
import com.cqjt.service.ICurriculumService;
import com.cqjt.util.PageCondition;
import com.cqjt.util.PageNameEnum;
import com.cqjt.util.PageUtil;

/**
 * 课件接口实现
 * @author LIJIN
 *
 */

@Service(value="coursewareService")
//@WebService(endpointInterface = "com.cqjt.service.ICurriculumService", serviceName = "curriculumService")
public class CoursewareServiceImpl extends BaseServiceImpl implements ICoursewareService {

    /**
     * 获取课件
     * @return 课件list
     */
	@Override
	public List<Courseware> getAllCourseware(Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		List<Courseware> cwList=(List<Courseware>) super.select("getcourseware", params);
		return cwList;
	}
	
	
	/**
	 * 分页
	 * 
	 * @param pageSize
	 *            每页显示条数
	 * @param pageNo
	 *            当前页数
	 * @param params
	 *            筛选条件
	 * @param request
	 * @return PageCondition
	 */
	@Override
	public PageCondition getAllCourseware(int pageSize, int pageNo,
			Map<String, Object> params, HttpServletRequest request) {
		try {
			int total = super.getCount("countcourseware", params);

			params.put("startNo", pageSize * (pageNo - 1));
			params.put("pageSize", pageSize);
			List dataList = super.select("coursewareforpage", params);

			PageCondition pagecondition = new PageCondition();
			String path = request.getContextPath();
			String basePath = request.getScheme() + "://"
					+ request.getServerName() + ":" + request.getServerPort()
					+ path + "/";
			String selectPath="";//链接中的查询条件
			Object curriculum_code = params.get("curriculum_code");
			Object curriculum_name = params.get("curriculum_name");
			//处理查询条件
			if(curriculum_name!=null&&!(curriculum_name+"").equals("")){
				try {
					curriculum_name = URLEncoder.encode(URLEncoder.encode(curriculum_name+"","utf-8"),"utf-8");
					selectPath = selectPath + "&curriculum_name="+curriculum_name;
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}
			if(!"".equals(curriculum_code) && curriculum_code != null){
				selectPath = selectPath + "&curriculum_code="+curriculum_code;
			}
			System.out.println(selectPath+"-------------------");
			//拼接链接
			if("".equals(selectPath) || selectPath==null){
				pagecondition.setShow(PageUtil.setPageHtmlStr(total, PageNameEnum.PAGESIZE, pageNo,
						basePath + "courseware/show",PageNameEnum.SHOWA, true));
			}else{
				pagecondition.setShow(PageUtil.setPageHtmlStr(total, PageNameEnum.PAGESIZE, pageNo,
						basePath + "courseware/show?"+selectPath,PageNameEnum.SHOWA, true));
			}
			pagecondition.setDataList(dataList);
			pagecondition.settotalCount(total);
			return pagecondition;
		} catch (DataAccessException datae) {
			datae.printStackTrace();
			return null;
		}
	}
	
	

    
    /**
     * 添加课件
     * @param 课件对象
     * @return true  or false
     */
	@Override
	public boolean addCourseware(Courseware Courseware) {
		Object o=super.insert("addcourseware", Courseware);
		return o==null?false:true;
	}



    /**
     * 根据课件code删除这条信息
     * @param courseware_code
     * @return true or false
     */
	@Override
	public boolean deleteCourseware(Courseware courseware) {
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("courseware_code", courseware.getCourseware_code());
		Integer o=super.delete("deletecourseware", params);
		return o==0?false:true;
	}

	
    /**
     * 根据课件code更新课件信息
     * @param curriculum对象 这个实体只需要code就可
     * @return true or false
     */
    public boolean updateCourseware(Courseware courseware){
    	return super.update("updatecourseware", courseware);
    }
}
