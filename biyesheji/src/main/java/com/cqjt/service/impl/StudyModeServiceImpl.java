package com.cqjt.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jws.WebService;

import com.cqjt.pojo.StudyMode;
import com.cqjt.service.IStudyModeService;

/**
 * <p>Title: StudyModeImpl.java</p>
 * <p>Description: MyEclipse 平台软件</p>
 * <p>Copyright: Copyright (c) 毕业设计小分队</p>
 * @author xiaoshujun
 * @version 1.0 创建时间：2014-3-21 上午11:02:05
 */
@WebService(endpointInterface = "com.cqjt.service.IStudyModeService", serviceName = "StudyModeService")
public class StudyModeServiceImpl extends BaseServiceImpl implements IStudyModeService {

	/* (non-Javadoc)
	 * @see com.cqjt.service.IStudyModeService#getStudyModeList()
	 */
	@Override
	public List<StudyMode> getAllStudyMode() {
		// TODO Auto-generated method stub
		String sqlName = "getAllStudyMode";
		Map<String, Object> params = new HashMap<String, Object>();
		
		@SuppressWarnings("unchecked")
		List<StudyMode> studyModes = (List<StudyMode>) super.select(sqlName, params);
		
		return studyModes;
	}

	/* (non-Javadoc)
	 * @see com.cqjt.service.IStudyModeService#addStudyMode(com.cqjt.pojo.StudyMode)
	 */
	@Override
	public boolean addStudyMode(StudyMode studyMode) {
		// TODO Auto-generated method stub
		String sqlName = "addStudyMode";
		boolean success = (Boolean) super.insert(sqlName, studyMode);
		return success;
	}

	/* (non-Javadoc)
	 * @see com.cqjt.service.IStudyModeService#updateStudyMode(com.cqjt.pojo.StudyMode)
	 */
	@Override
	public boolean updateStudyMode(StudyMode studyMode) {
		// TODO Auto-generated method stub
		String sqlName = "updateStudyMode";
		boolean success = super.update(sqlName, studyMode);
		return success;
	}

	/* (non-Javadoc)
	 * @see com.cqjt.service.IStudyModeService#deleteStudyMode(com.cqjt.pojo.StudyMode)
	 */
	@Override
	public Integer deleteStudyMode(StudyMode studyMode) {
		// TODO Auto-generated method stub
		String sqlName = "deleteStudyMode";
		Map<String, Object> params = new HashMap<String, Object>();
	    Integer success = super.delete(sqlName, params);
		return success;
	}

	@Override
	public StudyMode getStudyModeByCode(int code) {
		Map<String, Object> params=new HashMap<String, Object>();
		params.put("studymode_id", code);
		// TODO Auto-generated method stub
		return (StudyMode) super.find("getStudyModeById", params);
	}
	
	
}
