package com.cqjt.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jws.WebService;

import com.cqjt.pojo.Major;
import com.cqjt.service.IMajorService;


//@Service(value="majorService")
@WebService(endpointInterface = "com.cqjt.service.MajorService", serviceName = "majorService")
public class MajorServiceImpl extends BaseServiceImpl implements IMajorService {

/*	@Resource(name = "majorDAO")
	private IMajorDAO majorDAO;*/
	
	/**
	 * 得到所有XX列表
	 */
	@Override
	public List<Major> getMajorList() {
		String sqlName = "getallmajor";
		List<Major> majors=(List<Major>) super.select(sqlName, null);//majorDAO.getAllMajor();
		System.out.println("majors=="+majors.size());
		return majors;
	}
	public static void main(String[] args) {
		int i =(new MajorServiceImpl()).getMajorList().size();
		System.out.println(i);
	}
	@Override
	public boolean addMajor(Major major) {
		 Object object=insert("addMajor", major);
		 return object==null?false:true;
	}
	
	  /**
	   * 获取单个专业
	   * @param major
	   */
	@Override
	public Major getMajor(Map<String, Object> params){
		String sqlName = "getonemajor";
		List<Major> majors=(List<Major>) super.select(sqlName, params);
		if(majors!=null&&majors.size()>0)
		{
			return majors.get(0);
		}else
		{
			return null;
		}	
	}
	@Override
	public boolean updateMajor(Major major) {
		boolean f=super.update("updateMajor", major);
		return f;
	}
	@Override
	public List<Major> getMajorListOnlyCodeAndName() {
		String sqlName = "getMajorListOnlyCodeAndName";
		List<Major> majors=(List<Major>) super.select(sqlName, null);//majorDAO.getAllMajor();
		return majors;
	}
	@Override
	public boolean deleteMajor(Map<String, Object> params) {
		int i=super.delete("deletemajor", params);
		return i>0?true:false;
	}
}
