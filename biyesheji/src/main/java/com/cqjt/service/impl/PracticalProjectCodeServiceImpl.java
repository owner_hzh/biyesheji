package com.cqjt.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cqjt.pojo.PracticalProjectCode;
import com.cqjt.service.IPracticalProjectCodeService;

public class PracticalProjectCodeServiceImpl extends BaseServiceImpl implements IPracticalProjectCodeService{

	@Override
	public List<PracticalProjectCode> getAllPracticalProjectCode(
			Map<String, Object> params) {
		// TODO Auto-generated method stub
		@SuppressWarnings("unchecked")
		List<PracticalProjectCode> practicalProjectCodes=(List<PracticalProjectCode>) super.select("getAllPracticalProjectCode", params);
		return practicalProjectCodes;
	}

	@Override
	public PracticalProjectCode getPracticalProjectCodeByCode(int code) {
		// TODO Auto-generated method stub
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("ppcode", code);
		PracticalProjectCode practicalProjectCode=(PracticalProjectCode) super.find("getPracticalProjectCodeByCode", params);
		return practicalProjectCode;
	}

	@Override
	public boolean addPracticalProjectCode(
			PracticalProjectCode practicalProjectCode) {
		// TODO Auto-generated method stub
		Object o=super.insert("addPracticalProjectCode", practicalProjectCode);
		return o==null?false:true;
	}

	@Override
	public boolean updatePracticalProjectCode(
			PracticalProjectCode practicalProjectCode) {
		// TODO Auto-generated method stub
		Object o=super.insert("updatePracticalProjectCode", practicalProjectCode);
		return o==null?false:true;
	}

	@Override
	public boolean deletePracticalProjectCode(
			PracticalProjectCode practicalProjectCode) {
		// TODO Auto-generated method stub
		Object o=super.insert("deletePracticalProjectCode", practicalProjectCode);
		return o==null?false:true;
	}

}
