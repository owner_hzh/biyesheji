package com.cqjt.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.cqjt.pojo.TestCode;
import com.cqjt.pojo.TestCustomize;
import com.cqjt.service.ITestCodeService;
import com.cqjt.service.ITestCustomizeService;

/**
 * 定制试卷接口实现
 * @author LIJIN
 *
 */

@Service(value="testCustomizeService")
//@WebService(endpointInterface = "com.cqjt.service.ICurriculumService", serviceName = "curriculumService")
public class TestCustomizeServiceImpl extends BaseServiceImpl implements ITestCustomizeService {

    /**
     * 获取定制试卷
     * @return 定制试卷list
     */
	@Override
	public List<TestCustomize> getAllTestCustomize(Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		List<TestCustomize> cwList=(List<TestCustomize>) super.select("gettestcustomize", params);
		return cwList;
	}

    
    /**
     * 添加定制试卷
     * @param 定制试卷对象
     * @return true  or false
     */
	@Override
	public boolean addTestCustomize(TestCustomize testCustomize) {
		Object o=super.insert("addtestcustomize", testCustomize);
		return o==null?false:true;
	}
	
    /**
     * 批量添加定制试卷
     * @param 定制试卷对象的LIST
     * @return true  or false
     */
    @SuppressWarnings("null")
	public boolean addTestCustomizeByList(List<TestCustomize> testCustomizeList)
    {
    	boolean success=true;
    	/*for(int i=0;i<testCustomizeList.size();i++)
    	{
    		Object o= super.insert("addtestcustomize", testCustomizeList.get(i));
    		if(o==null)
    			success=false;
    	}*/
    	try {
			super.insert("addtestcustomize", testCustomizeList);
		} catch (Exception e) {
			success=false;
			e.printStackTrace();
		}
    	return success;
    }


    /**
     * 根据定制试卷code删除这条信息
     * @param TestCustomize_code
     * @return true or false
     */
	@Override
	public boolean deleteTestCustomize(TestCustomize testCustomize) {
		HashMap<String, Object> params=new HashMap<String, Object>();
		//params.put("curriculum_code", testCustomize.getCurriculum_code());
		params.put("number", testCustomize.getNumber());
		params.put("qt_code", testCustomize.getQt_code());
		//params.put("qt_code", testCustomize.getQt_code());
		//params.put("qusetion_code", testCustomize.getQusetion_code());
		Integer o=super.delete("deletetestcustomize", params);
		return o==0?false:true;
	}

	
    /**
     * 根据定制试卷code更新定制试卷信息
     * @param 这个实体只需要code就可
     * @return true or false
     */
    public boolean updateTestCustomize(TestCustomize testCustomize){
    	return super.update("updatetestcustomize", testCustomize);
    }


	@Override
	public TestCustomize getTestCustomizeByPrimaryKey(Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		TestCustomize cwList=(TestCustomize) super.find("gettestcustomize", params);
		return cwList;
	}
}
