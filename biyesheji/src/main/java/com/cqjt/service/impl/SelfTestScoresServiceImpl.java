package com.cqjt.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import com.cqjt.pojo.SelfTestScores;
import com.cqjt.service.ISelfTestScoresService;

/**
 * 自测考试成绩接口实现
 * @author LIJIN
 *
 */

@Service(value="selfTestScoresService")
//@WebService(endpointInterface = "com.cqjt.service.ICurriculumService", serviceName = "curriculumService")
public class SelfTestScoresServiceImpl extends BaseServiceImpl implements ISelfTestScoresService {

    /**
     * 获取自测考试单个大题成绩
     * @return 自测考试成绩list
     */
	@Override
	public List<SelfTestScores> getAllSelfTestScores(Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		List<SelfTestScores> cwList=(List<SelfTestScores>) super.select("getselftestscores", params);
		return cwList;
	}
	
	/**
	 * 获取自测考试总成绩
	 * @return 自测考试成绩list
	 */
	@Override
	public SelfTestScores getTotalScores(Map<String, Object> params) {
		@SuppressWarnings("unchecked")
		SelfTestScores cwList=(SelfTestScores) super.find("gettotalscores", params);
		return cwList;
	}

    
    /**
     * 添加自测考试成绩
     * @param 自测考试成绩对象
     * @return true  or false
     */
	@Override
	public Integer addSelfTestScores(SelfTestScores selfTestScores) {
		Integer o=(Integer) super.insert("addselftestscores", selfTestScores);
		return o;
	}
	

    /**
     * 根据自测考试成绩code删除这条信息
     * @param SelfTestScores_code
     * @return true or false
     */
	@Override
	public boolean deleteSelfTestScores(SelfTestScores selfTestScores) {
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("curriculum_code", selfTestScores.getTest_code());
		Integer o=super.delete("deleteselftestscores", params);
		return o==0?false:true;
	}

	
    /**
     * 根据自测考试成绩code更新自测考试成绩信息
     * @param 这个实体只需要code就可
     * @return true or false
     */
    public boolean updateSelfTestScores(SelfTestScores selfTestScores){
    	return super.update("updateselftestscores", selfTestScores);
    }

	@Override
	public boolean updateSelfTestScores(List<SelfTestScores> selfTestScores) {
		try {
			super.update("updateselftestscores", selfTestScores);
			return true;
		} catch (Exception e) {
			return false;
		}
		
	}
}
