package com.cqjt.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.cqjt.pojo.TeacherCharacter;
import com.cqjt.service.ITeacherCharacterService;
import com.cqjt.service.ITeacherCharacterService;

public  class TeacherCharacterServiceImpl extends BaseServiceImpl implements ITeacherCharacterService{

	@Override
	public List<TeacherCharacter> getAllTeacherCharacter(Map<String, Object> params) {
		// TODO Auto-generated method stub
		@SuppressWarnings("unchecked")
		List<TeacherCharacter> list = (List<TeacherCharacter>) super.select("getAllTeacherCharacter", params); 
		return list;
	}

	@Override
	public TeacherCharacter getTeacherCharacterByCode(int tc_code) {
		// TODO Auto-generated method stub
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("tc_code", tc_code);
		TeacherCharacter TeacherCharacter = (TeacherCharacter) super.find("getTeacherCharacterByCode", params);
		return TeacherCharacter;
	}

	@Override
	public boolean addTeacherCharacter(TeacherCharacter TeacherCharacter) {
		// TODO Auto-generated method stub
		Object o=super.insert("addTeacherCharacter",TeacherCharacter);
		return o==null?false:true;
	}

	@Override
	public boolean updateTeacherCharacter(TeacherCharacter TeacherCharacter) {
		// TODO Auto-generated method stub
		Object o=super.update("updateTeacherCharacter",TeacherCharacter);
		return o==null?false:true;
	}

	@Override
	public boolean deleteTeacherCharacter(TeacherCharacter TeacherCharacter) {
		// TODO Auto-generated method stub
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("tc_code", TeacherCharacter.getTc_code());
		Object o=super.delete("deleteTeacherCharacter",params);
		return o==null?false:true;
	}

	@Override
	public void uploadTeacherCharacterExcel(MultipartFile file, String path) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<TeacherCharacter> getAllTeacherCharacter1(
			Map<String, Object> params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean addTeacherCharacter1(TeacherCharacter TeacherCharacter) {
		// TODO Auto-generated method stub
		return false;
	}
	
}
