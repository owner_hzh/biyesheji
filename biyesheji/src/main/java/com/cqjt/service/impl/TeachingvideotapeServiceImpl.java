package com.cqjt.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jws.WebService;

import com.cqjt.pojo.Teachingvideotape;
import com.cqjt.service.ITeachingvideotapeService;

/**
 * 教学录像实现接口
 * @author Jianming
 *
 */
@WebService(endpointInterface = "com.cqjt.service.ITeachingvideotapeService", serviceName = "teachingvideotapeService")
public class TeachingvideotapeServiceImpl extends BaseServiceImpl
                                          implements ITeachingvideotapeService
{
	/**
	 * 获取某一课程的所有教学录像(通过课程的代码curriculum_code)
	 */
	@Override
	public List<Teachingvideotape> getAllTeachingvideotape(Map<String, Object> param) 
	{
		@SuppressWarnings("unchecked")
		List<Teachingvideotape> teachingvideotapes=(List<Teachingvideotape>) super.select("getAllTeachingvideotape", param);
		return teachingvideotapes;
	}
	/**
	 * 添加一个教学录像(通过一个教学录像实体)
	 */
	@Override
	public boolean addTeachingvideotape(Teachingvideotape teachingvideotape) 
	{
		Object success=super.insert("addTeachingvideotape",teachingvideotape);
		return success==null?false:true;
	}
	/**
	 * 删除一个教学录像(通过一个教学录像实体)
	 */
	@Override
	public boolean delTeachingvideotape(Teachingvideotape teachingvideotape) 
	{
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("teachingvideotape_code", teachingvideotape.getTeachingvideotape_code());
		Integer success=super.delete("delTeachingvideotape", params);
		return success==0?false:true;
	}
	/**
	 * 给一个教学录像的下载次数加1(通过一个教学录像实体)
	 */
	@Override
	public boolean updateTeachingvidotapeDowncount(
			Teachingvideotape teachingvideotape) 
	{
		return super.update("updateTVdown_count", teachingvideotape);
	}

	/**
	 * 根据录像代码获取录像实体
	 */
	@Override
	public Teachingvideotape getTeachingvideotape(int teachingvideotape_code) 
	{
		HashMap<String, Object> params=new HashMap<String, Object>();
		//System.out.println("teachingvideotape_code="+teachingvideotape_code);
		params.put("teachingvideotape_code", teachingvideotape_code);
		List<Teachingvideotape> teachingvideotape=(List<Teachingvideotape>) super.select("getTeachingvideotape", params);
		return teachingvideotape.get(0);
	}


}
