/**
 * 
 */
package com.cqjt.service;

import java.util.List;
import java.util.Map;

import com.cqjt.pojo.CurriculumCharacter;
import com.cqjt.pojo.StudyMode;

/**
 * <p>Title: ICurriculumCharacterService.java</p>
 * <p>Description: MyEclipse 平台软件</p>
 * <p>Copyright: Copyright (c) 毕业设计小分队</p>
 * @author xiaoshujun
 * @version 1.0 创建时间：2014-3-21 下午5:03:59
 */

public interface ICurriculumCharacterService extends BaseService{
	/*
	 * 获取所有课程性质
	 * @return
	 */
	public List<CurriculumCharacter> getAllCurriculumCharacter();
	
	/*
	 * 获取所有课程性质
	 * @return
	 */
	public CurriculumCharacter getCurriculumCharacterByCode(int code);
	/*
	 * 增加课程性质
	 * @curriculumCharacter
	 */
	public boolean addCurriculumCharacter(CurriculumCharacter curriculumCharacter);
	/*
	 * 修改课程性质
	 * @curriculumCharacter
	 */
	public boolean updateCurriculumCharacter(CurriculumCharacter curriculumCharacter);
	/*
	 * 删除课程性质
	 * @curriculumCharacter
	 */
	public Integer deleteCurriculumCharacter(CurriculumCharacter curriculumCharacter);
}	
