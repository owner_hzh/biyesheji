package com.cqjt.service;

import java.util.List;
import java.util.Map;

import com.cqjt.pojo.TestQuestion;

/*
 *	author: xiaoshujun
 * date: 2014-4-20
 * 试题类接口
 */
public interface ITestQuestionService {
	   /**
     * 获取所有的题
     * @return 题型list
     */
    public List<TestQuestion> getAllTestQuestion(Map<String, Object> params);
    
    /**
     * 随机获取题
     * @return 题型list
     */
    public List<TestQuestion> getRandQuestion(Map<String, Object> params);
    
    /**
     * 根据题型code获取一个题型实体
     * @param 题型code
     * @return 题型实体
     */
    public TestQuestion getTestQuestionByCode(int code);
    
    /**
     * 添加题型
     * @param TestQuestion对象
     * @return true  or false
     */
    public boolean addTestQuestion(TestQuestion testQuestion);
    
    /**
     * 根据题型code更新题型
     * @param TestQuestion对象 这个实体只需要code就可
     * @return true or false
     */
    public boolean updateTestQuestion(TestQuestion testQuestion);
    
    /**
     * 根据题型code删除这条信息
     * @param TestQuestion对象 这个实体只需要code就可
     * @return true or false
     */
    public boolean deleteTestQuestion(TestQuestion testQuestion);
}
