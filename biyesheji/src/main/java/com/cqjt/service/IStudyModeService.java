package com.cqjt.service;

import java.util.List;


import com.cqjt.pojo.StudyMode;

/**
 * <p>Title: IStudyModeService.java</p>
 * <p>Description: MyEclipse 平台软件</p>
 * <p>Copyright: Copyright (c) 毕业设计小分队</p>
 * @author xiaoshujun
 * @version 1.0 创建时间：2014-3-21 下午4:32:58
 */

public interface IStudyModeService extends BaseService {
	/*
	 * 获取所有修读方式
	 * @return
	 */
	public List<StudyMode> getAllStudyMode();
	
	/*
	 * 获取所有修读方式
	 * @return
	 */
	public StudyMode getStudyModeByCode(int code);
	/*
	 * 增加修读方式
	 * @studyMode
	 */
	public boolean addStudyMode(StudyMode studyMode);
	/*
	 * 修改修读方式
	 * @studyMode
	 */
	public boolean updateStudyMode(StudyMode studyMode);
	/*
	 * 删除修读方式
	 * @studyMode
	 */
	public Integer deleteStudyMode(StudyMode studyMode);
	
}
