package com.cqjt.service;

import java.util.List;
import java.util.Map;

import com.cqjt.pojo.TestCustomize;

/**
 * 定制试卷接口
 * @author LIJIN
 *
 */
public interface ITestCustomizeService {
    
    /**
     * 获取定制试卷(单个或者多个)
     * @return 试卷list
     */
    public List<TestCustomize> getAllTestCustomize(Map<String, Object> params);
    
    /**
     * 根据试卷编码和题型代码获取试题
     * @return 试卷TestCustomize
     */
    public TestCustomize getTestCustomizeByPrimaryKey(Map<String, Object> params);
    
    
    /**
     * 添加定制试卷
     * @param 定制试卷对象
     * @return true  or false
     */
    public boolean addTestCustomize(TestCustomize testCustomize);
    
    
    /**
     * 根据定制试卷code删除这条信息
     * @param  这个实体只需要code就可
     * @return true or false
     */
    public boolean deleteTestCustomize(TestCustomize testCustomize);
    
    /**
     * 根据定制试卷code更新定制试卷信息
     * @param这个实体只需要code就可
     * @return true or false
     */
    public boolean updateTestCustomize(TestCustomize testCustomize);
    
    /**
     * 批量添加定制试卷
     * @param 定制试卷对象的LIST
     * @return true  or false
     */
    public boolean addTestCustomizeByList(List<TestCustomize> testCustomizeList);
}
