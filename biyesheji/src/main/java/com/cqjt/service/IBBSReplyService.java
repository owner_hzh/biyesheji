package com.cqjt.service;

import java.util.List;
import java.util.Map;

import com.cqjt.pojo.BBSReply;

public interface IBBSReplyService extends BaseService{
	/**
     * 获取所有的BBS回帖列表
     * @return BBS回帖list
     */
    public List<BBSReply> getAllBBSReply(Map<String, Object> params);
    
    /**
     * 根据BBS回帖code获取一个BBS回帖实体
     * @param BBS回帖code
     * @return BBS回帖实体
     */
    public BBSReply getBBSReplyByCode(int code);
    
    /**
     * 添加BBS回帖
     * @param BBSReply对象
     * @return true  or false
     */
    public boolean addBBSReply(BBSReply bBSReply);
    
    /**
     * 根据BBS回帖code更新BBS回帖
     * @param BBSReply对象 这个实体只需要code就可
     * @return true or false
     */
    public boolean updateBBSReply(BBSReply bBSReply);
    
    /**
     * 根据BBS回帖code删除这条信息
     * @param BBSReply对象 这个实体只需要code就可
     * @return true or false
     */
    public boolean deleteBBSReply(BBSReply bBSReply);
    
    
    public Long getPageCounts(Map<String, Object> params);
    
	public List<BBSReply> pageList(Map<String,Object> params);
}
