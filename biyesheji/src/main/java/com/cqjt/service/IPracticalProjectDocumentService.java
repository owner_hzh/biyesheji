package com.cqjt.service;

import java.util.List;
import java.util.Map;

import com.cqjt.pojo.PracticalProjectDocument;

public interface IPracticalProjectDocumentService {
	/**
     * 获取所有的实战项目列表
     * @param Map<String, Object> params 要有 ppnumber和ppcode
     * @return 实战项目list
     */
    public List<PracticalProjectDocument> getAllPracticalProjectDocumentByCodeAndNum(Map<String, Object> params);
    
    /**
     * 根据实战项目code获取一个实战项目实体
     * @param 实战项目code
     * @return 实战项目实体
     */
    public PracticalProjectDocument getPracticalProjectDocumentByCode(int code);
    
    /**
     * 添加实战项目
     * @param PracticalProjectDocument对象
     * @return true  or false
     */
    public boolean addPracticalProjectDocument(PracticalProjectDocument practicalProjectDocument);
    
    /**
     * 根据实战项目code更新实战项目
     * @param PracticalProjectDocument对象 这个实体只需要code就可
     * @return true or false
     */
    public boolean updatePracticalProjectDocument(PracticalProjectDocument practicalProjectDocument);
    
    /**
     * 根据实战项目code删除这条信息
     * @param Map<String, Object> params 只需要ppd_code
     * @return true or false
     */
    public boolean deletePracticalProjectDocument(Map<String, Object> params);
    
    /**
     * 根据实战项目的实战文档序号(ppd_code)更新一个实战文档的下载次数
     * @param Map<String, Object> params 要有 ppd_code
     * @return true or false
     */
    public boolean updatePracticalProjectDocumentDownCount(Map<String, Object> params);
    /**
     * 根据传过来的文档实际路径删除文档
     * @param String docPath
     * @return true or false
     */
    public boolean deleteRelatedDoc(String docPath);
}
