package com.cqjt.service;

import java.util.List;
import java.util.Map;

import com.cqjt.pojo.TestCode;

/**
 * 试卷编码接口
 * @author LIJIN
 *
 */
public interface ITestCodeService {
    
    /**
     * 获取试卷编码(单个或者多个)
     * @return 试卷list
     */
    public List<TestCode> getAllTestCode(Map<String, Object> params);
    
    
    /**
     * 添加试卷编码
     * @param 试卷编码对象
     * @return true  or false
     */
    public int addTestCode(TestCode testCode);
    
    
    /**
     * 根据试卷编码code删除这条信息
     * @param  这个实体只需要code就可
     * @return true or false
     */
    public boolean deleteTestCode(TestCode testCode);
    
    /**
     * 根据试卷编码code更新试卷编码信息
     * @param这个实体只需要code就可
     * @return true or false
     */
    public boolean updateTestCode(TestCode testCode);
}
