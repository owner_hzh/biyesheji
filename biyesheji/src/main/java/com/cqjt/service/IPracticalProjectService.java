package com.cqjt.service;

import java.util.List;
import java.util.Map;

import com.cqjt.pojo.PracticalProject;

public interface IPracticalProjectService {
	/**
     * 获取所有的实战项目列表
     * @return 实战项目list
     */
    public List<PracticalProject> getAllPracticalProject(Map<String, Object> params);
    
    /**
     * 根据实战项目ppcode和ppnumber获取一个实战项目实体
     * @param 实战项目ppcode,ppnumber
     * @return 实战项目实体
     */
    public PracticalProject getPracticalProjectByCode(Map<String, Object> params);
    
    /**
     * 根据ppcode和ppnumber获取实战信息
     * @param ppcode
     * @param ppnumber
     * @return
     */
    public PracticalProject getPracticalProjectByCodeAndNumber(int ppcode,String ppnumber);
    
    /**
     * 获取最新的一条信息
     * @return
     */
    public PracticalProject getNewerPracticalProjectByCode();
    
    /**
     * 添加实战项目
     * @param PracticalProject对象
     * @return true  or false
     */
    public boolean addPracticalProject(PracticalProject practicalProject);
    
    /**
     * 根据实战项目code更新实战项目
     * @param PracticalProject对象 这个实体只需要code就可
     * @return true or false
     */
    public boolean updatePracticalProject(PracticalProject practicalProject);
    
    /**
     * 根据实战项目code删除这条信息
     * @param PracticalProject对象 这个实体只需要code就可
     * @return true or false
     */
    public boolean deletePracticalProject(PracticalProject practicalProject);
    /**
     * 根据页数获取基础实战的15条纪录
     * @param Map<String, Object> params 要有int型的“page”页数，和 int型“ppcode”类型号(1为老师，0为学生)
     * @return List<PracticalProject>
     */
    public List<PracticalProject> getPageBasicPracticalProjects(Map<String, Object> params);
    /**
     * 获取基础实战的页数（15条纪录为1页）
     * @param Map<String, Object> params 要有 int型“ppcode”类型号(1为老师，0为学生)
     * @return List<int>
     */
    public int[] getBasicPracticalProjectsPagenum(Map<String, Object> params);
    
    /**
     * 老师审题事务 系列方法
     * @param number 参数为实战项目备选表的序号 
     * @param code   实战题目类型号
     * @param isTeaOrStu  来源是老师还是学生  1 代表老师  2代表学生
     * @param loginID  登录者的id号
     * @return
     */
    public Boolean shenTiTransationMethod(int number,int code,int isTeaOrStu,String loginID);
    /**
     * 老师添加实战带实战文档事务 系列方法
       @param PracticalProject 一个实战实体
     * @param string ppd_name  传到实战项目参考文档的 文件名
     * @param string ppd_link 传到实战项目参考文档的 文件路径
     * @return Boolean 成功为true,失败为false
     */
    public Boolean teacherAddPracticingWithDoc(PracticalProject practicalProject,String ppd_name,String ppd_link);
    
}
