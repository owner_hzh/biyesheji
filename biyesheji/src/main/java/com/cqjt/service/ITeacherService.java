package com.cqjt.service;

import java.util.List;
import java.util.Map;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.MultipartFile;

import com.cqjt.pojo.Courseware;
import com.cqjt.pojo.Teacher;
import com.cqjt.pojo.TeacherInfor;
import com.cqjt.pojo.User;
import com.cqjt.util.PageCondition;

public interface ITeacherService {
	/**
	 * 把Excel数据导入到数据库 以数据库表字段为Excel表头格式
	 * @param file
	 * @param path
	 */
   public void uploadTeachersExcel(MultipartFile file, String path);
	/**
	 * 根据用户名读取教师的信息
	 * @param Map<String, Object> params
	 * return Teacher
	 * author jianming
	 */
   public Teacher getTeacherByUserid(Map<String, Object> params);
   
	/**
	 * 插入教师有关的信息
	 * @param teacher
	 * @param teacherInfor
	 * @param user
	 */
   public void insert(Teacher teacher,TeacherInfor teacherInfor,User user );
   
	
	/**
	 * 修改教师有关的信息
	 * @param teacher
	 * @param teacherInfor
	 */
	public void update(Teacher teacher, TeacherInfor teacherInfor,User user);
   
   /**
    * 获取课件(单个或者多个)
    * @return 课程list
    */
   public List<Teacher> getAllTeachers(Map<String, Object> params);
   
   /**
    * 分页
    * @param pageSize 每页显示条数
    * @param pageNo  当前页数
    * @param params  筛选条件
    * @param request
    * @return PageCondition
    */
   public PageCondition getAllTeachers(int pageSize, int pageNo,Map<String, Object> params,HttpServletRequest request);

   public int updateByPrimaryKeySelective(Teacher record) throws SQLException;

   public Teacher selectByPrimaryKey(String userId);

	/**
	 * 导出教师
	 * 
	 * @return 教师list
	 */
	public List<Teacher> exportTeachers(Map<String, Object> params);

   
   public List<Teacher> getTeacherFullInfoByUserid(Map<String, Object> params);

   public int deleteByPrimaryKey(String userId);
   
   public Teacher getTeacherByTeacherName(String name);
   
}
