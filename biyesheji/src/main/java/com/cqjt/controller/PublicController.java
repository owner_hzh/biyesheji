package com.cqjt.controller;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileUploadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;
import com.cqjt.util.UploadJSON;

/**
 * 公共Controller
 * @author LIJIN
 *
 */
@Controller
@RequestMapping(value = "/public/")
public class PublicController {

	/**
	 * 上传
	 * @param model
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws FileUploadException 
	 */	
	@RequestMapping(value = "upload", method = RequestMethod.POST)
	public void fileManager(HttpServletRequest request,HttpServletResponse response) throws IOException, FileUploadException{
		UploadJSON.uploadFile(request, response);
	}
}
