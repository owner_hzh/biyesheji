package com.cqjt.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cqjt.pojo.BBSSection;
import com.cqjt.pojo.BBSTopic;
import com.cqjt.pojo.Curriculum;
import com.cqjt.pojo.Major;
import com.cqjt.pojo.MenuLevelOne;
import com.cqjt.pojo.MenuLevelTwo;
import com.cqjt.pojo.Page;
import com.cqjt.pojo.QuestionTypes;
import com.cqjt.pojo.User;
import com.cqjt.service.IBBSSectionService;
import com.cqjt.service.IBBSTopicService;
import com.cqjt.service.ICurriculumCharacterService;
import com.cqjt.service.ICurriculumService;
import com.cqjt.service.IMajorService;
import com.cqjt.service.IMenuService;
import com.cqjt.service.IQuestionTypesService;
import com.cqjt.service.IStudyModeService;
import com.cqjt.util.MD5Util;



@Controller
@RequestMapping(value = "/")
public class MenuController {
	
	//注入菜单的Service
	@Autowired
	private IMenuService menuService;
	//注入专业的Service
	@Autowired
	private IMajorService majorService;
	//注入课程的Service
	@Autowired
	private ICurriculumService curriculumService;

	//注入题型的Serivce
	@Autowired
	private 
	IQuestionTypesService questionTypesService;

	@Autowired
	private ICurriculumCharacterService curriculumCharacterService;
	@Autowired
	private IStudyModeService studyModeService;

	@Autowired
	private IBBSSectionService bBSSectionService;

	@Autowired
	private IBBSTopicService bBSTopicService;
	/**
	 * 登录
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String login(Model model) 
	{
		return "login";
	}
	
	/**
	 * 到主页
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "home", method = RequestMethod.GET)
    public String getMenu(Model model, HttpServletRequest request,HttpServletResponse response ) 
    {
		User user = (User)request.getSession().getAttribute("loginuser");
    	List<MenuLevelOne> menuLevelOneList=menuService.getMenuOneLevelByRoleCode(user.getRole_code());
    	model.addAttribute("menuList", menuLevelOneList);
		return "home";
    }
	
    
	@RequestMapping(value ="/content" , method = RequestMethod.GET)
	public ModelAndView content() {
		return new ModelAndView("content");
	}
	
	/**
	 * 读取系统维护的菜单
	 * @return
	 */
    @RequestMapping(value ="/ssystem-maintenance" , method = RequestMethod.GET)
	public ModelAndView ssystem_maintenance() {			
    	List<MenuLevelTwo> menuLevelTwoList=menuService.getMenuLevelTwoAndThreeByMenuLevelOneCode(1);
		return new ModelAndView("ssystem-maintenance","menulist",menuLevelTwoList);
	}
	
	@RequestMapping(value ="/curriculum-system")
	public ModelAndView goCurriculumSystem() {
		/*
		 * return new ModelAndView("curriculum-system");
		 * 此处修改至如下
		 */
		return new ModelAndView("curriculum/curriculum");
		//return new ModelAndView("redirect:/curriculum/search");
	}
	@RequestMapping(value ="/teachers-qualification")
	public ModelAndView teachers_qualification() {
		return new ModelAndView("teachers-qualification");
	}
	
	
	/**
	 * 跳转的课件页面
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "courseware", method = RequestMethod.GET)
	public String goCourseware(ModelMap model,HttpServletRequest request, HttpServletResponse response) {
		List<Major> menuLevelTwoList=majorService.getMajorList();
		model.addAttribute("menuLevelTwoList", menuLevelTwoList);//保存二级菜单，给前台
		return "courseware/courseware";
	}
	
	
	//跳转到作业页面 author by 见鸣
	@RequestMapping(value = "assignment", method = RequestMethod.GET)
	public String goAssignmentMenu(ModelMap model,HttpServletRequest request, HttpServletResponse response)
	{
		List<Major> menuLevelTwoList=majorService.getMajorList();
		model.addAttribute("menuLevelTwoList", menuLevelTwoList);//保存二级菜单，给前台
		return "assignment/assignment";
	}
	//跳转到教学录像页面 author by 见鸣
	@RequestMapping(value = "teachingvideotape", method = RequestMethod.GET)
	public String goTeachingvideotapeMenu(ModelMap model,HttpServletRequest request, HttpServletResponse response)
	{
		List<Major> menuLevelTwoList=majorService.getMajorList();
		model.addAttribute("menuLevelTwoList", menuLevelTwoList);//保存二级菜单，给前台
		return "teachingvideotape/teachingvideotape";
	}
	//跳转到教辅资料界面 xiaoshuju 2014-5-5
	@RequestMapping(value ="/teaching-materials")
	public ModelAndView goTeachingMaterials(ModelMap model,HttpServletRequest request, HttpServletResponse response) {
		List<Major> menuLevelTwoList=majorService.getMajorList();
		model.addAttribute("menuLevelTwoList", menuLevelTwoList);//保存二级菜单，给前台
		return new ModelAndView("teachingdata/teaching-materials");
	}
	@RequestMapping(value ="/teaching-video")
	public ModelAndView teaching_video() {
		return new ModelAndView("teaching-video");
	}
	@RequestMapping(value ="/homework")
	public ModelAndView homework() {
		return new ModelAndView("homework");
	}

	/**
	 * 跳转到在线测试页面
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "online-testing", method = RequestMethod.GET)
	public String goOnlineTesting(ModelMap model,HttpServletRequest request, HttpServletResponse response) {
		User user = (User)request.getSession().getAttribute("loginuser");
		Map<String, Object> params =new HashMap<String, Object>();
		List<MenuLevelTwo> menuLevelTwoList =null;
		if(user.getRole_code()==6){
			menuLevelTwoList=menuService.getMenuLevelTwoAndThreeByMenuLevelOneCode(10);
		}else if(user.getRole_code()==2){
			menuLevelTwoList=menuService.getMenuLevelTwoAndThreeByMenuLevelOneCode(25);
		}else{
			menuLevelTwoList=menuService.getMenuLevelTwoAndThreeByMenuLevelOneCode(36);
		}
		model.addAttribute("menulist", menuLevelTwoList);//保存二级菜单，给前台
		return "onlinetest/online-test";
	}

	//添加/修改用户
	@RequestMapping(value ="/menu-content-add-user")
	public ModelAndView menu_content_add_user() {
		return new ModelAndView("menu/menu-content-add-user");
	}
	//修改密码
	@RequestMapping(value ="/menu-content-change-password")
	public ModelAndView menu_content_change_password() {
		return new ModelAndView("menu/menu-content-change-password");
	}
	//添加方向
	@RequestMapping(value ="/menu-content-add-direction")
	public ModelAndView menu_content_add_direction() {
		return new ModelAndView("menu/menu-content-add-direction");
	}
	//浏览/修改方向
	@RequestMapping(value ="/menu-content-change-direction")
	public ModelAndView menu_content_change_direction(ModelMap model) {
		List<Major> majors=majorService.getMajorListOnlyCodeAndName();
		model.addAttribute("majors", majors);
		return new ModelAndView("menu/menu-content-change-direction");
	}
	//添加性质
	@RequestMapping(value ="/menu-content-add-property")
	public ModelAndView menu_content_add_property() {
		return new ModelAndView("redirect:/curriculum/menu_content_add_property");
		//return new ModelAndView("menu/menu-content-add-property");
	}
	//浏览/修改性质
	@RequestMapping(value ="/menu-content-change-property")
	public ModelAndView menu_content_change_property(ModelMap model,Page page) {
		//List<Curriculum> curriculums=curriculumService.getAllCurriculum(null); 
		Map<String, Object> params=new HashMap<String, Object>();
		//分页显示		
		params.put("pageSize", page.getPageSize());
		params.put("pageNo", page.getPageNo());
					
		List<Curriculum> curriculums=curriculumService.getCurriculumByPages(params);

		 for(int i=0;i<curriculums.size();i++)
		 {
			 Curriculum c=curriculums.get(i);
			 c.setStudymode_name(studyModeService.getStudyModeByCode(c.getStudymode_id()).getStudymode_name());
			 c.setCc_name(curriculumCharacterService.getCurriculumCharacterByCode(c.getCc_code()).getCc_name());
		 }
		 model.addAttribute("curriculums", curriculums);
		 //数据总条数
		 page.setTotalCount(curriculumService.getCount());
		 page.setCurrentCount(curriculums.size());
		 model.addAttribute("page",page);//当前页的条数
		return new ModelAndView("menu/menu-content-change-property");
	}
	//添加题型
	@RequestMapping(value ="/menu-content-add-category")
	public ModelAndView menu_content_add_category() {
		return new ModelAndView("menu/menu-content-add-category");
	}
	//浏览/修改题型
	//author by jianming 
	@RequestMapping(value ="/menu-content-change-category")
	public ModelAndView menu_content_change_category(ModelMap model) 
	{
		List<QuestionTypes> questionTypesList=questionTypesService.getAllQuestionTypes(null);
		if(questionTypesList!=null&questionTypesList.size()!=0)
		{
		    model.addAttribute(questionTypesList);
		}
		return new ModelAndView("menu/menu-content-change-category");
	}
	
	@RequestMapping(value ="/SaveFile")
	public ModelAndView SaveFile() {
		return new ModelAndView("SaveFile");
	}
	//显示修改密码页面
	//author by jianming
	@RequestMapping(value ="/displaychangepassword")
	public ModelAndView displaychangepassword()
	{
		return new ModelAndView("changepassword");
	}

	
	//添加课程页面
	//author:xiaoshuju 2014-4-20
	@RequestMapping(value ="/menu-content-add-curriculum")
	public ModelAndView menu_content_add_curriculum() {
		return new ModelAndView("menu/menu-content-add-curriculum");
	}
	//查看课程详细页面
	//author:xiaoshuju 2014-4-20
	@RequestMapping(value ="/menu-content-check-curriculum")
	public ModelAndView menu_content_update_curriculum(HttpServletRequest request) {
		int curriculum_code = 0;
		if(request.getParameter("curriculum_code")!=null)
			curriculum_code = Integer.parseInt(request.getParameter("curriculum_code").toString().trim());
		Curriculum curriculum = curriculumService.getCurriculumByCode(curriculum_code);
		List<Major> majors = majorService.getMajorList();
		
		request.setAttribute("majors", majors);
		request.setAttribute("curriculum", curriculum);
		//return new ModelAndView("menu/menu-content-update-curriculum");
		//return new ModelAndView("curriculum/update-curriculum");
		return new ModelAndView("curriculum/check-curriculum");
	}
	
	//bbs管理 xiaoshujun 2014-5-3
	@RequestMapping(value ="/menu-content-change-bbs")
	public ModelAndView menu_content_change_bbs(HttpServletRequest request) {	
		List<BBSSection> bbsSections = bBSSectionService.getAllBBSSection(null);
		request.setAttribute("bbsSections",bbsSections );
		
		return new ModelAndView("menu/menu-content-change-bbs");
	}
	
	@RequestMapping(value ="/menu-content-change-bbstopic")
	public ModelAndView menu_content_change_bbstopic(HttpServletRequest request) {	
		List<BBSSection> bbsSections = bBSSectionService.getAllBBSSection(null);
		List<BBSTopic> bbsTopics = bBSTopicService.getAllBBSTopic(null);
		request.setAttribute("bbsSections",bbsSections );
		request.setAttribute("bbsTopics", bbsTopics);
		request.setAttribute("sid", -1);
		
		return new ModelAndView("bbs/bbstopic-manage");
	}
	
	//修改，跳入bbs xiaoshujun 2014-5-3
	@RequestMapping(value ="/bbs")
	public ModelAndView interactive_platform() {	
		return new ModelAndView("bbs/bbs");
	}
	
	/**
	 * 专业跳转
	 * @return
	 */
	@RequestMapping("/professional-introduction")
	public ModelAndView professional_introduction() {
		List<Major> majors=majorService.getMajorListOnlyCodeAndName();
		return new ModelAndView("professional-introduction","majors",majors);
	}
	
}
