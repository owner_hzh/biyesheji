package com.cqjt.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileUploadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.alibaba.fastjson.JSONObject;
import com.cqjt.pojo.Courseware;
import com.cqjt.pojo.Curriculum;
import com.cqjt.pojo.Teacher;
import com.cqjt.pojo.TeacherCharacter;
import com.cqjt.pojo.TeacherInfor;
import com.cqjt.pojo.TechnicalPost;
import com.cqjt.service.ITeacherCharacterService;
import com.cqjt.service.ITeacherInforServive;
import com.cqjt.service.ITeacherService;
import com.cqjt.service.ITechnicalPostService;
import com.cqjt.service.impl.TeacherServiceImpl;
import com.cqjt.util.UploadJSON;
/**
 * 师资情况controller
 * @author 文
 */

@Controller
@RequestMapping(value = "/teacherinfo")
public class TeacherInfoController {
	@Autowired
	private ITeacherService teacherService;
	@Autowired
	private ITeacherInforServive teacherInforServive;
	@Autowired
	private ITechnicalPostService technicalPostService;
	@Autowired
	private ITeacherCharacterService technicalCharacterService;
	//显示教师信息
	@RequestMapping(value="/teacherinfo/info",method=RequestMethod.GET)
	public ModelAndView getTeacher(ModelMap model, String user_id,String teacher_name) throws UnsupportedEncodingException
	{
		//Map<String, Object> param =new HashMap<String, Object>();

		//List<Teacher>  TeacherList=teacherService.getTeacherFullInfoByUserid(param);
		//List<TechnicalPost> TechnicalPostList=technicalPostService.getAllTechnicalPost(param);

		List<Teacher>  TeacherList=teacherService.getTeacherFullInfoByUserid(null);
		if(TeacherList!=null&&TeacherList.size()>0)
		{
			model.addAttribute("TeacherList", TeacherList);
		}
		return new ModelAndView("teacherinfo/show-teacherinfo");		
	}
	//显示单个教师详细信息
	@RequestMapping(value="/showTeacherDetail")
	public ModelAndView showTeacherDetail(ModelMap model, String user_id) 
	{
		System.out.println("进入成功");
		TeacherInfor teacher=teacherInforServive.getTeacherInforByCode(user_id.trim());
		System.out.println(teacher.getUser_id());
		System.out.println(teacher.getTeacher_name());
		System.out.println(teacher.getTeacher_sex());
		System.out.println(teacher.getTeacher_date());
		model.addAttribute("teacher", teacher);
		return new ModelAndView("teacherinfo/teacherDetail");		
	}
	//修改界面显示教师信息
	//author by 文 2014-5-18
	@RequestMapping(value="/displayTeacherModify")
	public ModelAndView teacherModify(ModelMap model, String user_id) 
	{
		System.out.println("进入成功");
		TeacherInfor teacher=teacherInforServive.getTeacherInforByCode(user_id.trim());
		List<TeacherCharacter> teacherCharacters=technicalCharacterService.getAllTeacherCharacter(null);
		List<TechnicalPost> technicalPost=technicalPostService.getAllTechnicalPost(null);
		model.addAttribute("teacher", teacher);
		model.addAttribute("teacherCharacters", teacherCharacters);
		model.addAttribute("technicalPost", technicalPost);
		return new ModelAndView("teacherinfo/teacherModify");		
	}
	//修改界面修改教师信息
	//author by 文 2014-5-18
		@RequestMapping(value="/teacherModify",method=RequestMethod.POST )
		public ModelAndView updateTeacherinfor(TeacherInfor teacherinfor,Teacher newteacher,
				HttpServletRequest request, HttpServletResponse response,ModelMap model) throws IOException, SQLException{
			//System.out.println("教师信息:"+teacherinfor.toString());
			teacherInforServive.updateTeacherInfor(teacherinfor);
			teacherService.updateByPrimaryKeySelective(newteacher);
			TeacherInfor teacher=teacherInforServive.getTeacherInforByCode(teacherinfor.getUser_id());
			model.addAttribute("teacher", teacher);
			return new ModelAndView("teacherinfo/teacherDetail");	
		}
		//头像上传
		@RequestMapping(value="/uploadIco",method=RequestMethod.POST )
		public void uploadIco(HttpServletRequest request, HttpServletResponse response ,String userid) throws IOException, FileUploadException{
			JSONObject json  = UploadJSON.uploadFile(request, response);
			if(json.getString("error").equals("0")){
				TeacherInfor teacherinfor=new TeacherInfor();
				teacherinfor.setUser_id(userid);
				teacherinfor.setIconpath(json.getString("url"));
				System.out.println("========"+userid+"========"+json.getString("url"));
				teacherInforServive.updateTeacherInfor(teacherinfor);
			}
		}
}
