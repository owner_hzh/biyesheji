package com.cqjt.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cqjt.pojo.Major;
import com.cqjt.service.IMajorService;
import com.cqjt.util.GsonUtil;


@Controller
@RequestMapping(value = "/major")
public class MajorController {
	
	@Resource(name = "majorService")
	private IMajorService majorService;
	
	//显示详细的专业信息 showdetailmajor
	@RequestMapping(value = "/showdetailmajor", method = RequestMethod.GET)  
	public ModelAndView showDetailMajor(int id,ModelMap model)
	{
		Map<String, Object> params=new HashMap<String, Object>();
		params.put("major_code", id);
		Major major=majorService.getMajor(params);
		model.addAttribute("major", major);
		return new ModelAndView("professional/show-detail");		
	}
	
	//点击更新跳转页面
	@RequestMapping(value = "/updatemajor", method = RequestMethod.GET)  
	public ModelAndView updateMajor(int id,ModelMap model)
	{
		Map<String, Object> params=new HashMap<String, Object>();
		params.put("major_code", id);
		Major m=majorService.getMajor(params);
		model.addAttribute("major", m);
		return new ModelAndView("professional/update-major-page");		
	}
	
	//更新专业信息
	@RequestMapping(value = "/updatehtml", method = RequestMethod.POST)  
	public void updateHtml(String fild,String fildString,int major_code,HttpServletResponse response)
	{
		System.out.println("---fild--"+fild+"---major_code--"+major_code);
		Major major=new Major();
		major.setMajor_code(major_code);
		// 获得属性的首字母并转换为大写，与setXXX对应
        String firstLetter = fild.substring(0, 1).toUpperCase();
        String setMethodName = "set" + firstLetter
                + fild.substring(1);

        try {
        	Method setMethod = Major.class.getMethod(setMethodName,String.class);
			setMethod.invoke(major, fildString);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
        PrintWriter out=null;
		boolean f=majorService.updateMajor(major);
		try {
			out=response.getWriter();
			out.print(f);
		} catch (IOException e) {
			out.print("false");
			e.printStackTrace();
		}finally{
			if(out!=null)
				out.close();
		}
	}
	
	//添加专业方向
	@RequestMapping(value = "/addMajor", method = RequestMethod.POST)      
	public void addRole(@ModelAttribute("major")Major major,HttpServletResponse response) 
	{          
		boolean flag=majorService.addMajor(major); 
		PrintWriter out = null;
		try {
			out = response.getWriter();
		} catch (IOException e) {
			e.printStackTrace();
		}
		out.print(flag);  
	}
	
	//检查专业名称
	@RequestMapping(value = "/checkmajor", method = RequestMethod.POST)      
	public void checkMajor(String major_name,HttpServletResponse response) 
	{          
		Map<String, Object> params=new HashMap<String, Object>();
		params.put("major_name", major_name);
		Major m=majorService.getMajor(params);
		boolean flag=false;
		if(m!=null)
		{
			flag=true;
		}
		PrintWriter out = null;
		try {
			out = response.getWriter();
		} catch (IOException e) {
			e.printStackTrace();
		}
		out.print(flag);  
	}
	
	//系统维护中修改专业点击查看查询major数据
	@RequestMapping(value = "/sysmain/updatemajor", method = RequestMethod.GET)  
	public ModelAndView sysMainUpdateMajor(int code,ModelMap model,HttpServletResponse response)
	{
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json; charset=UTF-8");
		Map<String, Object> params=new HashMap<String, Object>();
		params.put("major_code", code);
		Major m=majorService.getMajor(params);
		model.addAttribute("major", m);
		return new ModelAndView("professional/menu-content-change-direction-change");	
	}
	
	//系统维护中修改专业点击删除major数据
	@RequestMapping(value = "/sysmain/deletemajor", method = RequestMethod.GET)  
	public void sysDeleteMajor(int code,ModelMap model,HttpServletResponse response) throws IOException
	{
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json; charset=UTF-8");
		Map<String, Object> params=new HashMap<String, Object>();
		params.put("major_code", code);
		boolean m=majorService.deleteMajor(params);
		PrintWriter out=response.getWriter();
		out.print(m);
	}
	
	//系统维护中修改专业提交按钮事件
	@RequestMapping(value = "/sysmain/submitMajor", method = RequestMethod.POST)      
	public void submitMajor(@ModelAttribute("major")Major major,HttpServletResponse response) 
	{          
		boolean flag=majorService.updateMajor(major); 
		PrintWriter out = null;
		try {
			out = response.getWriter();
		} catch (IOException e) {
			e.printStackTrace();
		}
		out.print(flag);  
	}
	
	//涓撲笟闈欐�椤甸潰璇锋眰
	@RequestMapping("/static-html")
	public ModelAndView static_html() {
		return new ModelAndView("static-html");
	}
	
	//添加角色
	@RequestMapping(value = "/getallmajor", method = RequestMethod.GET)      
	public ModelAndView getAllMajor(Model model,HttpServletRequest request) 
	{          
		String url="";
		List<Major> majors = majorService.getMajorList();
		model.addAttribute("majorlist", majors);
		if(request.getParameter("url")!=null)
			url = request.getParameter("url").toString().trim();
		ModelAndView mv = new ModelAndView(url);
		mv.addObject("majorlist", majors);
		return mv;
	}	
	
}
