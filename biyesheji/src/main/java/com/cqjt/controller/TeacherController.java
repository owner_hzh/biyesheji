package com.cqjt.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileUploadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.cqjt.pojo.Courseware;
import com.cqjt.pojo.Curriculum;
import com.cqjt.pojo.Major;
import com.cqjt.pojo.Teacher;
import com.cqjt.pojo.TeacherInfor;
import com.cqjt.pojo.User;
import com.cqjt.service.ICoursewareService;
import com.cqjt.service.ICurriculumService;
import com.cqjt.service.ITeacherService;
import com.cqjt.service.IUserService;
import com.cqjt.util.DocConverter;
import com.cqjt.util.MD5Util;
import com.cqjt.util.PageCondition;
import com.cqjt.util.PageNameEnum;
import com.cqjt.util.UploadJSON;

/**
 * 教师Controller
 * @author LIJIN
 *
 */
@Controller
@RequestMapping(value = "/teacher/")
public class TeacherController {
	//注入用户的Service
	@Autowired
	private IUserService userService;
	//注入教师的Service
	@Autowired
	private ITeacherService teacherService;

	
	//教师导入
	@RequestMapping(value ="upload")
	public void menu_content_import_teachers(HttpServletRequest request, HttpServletResponse response) throws IOException, FileUploadException {
		JSONObject json  = UploadJSON.uploadFile(request, response);
		//return "menu/menu-content-import-teachers";
	}
	
	//教师列表
	@RequestMapping(value ="menu-content-show-teachers", method = RequestMethod.GET)
	public String menu_content_show_teachers(ModelMap model,HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {
		Map<String, Object> params =new HashMap<String, Object>();
		String teacher_name= request.getParameter("teacher_name");
		String teacher_sex= request.getParameter("teacher_sex");
		if(teacher_name != null && teacher_name.trim().length() > 0) {
			teacher_name = 	java.net.URLDecoder.decode(teacher_name,"utf-8");
        	params.put("teacher_name", teacher_name);
        }

		if(!"".equals(teacher_sex) && teacher_sex != null && !teacher_sex.equals("null")){
			params.put("teacher_sex", Integer.parseInt(teacher_sex));
		}
		//设置分页  获得当前页码
		String pageNoStr = request.getParameter("page"); 
        int pageNo = 1;  
        if(pageNoStr != null && !pageNoStr.equals("")){
        	pageNo = Integer.parseInt(pageNoStr);  
        }
        PageCondition pagecondition = teacherService.getAllTeachers(PageNameEnum.PAGESIZE, pageNo,params,request);
        model.addAttribute("pagecondition", pagecondition);
        model.addAttribute("teacher_name", teacher_name);
        model.addAttribute("teacher_sex", teacher_sex);
		return "menu/menu-content-show-teachers";
	}	
	//添加教师
	@RequestMapping(value ="menu-content-add-teachers", method = RequestMethod.GET)
	public String menu_content_add_teachers(ModelMap model,HttpServletRequest request, HttpServletResponse response,String id) {
		System.out.println("============"+id+"--------------");
		String id1 = request.getParameter("id");
		System.out.println("============"+id+"--------------");
		if(id!=null){
			System.out.println("============"+id);
			Map<String, Object> params =new HashMap<String, Object>();
			params.put("user_id", id);
			Teacher teacher= teacherService.getTeacherByUserid(params);
			model.addAttribute("oldteacher",teacher);
		}
		return "menu/menu-content-add-teachers";
	}
	//添加教师
	@RequestMapping(value ="menu-content-add-teachers", method = RequestMethod.POST)
	public String add_teachers(User user,HttpServletRequest request, HttpServletResponse response,Teacher teacher ,TeacherInfor teInfor) {
		user.setPassword(MD5Util.MD5(user.getPassword()));
		teacherService.insert(teacher,teInfor,user);
		return "redirect:menu-content-show-teachers";
	}
	//修改教师
	@RequestMapping(value ="menu-content-update-teachers", method = RequestMethod.POST)
	public String update_teachers(User user,HttpServletRequest request, HttpServletResponse response,Teacher teacher ,TeacherInfor teInfor) {
		teacherService.update(teacher,teInfor,user);
		return "redirect:menu-content-show-teachers";
	}
	
	//删除教师
	@RequestMapping(value ="delete", method = RequestMethod.GET)
	public void deleteTeacher(HttpServletRequest request, HttpServletResponse response,String userId) throws IOException {
		PrintWriter out = response.getWriter();
		int i = teacherService.deleteByPrimaryKey(userId);
		if(i>0){
			out.print("success");
		}else{
			out.print("fail");
		}
	}
	
	//查看是否已注册
	@RequestMapping(value ="checkuser", method = RequestMethod.POST)
	public void checkUser(User user ,HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		User rs = userService.getUserid(user.getUser_id());
		if(rs!=null){
			out.print("have");
		}else{
			out.print("no");
		}
	}
	
}
