package com.cqjt.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.ibatis.migration.commands.NewCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cqjt.pojo.MenuLevelOne;
import com.cqjt.pojo.Role;
import com.cqjt.pojo.Student;
import com.cqjt.pojo.Teacher;
import com.cqjt.pojo.User;
import com.cqjt.service.ICurriculumService;
import com.cqjt.service.IMajorService;
import com.cqjt.service.IMenuService;
import com.cqjt.service.IRoleService;
import com.cqjt.service.IStudentService;
import com.cqjt.service.ITeacherService;
import com.cqjt.service.IUserService;
import com.cqjt.util.AuthImage;
import com.cqjt.util.MD5Util;

/**
 * Handles requests for the application home page.
 */
@Controller
@RequestMapping(value = "/login")
public class HomeController {
	//注入课程的Service
	@Autowired
	private IUserService userService;
	
	//注入角色的Service
	@Autowired
	private IRoleService roleService;
	
	//注入菜单的Service
	@Autowired
	private IMenuService menuService;
	
	//注入教师的Service
	@Autowired
	private ITeacherService teacherService;
	
	//注入学生的Service
	@Autowired
	private IStudentService studentService;
	
	
	/**
	 * 验证码生成
	 * 
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "verfy_admin", method = RequestMethod.GET)
	public void verfyAdmin(ModelMap model, HttpServletRequest request,
			HttpServletResponse response) {
		AuthImage.getImg(request, response);// 生成验证码
	}
	
	/**
	 * GET 判断验证码是否正确
	 * 
	 * @param model
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "ajaxverify", method = RequestMethod.GET)
	public void ajaxVerify(ModelMap model, HttpServletRequest request,
			HttpServletResponse response, String verify) throws IOException {
		response.setCharacterEncoding("utf-8");
	    HttpSession session = request.getSession();// 获得session对象
		PrintWriter out = response.getWriter();
		// 将用户输入的验证码与生成的验证码进行对比
		if (verify.equalsIgnoreCase((String) session.getAttribute("verfycode"))) {
			out.print("success");
		} else {
			out.print("fail");
		}
	}
	
	
	/**
	 * POST 登陆请求
	 * 
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "userlogin", method = RequestMethod.POST)
	public void userLogin(ModelMap model, HttpServletRequest request,
			HttpServletResponse response ,User user) throws IOException {
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession();// 获得session对象
		User rsuser = (User) userService.getUserid(user.getUser_id());// 根据用户名查找用户，返回一个User对象
		if (rsuser == null) {// 判断是否存在该用户
			out.print("noUser");
		}else {
			String tmep = MD5Util.MD5(user.getPassword());
			if (tmep.equals(rsuser.getPassword())) {// 判断密码是否正确
				//String roleString = roleTOString(rsuser.getRole_code());
				System.out.println(rsuser.getUser_id()+"&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&"+rsuser.getUser_id().length());
				if(rsuser.getUser_id().equals("0")){//游客登录
					rsuser.setName("游客");
				}else if(rsuser.getUser_id().length()>=8) {//学生登录
					Student student= studentService.selectByPrimaryKey(rsuser.getUser_id());
					rsuser.setName(student.getStudent_name());
					rsuser.setMajor_code(student.getMajor_code());
				}else{//教师登录
					Teacher teacher = teacherService.selectByPrimaryKey(rsuser.getUser_id());
					rsuser.setName(teacher.getTeacher_name());
				}
				//session.setAttribute("roleString", roleString);// 将用户权限保存到session中
				session.setAttribute("loginuser", rsuser);// 将用户保存到session中
				out.print("success");
			} else {
				out.print("fail");
			}
		}
	}
	//处理角色权限
	public String roleTOString(int role_code){
		List<MenuLevelOne> roles = menuService.getMenuOneLevelByRoleCode(role_code);
		StringBuffer sb = new StringBuffer("");
		for (int i = 0; i < roles.size(); i++) {
			sb.append(roles.get(i).getMenu_level_one_name());
		}
		return sb.toString();
	}
	
	
	/**
	 *退出
	 * 
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "loginout", method = RequestMethod.GET)
	public String loginOut(ModelMap model, HttpServletRequest request,
			HttpServletResponse response) {
		HttpSession session = request.getSession();// 获得session对象
		User logoUser=(User) session.getAttribute("loginuser");
		session.removeAttribute("loginuser");//移除用户
		session.removeAttribute("rolestring");//移除用户权限
		return "redirect:/";
	}
	
	/**
	 * 无权向页面
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "noauth", method = RequestMethod.GET)
	public String noauth(ModelMap model, HttpServletRequest request,
			HttpServletResponse response) {
		System.out.println("================没有权限============");
		return "noauth";
	}
}
