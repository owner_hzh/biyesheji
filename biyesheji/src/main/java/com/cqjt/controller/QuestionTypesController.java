package com.cqjt.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileUploadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cqjt.pojo.QuestionTypes;
import com.cqjt.service.IQuestionTypesService;

/*
 * 
 *	author: xiaoshujun
 *  date: 2014-4-20
 *  试题类型 controller
 */
@Controller
@RequestMapping(value = "/questiontypes/")
public class QuestionTypesController {
	@Resource(name = "questionTypesService")
	private IQuestionTypesService questionTypesService;
	
	
	// 添加题型
	@RequestMapping(value = "addquestiontypes", method = RequestMethod.POST)
	public ModelAndView addQuestionTypes(HttpServletRequest request,HttpServletResponse response,QuestionTypes questionTypes)
    {
		System.out.println(questionTypes);
		questionTypesService.addQuestionTypes(questionTypes);
		return new ModelAndView("/menu/menu-content-add-category");
	}
	//获取挺行详情
	@RequestMapping(value="showQusetionTypeDetail")
	public ModelAndView showQusetionTypeDetail(ModelMap model,HttpServletRequest request,HttpServletResponse response,String qt_code)
	{
		QuestionTypes questionType=questionTypesService.getQuestionTypesByCode(Integer.parseInt(qt_code));
		model.addAttribute("qusrtiontype", questionType);
		return new ModelAndView("/menu/menu-content-update-category");
	}
	//更新题型
	@RequestMapping(value="updateQusetionType")
	public void updateQusetionType(HttpServletRequest request,HttpServletResponse response,QuestionTypes questionType)
	{
		response.setContentType("text/html; charset=UTF-8");
		System.out.println(questionType);
		questionTypesService.updateQuestionTypes(questionType);
	}
	// 删除题型
	@RequestMapping(value = "deletequestiontypes")
	public void deleteQuestionTypes(HttpServletRequest request,HttpServletResponse response,QuestionTypes questionTypes) throws IOException
	{
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		Boolean success = questionTypesService.deleteQuestionTypes(questionTypes);
		if (success) {
			out.print("success");
		} else {
			out.print("fail");
		}
	}
}
