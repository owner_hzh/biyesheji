package com.cqjt.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileUploadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;
import com.cqjt.pojo.Courseware;
import com.cqjt.pojo.TeachingData;
import com.cqjt.pojo.User;
import com.cqjt.service.ICurriculumService;
import com.cqjt.service.ITeachingDataService;
import com.cqjt.util.DocConverter;
import com.cqjt.util.UploadJSON;

/*
 * 教辅资料@controller
 * autor:xiaoshujun
 * 
 */
@Controller
@RequestMapping(value = "/teachingdata/")
public class TeachingDataController {
	
	@Autowired
	private ITeachingDataService teachingDataService;
	
	//显示相关课程的教辅资料
	@RequestMapping(value = "showTeachingdata", method = RequestMethod.GET)
	public String showTeachingData(ModelMap model,HttpServletRequest request, HttpServletResponse response , String curriculum_code ,String curriculum_name) throws UnsupportedEncodingException {	
		
		Map<String, Object> params =new HashMap<String, Object>();	
		curriculum_name=java.net.URLDecoder.decode(curriculum_name,"utf-8");
		params.put("curriculum_code", Long.parseLong(curriculum_code));
		List<TeachingData> teachingDataList = teachingDataService.getAllTeachingData(params);//根据专业code找到相关教辅资料
		model.addAttribute("curriculum_name", curriculum_name);//保存课程名
		model.addAttribute("curriculum_code", curriculum_code);//保存课程code
		if(teachingDataList!=null && teachingDataList.size()!=0){
			model.addAttribute("teachingDataList", teachingDataList);//保存查询出来的教辅资料
		}
		int role=((User)request.getSession().getAttribute("loginuser")).getRole_code();
		model.addAttribute("loginuserRole", role);
		return "teachingdata/show-teaching-materials";
	}
	
	//上传课程的教辅资料
	@RequestMapping(value = "uploadTeachingdata", method = RequestMethod.POST)
	public void uploadTeachingData(ModelMap model,HttpServletRequest request, HttpServletResponse response ,String curriculum_code) throws IOException, FileUploadException {
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		JSONObject json  = UploadJSON.uploadFile(request, response);
		if(json.getString("error").equals("0")){
			TeachingData td = new TeachingData();
			td.setCurriculum_code(Integer.parseInt(curriculum_code));//设置教辅资料的课程code
			td.setLocation(json.getString("url"));//设置教辅资料文件链接（相对路径）
			td.setFilename(json.getString("fileName")); //设置教辅资料名
			System.out.println("11111111");
			System.out.println(Integer.parseInt(curriculum_code));
			System.out.println(json.getString("url")+":"+json.getString("fileName"));
			teachingDataService.addTeachingData(td);
		}else{
			out.println("shibai");
			System.out.println("shibai");
		}
	}
	
	//下载课程的教辅资料
	@RequestMapping(value = "downTeachingdata", method = RequestMethod.POST)
	public void downTeacingDate(ModelMap model,HttpServletRequest request, HttpServletResponse response ,TeachingData teachingData) throws IOException, FileUploadException {
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		System.out.println(teachingData.getFilename());
		Boolean success = teachingDataService.updateTeachingData(teachingData);
		if (success) {
			out.print("success");
		} else {
			out.print("fail");
		}		
	}
	// 删除课程的课件
	@RequestMapping(value = "deleteTeachingdata", method = RequestMethod.GET)
	public void deleteTeachingDate(HttpServletRequest request,HttpServletResponse response, TeachingData teachingData)throws IOException, FileUploadException {
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		Boolean success = teachingDataService.delTeachingData(teachingData);
		if (success) {
			out.print("success");
		} else {
			out.print("fail");
		}
	}
	// 预览课程的课件
	@RequestMapping(value = "onlinelook", method = RequestMethod.GET)
	public String lookTeachingData(ModelMap model,HttpServletRequest request,HttpServletResponse response)throws IOException, FileUploadException {
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		String url = DocConverter.doconvert(request, response);
		model.addAttribute("swfpath", url);//保存课程code
		return "teachingdata/show-cw";
	}
}
