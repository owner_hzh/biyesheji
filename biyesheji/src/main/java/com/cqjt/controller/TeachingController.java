package com.cqjt.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cqjt.pojo.Curriculum;
import com.cqjt.pojo.CurriculumCharacter;
import com.cqjt.pojo.Major;
import com.cqjt.pojo.StudyMode;
import com.cqjt.pojo.Teacher;
import com.cqjt.pojo.Teaching;
import com.cqjt.pojo.User;
import com.cqjt.service.ICurriculumCharacterService;
import com.cqjt.service.ICurriculumService;
import com.cqjt.service.IMajorService;
import com.cqjt.service.IStudyModeService;
import com.cqjt.service.ITeacherService;
import com.cqjt.service.ITeachingService;
import com.cqjt.service.IUserService;

/*
 * autor:xiaoshujun
 * 
 */
@Controller
@RequestMapping(value = "/curriculum/teaching/")
public class TeachingController {
	
	@Autowired
	private ITeachingService teachingService;
	@Autowired
	private ITeacherService teacherService;
	@Autowired
	private ICurriculumService curriculumService;
	@Autowired
	private IMajorService majorService;
	//注入修学模式
	@Autowired
	private	IStudyModeService studyModeService;
	//注入课程性质
	@Autowired
	private	ICurriculumCharacterService curriculumCharacterService;
	
	/*@Autowired
	private	IUserService userService;*/
	
//	private static String[] ids = null;
	
	//显示相关专业的课程
	@RequestMapping(value = "show", method = RequestMethod.GET)
	public ModelAndView showTeaching(ModelMap model,HttpServletRequest request, HttpServletResponse response)
	{
		
		int role=((User)request.getSession().getAttribute("loginuser")).getRole_code();
		request.setAttribute("loginuserRole", role);	
		return new ModelAndView("/curriculum/teaching/show-teaching");
	}
	@RequestMapping(value = "add-teaching",method = {RequestMethod.GET,RequestMethod.POST})
	public ModelAndView addTeaching(Model model,HttpServletRequest request, HttpServletResponse response){
		if (request.getParameter("method")!=null&&request.getParameter("method").equals("load")) {
			return new ModelAndView("/curriculum/teaching/add-teaching");
		}
		else {
			if(request.getParameter("userid")==null)
				return SearchCurriculum(request, model);
			else{
				String ids=request.getParameter("ids");
				List<Integer> idlist = new ArrayList<Integer>();
				if(ids!=null){
					org.json.JSONArray jsonArray = new org.json.JSONArray(ids);
					
					for(int i = 0; i <jsonArray.length(); i++) {
						System.out.println("i:"+i);
						 JSONObject jsonObj = (JSONObject) jsonArray.get(i);
						 if(!jsonObj.getString("id").trim().equals(""))
							idlist.add(Integer.parseInt(jsonObj.getString("id").trim()));
					}
				}
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("user_id", request.getParameter("userid").trim());
				Teacher teacher = teacherService.getTeacherByUserid(params);
				Teaching teaching = new Teaching();
				teaching.setUser_id(teacher.getUser_id());
				for (Integer integer : idlist) {
					teaching.setCurriculum_code(integer);
					teachingService.addTeaching(teaching);
				}
			}
		}
	//	return new ModelAndView("/curriculum/teaching/add-teaching");
		return SearchCurriculum(request, model);
	}
	
	//查询
	@RequestMapping(value = "search-curriculum-by-teacherid", method = {RequestMethod.GET,RequestMethod.POST})
	public ModelAndView searchingTeaching(ModelMap model,HttpServletRequest request, HttpServletResponse response){
		int role=((User)request.getSession().getAttribute("loginuser")).getRole_code();
		request.setAttribute("loginuserRole", role);	
		String userid = "";
		if(!request.getParameter("userid").trim().equals("")){
			userid = request.getParameter("userid").trim();
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("user_id", request.getParameter("userid").trim());
			Teacher teacher = teacherService.getTeacherByUserid(params);
			if(teacher!=null){
				List<Curriculum> curriculums = curriculumService.getTeacherCurriculumListByUserId(teacher.getUser_id());
				request.setAttribute("curriculum_list", curriculums);
			}
			request.setAttribute("teacherid", userid);
		}
		return new ModelAndView("/curriculum/teaching/show-teaching");
	}
	//查询
	@RequestMapping(value = "delete-teaching", method = {RequestMethod.GET,RequestMethod.POST})
	public ModelAndView deleteTeaching(ModelMap model,HttpServletRequest request, HttpServletResponse response){
		ModelAndView mv = new ModelAndView("/curriculum/teaching/show-teaching");
    	int curriculum_code = 0;
    	String userid="";
    	Teaching teaching = new Teaching();
		if(request.getParameter("curriculum_code")!=null&&request.getParameter("userid")!=null)
		{
			curriculum_code = Integer.parseInt(request.getParameter("curriculum_code").toString().trim());
			userid = request.getParameter("userid");
 			teaching.setUser_id(userid);
			teaching.setCurriculum_code(curriculum_code);
			teachingService.delTeaching(teaching);
		}
		return mv;
	}
	@RequestMapping(value="search",method = {RequestMethod.GET,RequestMethod.POST} )
	public ModelAndView SearchCurriculum(HttpServletRequest request,Model model){
		ModelAndView mv = new ModelAndView("/curriculum/teaching/list-curriculum");
		Map<String, Object> params = new HashMap<String, Object>();
		Integer semester = null,studyMode_id=null,major_code=null,cc_code=null;
		
		List<CurriculumCharacter> curriculumCharacters = curriculumCharacterService.getAllCurriculumCharacter();
		List<Major> majors = majorService.getMajorList();
		List<StudyMode> studyModes = studyModeService.getAllStudyMode();
		
		//三个过滤下拉列表
		model.addAttribute("curriculumcharacterclist",curriculumCharacters);
		model.addAttribute("majorlist", majors);
		model.addAttribute("studymodelist", studyModes);
		
		if(request.getParameter("major_select")!=null&&!request.getParameter("major_select").trim().equals("-1")){
			major_code = Integer.parseInt(request.getParameter("major_select").toString().trim());
			model.addAttribute("majorcode_save", major_code);  //保存当前下拉列表的值，下同
			params.put("major_code",  major_code);
		}else {
			params.put("major_code",  null);
		}
		if (request.getParameter("curriculumcharacter_select")!=null&&!request.getParameter("curriculumcharacter_select").trim().equals("-1")) {
			cc_code = Integer.parseInt(request.getParameter("curriculumcharacter_select").toString().trim());
			model.addAttribute("cccode_save", cc_code);
			params.put("cc_code",cc_code);
			
		}else {
			params.put("cc_code",null);
		}
		if(request.getParameter("semester_select")!=null&&!request.getParameter("semester_select").trim().equals("-1")){
			 semester=Integer.parseInt(request.getParameter("semester_select").toString().trim());
			 model.addAttribute("semestercode_save", semester);
			 params.put("semester_code",semester);
		}else {
			params.put("semester_code",null);
		}
		if(request.getParameter("studymode_select")!=null&&!request.getParameter("studymode_select").trim().equals("-1")){
			 studyMode_id=Integer.parseInt(request.getParameter("studymode_select").toString().trim());
			 model.addAttribute("studymodeid_save",studyMode_id);
			 params.put("studymode_id", studyMode_id);
		}else {
			params.put("studymode_id", null);
		}
		
		List<Curriculum> list= curriculumService.getAllCurriculum(params);
		if(request.getParameter("userid")!=null){
			String userid = request.getParameter("userid").trim();
			
		/*	User user=userService.getUserid(userid);
			
			if(user==null)
				return new ModelAndView("/curriculum/teaching/show");*/
			
			request.setAttribute("userid", userid);
			
			List<Teaching> clist = teachingService.getTeachingByUserid(userid);
			System.out.println("clistsize:"+clist.size());
			list=Filter(list,clist);
		}

		model.addAttribute("curriculum_list",list);
		return mv;		
	}
	public List<Curriculum> Filter(List<Curriculum> source,List<Teaching> filters){
		for (int i = 0; i < source.size(); i++) {
			for (int j = 0; j < filters.size(); j++) {
				if(source.get(i).getCurriculum_code()==filters.get(j).getCurriculum_code()){
					source.remove(i);
				//	filters.remove(j);
					
				}
			}
		}
		return source;
	}
	
}
