package com.cqjt.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileUploadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cqjt.pojo.Courseware;
import com.cqjt.pojo.Curriculum;
import com.cqjt.pojo.Major;
import com.cqjt.pojo.Page;
import com.cqjt.pojo.QuestionTypes;
import com.cqjt.pojo.SelfTest;
import com.cqjt.pojo.SelfTestScores;
import com.cqjt.pojo.Student;
import com.cqjt.pojo.Teacher;
import com.cqjt.pojo.TestCode;
import com.cqjt.pojo.TestCustomize;
import com.cqjt.pojo.TestQuestion;
import com.cqjt.pojo.TestRecord;
import com.cqjt.pojo.User;
import com.cqjt.service.ICoursewareService;
import com.cqjt.service.ICurriculumService;
import com.cqjt.service.IOutLineService;
import com.cqjt.service.IQuestionTypesService;
import com.cqjt.service.ISelfTestScoresService;
import com.cqjt.service.ISelfTestService;
import com.cqjt.service.IStudentService;
import com.cqjt.service.ITeacherService;
import com.cqjt.service.ITestCodeService;
import com.cqjt.service.ITestCustomizeService;
import com.cqjt.service.ITestQuestionService;
import com.cqjt.service.ITestRecordService;
import com.cqjt.util.DocConverter;
import com.cqjt.util.GsonUtil;
import com.cqjt.util.PageCondition;
import com.cqjt.util.PageNameEnum;
import com.cqjt.util.StringUtil;
import com.cqjt.util.UploadJSON;
import com.google.gson.reflect.TypeToken;

/**
 * 在线测试Controller
 * @author LIJIN
 *
 */
@Controller
@RequestMapping(value = "/online/")
public class OnlineTestController {
	//注入课程的Service
	@Autowired
	private ICurriculumService curriculumService;
	@Autowired
	private IQuestionTypesService questionTypesService;
	@Autowired
	private IOutLineService outLineService;
	@Autowired
	private ITestQuestionService testQuestionService;
	@Autowired
	private ITestCodeService testCodeService;
	@Autowired
	private ITeacherService teacherService;
	@Autowired
	private ITestCustomizeService testCustomizeService;
	@Autowired
	private ITestRecordService testRecordService;
	@Autowired
	private ISelfTestService selfTestService;
	@Autowired
	private ISelfTestScoresService selfTestScoresService;
	@Autowired
	private IStudentService studentService;

	/**
	 * 显示该学生所属专业的相关课程
	 * @param model
	 * @param request
	 * @param response
	 * @param type  用于页面判断是考试还是查询
	 * @param id  
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	@RequestMapping(value = "majorcurriculum", method = RequestMethod.GET)
	public String majorCurriculum(ModelMap model,HttpServletRequest request, HttpServletResponse response ,String type,int id) throws UnsupportedEncodingException {
		User user = (User)request.getSession().getAttribute("loginuser");
		model.addAttribute("type", type);//用于页面判断是考试还是查询
		model.addAttribute("id", id);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("major_code", user.getMajor_code());
		params.put("type", type);
		params.put("id", id);
		//List<Curriculum> curriculums=curriculumService.getAllCurriculum(params);
		String pageNoStr = request.getParameter("page"); 
        int pageNo = 1;  
        if(pageNoStr != null && !pageNoStr.equals("")){
        	pageNo = Integer.parseInt(pageNoStr);  
        }
		PageCondition pagecondition = curriculumService.getAllCurriculum(PageNameEnum.PAGESIZE, pageNo,params,request);
		model.addAttribute("pagecondition", pagecondition);//二级菜单一项的所有课程
		return "onlinetest/major-curriculum";
	}
	
	/**
	 * 显示该课程拥有的试卷
	 * @param model
	 * @param request
	 * @param response
	 * @param curriculum_code 课程代码，用于查询
	 * @param curriculum_name 课程名，传回页面显示
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	@RequestMapping(value = "showtest", method = RequestMethod.GET)
	public String showTest(ModelMap model,HttpServletRequest request, HttpServletResponse response , String curriculum_code ,String curriculum_name) throws UnsupportedEncodingException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("curriculum_code",curriculum_code);
		List<TestCode> testCodes=testCodeService.getAllTestCode(params);
		if(testCodes!=null&&testCodes.size()>0)
		{
			for (TestCode testCode : testCodes) {
				Teacher teacher = teacherService.selectByPrimaryKey(testCode.getUser_id());
				if (teacher!=null) {
					testCode.setUser_name(teacher.getTeacher_name());
				}
			}
			model.addAttribute("testCodes", testCodes);
		}
		curriculum_name = java.net.URLDecoder.decode(curriculum_name,"utf-8");
		model.addAttribute("curriculum_name", curriculum_name);
		return "onlinetest/show-test";
	}
	
	
	/**
	 * 生成试卷，并展示
	 * @param model
	 * @param request
	 * @param response
	 * @param title  
	 * @param curriculum_code  课程代码
	 * @param number  试卷编码
	 * @param start   开始章节
	 * @param end     结束章节
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	@RequestMapping(value = "showpaper", method = RequestMethod.GET)
	public String showPaper(ModelMap model,HttpServletRequest request, HttpServletResponse response ,String title , String curriculum_code ,String number,String start,String end) throws UnsupportedEncodingException {
		User user = (User)request.getSession().getAttribute("loginuser");
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("curriculum_code",curriculum_code);
		params.put("number",number);
		List<TestCustomize> testCustomizes=testCustomizeService.getAllTestCustomize(params);//拿到这张试卷的题型，每个题型的数量
		if(testCustomizes!=null&&testCustomizes.size()>0)
		{
			//生成考试记录，将残缺的记录放进《考试记录表》
			TestRecord tr = new TestRecord();
			tr.setNumber(Integer.parseInt(number));//设置考试编码
			tr.setTest_date(new Timestamp(new Date().getTime()));//考试时间
			tr.setUser_id(user.getUser_id());//考试人
			tr.setIs_check(0);//审核状态
			Integer test_code = testRecordService.addTestRecord(tr);//返回考试卷编码
			for (TestCustomize testCustomize : testCustomizes) {//循环得到题型，数量
				Map<String, Object> choose = new HashMap<String, Object>();
				choose.put("curriculum_code",curriculum_code);//课程
				choose.put("qt_code", testCustomize.getQt_code());//题型
				choose.put("start", start);//开始章节
				choose.put("end", end);//结束章节
				choose.put("num", testCustomize.getQuestion_num());//设置试题数量
				List<TestQuestion> testQuestions = testQuestionService.getRandQuestion(choose);//随机获得试题
				//把生成的题存到《自测试卷表中》
				testQuestions=addSelfTest(testQuestions, test_code ,number);
				//把题放进该题型
				testCustomize.settQuestions(testQuestions);
				//设置题型名
				QuestionTypes questionTypes=questionTypesService.getQuestionTypesByCode(testCustomize.getQt_code());
				testCustomize.setQt_name(questionTypes.getQt_name());
			}
			model.addAttribute("testCustomizes", testCustomizes);
			model.addAttribute("test_code", test_code);//保存考试卷编码
		}
		title = java.net.URLDecoder.decode(title,"utf-8");
		model.addAttribute("title", title);
		return "onlinetest/show-paper";
	}
	
	//显示该教师上的相关课程
	@RequestMapping(value = "showcurriculum", method = RequestMethod.GET)
	public String showCurriculum(ModelMap model,HttpServletRequest request, HttpServletResponse response , String curriculum_code ,String curriculum_name,String type,int id) throws UnsupportedEncodingException {
		User user = (User)request.getSession().getAttribute("loginuser");
		model.addAttribute("type", type);//用于页面判断是出题还是定制试卷
		model.addAttribute("id", id);
		List<Curriculum> curriculums=curriculumService.getTeacherCurriculumListByUserId(user.getUser_id());
		if(curriculums!=null&&curriculums.size()>0)
		{
			model.addAttribute("curriculums", curriculums);
		}
		
		return "onlinetest/show-curriculum";
	}
	
	//老师出题
	@RequestMapping(value = "setquestion", method = RequestMethod.GET)
	public String setQuestion(ModelMap model,HttpServletRequest request, HttpServletResponse response , String curriculum_code,String curriculum_name,int id) throws UnsupportedEncodingException {	
		model.addAttribute("id", id);
		curriculum_name=java.net.URLDecoder.decode(curriculum_name,"utf-8");
		model.addAttribute("curriculum_name", curriculum_name);//保存课程名
		model.addAttribute("curriculum_code", curriculum_code);//保存课程code
		List<QuestionTypes> questionTypes=questionTypesService.getAllQuestionTypes(null);
		model.addAttribute("questionTypes", questionTypes);
		Curriculum c=curriculumService.getCurriculumByCode(Integer.parseInt(curriculum_code));
		List<Integer> chapters=new ArrayList<Integer>();
		for(int i=0;i<c.getAllchapter();i++)
		{
			chapters.add(i+1);
		}
		model.addAttribute("chapters", chapters);
		return "onlinetest/set-question";
	}
	//定制试卷名和章节
	//author by jianming 
	@RequestMapping(value = "custom_papernameandchapter", method = RequestMethod.GET)
	public String custom_papernameandchapter(ModelMap model,HttpServletRequest request, HttpServletResponse response , String curriculum_code) throws UnsupportedEncodingException 
	{
		Curriculum curriculum=curriculumService.getCurriculumByCode(Integer.parseInt(curriculum_code));
		model.addAttribute("curriculum",curriculum);
		return "onlinetest/custom-paper-nameandchapter";
	}
	
	//显示定制试卷
	// author by jianming
	@RequestMapping(value = "custompaper", method = RequestMethod.GET)
	public String customPaper(ModelMap model,HttpServletRequest request, HttpServletResponse response ,String curriculum_code,String page_name, int start_chapter,int end_chapter) throws UnsupportedEncodingException 
	{
		Curriculum curriculum=curriculumService.getCurriculumByCode(Integer.parseInt(curriculum_code));
		page_name=java.net.URLDecoder.decode(page_name,"utf-8");//转码
		model.addAttribute("curriculum",curriculum);
		model.addAttribute("page_name", page_name);
		model.addAttribute("start_chapter", start_chapter);
		model.addAttribute("end_chapter", end_chapter);
		List<QuestionTypes> questionTypeList = questionTypesService.getQusetionTypeListByCodeAndChapter(Integer.parseInt(curriculum_code),start_chapter,end_chapter);
		if(questionTypeList!=null&questionTypeList.size()!=0)
			model.addAttribute("questionTypeList", questionTypeList);
		return "onlinetest/custom-paper";
	}
	//定制试卷
	// author by jianming
	@RequestMapping(value = "customizetest", method = RequestMethod.POST)
	public void customizeTest(HttpServletRequest request, HttpServletResponse response,int start_chapter,int end_chapter,int curriculum_code,String page_name,int total_score) throws IOException
	{
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out=response.getWriter();
		String data=request.getParameter("data");
        JSONArray array=JSONObject.parseArray(data);
		//System.out.println("字符串:"+array.toJSONString());
		System.out.println("开始章节:"+start_chapter);
		System.out.println("结束章节:"+end_chapter);
		System.out.println("课程代码:"+curriculum_code);
		System.out.println("试卷名:"+page_name);
		System.out.println("试卷总分值:"+total_score);
		//设置试卷编码表
		TestCode testcode=new TestCode();
		testcode.setCurriculum_code(curriculum_code);
		testcode.setTitle(page_name);
		User user = (User)request.getSession().getAttribute("loginuser");
		testcode.setUser_id(user.getUser_id());
		testcode.setStart(start_chapter);
		testcode.setEnd(end_chapter);
		testcode.setTotalscore(total_score);
		int testcode_number=testCodeService.addTestCode(testcode);
		System.out.println("试卷编码表的主键:"+testcode_number);
		//设置试卷定制表
		List<TestCustomize> list=new ArrayList<TestCustomize>();
		for(int i=0;i<array.size();i++)
		{
		;
			JSONObject json=array.getJSONObject(i);
			TestCustomize testcustomize=new TestCustomize();
			testcustomize.setNumber(testcode_number);
			testcustomize.setQt_code(json.getIntValue("qt_code"));
			int num=json.getIntValue("number");
			int score=json.getIntValue("score");
			int totalscore=num*score;
			testcustomize.setQuestion_num(num);
			testcustomize.setQuestion_score(score);
			testcustomize.setQuestion_allscore(totalscore);
			if(testcustomize.getQuestion_allscore()!=0)//判断该题型有没有总分
			    list.add(testcustomize);
			System.out.println(testcustomize);
		}
		boolean success=testCustomizeService.addTestCustomizeByList(list);
		if(success)
			out.print("success");
		else
			out.print("faile");
	}
	//ckeditor上传图片
		@RequestMapping(value ="/ckeditorupload",method = RequestMethod.POST)
		public void ckeditorUpload(int CKEditorFuncNum, HttpServletRequest request,HttpServletResponse response) throws IOException, FileUploadException {
			response.setCharacterEncoding("UTF-8");
			response.setContentType("text/html; charset=UTF-8");
			JSONObject json  = UploadJSON.uploadFile(request, response);			
			PrintWriter out = null;
			try {
				out = response.getWriter();
			} catch (IOException e1) {
				e1.printStackTrace();
			}	
			try {										
				String ext=json.getString("extension");
				String f="";
				if(!"jpg gif bmp png jpeg".contains(ext))
				{
					out.print("<font color=\"red\"size=\"2\">*文件格式不正确（必须为.jpg/.gif/.bmp/.png文件）</font>");
					return ;
				}										
				out.println("<script type=\"text/javascript\">");
				out.println("window.parent.CKEDITOR.tools.callFunction("+CKEditorFuncNum+",'"+ json.getString("url") + "')"); 
				out.println("</script>");
				
			} catch (Exception e) {
				out.println("<font color=\"red\" size=\"2\">*文件大小不得大于600k</font>");
			}finally
			{
				if(out!=null)
				   out.close();
			}		
		}
		
		//上传试卷题目数据
		@RequestMapping(value = "timucommit", method = RequestMethod.POST)
		public void timucommit(ModelMap model,TestQuestion testQuestion, HttpServletRequest request, HttpServletResponse response) throws IOException {
			if(testQuestion.getQt_code()==1||testQuestion.getQt_code()==2)
			{
				//单选与多选对答案排序
				testQuestion.setAnswer(StringUtil.sort(testQuestion.getAnswer()));
			}
			boolean f=testQuestionService.addTestQuestion(testQuestion);
			PrintWriter out = response.getWriter();
			out.print(f);
			if(out!=null)
				out.close();
		}

		
		//教师改题的跳转页面
		@RequestMapping(value = "checktestpaper", method = RequestMethod.GET)
		public String checkTestPaper(ModelMap model,Page page,int id,HttpServletRequest request, HttpServletResponse response) throws IOException {
			User user = (User)request.getSession().getAttribute("loginuser");
			model.addAttribute("id", id);
			
			//List<Curriculum> curriculums=curriculumService.getTeacherCurriculumListByUserId(user.getUser_id());
			Map<String, Object> params=new HashMap<String, Object>();
			params.put("user_id", user.getUser_id());
			params.put("pageSize", page.getPageSize());
			params.put("pageNo", page.getPageNo());
			page.setTotalCount(curriculumService.getTeacherCurriculumCount(user.getUser_id()));
			List<Curriculum> curriculums=curriculumService.getTeacherCurriculumListByUserIdPages(params);
			page.setCurrentCount(curriculums.size());
			/////////////////////////////////////////////////////////////////////
			if(curriculums!=null&&curriculums.size()>0)
			{
				model.addAttribute("curriculums", curriculums);
			}
			model.addAttribute("page", page);
			return "onlinetest/teacher-check-test-paper";
		}
		
		//老师改题 选择课程下面的学生
		@RequestMapping(value = "choosestudent", method = RequestMethod.GET)
		public String chooseStudent(ModelMap model,HttpServletRequest request, HttpServletResponse response , String curriculum_code,String curriculum_name,int id) throws UnsupportedEncodingException {	
			model.addAttribute("id", id);
			curriculum_name=java.net.URLDecoder.decode(curriculum_name,"utf-8");
			model.addAttribute("curriculum_name", curriculum_name);//保存课程名
			model.addAttribute("curriculum_code", curriculum_code);//保存课程code
			Map<String, Object> params=new HashMap<String, Object>();
			params.put("curriculum_code", curriculum_code);
			List<TestCode> testCodes=testCodeService.getAllTestCode(params);

			List<TestRecord> testRecords=new ArrayList<TestRecord>();//考试记录列表
			//List<Student> students=new ArrayList<Student>();//学生列表
			
			//根据试卷编码表中的课程id获取考试记录id 且是没有审核的考试记录
			for(int i=0;i<testCodes.size();i++)
			{
				Map<String, Object> pra=new HashMap<String, Object>();
				pra.put("number", testCodes.get(i).getNumber());
				pra.put("is_check", 0);//没有审核的试卷
				List<TestRecord> testRecord=testRecordService.getAllTestRecord(pra);
                if(testRecord!=null&&testRecord.size()>0)
                {
                	//testRecords.add(testRecord.get(0));
                	testRecords.addAll(testRecord);
                }
				
			}
			//根据考试记录表中的学生id获取学生名字
			for(int j=0;j<testRecords.size();j++)
			{
				Map<String, Object> sparams=new HashMap<String, Object>();
				sparams.put("user_id", testRecords.get(j).getUser_id());
				List<Student> s=studentService.getAllStudents(sparams);
				if(s!=null||s.size()>0)
				{
					testRecords.get(j).setStudent_name(s.get(0).getStudent_name());
				}else
				{
					testRecords.get(j).setStudent_name("未知");
				}
				
			}
			model.addAttribute("testRecords", testRecords);//考试记录列表
			return "onlinetest/teacher-check-student-list";
		}
		
		//老师选择学生后进入改题的页面
		@RequestMapping(value = "teachercheckitem", method = RequestMethod.GET)
		public String teacherCheckItem(ModelMap model,HttpServletRequest request, HttpServletResponse response , String curriculum_code,String curriculum_name,int test_code,int id,String student_name) throws UnsupportedEncodingException {	
			model.addAttribute("test_code", test_code);//考试卷编码
			model.addAttribute("id", id);//选择的菜单id
			student_name=java.net.URLDecoder.decode(student_name,"utf-8");
			model.addAttribute("student_name", student_name);//保存学生姓名
			curriculum_name=java.net.URLDecoder.decode(curriculum_name,"utf-8");
			model.addAttribute("curriculum_name", curriculum_name);//保存课程名
			model.addAttribute("curriculum_code", curriculum_code);//保存课程code
			
			Map<String, Object> params=new HashMap<String, Object>();
			params.put("test_code", test_code);
			List<SelfTest> selfTests=selfTestService.getAllSelfTestBigger(params);			
			/*model.addAttribute("number", test_code);//试卷编码
			model.addAttribute("qt_code", test_code);//题型代码
*/			//获取自测试卷当中题目的题干
			for(int i=0;i<selfTests.size();i++)
			{
				SelfTest st=selfTests.get(i);
				//获取题干
				TestQuestion t=testQuestionService.getTestQuestionByCode(st.getTq_id());
				if(t!=null)
				{
					st.setIntroduction(t.getIntroduction());
				}				
			}
			
			//备份数据
			List<SelfTest> selfTestsBackup=new ArrayList<SelfTest>();
			selfTestsBackup.addAll(selfTests);
			
			//根据题型code分类 得到有几种类型的题目  begin
			for ( int x = 0 ; x < selfTestsBackup.size() - 1 ; x ++ ) {
			     for ( int y = selfTestsBackup.size() - 1 ; y > x; y -- ) {
			       if (selfTestsBackup.get(y).getQt_code()==selfTestsBackup.get(x).getQt_code()) {
			    	   selfTestsBackup.remove(y);
			       }
			      }
			    }
			//根据题型code分类 得到有几种类型的题目  end
			
			List<TestCustomize> testCustomizes=new ArrayList<TestCustomize>();
			for(int i=0;i<selfTestsBackup.size();i++)
			{
				SelfTest st=selfTestsBackup.get(i);
				//获取分值 数量 总分
				Map<String, Object> naqparams=new HashMap<String, Object>();
				naqparams.put("number", st.getNumber());
				naqparams.put("qt_code", st.getQt_code());
				TestCustomize tc=testCustomizeService.getTestCustomizeByPrimaryKey(naqparams);
				List<SelfTest> selfTestList=new ArrayList<SelfTest>();
				//获取题型名称
				QuestionTypes questionTypes =questionTypesService.getQuestionTypesByCode(st.getQt_code());
				if(questionTypes!=null)
				{
					tc.setQt_name(questionTypes.getQt_name());
				}		
				
				//分类获取SelfTest
				for(int j=0;j<selfTests.size();j++)
				{
					if(selfTests.get(j).getQt_code()==tc.getQt_code())
					{
						selfTestList.add(selfTests.get(j));
					}
				}
				tc.setSelfTests(selfTestList);
				
				testCustomizes.add(tc);
			}
            
			model.addAttribute("testCustomizes", testCustomizes);//保存自测试卷列表
			return "onlinetest/teacher-check-student-titlelist";
		}
		
	//老师提交给各个大题打的分数 更新自测考试成绩表
	@RequestMapping(value = "laoshidafen", method = RequestMethod.POST)
	public void laoShiDaFen(String JsonString,HttpServletRequest request, HttpServletResponse response) throws IOException {			
		Type typeOfT=new TypeToken<List<SelfTestScores>>() {}.getType();
		List<SelfTestScores> selfTestScores=GsonUtil.getGson().fromJson(JsonString, typeOfT);
		boolean flag=selfTestScoresService.updateSelfTestScores(selfTestScores);
		
		PrintWriter out=response.getWriter();
		out.print(flag);
		
		///////得到考试的总成绩
		Map<String, Object> params=new HashMap<String, Object>();
		params.put("test_code", selfTestScores.get(0).getTest_code());
		params.put("number", selfTestScores.get(0).getNumber());
		 SelfTestScores sTotalScore=selfTestScoresService.getTotalScores(params);
		 //更新考试记录表
		 TestRecord testRecord=new TestRecord();
		 Date date = new Date();       
		 Timestamp nousedate = new Timestamp(date.getTime());
		 testRecord.setCheck_time(nousedate);
		 testRecord.setIs_check(1);//已审核
		 testRecord.setNumber(selfTestScores.get(0).getNumber());
		 testRecord.setSelfallscore(sTotalScore.getAll_stscore());
		 User user = (User)request.getSession().getAttribute("loginuser");
		 testRecord.setTea_user_id(user.getUser_id());
		 testRecord.setTeaallscore(sTotalScore.getAll_teacher_score());
		 testRecord.setTest_code(selfTestScores.get(0).getTest_code());
		testRecordService.updateTestRecord(testRecord);
		///////
	}

	
	/**
	 * 
	 * @param testQuestions 试题list
	 * @param test_code  考试卷编码
	 */
	public List<TestQuestion> addSelfTest(List<TestQuestion> testQuestions, Integer test_code ,String number) {
		//将试题插入到《自测试卷表》
		for (TestQuestion testQuestion : testQuestions) {
			SelfTest selfTest=new SelfTest();
			selfTest.setTq_id(testQuestion.getTq_id());//设置试题编号
			selfTest.setQt_code(testQuestion.getQt_code());//设置题型
			selfTest.setNumber(Integer.parseInt(number));//设置试卷编码
			selfTest.setTest_code(test_code);//设置考试卷编码
			selfTest.setAnswer(testQuestion.getAnswer());//设置答案
			Integer id = selfTestService.addSelfTest(selfTest);//返回这道题插入《自测试卷表》后的唯一标识（主键）
			testQuestion.setSelf_test_id(id);
		}
		return testQuestions;
	}


	
	/**
	 * 答案提交
	 * @param model
	 * @param request
	 * @param response
	 * @param answerStr 包含了《自测试卷》中的题号和该题所填写的答案。格式-->id:answer
	 * @param test_code 考试卷编码
	 * @param number 试卷编码
	 * @param qt_code 题型code
	 * @param question_score 每题分值
	 * @param commitType 提交类型（1:客观 ，2:主观）
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping(value = "commitanswer", method = RequestMethod.POST)
	public void commitAnswer(ModelMap model,HttpServletRequest request, HttpServletResponse response ,String answerStr , String test_code,String number,String qt_code,String question_score,String commitType) throws IOException {
		response.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		//User user = (User)request.getSession().getAttribute("loginuser");
		float total = 0; //主观题总分
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("number",number);
		String []keyValue = answerStr.split(",");
		SelfTest selfTest = new SelfTest();
		if(Integer.parseInt(commitType)==1){
			for (int i = 0; i < keyValue.length; i++) {//保存答案到《自测试卷》
				selfTest.setId(Integer.parseInt(keyValue[i].split(":")[0]));
				selfTest.setSelf_answer(keyValue[i].split(":")[1]);
				selfTestService.updateSelfTest(selfTest);
			}
		}else{//主观题
			for (int i = 0; i < keyValue.length; i++) {//保存答案到《自测试卷》
				selfTest.setId(Integer.parseInt(keyValue[i].split(":")[0]));
				selfTest.setSelf_answer(keyValue[i].split(":")[1]);
				total += Float.parseFloat(keyValue[i].split(":")[2]);
				selfTestService.updateSelfTest(selfTest);
			}
		}
		//保存该大题测试结果到《自测考试成绩》
		saveToSelfTestScores(test_code, number, qt_code, question_score,commitType,total);
		out.print("success");
	}
	
	/**
	 * @param test_code 考试卷编码
	 * @param number 试卷编码
	 * @param qt_code 题型code
	 * @param question_score 每题分值
	 * @param commitType 提交类型（1:客观 ，2:主观）
	 * @param total 主观题总分
	 */
	public void saveToSelfTestScores(String test_code,String number,String qt_code,String question_score,String commitType,float total) {
		//将试题插入到《自测考试成绩》
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("test_code",test_code);
		params.put("qt_code",qt_code);
		Integer doRight=selfTestService.getRight(params);//计算总分
		Float allScores = doRight * Float.parseFloat(question_score);
		SelfTestScores selfTestScores = new SelfTestScores();
		selfTestScores.setTest_code(Integer.parseInt(test_code));//设置考试卷编码
		selfTestScores.setQt_code(Integer.parseInt(qt_code));//设置题型代码
		selfTestScores.setNumber(Integer.parseInt(number));//设置试卷编码
		if(Integer.parseInt(commitType)==1){
			selfTestScores.setStscore(allScores);//设置自测评分
			selfTestScores.setTeacher_score(allScores);//教师教师评分
		}else{
			selfTestScores.setStscore(total);//设置自测评分
			selfTestScores.setTeacher_score(0f);//教师教师评分
		}
		selfTestScoresService.addSelfTestScores(selfTestScores);
	}
	
	
	/**
	 * 保存考试记录
	 * @param model
	 * @param request
	 * @param response
	 * @param test_code 考试卷编码
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "commitrecord", method = RequestMethod.GET)
	public String commitRecord(ModelMap model,HttpServletRequest request, HttpServletResponse response , String test_code) throws IOException {
		//response.setCharacterEncoding("utf-8");
		//PrintWriter out = response.getWriter();
		//User user = (User)request.getSession().getAttribute("loginuser");
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("test_code", test_code);
		SelfTestScores selfTestScores = selfTestScoresService.getTotalScores(params);
		TestRecord testRecord = new TestRecord();
		testRecord.setTest_code(Integer.parseInt(test_code));
		if(selfTestScores.getAll_stscore()!=null){
			testRecord.setSelfallscore(selfTestScores.getAll_stscore());
		}else{
			testRecord.setSelfallscore(0f);
		}
		if(selfTestScores.getAll_teacher_score()!=null){
			testRecord.setTeaallscore(selfTestScores.getAll_teacher_score());
		}else{
			testRecord.setTeaallscore(0f);
		}
		testRecordService.updateTestRecord(testRecord);
		return "redirect:majorcurriculum?type=1&id=0";
	}
	
	
	/**
	 * 保存考试记录
	 * @param model
	 * @param request
	 * @param response
	 * @param test_code 考试卷编码
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "showrecord", method = RequestMethod.GET)
	public String showRecord(ModelMap model,HttpServletRequest request, HttpServletResponse response , String curriculum_code ,String curriculum_name) throws UnsupportedEncodingException {
		User user = (User)request.getSession().getAttribute("loginuser");
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("curriculum_code",curriculum_code);
		//设置分页  获得当前页码
		String pageNoStr = request.getParameter("page"); 
		int pageNo = 1;  
		if(pageNoStr != null && !pageNoStr.equals("")){
			pageNo = Integer.parseInt(pageNoStr);  
		}
		//PageCondition pagecondition = testCodeService.getAllTestCode(PageNameEnum.PAGESIZE, pageNo,params,request);
		List<TestCode> testCodes=testCodeService.getAllTestCode(params);
		//List<TestCode> testCodes = (List<TestCode>) pagecondition.getDataList();
		if(testCodes!=null&&testCodes.size()>0)
		{
			for (TestCode testCode : testCodes) {
				Map<String, Object> choose = new HashMap<String, Object>();
				choose.put("number",testCode.getNumber());//试卷编码
				choose.put("user_id",user.getUser_id());//考试学生
				List<TestRecord> testRecords = testRecordService.getAllTestRecord(choose);
				for (TestRecord testRecord : testRecords) {
					if (testRecord.getIs_check()!=0) {
						Teacher teacher = teacherService.selectByPrimaryKey(testRecord.getTea_user_id());
						testRecord.setTea_user_name(teacher.getTeacher_name());
					}
				}
				if(testRecords!=null&&testRecords.size()>0){
					testCode.setTestRecords(testRecords);
				}
			}
			model.addAttribute("testCodes", testCodes);
		}
		curriculum_name = java.net.URLDecoder.decode(curriculum_name,"utf-8");
		model.addAttribute("curriculum_name", curriculum_name);
		return "onlinetest/show-testrrecord";
	}
}
