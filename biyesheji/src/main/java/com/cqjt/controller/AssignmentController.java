package com.cqjt.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileUploadException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.cqjt.pojo.Assignment;
import com.cqjt.pojo.Courseware;
import com.cqjt.pojo.User;
import com.cqjt.service.impl.AssignmentServiceImpl;
import com.cqjt.util.FileOperation;
import com.cqjt.util.UploadJSON;
/**
 * 作业controller
 * @author 见鸣
 *
 */
@Controller
@RequestMapping(value = "/assignment/")
public class AssignmentController 
{
	@Resource(name = "assignmentService")
	private AssignmentServiceImpl assignmentService;
	
	//显示相关课程的作业
	@RequestMapping(value = "showAssignment", method = RequestMethod.GET)
	public String getAssignment(ModelMap model,HttpServletRequest request, HttpServletResponse response, String curriculum_code,String curriculum_name) throws UnsupportedEncodingException
	{
		Map<String, Object> param =new HashMap<String, Object>();
		param.put("curriculum_code", Integer.parseInt(curriculum_code));
		curriculum_name=java.net.URLDecoder.decode(curriculum_name,"utf-8");
		List<Assignment> assignmentList=assignmentService.getAllAssignment(param);//根据param里的curriculum_code找到相关作业
		model.addAttribute("curriculum_name", curriculum_name);//保存课程名
		model.addAttribute("curriculum_code", curriculum_code);//保存课程code
		if(assignmentList!=null&&assignmentList.size()!=0)
		{
			model.addAttribute("assignmentList", assignmentList);
		}
		int role=((User)request.getSession().getAttribute("loginuser")).getRole_code();
		model.addAttribute("loginuserRole", role);
		return "assignment/show-assignmnet";
	}
	//删除作业
	@RequestMapping(value = "delAssignment", method = RequestMethod.GET)
	public void delAssignment(HttpServletRequest request,HttpServletResponse response,Assignment assignment) throws IOException
	{
		
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("assignment_code", assignment.getAssignment_code());
		Assignment delAssignment=assignmentService.getAllAssignment(params).get(0);
		boolean success=assignmentService.delAssignment(assignment);
		if (success) 
		{
			String phyPath = request.getSession().getServletContext().getRealPath("/");
			String docPath=phyPath+delAssignment.getLocation();
			System.out.println("作业删除路径:"+docPath);
			FileOperation.DeleteFolder(docPath);
			out.print("success");
		} else {
			out.print("fail");
		}
	}
	
	//上传课程的作业
	@RequestMapping(value = "uploadAssignment", method = RequestMethod.POST)
	public void uploadAssignment(ModelMap model,HttpServletRequest request, HttpServletResponse response ,String curriculum_code) throws IOException, FileUploadException {
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		JSONObject json  = UploadJSON.uploadFile(request, response);
		if(json.getString("error").equals("0"))
		{
			Assignment assignment = new Assignment();
			assignment.setCurriculum_code(Integer.parseInt(curriculum_code));//设置课件的课程code
			assignment.setFilename(json.getString("fileName"));//设置课件名
			assignment.setLocation(".."+json.getString("url"));//设置文件链接（相对路径）
			assignmentService.addAssignment(assignment);
		}
		else{
			out.println("shibai");
			System.out.println("shibai");
		}
	}
	//下载课程的作业
	@RequestMapping(value = "downAssignment", method = RequestMethod.POST)
	public void downAssignment(ModelMap model,HttpServletRequest request, HttpServletResponse response ,Assignment assignment) throws IOException, FileUploadException 
	{
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		Boolean success = assignmentService.updateAssignmentDowncount(assignment);
		if (success) {
			out.print("success");
		} else {
			out.print("fail");
		}
	}
}
