package com.cqjt.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileUploadException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONObject;
import com.cqjt.pojo.Assignment;
import com.cqjt.pojo.Teachingvideotape;
import com.cqjt.pojo.User;
import com.cqjt.service.impl.TeachingvideotapeServiceImpl;
import com.cqjt.util.FileOperation;
import com.cqjt.util.UploadFile;
import com.cqjt.util.UploadFile.SelectException;
import com.cqjt.util.UploadJSON;

/**
 * 教学录像controller
 * @author 见鸣
 *
 */
@Controller
@RequestMapping(value = "/teachingvideotape/")
public class TeachingvideotapeController 
{
	@Resource(name ="teachingvideotapeService")
	private TeachingvideotapeServiceImpl teachingvideotapeService;
	
	//显示相关课程的教学录像

	@RequestMapping(value = "showTeachingvideotape", method = RequestMethod.GET)
	public String getTeachingvideotape(ModelMap model,HttpServletRequest request, HttpServletResponse response, String curriculum_code,String curriculum_name) throws UnsupportedEncodingException
	{
		Map<String, Object> param =new HashMap<String, Object>();
		param.put("curriculum_code", Integer.parseInt(curriculum_code));
		curriculum_name=java.net.URLDecoder.decode(curriculum_name,"utf-8");
		List<Teachingvideotape> teachingvideotapeList=teachingvideotapeService.getAllTeachingvideotape(param);
		model.addAttribute("curriculum_name", curriculum_name);//保存课程名
		model.addAttribute("curriculum_code", curriculum_code);//保存课程code
		if(teachingvideotapeList!=null&&teachingvideotapeList.size()!=0)
		{
			model.addAttribute("TeachingvideotapeList",teachingvideotapeList);
		}
		int role=((User)request.getSession().getAttribute("loginuser")).getRole_code();
		model.addAttribute("loginuserRole", role);
		return "teachingvideotape/show-teachingvideotape";
	}
	//删除教学录像
	@RequestMapping(value = "delTeachingvideotape", method = RequestMethod.GET)
	public void Teachingvideotape(HttpServletRequest request,HttpServletResponse response,Teachingvideotape teachingvideotape) throws IOException
	{
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		String phyPath = request.getSession().getServletContext().getRealPath("/");
		teachingvideotape=teachingvideotapeService.getTeachingvideotape(teachingvideotape.getTeachingvideotape_code());
		boolean success=teachingvideotapeService.delTeachingvideotape(teachingvideotape);
		if (success) 
		{
			String docPath=phyPath+teachingvideotape.getLocation();
			FileOperation.DeleteFolder(docPath);
			out.print("success");
		} else {
			out.print("fail");
		}
	}
	
	//下载课程的教学录像
	@RequestMapping(value = "downTeachingvideotape", method = RequestMethod.POST)
	public void downCourseware(ModelMap model,HttpServletRequest request, HttpServletResponse response ,Teachingvideotape teachingvideotape) throws IOException, FileUploadException 
	{
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		Boolean success = teachingvideotapeService.updateTeachingvidotapeDowncount(teachingvideotape);
		if (success) {
			out.print("success");
		} else {
			out.print("fail");
		}
	}
	//上传课程的教学录像
	@RequestMapping(value = "uploadTeachingvideotape", method = RequestMethod.POST)
	public void uploadTeachingvideotape(HttpServletRequest request, HttpServletResponse response ,String curriculum_code) throws IOException, FileUploadException, IllegalStateException, SelectException{
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		JSONObject json  = UploadJSON.uploadFile(request, response);
		System.out.println("错误提示:"+json.getString("error"));
		if(json.getString("error").equals("0")){
			Teachingvideotape teachingvideotape = new Teachingvideotape();
			teachingvideotape.setCurriculum_code(Integer.parseInt(curriculum_code));//设置教学录像的课程code
			teachingvideotape.setFilename(json.getString("fileName"));//设置教学录像名
			teachingvideotape.setLocation(".."+json.getString("url"));//设置文件链接（相对路径）
			System.out.println("录像课程号:"+teachingvideotape.getCurriculum_code());
			System.out.println("录像名:"+teachingvideotape.getFilename());
			System.out.println("录像链接位置:"+teachingvideotape.getLocation());
			teachingvideotapeService.addTeachingvideotape(teachingvideotape);
		}else{
			out.println("shibai");
			System.out.println("shibai");
	    }
	}

	
	//播放跳转
	@RequestMapping(value = "videoPlay", method = RequestMethod.GET)
	public String videoPlay(ModelMap model,HttpServletRequest request, HttpServletResponse response,String curriculum_code,String curriculum_name,String teachingvideotape_code,String teachingvideotape_filename) throws UnsupportedEncodingException
	{
		System.out.println("curriculum_code="+curriculum_code);
		curriculum_name=java.net.URLDecoder.decode(curriculum_name,"utf-8");
		teachingvideotape_filename=java.net.URLDecoder.decode(teachingvideotape_filename,"utf-8");
		model.addAttribute("curriculum_code", curriculum_code);
		model.addAttribute("curriculum_name", curriculum_name);
		model.addAttribute("teachingvideotape_filename",teachingvideotape_filename);
		Teachingvideotape teachingvideotape=teachingvideotapeService.getTeachingvideotape(Integer.parseInt(teachingvideotape_code));
		//System.out.println("teachingvideotape.getLocation()"+teachingvideotape.getLocation());
		model.addAttribute("teachingvideotape_location", teachingvideotape.getLocation());
		return "teachingvideotape/videoPlay";
	}

}
