package com.cqjt.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cqjt.pojo.Curriculum;
import com.cqjt.pojo.CurriculumCharacter;
import com.cqjt.pojo.Major;
import com.cqjt.pojo.MenuLevelTwo;
import com.cqjt.pojo.StudyMode;
import com.cqjt.pojo.User;
import com.cqjt.service.ICurriculumCharacterService;
import com.cqjt.service.ICurriculumService;
import com.cqjt.service.IMajorService;
import com.cqjt.service.IStudyModeService;
import com.cqjt.util.PageCondition;
import com.cqjt.util.PageNameEnum;
import com.cqjt.util.model.Page;

/**
 * 课程Controller
 * @author LIJIN
 *
 */
@Controller
@RequestMapping(value = "/curriculum/")
public class CurriculumController extends BaseController{
	//注入专业的Service
	@Autowired
	private IMajorService majorService;
	//注入课程的Service
	@Autowired
	private ICurriculumService curriculumService;
	//注入修学模式
	@Autowired
	private	IStudyModeService studyModeService;
	//注入课程性质
	@Autowired
	private	ICurriculumCharacterService curriculumCharacterService;
	
	
	//显示相关专业的课程
	@RequestMapping(value = "show", method = RequestMethod.GET)
	public String showCurriculums(ModelMap model,HttpServletRequest request, HttpServletResponse response , String major_name) throws UnsupportedEncodingException 
	{
		curriculumsDisplayForPage(model,request,response,major_name);
		return "courseware/show-curriculum";
	}
	//显示相关专业的课程
	@RequestMapping(value = "show_TeachingMaterials", method = RequestMethod.GET)
	public String showTeachingMaterialsCurriculums(ModelMap model,HttpServletRequest request, HttpServletResponse response , String major_name) throws UnsupportedEncodingException 
	{
		
		curriculumsDisplay(model,request,response,major_name);
		return "teachingdata/show-curriculum";
	}
	//通过专业名major_name读取课程信息----分页用
	public void curriculumsDisplayForPage(ModelMap model,HttpServletRequest request, HttpServletResponse response , String major_name) throws UnsupportedEncodingException
	{
		major_name=java.net.URLDecoder.decode(major_name,"utf-8");
		Map<String, Object> params =new HashMap<String, Object>();	
		params.put("major_name", major_name);
		Major majors=majorService.getMajor(params);//根据专业名找到相关专业
		if(majors!=null){
			params.put("major_code", majors.getMajor_code());
			//System.out.println(majors.getMajor_code());
			//List<Curriculum> curriculums=curriculumService.getAllCurriculum(params);//根据专业code找相关的课程
			//设置分页  获得当前页码
			String pageNoStr = request.getParameter("page"); 
			int pageNo = 1;  
			if(pageNoStr != null && !pageNoStr.equals("")){
				pageNo = Integer.parseInt(pageNoStr);  
			}
			PageCondition pagecondition = curriculumService.getAllCurriculum(PageNameEnum.PAGESIZE, pageNo,params,request);
			model.addAttribute("pagecondition", pagecondition);//二级菜单一项的所有课程
		}
	}
	//通过专业名major_name读取课程信息
	public void curriculumsDisplay(ModelMap model,HttpServletRequest request, HttpServletResponse response , String major_name) throws UnsupportedEncodingException
	{
		major_name=java.net.URLDecoder.decode(major_name,"utf-8");
		Map<String, Object> params =new HashMap<String, Object>();	
		params.put("major_name", major_name);
		Major majors=majorService.getMajor(params);//根据专业名找到相关专业
		if(majors!=null){
			params.put("major_code", majors.getMajor_code());
			//System.out.println(majors.getMajor_code());
			List<Curriculum> curriculums=curriculumService.getAllCurriculum(params);//根据专业code找相关的课程
			model.addAttribute("curriculums", curriculums);//二级菜单一项的所有课程
		}
	}
	//返回作业的课程信息 
	//author by JianMing
	@RequestMapping(value = "show_AssignmentCurriculums", method = RequestMethod.GET)
	public String showAssignmentCurriculums(ModelMap model,HttpServletRequest request, HttpServletResponse response , String major_name) throws UnsupportedEncodingException
	{
		//curriculumsDisplay(model,request,response,major_name);
		curriculumsDisplayForPage(model,request,response,major_name);
		return "assignment/show-curriculum";
	}
	//返回教学录像的课程信息 
	//author by JianMing
	@RequestMapping(value = "show_TeachingvideotapeCurriculums", method = RequestMethod.GET)
	public String showTeachingvideotapeCurriculums(ModelMap model,HttpServletRequest request, HttpServletResponse response , String major_name) throws UnsupportedEncodingException
	{
		//curriculumsDisplay(model,request,response,major_name);
		curriculumsDisplayForPage(model,request,response,major_name);
		return "teachingvideotape/show-curriculum";
	}
	
	//返回休学模式列表
	//author by xiaoshujun 2014-4-20
	@RequestMapping(value="getstudymodelist",method=RequestMethod.GET ) 
	public ModelAndView getStudyModeList(Model model,HttpServletRequest request){
		String url="";
		List<StudyMode> studyModes = studyModeService.getAllStudyMode();
		model.addAttribute("studymodelist", studyModes);
		if(request.getParameter("url")!=null)
			url = request.getParameter("url").toString().trim();
		ModelAndView mv = new ModelAndView(url);
		mv.addObject("studymodelist", studyModes);
		return mv;
	}
	//返回课程性质列表
	//author by xiaoshujun 2014-4-20
	@RequestMapping(value="getcurriculumcharacterlist",method=RequestMethod.GET )
	public ModelAndView getCurriculumCharacterList(Model model,HttpServletRequest request){
		
		String url="";
		if(request.getParameter("url")!=null)
			url = request.getParameter("url").toString().trim();
		ModelAndView mv = new ModelAndView(url);
		List<CurriculumCharacter> curriculumCharacters = curriculumCharacterService.getAllCurriculumCharacter();
		model.addAttribute("curriculumcharacterlist", curriculumCharacters);
		mv.addObject("curriculumcharacterlist", curriculumCharacters);
		return mv;
	}
	
	//增加课程
	//author by xiaoshujun 2014-4-20
	@RequestMapping(value="addcurriculum",method={RequestMethod.POST,RequestMethod.GET} ,produces = {"application/json;charset=UTF-8"})
	@ResponseBody 
	public ModelAndView addCurriculum(
			@ModelAttribute("curriculum")Curriculum curriculum,
			HttpServletRequest request){
		ModelAndView mv = new ModelAndView("/menu/menu-content-add-curriculum");
		System.out.println(curriculum);
		curriculumService.addCurriculum(curriculum);
		return mv;
	}
	//删除课程
	//author by xiaoshujun 2014-4-20
	@RequestMapping(value="deletecurriculum",method={RequestMethod.POST,RequestMethod.GET} ,produces = {"application/json;charset=UTF-8"})
	@ResponseBody 
	public ModelAndView deleteCurriculum(HttpServletRequest request){
		ModelAndView mv = new ModelAndView("/menu/menu-content-update-curriculum");
    	int curriculum_code = 0;
    	Curriculum curriculum = null;
		if(request.getParameter("curriculum_code")!=null)
		{
			curriculum_code = Integer.parseInt(request.getParameter("curriculum_code").toString().trim());
			curriculum = curriculumService.getCurriculumByCode(curriculum_code);
		}
		curriculumService.deleteCurriculum(curriculum);
		return mv;
	}
	//修改课程
	//author by xiaoshujun 2014-4-20
	@RequestMapping(value="updatecurriculum",method=RequestMethod.POST )
	public ModelAndView updateCurriculum(
			@ModelAttribute("curriculum")Curriculum curriculum,
			Model model,
			HttpServletRequest request){
	//	ModelAndView mv = new ModelAndView("/menu/menu-content-update-curriculum");
	//	ModelAndView mv = new ModelAndView("/curriculum/curriculum-system");
		curriculumService.updateCurriculum(curriculum);
	//	return mv;
		return curriculumSearch(request, model);
	}
	
	//课程体系查询，也是通过该cotroller进入curriculum-system
	//author by xiaoshujun 2014-4-20
	@RequestMapping(value="search",method=RequestMethod.GET )
	public ModelAndView curriculumSearch(HttpServletRequest request,Model model){
	    //返回页面地址
		ModelAndView mv = new ModelAndView();
		
		Map<String, Object> params = new HashMap<String, Object>();
		Integer semester = null,studyMode_id=null,major_code=null,cc_code=null;
		
		List<CurriculumCharacter> curriculumCharacters = curriculumCharacterService.getAllCurriculumCharacter();
		List<Major> majors = majorService.getMajorList();
		List<StudyMode> studyModes = studyModeService.getAllStudyMode();
		
		//三个过滤下拉列表
		model.addAttribute("curriculumcharacterclist",curriculumCharacters);
		model.addAttribute("majorlist", majors);
		model.addAttribute("studymodelist", studyModes);
		
		if(request.getParameter("major_select")!=null&&!request.getParameter("major_select").trim().equals("-1")){
			major_code = Integer.parseInt(request.getParameter("major_select").toString().trim());
			model.addAttribute("majorcode_save", major_code);  //保存当前下拉列表的值，下同

			params.put("major_code",  major_code);
		}else {
			//model.addAttribute("majorcode_save", -1);
			params.put("major_code",  null);
		}
		if (request.getParameter("curriculumcharacter_select")!=null&&!request.getParameter("curriculumcharacter_select").trim().equals("-1")) {
			cc_code = Integer.parseInt(request.getParameter("curriculumcharacter_select").toString().trim());
			model.addAttribute("cccode_save", cc_code);
			params.put("cc_code",cc_code);
			
		}else {
			params.put("cc_code",null);
		}
		if(request.getParameter("semester_select")!=null&&!request.getParameter("semester_select").trim().equals("-1")){
			 semester=Integer.parseInt(request.getParameter("semester_select").toString().trim());
			 model.addAttribute("semestercode_save", semester);
			 params.put("semester_code",semester);
		}else {
			params.put("semester_code",null);
		}
		if(request.getParameter("studymode_select")!=null&&!request.getParameter("studymode_select").trim().equals("-1")){
			 studyMode_id=Integer.parseInt(request.getParameter("studymode_select").toString().trim());
			 model.addAttribute("studymodeid_save",studyMode_id);
			 params.put("studymode_id", studyMode_id);
		}else {
			params.put("studymode_id", null);
		}
		
		
        //添加查询条件  
        // ... params.put("name","jack");...  
          
        //获取总条数  
        Long totalCount = curriculumService.getPageCounts(params);
        //设置分页对象  
        Page page = executePage(request,totalCount);          
        //压入查询参数:开始条数与结束条灵敏  
        params.put("startIndex", page.getBeginIndex());  
        params.put("endIndex", page.getEndinIndex());  
		
		
	//	List<Curriculum> list= curriculumService.getAllCurriculum(params);
        List<Curriculum> list= curriculumService.pageList(params);
        mv.setViewName("/curriculum/curriculum-system");
	//	model.addAttribute("curriculum_list",list);
		mv.addObject("curriculum_list",list);
		return mv;		
	}
	
	//系统管理里面添加课程的跳转
	@RequestMapping(value="menu_content_add_property",method=RequestMethod.GET )
	public ModelAndView menu_content_add_property(ModelMap model,HttpServletRequest request){
		List<Major> majors=majorService.getMajorListOnlyCodeAndName();
		model.addAttribute("majors", majors);
		return new ModelAndView("menu/menu-content-add-property");
	}
	
	//系统管理里面添加课程的提交事件
	@RequestMapping(value="sys/addCurriculum",method=RequestMethod.POST )
	public ModelAndView sysAddCurriculum(Curriculum curriculum,HttpServletRequest request){
		curriculum.setAll_time(curriculum.getComputer_time()+curriculum.getPractice_time()+curriculum.getTheory_time());
		boolean f=curriculumService.addCurriculum(curriculum);
		return new ModelAndView("redirect:/curriculum/menu_content_add_property");
	}
	
	//系统管理里面删除课程的提交事件
	@RequestMapping(value="sys/deletecurriculum",method=RequestMethod.GET )
	public void sysDeleteCurriculum(int code,HttpServletRequest request,HttpServletResponse response) throws IOException{
		Curriculum curriculum=new Curriculum();
		curriculum.setCurriculum_code(code);
		boolean f=curriculumService.deleteCurriculum(curriculum);
		PrintWriter out=response.getWriter();
		out.print(f);
	}
	
	//检查课程是否存在
	@RequestMapping(value="sys/checkcurriculum",method=RequestMethod.POST )
	public void checkCurriculum(String curriculum_name,HttpServletRequest request, HttpServletResponse response) throws IOException{		
		
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		Curriculum f=curriculumService.getCurriculumByCurriculumName(curriculum_name);
		if(f!=null){
			out.print("have");
		}else{
			out.print("no");
		}
	}
	
	//修改课程的弹出框显示页面
	@RequestMapping(value="sys/updatecurriculum",method=RequestMethod.GET )
	public ModelAndView sysUpdateCurriculum(int code,HttpServletRequest request,ModelMap model){
		Curriculum f=curriculumService.getCurriculumByCode(code);
		List<Major> majors=majorService.getMajorListOnlyCodeAndName();
		model.addAttribute("majors", majors);
		model.addAttribute("curriculum", f);
		return new ModelAndView("curriculum/update-curriculum");
	}
	
	@RequestMapping(value="sys/submitupdatecurriculum",method=RequestMethod.POST )
	public void submitupdatecurriculum(Curriculum curriculum,HttpServletRequest request){
		curriculum.setAll_time(curriculum.getComputer_time()+curriculum.getPractice_time()+curriculum.getTheory_time());
		boolean f=curriculumService.updateCurriculum(curriculum);
	}
}
