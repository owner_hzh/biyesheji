package com.cqjt.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.cqjt.pojo.Role;
import com.cqjt.pojo.User;
import com.cqjt.service.IRoleService;
import com.cqjt.service.ITeacherService;
import com.cqjt.service.IUserService;
import com.cqjt.util.MD5Util;

@Controller  
public class UserManageController { 
	
	@Resource(name="roleService")
	private IRoleService roleService;
	@Resource(name = "teacherService")
	private ITeacherService teacherService;
	@Resource(name="userService")
	private IUserService userService;
	private final String uploadLocation = "upload\\";
	//鏁版嵁瀵煎叆
	@RequestMapping(value = "/importTeacher", method = RequestMethod.POST)      
	public ModelAndView importData(@RequestParam(value = "file", required = false) MultipartFile file, HttpServletRequest request,HttpServletResponse response) 
	{     
		String path = request.getSession().getServletContext().getRealPath("/")+uploadLocation;	
		teacherService.uploadTeachersExcel(file, path);
		//鍚戝墠鍙版墦鍗版暟鎹�
		PrintWriter out = null;
		try {
			out = response.getWriter();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		out.print("success");
		return null;//new ModelAndView("menu-content-import-teachers");    
	}		
	
	//娣诲姞瑙掕壊
	@RequestMapping(value = "/addRole", method = RequestMethod.POST)      
	public ModelAndView addRole(@ModelAttribute("role")Role role,HttpServletResponse response) {          
		boolean flag=roleService.addRole(role); 
		PrintWriter out = null;
		try {
			out = response.getWriter();
		} catch (IOException e) {
			e.printStackTrace();
		}
		out.print(flag);
		return null;    
		}
	
	//修改密码
	//author by jianming
	@RequestMapping(value ="/changepassword")
	public void changepassword(HttpServletRequest request,HttpServletResponse response,String oldpassword,String newpassword,String re_newpassword) throws IOException
	{
		System.out.println("旧密码"+oldpassword);
		System.out.println("新密码"+newpassword);
		System.out.println("确认密码"+re_newpassword);
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		User user = (User)request.getSession().getAttribute("loginuser");
		oldpassword = MD5Util.MD5(oldpassword);
		if(!oldpassword.equals(user.getPassword()))
			out.print("fail");
		else
		{
		    if(newpassword.equals(re_newpassword))
		    {
			    user.setPassword(MD5Util.MD5(newpassword));
			    if(userService.updateUser(user))
			    	out.print("success");
			    else
			    	out.print("fail");
		    }
		    else
			    out.print("fail");
	       }
	  }
}
