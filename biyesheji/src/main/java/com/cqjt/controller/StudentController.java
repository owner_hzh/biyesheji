package com.cqjt.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileUploadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.cqjt.pojo.Courseware;
import com.cqjt.pojo.Curriculum;
import com.cqjt.pojo.Major;
import com.cqjt.pojo.Student;
import com.cqjt.pojo.Teacher;
import com.cqjt.pojo.TeacherInfor;
import com.cqjt.pojo.User;
import com.cqjt.service.ICoursewareService;
import com.cqjt.service.ICurriculumService;
import com.cqjt.service.IStudentService;
import com.cqjt.service.IUserService;
import com.cqjt.util.DocConverter;
import com.cqjt.util.MD5Util;
import com.cqjt.util.PageCondition;
import com.cqjt.util.PageNameEnum;
import com.cqjt.util.UploadJSON;

/**
 * 学生Controller
 * @author LIJIN
 *
 */
@Controller
@RequestMapping(value = "/student/")
public class StudentController {
	//注入用户的Service
	@Autowired
	private IUserService userService;
	//注入用户的Service
	@Autowired
	private IStudentService studentService;
	
	//学生用户导入
	@RequestMapping(value ="upload")
	public void menu_content_import_students(HttpServletRequest request, HttpServletResponse response) throws IOException, FileUploadException {
		JSONObject json  = UploadJSON.uploadFile(request, response);
	}
	
	//学生列表----分页
	@RequestMapping(value ="menu-content-show-students", method = RequestMethod.GET)
	public String menu_content_show_students(ModelMap model,HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {
		Map<String, Object> params =new HashMap<String, Object>();	
		String student_name= request.getParameter("student_name");
		String student_sex= request.getParameter("student_sex");
		String user_id= request.getParameter("user_id");
		String grade= request.getParameter("grade");
		if(student_name != null && student_name.trim().length() > 0) {
			student_name = 	java.net.URLDecoder.decode(student_name,"utf-8");
        	params.put("student_name", student_name);
        }
		
		if(user_id != null && user_id.trim().length() > 0) {
			user_id = 	java.net.URLDecoder.decode(user_id,"utf-8");
			params.put("user_id", user_id);
		}

		if(!"".equals(student_sex) && student_sex != null && !student_sex.equals("null")){
			params.put("student_sex", Integer.parseInt(student_sex));
		}
		
		if(!"".equals(grade) && grade != null && !grade.equals("null")){
			params.put("grade", Integer.parseInt(grade));
		}
		//设置分页  获得当前页码
		String pageNoStr = request.getParameter("page"); 
		int pageNo = 1;  
		if(pageNoStr != null && !pageNoStr.equals("")){
			pageNo = Integer.parseInt(pageNoStr);  
		}
		PageCondition pagecondition = studentService.getAllStudents(PageNameEnum.PAGESIZE, pageNo,params,request);
		model.addAttribute("pagecondition", pagecondition);
		model.addAttribute("student_name", student_name);
		model.addAttribute("student_sex", student_sex);
		model.addAttribute("user_id", user_id);
		model.addAttribute("grade", grade);
		return "menu/menu-content-show-students";
	}	
	//添加学生
	@RequestMapping(value ="menu-content-add-students", method = RequestMethod.GET)
	public String menu_content_add_students() {
		return "menu/menu-content-add-students";
	}
	//添加学生
	@RequestMapping(value ="menu-content-add-students", method = RequestMethod.POST)
	public String add_teachers(HttpServletRequest request, HttpServletResponse response,Student student,User user) {
		user.setPassword(MD5Util.MD5(user.getPassword()));
		studentService.insert(student,user);
		return "redirect:menu-content-show-students";
	}
	

	//删除学生
	@RequestMapping(value ="delete", method = RequestMethod.GET)
	public void deleteStudent(HttpServletRequest request, HttpServletResponse response,String userId) throws IOException {
		PrintWriter out = response.getWriter();
		int i = studentService.deleteByPrimaryKey(userId);
		if(i>0){
			out.print("success");
		}else{
			out.print("fail");
		}
	}
	
	//查看是否已注册
	@RequestMapping(value ="checkuser", method = RequestMethod.POST)
	public void checkUser(User user ,HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		User rs = userService.getUserid(user.getUser_id());
		if(rs!=null){
			out.print("have");
		}else{
			out.print("no");
		}
	}
}
