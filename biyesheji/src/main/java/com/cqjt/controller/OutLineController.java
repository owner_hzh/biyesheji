package com.cqjt.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.fileupload.FileUploadException;
import org.apache.ibatis.annotations.Param;
import org.apache.poi.hwpf.Word2Forrest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.alibaba.fastjson.JSONObject;
import com.cqjt.pojo.Assignment;
import com.cqjt.pojo.Courseware;
import com.cqjt.pojo.Curriculum;
import com.cqjt.pojo.CurriculumOutline;
import com.cqjt.pojo.Major;
import com.cqjt.pojo.User;
import com.cqjt.service.ICurriculumService;
import com.cqjt.service.IMajorService;
import com.cqjt.service.IOutLineService;
import com.cqjt.util.FileOperation;
import com.cqjt.util.GsonUtil;
import com.cqjt.util.Hanzi2Pinyin;
import com.cqjt.util.UploadJSON;
import com.cqjt.util.WordToHtml;

/**
 * 大纲控制层
 * @author Owner
 *
 */

@Controller
@RequestMapping(value = "/outline")
public class OutLineController {
	
	//注入菜单的Service
	@Autowired
	private IOutLineService outLineService;
	@Autowired
	private ICurriculumService curriculumService;
	@Autowired
	private IMajorService majorService;
	/**
	 * 大纲初始化显示数据
	 * @param request
	 * @return
	 */
	@RequestMapping(value ="/outline", method = RequestMethod.GET)
	public ModelAndView outline(HttpServletRequest request) {
		//根据专业查询课程
		List<Major> majors=majorService.getMajorList();
		for(int i=0;i<majors.size();i++)
		{
			Major m=majors.get(i);
			Map<String,Object> map=new HashMap<String,Object>();
			map.put("major_code", m.getMajor_code());
			List<Curriculum> curriculums=curriculumService.getAllCurriculum(map);
			m.setCurriculums(curriculums);
		}
		System.out.println("大纲的菜单项"+majors.toString());
		return new ModelAndView("outline","majors",majors);
	}
	
	/**
	 * 以code查询大纲信息 返回json
	 * @param code
	 * @param response
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping(value ="/getDataByCode", method = RequestMethod.GET)
	public  void getDataByCode(@ModelAttribute("code")int code,@ModelAttribute("ocode")int ocode,HttpServletResponse response,HttpServletRequest request) throws IOException {
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json; charset=UTF-8");
		CurriculumOutline outline =new CurriculumOutline();
		String path = request.getContextPath();
		String netPic = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
		String basePath = request.getSession().getServletContext().getRealPath("/");
		//basePath              D:\Java\MyEclipse\.metadata\.me_tcat\webapps\biyesheji\
		//outline.getLocation() resources\doc\testdocx_283385\
		//outline.getFilename() testdocx.docx
		//netPic                http://localhost:8080/biyesheji/
		PrintWriter out = null;
		out = response.getWriter();
		try {
			outline =outLineService.getOutlineByCode(code,ocode);
			String docPath=basePath.split("biyesheji")[0]+outline.getLocation().substring(1);
			System.out.println("Controller====docPath==="+docPath);
			String filePath=outLineService.word2HtmlConverter(docPath
					         ,outline.getLocation());
			System.out.println("返回的html文件网络路径=="+filePath);
			String l=outline.getLocation();
			outline.setHtmlfile(l.substring(0, l.lastIndexOf("."))+"/"+filePath.substring(filePath.lastIndexOf("/")+1));
			out.print(GsonUtil.getGson().toJson(outline));
		} catch (NullPointerException e1) {
			System.out.println("------>>>>NullPointerException");						
			out.print(GsonUtil.getGson().toJson(outline));
		}catch (FileNotFoundException e) {
			System.out.println("------>>>>FileNotFoundException");
			outline.setFilename("");	
			out.print(GsonUtil.getGson().toJson(outline));
		} catch (TransformerException e) {
			System.out.println("------>>>>TransformerException");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("------>>>>IOException");
			outline.setFilename("IOException");	
			out.print(GsonUtil.getGson().toJson(outline));
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			System.out.println("------>>>>ParserConfigurationException");
			e.printStackTrace();
		}finally{
			if(out!=null)
			  out.close();
		}	
	}
	
	/**
	 * 只读方式打开doc
	 * @param request
	 * @return
	 */
	@RequestMapping(value ="/page-office", method = RequestMethod.GET)
	public ModelAndView openOffice(@ModelAttribute("file")String docFile,Model model,HttpServletRequest request) {
		//String docFile="resources/doc/test.doc";
		String basePath = request.getSession().getServletContext().getRealPath("/");
		if(docFile==null)
		{
			return new ModelAndView("outline/no-file-found");
		}else if(docFile.substring(docFile.lastIndexOf("/")) .equals(null))
		{			
			return new ModelAndView("outline/no-file-found");
		}else
		{
			try {
				String path = request.getContextPath();
				String netPic = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
				String filePath=outLineService.word2HtmlConverter(basePath+docFile,netPic+docFile.substring(0, docFile.lastIndexOf("/")+1)).replace(basePath, "");			
				model.addAttribute("htmlfile", filePath);
			} catch (Exception e) {
				e.printStackTrace();
				return new ModelAndView("outline/no-file-found");
			}
			return null; //new ModelAndView(filePath.substring(filePath.lastIndexOf("/"),filePath.length()-1));
		}	
	}
	
	/**
	 * 编辑状态打开doc
	 * @param request
	 * @return
	 */
	@RequestMapping(value ="/editOffice", method = RequestMethod.GET)
	public ModelAndView editOffice(@ModelAttribute("file")String docFile,HttpServletRequest request) {
		//String docFile="resources/doc/test.doc";
		//outLineService.editPageOffice(request, docFile);
		return null;
	}
	
	/**
	 * 保存doc
	 * @param response
	 * @param request
	 * @return
	 */
	@RequestMapping(value ="/SaveFile",method = RequestMethod.POST)
	public ModelAndView SaveFile(@ModelAttribute("file")String docFile,HttpServletResponse response,HttpServletRequest request) {
		String docPath=docFile.substring(0, docFile.lastIndexOf("/")+1);//"resources/doc/";
		//outLineService.saveFile(request, response, docPath);
		return null;
	}
	
	/**
	 * 文件不存在的跳转
	 * @return
	 */
	@RequestMapping(value ="/nofile",method = RequestMethod.GET)
	public String nofile(@ModelAttribute("code")int code,int ocode,Model model) {
		model.addAttribute("code", code);
		model.addAttribute("ocode", ocode);
		return "outline/no-file-found";
	}
			
	/**
	 * outline页面初始化iframe显示的一个页面
	 * @return
	 */
	@RequestMapping(value ="/outlineInitial",method = RequestMethod.GET)
	public ModelAndView outlineInitial() {
		return new ModelAndView("outline/outline-initial");
	}
	
	/**
	 * 删除文档
	 * @return
	 */
	@RequestMapping(value ="/deleteDoc",method = RequestMethod.POST)
	public void deleteDoc(@ModelAttribute("code")int code,@ModelAttribute("ocode")int ocode,String doc,HttpServletResponse response,HttpServletRequest request) {
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		CurriculumOutline curriculumOutline=new CurriculumOutline();
		curriculumOutline.setCurriculum_code(code);
		curriculumOutline.setCurriculumoutline_code(ocode);
		
		//----128---testdocx.docx---resources/doc/testdocx.html		
		//物理路径
		String phyPath = request.getSession().getServletContext().getRealPath("/");
		//文档路径
		String docPath=phyPath+doc;
		System.out.println("删除的文件======"+docPath);
		//html页面路径
		//String htmlPath=phyPath+html;
		//图片路径
		//resources/doc/201404/20140403_221941_573320.docx
		//String picPath=phyPath+doc.substring(0, doc.lastIndexOf("/"))+"/docPic"+doc.substring(doc.lastIndexOf("/"), doc.lastIndexOf("."))+"/";
		//System.out.println("picture===="+phyPath+"resources/doc/docPic/"+doc.substring(0, doc.lastIndexOf(".")));
		//System.out.println("--deleteDoc--code=="+code+"---doc=="+docPath+"---html=="+htmlPath+"-----pic=="+picPath);
		PrintWriter out = null;
		try {			
			out = response.getWriter();	
			if(outLineService.deleteOutline(curriculumOutline))
			{
				outLineService.deleteRelatedFiles(docPath);								
				out.print(true);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			out.close();
		}
	}
	
	/**
	 * 在列表那里删除文档
	 * 因为传值的长度有问题
	 * 只能先查找再删除
	 * @return
	 */
	@RequestMapping(value ="/deleteDoc1",method = RequestMethod.POST)
	public void deleteDoc1(@ModelAttribute("code")int code,@ModelAttribute("ocode")int ocode,String doc,HttpServletResponse response,HttpServletRequest request) {
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		CurriculumOutline curriculumOutline=new CurriculumOutline();
		curriculumOutline.setCurriculum_code(code);
		curriculumOutline.setCurriculumoutline_code(ocode);
		
		CurriculumOutline c=outLineService.getOutlineByCode(code, ocode);
		
        String phypath=request.getSession().getServletContext().getRealPath("/");
		String file=c.getLocation();	
		String basepath=phypath.split("biyesheji")[0]+file.substring(1);
		String wordfile=basepath;
		String htmlfolder=wordfile.substring(0, wordfile.lastIndexOf("."))+"/";
		System.out.println("word文档========"+wordfile);
		System.out.println("htmlfolder文件夹========"+htmlfolder);
		//html页面路径
		//String htmlPath=phyPath+html;
		//图片路径
		//resources/doc/201404/20140403_221941_573320.docx
		//String picPath=phyPath+doc.substring(0, doc.lastIndexOf("/"))+"/docPic"+doc.substring(doc.lastIndexOf("/"), doc.lastIndexOf("."))+"/";
		//System.out.println("picture===="+phyPath+"resources/doc/docPic/"+doc.substring(0, doc.lastIndexOf(".")));
		//System.out.println("--deleteDoc--code=="+code+"---doc=="+docPath+"---html=="+htmlPath+"-----pic=="+picPath);
		PrintWriter out = null;
		try {			
			out = response.getWriter();	
			if(outLineService.deleteOutline(curriculumOutline))
			{						
				/*outLineService.deleteRelatedFiles(wordfile);	
				outLineService.deleteRelatedFiles(htmlfolder);*/
				FileOperation.deleteFile(wordfile);
				FileOperation.deleteDirectory(htmlfolder);
				out.print(true);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			out.close();
		}
	}	
	
	/**
	 * 接收上传的文件
	 * @param code
	 * @return
	 * @throws FileUploadException 
	 * @throws IOException 
	 */
	@RequestMapping(value ="/uploadDoc",method = RequestMethod.POST)
	public void receiveDoc(@ModelAttribute("code") String code, HttpServletRequest request,HttpServletResponse response,RedirectAttributes attr) throws IOException, FileUploadException {
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		
		/*//自己的上传  @RequestParam(value = "file", required = false) MultipartFile file, 
		String b=request.getSession().getServletContext().getRealPath("/");
		String basePath =b +"resources/doc/";
		//D:\Java\MyEclipse\.metadata\.me_tcat\webapps\biyesheji\resources/doc/testdocx_218618/testdocx.docx
		String f=outLineService.saveDocToFolder(file, basePath);	
		System.out.println("fuck ------------------------"+f);
		f="resources/doc/"+f.split("resources/doc/")[1];
		String name=file.getOriginalFilename();
		String path=f.substring(0, f.lastIndexOf("/")+1);
		
		CurriculumOutline c=new CurriculumOutline();
		c.setCo_name(name);
		c.setCurriculum_code(Integer.parseInt(code));
		c.setFilename(Hanzi2Pinyin.getFullSpell(name));
		c.setLocation(path);*/
		
		JSONObject json  = UploadJSON.uploadFile(request, response);
		CurriculumOutline c=new CurriculumOutline();
		if(json.getString("error").equals("0"))
		{			
			c.setCo_name(json.getString("fileName"));
			c.setCurriculum_code(Integer.parseInt(code));
			c.setFilename(json.getString("fileName"));
			c.setLocation(json.getString("url"));
		}
				
		boolean add=false;
		try {
			add = outLineService.addOutline(c);
		} catch (SQLException e1) {
			add = outLineService.updateOutline(c);
			e1.printStackTrace();
		}
		if(add)
		{
			PrintWriter out = null;
			try {
				out = response.getWriter();		
				out.print(true);
			} catch (IOException e) {
				e.printStackTrace();
			}finally
			{
				out.close();
			}
		}
		//return "redirect:/page-office";
	}
	
	//outline-list页面跳转 
	@RequestMapping(value ="/outlineList",method = RequestMethod.GET)
	public String outlineList(ModelMap model,@ModelAttribute("code")int code,HttpServletRequest request) {
		List<CurriculumOutline> outLineList=outLineService.getOutlineListByCode(code);
		model.addAttribute("outLineList",outLineList);
		model.addAttribute("code", code);
		if(outLineList!=null&&outLineList.size()>0)
		{
			model.addAttribute("ocode", outLineList.get(0).getCurriculumoutline_code());
		}
		
		User user = (User)request.getSession().getAttribute("loginuser");
		model.addAttribute("role", user.getRole_code());
		
		return "outline/outline-list";
	}
	
	//编辑html页面
	@RequestMapping(value ="/edithtml",method = RequestMethod.GET)
	public String edithtml(Model model,int code,int ocode,HttpServletRequest request) throws FileNotFoundException, NullPointerException, TransformerException, IOException, ParserConfigurationException {
		CurriculumOutline outLine=outLineService.getOutlineByCode(code, ocode);
		String phypath=request.getSession().getServletContext().getRealPath("/");
		String l=outLine.getLocation();
		
		String fname=l.substring(l.lastIndexOf("/"), l.lastIndexOf(".")+1)+"html";
		File f=new File(fname);
		if(!f.exists())
		{
			String docPath=phypath.split("biyesheji")[0]+l.substring(1);
			outLineService.word2HtmlConverter(docPath,l);
		}
		
		
		String basepath=phypath.split("biyesheji")[0]+l.substring(1);
		String htmlfile=basepath.substring(0, basepath.lastIndexOf("."))+fname;
		System.out.println("html文件全路径==========="+htmlfile);
		String s="";
		
		try {
			s=WordToHtml.readFile2String(htmlfile,"GB2312").toString();
			 Pattern p = Pattern.compile("<title.*?/title>");
		     Matcher m = p.matcher(s);
		     if(m.find()) {
		       s.replaceFirst(m.group(), "");
		     }
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		model.addAttribute("code", code);
		model.addAttribute("ocode", ocode);
		model.addAttribute("htmlfile", l);
		model.addAttribute("htmlString", s);
		return "outline/edit";
	}
	
	//保存修改后的html文件
	@RequestMapping(value ="/savehtml",method = RequestMethod.POST)
	public void savehtml(String data,String file,HttpServletRequest request,HttpServletResponse response) {
        //System.out.println("修改后的数据html"+data);
        //System.out.println("修改后的数据html文件路径"+file);
		String hmeta="<html> <head>  <META http-equiv="+"\""+"Content-Type"+"\""+" content="+"\""+"text/html; charset=GB2312"+"\""+">";
		data=hmeta+data.replaceFirst("<html>", "").replaceFirst("<head>", "");
		
		//System.out.println("------..."+data);
		String phypath=request.getSession().getServletContext().getRealPath("/");
		
		String fname=file.substring(file.lastIndexOf("/"), file.lastIndexOf(".")+1)+"html";
		
		
		String basepath=phypath.split("biyesheji")[0]+file.substring(1);
		String htmlfile=basepath.substring(0, basepath.lastIndexOf("."))+fname;

		String wordfile=basepath;
		
		WordToHtml.writeFile(data, htmlfile, "GB2312");
		try {
			WordToHtml.html2Word(htmlfile, wordfile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		PrintWriter out = null;
		try {
			out = response.getWriter();		
			out.print(true);
		} catch (IOException e) {
			e.printStackTrace();
		}finally
		{
			out.close();
		}
	}
	
	//ckeditor上传图片
	@RequestMapping(value ="/ckeditorupload",method = RequestMethod.POST)
	public void ckeditorUpload(int CKEditorFuncNum, HttpServletRequest request,HttpServletResponse response) throws IOException, FileUploadException {
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		//MultipartFile upload,
		
		JSONObject json  = UploadJSON.uploadFile(request, response);
		
		PrintWriter out = null;
		try {
			out = response.getWriter();
		} catch (IOException e1) {
			e1.printStackTrace();
		}	
		try {			
			
			/*String b=request.getSession().getServletContext().getRealPath("/");
			String basePath =b +"resources/ckeditor/uploadfiles/";
			
			String name=upload.getOriginalFilename();*/
			
			String ext=json.getString("extension");
			String f="";
			if(!"jpg gif bmp png jpeg".contains(ext))
			{
				out.print("<font color=\"red\"size=\"2\">*文件格式不正确（必须为.jpg/.gif/.bmp/.png文件）</font>");
				return ;
			}/*else
			{
				f=outLineService.saveDocToFolder(upload, basePath);	
				f="resources/ckeditor/uploadfiles/"+f.split("resources/ckeditor/uploadfiles/")[1];
			}*/															
			
			/*String p = request.getContextPath();
			String netPic = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+p+"/";
			netPic=netPic+f;*/
									
			out.println("<script type=\"text/javascript\">");
			out.println("window.parent.CKEDITOR.tools.callFunction("+CKEditorFuncNum+",'"+ json.getString("url") + "')"); 
			out.println("</script>");
			
		} catch (Exception e) {
			out.println("<font color=\"red\" size=\"2\">*文件大小不得大于600k</font>");
		}finally
		{
			if(out!=null)
			   out.close();
		}		
	}
	
}
