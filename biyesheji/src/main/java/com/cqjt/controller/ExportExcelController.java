package com.cqjt.controller;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.xssf.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.alibaba.fastjson.JSONObject;
import com.cqjt.pojo.Student;
import com.cqjt.pojo.Teacher;
import com.cqjt.pojo.TeacherInfor;
import com.cqjt.pojo.User;
import com.cqjt.service.ExportExcelService;
import com.cqjt.service.IStudentService;
import com.cqjt.service.ITeacherService;
import com.cqjt.service.IUserService;
import com.cqjt.util.MD5Util;
import com.cqjt.util.ReadExcel;
/**
 * 导入导出的Controller
 * @author LIJIN
 *
 */
@Controller
@RequestMapping(value = "/excel/")
public class ExportExcelController{

	// 注入用户的service
	@Autowired
	private ExportExcelService exportExcelServiceService;
	// 注入用户的service
	@Autowired
	private IUserService userService;
	//注入教师的Service
	@Autowired
	private ITeacherService teacherService;
	//注入用户的Service
	@Autowired
	private IStudentService studentService;
	

  
	/**
	 * 导出教师基本信息
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "exportteacher", method = RequestMethod.GET)
    public String exportTeacher(HttpServletResponse response,HttpServletRequest request) throws IOException  
    {  
        response.setContentType("application/binary;charset=ISO8859_1"); 
        try  
        {  
			SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
			String str = "Teacher_" + df.format(new Date()) + ".xlsx";
			String fileName = new String(str.getBytes(), "ISO8859_1"); 
            ServletOutputStream outputStream = response.getOutputStream();
            response.addHeader("Content-Disposition", "attachment;filename=" + fileName);// 组装附件名称和格式
            String[] titles = { "教师工号", "教师姓名", "教师角色" , "教师职称", "教师性质", "教师性别", "出生日期" };
            Map<String, Object> params =new HashMap<String, Object>();	
            exportExcelServiceService.inExportTeacher(titles, outputStream,params);           
        }  
        catch (IOException e)  
        {  
            e.printStackTrace();  
        }
        return null;
    }
	
	/**
	 * 导出学生基本信息
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "exportstudent", method = RequestMethod.GET)
	public String exportStudent(HttpServletResponse response,HttpServletRequest request) throws IOException  
	{  
		response.setContentType("application/binary;charset=ISO8859_1"); 
		try  
		{  
			SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
			String str = "Student_" + df.format(new Date()) + ".xlsx";
			String fileName = new String(str.getBytes(), "ISO8859_1"); 
			ServletOutputStream outputStream = response.getOutputStream();
			response.addHeader("Content-Disposition", "attachment;filename=" + fileName);// 组装附件名称和格式
			String[] titles = { "学生学号", "学生姓名", "学生角色" , "所属专业", "所属年级", "学生性别", "毕业时间" };
			Map<String, Object> params =new HashMap<String, Object>();	
			exportExcelServiceService.inExportStudent(titles, outputStream,params);           
		}  
		catch (IOException e)  
		{  
			e.printStackTrace();  
		}
		return null;
	}

	

	
	/**
	 * 导入教师
	 * @param request
	 * @param response
	 * @param url
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "importteacher", method = RequestMethod.GET) 
	public void importTeacher(HttpServletRequest request, HttpServletResponse response , String url) throws IOException  
	{  
		response.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		JSONObject obj = readTeacherExcel(request, response, url);
		out.print(obj.toString()); 
	}
	
	/**
	 * 读取上传的teacherexcel数据
	 * @param request
	 * @param response
	 * @param url
	 * @return
	 * @throws IOException
	 */
	public JSONObject readTeacherExcel(HttpServletRequest request,
			HttpServletResponse response, String url) throws IOException {
		File upfile = new File(url);
		List list = ReadExcel.readExcel(upfile);

		int success = 0;// 记录导入成功的教师数
		int countidWrong = 0; // 记录id长度格式错误的教师信息数量
		int countidnull = 0; // 记录id为空的教师信息数量
		int alreadyhave = 0; // 记录已注册的教师信息数量
		int wrongInfo=0;  //信息残缺的数量
		int faile = 0; // 记录导入失败的教师数量
		Boolean b = false; // 判断是否成功
		String message = ""; // 返回的消息

		System.out.println("=======================");
		if (list != null && list.size() > 0) {
			System.out.println("============+++++++++++++++++++===========");
			List firstList = (List) list.get(0);// 表的第一行数据
			System.out.println(" firstList.size()=" + firstList.size());

			/*
			 * //==============表头判断================================== String[]
			 * titles={"用户名","教师姓名"}; JSONObject
			 * checktitles=this.checkTitles(titles, firstList);
			 * if(checktitles.getIntValue("error")==0){ b=true; }else{ b=false;
			 * } message=checktitles.getString("message");
			 * //==============表头判断结束=============================
			 */

			// 当表头正确时
			for (int i = 1; i < list.size(); i++) {
				System.out.println("============??????????????????????===========");
				List l = (List) list.get(i);
				if(l.size()==3){//数据信息完全
					Teacher teacher = new Teacher();
					User user = new User();
					TeacherInfor teacherInfor = new TeacherInfor();
					String id = (String) l.get(0);// 用户名
					String name = (String) l.get(1);// 姓名
					String sex = (String) l.get(2);// 姓名
					// ------USER------
					user.setUser_id(id);
					user.setRole_code(3);
					user.setPassword(MD5Util.MD5("123456"));
	
					// ------teacher-----
					teacher.setUser_id(id);
					teacher.setTeacher_name(name);
	
					// ------teacherInfo------
					teacherInfor.setTc_code(1);//自动默认教师性质为院内教师
					teacherInfor.setTechnical_code(1);//自动默人教师职称为教授
					teacherInfor.setUser_id(id);
					if(sex != null && sex != ""){
						teacherInfor.setTeacher_sex(Integer.parseInt(sex));
					}
	
					if (id != null && id != "") {// 导入的教师id不为空
						if (id.length() == 7) {// 教师id正确
							// 根据id查找该教师是否存在
							User rs = userService.getUserid(user.getUser_id());
							if (rs != null) {// 教师存在
								alreadyhave++;
								faile++;
							} else {// 教师不存在
								teacherService.insert(teacher, teacherInfor, user);
								success++;
								 b=true; 
							}
						} else {// 格式错误
							countidWrong++;
							faile++;
						}
	
					} else {// 导入id为空
						faile++;
						countidnull++;
					}
				}else{
					wrongInfo++;
					faile++;
				}
			}
		}

		message = "成功导入" + success + "位教师" + ".\n导入失败的有" + faile
				+ "位教师,其中已存在的教师有：" + alreadyhave + "位,id为空的：" + countidnull
				+ "位,id格式错误的：" + countidWrong + "位,信息残缺的有："+wrongInfo+"位。";
		JSONObject obj = new JSONObject();
		obj.put("msg", message);
		if (b)
			obj.put("error", 0);// 成功导入
		else
			obj.put("error", 1);
		return obj;
	}
	
	
	/**
	 * 导入学生
	 * @param request
	 * @param response
	 * @param url
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "importstudent", method = RequestMethod.GET) 
	public void importStudent(HttpServletRequest request, HttpServletResponse response , String url) throws IOException  
	{  
		response.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		JSONObject obj = readStudentExcel(request, response, url);
		out.print(obj.toString()); 
	}
	/**
	 * 读取上传的studentexcel数据
	 * @param request
	 * @param response
	 * @param url
	 * @return
	 * @throws IOException
	 */
	public JSONObject readStudentExcel(HttpServletRequest request,
			HttpServletResponse response, String url) throws IOException {
		File upfile = new File(url);
		List list = ReadExcel.readExcel(upfile);
		
		int success = 0;// 记录导入成功的学生数
		int countidWrong = 0; // 记录id长度格式错误的学生信息数量
		int countidnull = 0; // 记录id为空的学生信息数量
		int alreadyhave = 0; // 记录已注册的学生信息数量
		int wrongInfo=0;  //信息残缺的数量
		int faile = 0; // 记录导入失败的学生数量
		Boolean b = false; // 判断是否成功
		String message = ""; // 返回的消息
		
		System.out.println("=======================");
		if (list != null && list.size() > 0) {
			System.out.println("============+++++++++++++++++++===========");
			List firstList = (List) list.get(0);// 表的第一行数据
			System.out.println(" firstList.size()=" + firstList.size());
			
			/*
			 * //==============表头判断================================== String[]
			 * titles={"用户名","学生姓名"}; JSONObject
			 * checktitles=this.checkTitles(titles, firstList);
			 * if(checktitles.getIntValue("error")==0){ b=true; }else{ b=false;
			 * } message=checktitles.getString("message");
			 * //==============表头判断结束=============================
			 */
			SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat date1 = new SimpleDateFormat("yyyy");
			// 当表头正确时
			for (int i = 1; i < list.size(); i++) {
				System.out.println("============??????????????????????===========");
				List l = (List) list.get(i);
				if(l.size()==4){//数据信息完全
					Student student = new Student();
					User user = new User();
					String id = (String) l.get(0);// 用户名
					String name = (String) l.get(1);// 姓名
					String sex = (String) l.get(2);//性别
					String major = (String) l.get(3);//专业
					
					// ------USER------
					user.setUser_id(id);
					user.setRole_code(2);//自动设置角色
					user.setPassword(MD5Util.MD5("123456"));
					
					// ------student-----
					student.setUser_id(id);
					student.setStudent_name(name);
					if(sex!=null){//设置性别
						student.setStudent_sex(Integer.parseInt(sex));
					}else{
						student.setStudent_sex(-1);
					}
					if(major!=null){//设置专业
						student.setMajor_code(Integer.parseInt(major));
					}else{
						student.setMajor_code(-1);
					}
					student.setGrade(Integer.parseInt(date1.format((new Date()))));//设设置年纪
					Calendar curr = Calendar.getInstance();
					curr.set(Calendar.YEAR,curr.get(Calendar.YEAR)+4);//推迟4年
					Date gradute_date=curr.getTime();
					student.setGradute_date(date.format(gradute_date));//设置毕业时间
					
					if (id != null && id != "") {// 导入的学生id不为空
						if (id.length() == 8) {// 学生id正确
							// 根据id查找该学生是否存在
							User rs = userService.getUserid(user.getUser_id());
							if (rs != null) {// 学生存在
								alreadyhave++;
								faile++;
							} else {// 学生不存在
								studentService.insert(student,user);
								success++;
								b=true; 
							}
						} else {// 格式错误
							countidWrong++;
							faile++;
						}
						
					} else {// 导入id为空
						faile++;
						countidnull++;
					}
				}else{
					wrongInfo++;
					faile++;
				}
			}
		}
		
		message = "成功导入" + success + "位学生" + ".\n导入失败的有" + faile
				+ "位学生,其中已存在的学生有：" + alreadyhave + "位,id为空的：" + countidnull
				+ "位,id格式错误的：" + countidWrong + "位,信息残缺的有："+wrongInfo+"位。";
		JSONObject obj = new JSONObject();
		obj.put("msg", message);
		if (b)
			obj.put("error", 0);// 成功导入
		else
			obj.put("error", 1);
		return obj;
	}
	
	/**
	 * 判断list里的数据是否与titles匹配
	 * @param titles
	 * @param list
	 * @return
	 */
	public JSONObject checkTitles(String[] titles,List list){
		JSONObject obj = new JSONObject();
		int error=0; //0表示没有错， 1表示列数不够，2表示title错误
		String message="";
		//开始表头的判断
		if(list!=null && list.size()>0);{
			if(list.size()!=titles.length){//表的列数不为titles列
				message="导入失败，表的列数不正确，请仔细参考导入模版！";
				error=1;
			}else{
				for(int i=0;i<list.size();i++){
					String  celltitle=(String) list.get(i);
					if(!celltitle.contains(titles[i])){
						message="导入失败，表头内容不正确，请仔细参考导入模版！";
						error = 2;
					}else{
						error=0;
					}
					if(error!=0){//导入格式不正确，终止循环
						break;
					}
				}
			}		
		}//结束表头的判断
		obj.put("error", error);
		obj.put("message", message);
		return obj;
	}
}


