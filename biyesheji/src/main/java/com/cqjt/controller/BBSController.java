package com.cqjt.controller;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cqjt.pojo.BBSReply;
import com.cqjt.pojo.BBSSection;
import com.cqjt.pojo.BBSTopic;
import com.cqjt.pojo.User;
import com.cqjt.service.IBBSReplyService;
import com.cqjt.service.IBBSSectionService;
import com.cqjt.service.IBBSTopicService;
import com.cqjt.util.model.Page;

@Controller
@RequestMapping(value = "/bbs/")
public class BBSController extends BaseController{
	@Autowired
	private IBBSSectionService bBSSectionService;
	
	@Autowired
	private IBBSTopicService bBSTopicService;
	
	@Autowired
	private IBBSReplyService bBSReplyService;
	
	@RequestMapping(value = "show-bbs", method = RequestMethod.GET)
	public ModelAndView showBBS(ModelMap model,HttpServletRequest request, HttpServletResponse response ) throws UnsupportedEncodingException {
		
		List<BBSSection> bbsSections = bBSSectionService.getAllBBSSection(null);
		request.setAttribute("bbsSections",bbsSections );
		return new ModelAndView("bbs/show-bbs");
	}
	
	
	@RequestMapping(value = "show-bbssection", method = RequestMethod.GET)
	public ModelAndView showBBSSection(ModelMap model,HttpServletRequest request, HttpServletResponse response ) throws UnsupportedEncodingException {
		List<BBSSection> bbsSections = bBSSectionService.getAllBBSSection(null);
		request.setAttribute("bbsSections",bbsSections );
		return new ModelAndView("bbs/show-bbssection");
	}
	@RequestMapping(value = "add-bbssection", method = {RequestMethod.GET,RequestMethod.POST})
	public ModelAndView addBBSSection(ModelMap model,
			HttpServletRequest request, 
			HttpServletResponse response ,
			@ModelAttribute("bBSSection")BBSSection bBSSection ) throws IOException {
		if(bBSSection.getSname()!=null&&bBSSection.getSmasterid()!=null)
			bBSSectionService.addBBSSection(bBSSection);
		return new ModelAndView("bbs/add-bbssection");
	}
	@RequestMapping(value = "delete-bbssection", method = RequestMethod.GET)
	public ModelAndView deleteBBSSection(ModelMap model,
			HttpServletRequest request, 
			HttpServletResponse response ,
			BBSSection bBSSection) throws UnsupportedEncodingException {
		
		bBSSectionService.deleteBBSSection(bBSSection);
		return new ModelAndView("bbs/delete-bbssection");
	}
	@RequestMapping(value = "delete-bbstopic", method = RequestMethod.GET)
	public ModelAndView deleteBBSTopic(ModelMap model,
			HttpServletRequest request, 
			HttpServletResponse response) throws UnsupportedEncodingException {
		int tid = 0;
		if(request.getParameter("tid")!=null){
			tid =Integer.parseInt(request.getParameter("tid").trim());
		//	request.setAttribute("tid", tid);
		}
		
		
		BBSTopic bbsTopic = bBSTopicService.getBBSTopicByCode(tid);
		System.out.println("bbstopic:"+bbsTopic);
		bBSTopicService.deleteBBSTopic(bbsTopic);	
		return new ModelAndView("redirect:/menu-content-change-bbstopic");
	}
	@RequestMapping(value = "delete-bbsreply", method = RequestMethod.GET)
	public String deleteBBSReply(ModelMap model,
			HttpServletRequest request, 
			HttpServletResponse response ,
			BBSReply bbsReply) throws UnsupportedEncodingException {
		bBSReplyService.deleteBBSReply(bbsReply);
		return "true";//new ModelAndView("bbs/delete-bbssection");
	}
	@RequestMapping(value = "update-bbssection", method = RequestMethod.GET)
	public ModelAndView updateBBSSection(ModelMap model,
			HttpServletRequest request, 
			HttpServletResponse response ,
			BBSSection bBSSection ) throws UnsupportedEncodingException {
		bBSSectionService.updateBBSSection(bBSSection);
		return new ModelAndView("bbs/update-bbssection");
	}
	@RequestMapping(value = "show-bbstopicmanagelist", method = RequestMethod.GET)
	public ModelAndView showBBSTopicManageList(ModelMap model,HttpServletRequest request, HttpServletResponse response ) throws UnsupportedEncodingException {
		
		ModelAndView mv = new ModelAndView();
		HashMap<String, Object> params=new HashMap<String, Object>();
		List<BBSSection> bbsSections = bBSSectionService.getAllBBSSection(null);
		request.setAttribute("bbsSections",bbsSections );
		
		int sid = -1;
		if(request.getParameter("sid")!=null&&Integer.parseInt(request.getParameter("sid").trim())!=-1){
			sid = Integer.parseInt(request.getParameter("sid").trim());
			request.getSession().setAttribute("sid", sid);
		}else {
			mv.setViewName("bbs/bbstopic-manage");
			request.getSession().setAttribute("sid", -1);
			mv.addObject("bbsTopics",bBSTopicService.getAllBBSTopic(null));    
			return mv;
		}
		if(sid==-1)
			sid=(Integer) request.getSession().getAttribute("sid");
		
		params.put("tsid", sid);
		
        //添加查询条件  
        //获取总条数  
        Long totalCount = bBSTopicService.getPageCounts(params);
        //设置分页对象  
        Page page = executePage(request,totalCount);          
        //如排序  
        if(page.isSort()){  
            params.put("orderName",page.getSortName());   
            params.put("descAsc",page.getSortState());  
        }else{  
            //没有进行排序,默认排序方式  
            params.put("orderName","ttime");    
            params.put("descAsc","asc");  
        }  
        //压入查询参数:开始条数与结束条灵敏  
        params.put("startIndex", page.getBeginIndex());  
        params.put("endIndex", page.getEndinIndex());  
		List<BBSTopic> bbsTopics = bBSTopicService.pageList(params);
		mv.addObject("sid", sid);
		mv.addObject("bbsTopics",bbsTopics);    
		mv.setViewName("bbs/bbstopic-manage");
		
		return mv;
	}
	//展示主题列表 author:xiaoshujun 2014-5-14
	@RequestMapping(value = "show-bbstopiclist", method = RequestMethod.GET)
	public ModelAndView showBBSTopic(ModelMap model,HttpServletRequest request, HttpServletResponse response ) throws UnsupportedEncodingException {
		
		ModelAndView mv = new ModelAndView();
		HashMap<String, Object> params=new HashMap<String, Object>();
		int sid = -1;
		if(request.getParameter("sid")!=null){
			sid = Integer.parseInt(request.getParameter("sid").trim());
			request.getSession().setAttribute("sid", sid);
		}
		if(sid==-1)
			sid=(Integer) request.getSession().getAttribute("sid");
		
		params.put("tsid", sid);
		
        //添加查询条件  
        //获取总条数  
        Long totalCount = bBSTopicService.getPageCounts(params);
        //设置分页对象  
        Page page = executePage(request,totalCount);          
        //如排序  
        if(page.isSort()){  
            params.put("orderName",page.getSortName());   
            params.put("descAsc",page.getSortState());  
        }else{  
            //没有进行排序,默认排序方式  
            params.put("orderName","ttime");    
            params.put("descAsc","asc");  
        }  
        //压入查询参数:开始条数与结束条灵敏  
        params.put("startIndex", page.getBeginIndex());  
        params.put("endIndex", page.getEndinIndex());  
		List<BBSTopic> bbsTopics = bBSTopicService.pageList(params);
		mv.addObject("sid", sid);
		mv.addObject("bbsTopics",bbsTopics);    
		mv.setViewName("bbs/show-bbstopic");
		
		return mv;
	}
	@RequestMapping(value = "show-bbstopiccontent", method = RequestMethod.GET)
	public ModelAndView showBBSTopicContent(ModelMap model,HttpServletRequest request, HttpServletResponse response ) throws UnsupportedEncodingException {
		
		ModelAndView mv =  new ModelAndView("bbs/show-bbstopiccontent");
		int tid = -1;
		if(request.getParameter("tid")!=null){
			tid = Integer.parseInt(request.getParameter("tid").trim());
			if(request.getSession().getAttribute("tid")!=null)
				request.getSession().removeAttribute("tid");
			request.getSession().setAttribute("tid", tid);
		}
		else if(request.getParameter("rsid")!=null){
			tid = Integer.parseInt(request.getParameter("rtid").trim());
			if(request.getSession().getAttribute("tid")!=null)
				request.getSession().removeAttribute("tid");
			request.getSession().setAttribute("tid", tid);
		}
		HashMap<String, Object> params=new HashMap<String, Object>();
		params.put("tid", request.getSession().getAttribute("tid"));

	//	BBSTopic bbsTopic = bBSTopicService.getBBSTopicByCode(tid);
		BBSTopic bbsTopic = bBSTopicService.getBBSTopicByCode(Integer.parseInt(request.getSession().getAttribute("tid").toString()));
		/* 主贴点击次数+1 */
		if(request.getParameter("count")!=null){
			bbsTopic.setTclickcount(bbsTopic.getTclickcount()+1);
			bBSTopicService.updateBBSTopic(bbsTopic);
		}
		
		/*获得回复贴列表*/
		HashMap<String, Object> replyparams=new HashMap<String, Object>();
		replyparams.put("rtid", request.getSession().getAttribute("tid"));
		/*保存回复者*/
		User user=(User)request.getSession().getAttribute("loginuser");
		//添加查询条件  
        //获取总条数  
        Long totalCount = bBSReplyService.getPageCounts(replyparams);
        //设置分页对象  
        Page page = executePage(request,totalCount);          
        //压入查询参数:开始条数与结束条灵敏  
        replyparams.put("startIndex", page.getBeginIndex());  
        replyparams.put("endIndex", page.getEndinIndex());  
        List<BBSReply> replies = (List<BBSReply>) bBSReplyService.pageList(replyparams);
        mv.addObject("bbsTopic",bbsTopic);
        mv.addObject("bbsReplies", replies);
        mv.addObject("ruid", user.getUser_id());
		return mv;
	}
	@RequestMapping(value = "add-bbstopic", method = {RequestMethod.GET,RequestMethod.POST})
	public ModelAndView addBBSTopic(ModelMap model,
			HttpServletRequest request, 
			HttpServletResponse response ,
			@ModelAttribute("bBSTopic")BBSTopic bbsTopic ) throws IOException {
		int sid = -1;
		List<BBSSection> bbsSections = bBSSectionService.getAllBBSSection(null);//保存版块列表，用于下拉列表
		request.setAttribute("bbsSections",bbsSections );
		User user=(User)request.getSession().getAttribute("loginuser");
		if(request.getParameter("sid")!=null&&request.getParameter("method")!=null&&request.getParameter("method").equals("load")){
			sid = Integer.parseInt(request.getParameter("sid").trim());
			request.setAttribute("sid", sid);// 保存当前板块标号
			return new ModelAndView("bbs/add-bbstopic");
		}else if(request.getParameter("sid")!=null){
			sid = Integer.parseInt(request.getParameter("sid").trim());
			request.setAttribute("sid", sid);// 保存当前板块标号b
			bbsTopic.setTuid(user.getUser_id());
			bbsTopic.setTtime(new Timestamp(System.currentTimeMillis()));
			bbsTopic.setTlastclickt(new Timestamp(System.currentTimeMillis()));
			bBSTopicService.addBBSTopic(bbsTopic);
		}
		return new ModelAndView("bbs/add-bbstopic");		
	}
	@RequestMapping(value = "add-bbsreply", method = {RequestMethod.GET,RequestMethod.POST})
	public ModelAndView addBBSReply(ModelMap model,
			HttpServletRequest request, 
			HttpServletResponse response ,
			@ModelAttribute("bBSReply")BBSReply bBSReply ) throws IOException {
		bBSReply.setRtime(new Timestamp(System.currentTimeMillis()));
		
		bBSReplyService.addBBSReply(bBSReply);
		return showBBSTopicContent(model,request,response);
	
	}
}
