package com.cqjt.controller;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileUploadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;
import com.cqjt.pojo.Courseware;
import com.cqjt.pojo.Curriculum;
import com.cqjt.pojo.Major;
import com.cqjt.pojo.User;
import com.cqjt.service.ICoursewareService;
import com.cqjt.service.ICurriculumService;
import com.cqjt.util.DocConverter;
import com.cqjt.util.PageCondition;
import com.cqjt.util.PageNameEnum;
import com.cqjt.util.UploadJSON;

/**
 * 课件Controller
 * @author LIJIN
 *
 */
@Controller
@RequestMapping(value = "/courseware/")
public class CoursewareController {
	//注入课程的Service
	@Autowired
	private ICurriculumService curriculumService;
	
	//注入课件的Service
	@Autowired
	private ICoursewareService coursewareService;
	
	//显示相关课程的课件--分页
	@RequestMapping(value = "show", method = RequestMethod.GET)
	public String showCourseware(ModelMap model,HttpServletRequest request, HttpServletResponse response , String curriculum_code ,String curriculum_name) throws UnsupportedEncodingException {
		Map<String, Object> params =new HashMap<String, Object>();	
		curriculum_name=java.net.URLDecoder.decode(curriculum_name,"utf-8");
		params.put("curriculum_code", Long.parseLong(curriculum_code));
		params.put("curriculum_name", curriculum_name);
		//List<Courseware> coursewareList=coursewareService.getAllCourseware(params);//根据专业code找到相关课件
		//设置分页  获得当前页码
		String pageNoStr = request.getParameter("page"); 
		int pageNo = 1;  
		if(pageNoStr != null && !pageNoStr.equals("")){
			pageNo = Integer.parseInt(pageNoStr);  
		}
		PageCondition pagecondition = coursewareService.getAllCourseware(PageNameEnum.PAGESIZE, pageNo,params,request);
		model.addAttribute("curriculum_name", curriculum_name);//保存课程名
		model.addAttribute("curriculum_code", curriculum_code);//保存课程code
		model.addAttribute("pagecondition", pagecondition);//保存查询出来的课件
		return "courseware/show-courseware";
	}
	
	//上传课程的课件
	@RequestMapping(value = "upload", method = RequestMethod.POST)
	public void uploadCourseware(HttpServletRequest request, HttpServletResponse response ,String curriculum_code,String user_id) throws IOException, FileUploadException {
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		//User user = (User)request.getSession().getAttribute("loginuser");
		//System.out.println("=======1=============="+user.getUser_id());
		JSONObject json  = UploadJSON.uploadFile(request, response);
		
		if(json.getString("error").equals("0")){
			Courseware cw = new Courseware();
			cw.setCurriculum_code(Long.parseLong(curriculum_code));//设置课件的课程code
			cw.setFilename(json.getString("fileName"));//设置课件名
			cw.setLocation(json.getString("url"));//设置文件链接（相对路径）
			cw.setOnlinelook(0);//设置在线预览次数
			cw.setDowncount(0);//设置下载次数
			cw.setSourcetype(json.getString("extension").toUpperCase());//资源类型
			java.util.Date date = new java.util.Date();
			cw.setUploaddate(new Date(date.getTime()));//设置上传日期
			cw.setCourseware_version("未知版本");//设置版本
		//	System.out.println("=========2============"+user.getUser_id());
			cw.setUser_id(user_id);//设置上传人
			coursewareService.addCourseware(cw);
		}else{
			System.out.println("shibai");
		}
		//return "redirect:show";
	}
	
	//下载课程的课件
	@RequestMapping(value = "down", method = RequestMethod.POST)
	public void downCourseware(ModelMap model,HttpServletRequest request, HttpServletResponse response ,Courseware courseware) throws IOException, FileUploadException {
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		System.out.println(courseware.getCourseware_code()+"---------->"+courseware.getDowncount());
		Boolean success = coursewareService.updateCourseware(courseware);
		if (success) {
			out.print("success");
		} else {
			out.print("fail");
		}
	}
		
	// 删除课程的课件
	@RequestMapping(value = "delete", method = RequestMethod.GET)
	public void deleteCourseware(HttpServletRequest request,HttpServletResponse response, Courseware courseware)throws IOException, FileUploadException {
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		Boolean success = coursewareService.deleteCourseware(courseware);
		if (success) {
			String wpath = request.getSession().getServletContext().getRealPath("/");//获得服务器路径
			if(courseware.getLocation()!=null){
				File parentFile = new File(wpath);
				File file = new File(parentFile.getParent()+courseware.getLocation());
				String path = courseware.getLocation();
				path = path.substring(0, path.lastIndexOf(".")+1);
				File fileSWF = new File(parentFile.getParent()+path+"swf");
				if(file.exists()){
					file.delete();
				}
				if(fileSWF.exists()){
					fileSWF.delete();
				}
			}
			out.print("success");
		} else {
			out.print("fail");
		}
	}
	
	// 预览课程的课件
	@RequestMapping(value = "onlinelook", method = RequestMethod.GET)
	public String lookCourseware(ModelMap model,HttpServletRequest request,HttpServletResponse response)throws IOException, FileUploadException {
		String courseware_name = request.getParameter("filename");
		courseware_name=new String(courseware_name.getBytes("ISO-8859-1"),"utf-8");
		String url = DocConverter.doconvert(request, response);
		model.addAttribute("courseware_name", courseware_name);//保存课件名
		model.addAttribute("url", url);//保存swf文件路径，用于页面判断是否存在该文件。
		return "courseware/show-cw";
	}
}
