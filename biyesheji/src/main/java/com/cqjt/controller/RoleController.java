package com.cqjt.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileUploadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.cqjt.pojo.Courseware;
import com.cqjt.pojo.Curriculum;
import com.cqjt.pojo.Major;
import com.cqjt.pojo.Role;
import com.cqjt.service.ICoursewareService;
import com.cqjt.service.ICurriculumService;
import com.cqjt.service.IRoleService;
import com.cqjt.util.DocConverter;
import com.cqjt.util.UploadJSON;

/**
 * 课件Controller
 * @author LIJIN
 *
 */
@Controller
@RequestMapping(value = "/role/")
public class RoleController {
	//注入角色的Service
	@Autowired
	private IRoleService roleService;
	

	//添加角色
	@RequestMapping(value ="menu-content-add-role")
	public ModelAndView menu_content_add_role() {
		System.out.println("*************&&&&&&&&&&&&&&&&&&***************");
		return new ModelAndView("menu/menu-content-add-role");
	}
	//角色权限设置
	@RequestMapping(value ="menu-content-role-setting")
	public ModelAndView menu_content_role_setting() {
		return new ModelAndView("menu/menu-content-role-setting");
	}
	
	//添加角色
	@RequestMapping(value = "/addRole", method = RequestMethod.POST)      
	public ModelAndView addRole(@ModelAttribute("role")Role role,HttpServletResponse response) {          
		boolean flag=roleService.addRole(role); 
		PrintWriter out = null;
		try {
			out = response.getWriter();
		} catch (IOException e) {
			e.printStackTrace();
		}
		out.print(flag);
		return null;    
	}
	
}
