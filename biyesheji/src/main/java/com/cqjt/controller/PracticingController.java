package com.cqjt.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.fileupload.FileUploadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.orm.ibatis.SqlMapClientTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.alibaba.fastjson.JSONObject;
import com.cqjt.pojo.AlternativePracticalProject;


import com.cqjt.pojo.CurriculumOutline;
import com.cqjt.pojo.Assignment;
import com.cqjt.pojo.CurriculumOutline;
import com.cqjt.pojo.MenuLevelThree;
import com.cqjt.pojo.MenuLevelTwo;
import com.cqjt.pojo.PracticalProject;
import com.cqjt.pojo.StudentPracticalRecord;
import com.cqjt.pojo.Teacher;
import com.cqjt.pojo.PracticalProjectCode;
import com.cqjt.pojo.PracticalProjectDocument;
import com.cqjt.pojo.Student;
import com.cqjt.pojo.User;
import com.cqjt.service.IAlternativePracticalProjectService;
import com.cqjt.service.IMenuService;
import com.cqjt.service.IOutLineService;
import com.cqjt.service.IPracticalProjectCodeService;
import com.cqjt.service.IPracticalProjectDocumentService;
import com.cqjt.service.IPracticalProjectService;
import com.cqjt.service.IStudentPracticalRecordService;
import com.cqjt.service.IStudentService;
import com.cqjt.service.ITeacherService;
import com.cqjt.util.UploadJSON;
import com.cqjt.util.Hanzi2Pinyin;
import com.cqjt.util.UploadJSON;
import com.cqjt.util.Hanzi2Pinyin;
import com.cqjt.util.WordToHtml;
import com.ibatis.common.util.PaginatedList;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.ibatis.sqlmap.client.SqlMapSession;
import com.ibatis.sqlmap.client.event.RowHandler;
import com.ibatis.sqlmap.engine.execution.BatchException;


/**
 * 实战演练Controller
 * @author Owner
 *
 */

@Controller
@RequestMapping(value = "/practicing")
public class PracticingController {
	
	//注入菜单的Service
	@Autowired
	private IMenuService menuService;
	//注入学生信息的Service
	@Autowired
	private IStudentService studentService;
	//注入学生上传到审题库的Service
	@Autowired
	private IAlternativePracticalProjectService alternativePracticalProjectService;

	//注入实战的Service
	@Autowired
	private IPracticalProjectService IPracticalProjectService;
	

	//注入实战项目编码的Service
	@Autowired
	private IPracticalProjectCodeService practicalProjectCodeService;
	//注入实战项目的Service
	@Autowired
	private IPracticalProjectService practicalProjectService;
	//为了获取在线预览doc或docx文件 保存上传文件
	@Autowired
	private IOutLineService outLineService;
	//为了获取老师打分的信息
	@Autowired
	private IStudentPracticalRecordService studentPracticalRecordService;
	
	//为了获取老师的名字
	@Autowired
	private ITeacherService teacherService;
	
	//为了获取实战的文档
	@Autowired
	private IPracticalProjectDocumentService practicalProjectDocumentService;
	


	//返回实战演练主页面 并返回对应的菜单项
	@RequestMapping(value ="/practicing")
	public ModelAndView practicing(HttpServletRequest request, HttpServletResponse response) 
	{
		int teacher=3;
		int sutdent=2;
		int manager=6;
		User user = (User)request.getSession().getAttribute("loginuser");
		List<MenuLevelTwo>  menuLevelTwoList=null;
		if(user!=null)
		{
		    if(user.getRole_code()==manager)
		    {
		        menuLevelTwoList=menuService.getMenuLevelTwoAndThreeByMenuLevelOneCode(11);//见鸣数据库为11，召辉的为13
	    	}
	    	else if(user.getRole_code()==teacher)
	    	{
	    		menuLevelTwoList=menuService.getMenuLevelTwoAndThreeByMenuLevelOneCode(37);
	    	}
	    	else//剩下的等于学生
	    	{
	    		menuLevelTwoList=menuService.getMenuLevelTwoAndThreeByMenuLevelOneCode(26);//见鸣数据库为11，召辉的为13
	    	}
		}
		return new ModelAndView("practicing/practicing","menuLevelTwoList",menuLevelTwoList);
	}
	
	@RequestMapping(value ="/practicing/showpracticing")
	public ModelAndView practicingMenuClick(ModelMap model,HttpServletRequest request, HttpServletResponse response,String name,String id) throws UnsupportedEncodingException, SQLException {
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		name=java.net.URLDecoder.decode(name,"utf-8");
		model.addAttribute("name", name);
		model.addAttribute("id", id);		
		//审题菜单跳转页面

		if(name.contains("审题"))
		{						
			shenTiMethod(model);
			return new ModelAndView("practicing/review");
		}else if(name.contains("基础/项目实战"))
		{
			getbasicPracticalProject(model,request,response);
			return new ModelAndView("practicing/show-practicing");
		}
		else if(name.contains("评分"))
		{
			pingFenMethod(model,request);
			return new ModelAndView("practicing/score-practicing");
		}
		/*******以下两个为学生实战的控制跳转 开始********/
		else if(name.contains("提交实战答案"))
		{
			tiJiaoShiZhanDaAn(model,request,0);//没有打分的记录
			return new ModelAndView("practicing/student-answer");
		}
		else if(name.contains("查看实战"))
		{
			tiJiaoShiZhanDaAn(model,request,1);//打分的记录
			return new ModelAndView("practicing/student-check-answer");
		}
		/*******学生实战的控制跳转结束********/
		else
		{
			return null;
		}
	}

	//提交实战答案
	private void tiJiaoShiZhanDaAn(ModelMap model,HttpServletRequest request, int isDaFen) throws SQLException {
		StudentPracticalRecord record=new StudentPracticalRecord();
		User user = (User)request.getSession().getAttribute("loginuser");
		record.setUser_id(user.getUser_id());  //登陆者的id号   user.getUser_id()
		record.setIs_audit(isDaFen);//没有打分的
		List<StudentPracticalRecord> l=studentPracticalRecordService.selectByAny(record);
		for(int i=0;i<l.size();i++)
		{
			Teacher t=teacherService.selectByPrimaryKey(l.get(i).getTea_user_id());
			if(t!=null)
			{
				if(t.getTeacher_name()==null)
				{
					t.setTeacher_name("佚名");
				}
				l.get(i).setTea_user_name(t.getTeacher_name());
			}else
			{
				l.get(i).setTea_user_name("佚名");
			}	
			
			PracticalProjectCode cn=practicalProjectCodeService.getPracticalProjectCodeByCode(l.get(i).getPpcode());
			if(cn!=null)
			{
				if(cn.getPptype_name()==null)
				{
					cn.setPptype_name("未知类型");
				}
				l.get(i).setPpcode_name(cn.getPptype_name());
			}else
			{
				l.get(i).setPpcode_name("未知类型");
			}
			
			PracticalProject practicalProject =practicalProjectService.getPracticalProjectByCodeAndNumber(l.get(i).getPpcode(), l.get(i).getPpnumber());
			l.get(i).setPracticalProject(practicalProject);
		}
		model.addAttribute("sList", l);
	}

	/**
	 * 评分的跳转方法
	 * @param model
	 * @param request 
	 * @throws SQLException
	 */
	private void pingFenMethod(ModelMap model, HttpServletRequest request) throws SQLException {
		StudentPracticalRecord record=new StudentPracticalRecord();
		User user = (User)request.getSession().getAttribute("loginuser");
		record.setTea_user_id(user.getUser_id());//登陆者ID  user.getUser_id()
		List<StudentPracticalRecord> sList=studentPracticalRecordService.selectByTeaUserId(record);
		for(int i=0;i<sList.size();i++)
		{
			Student s=studentService.selectByPrimaryKey(sList.get(i).getUser_id());
			if(s!=null)
			{
				if(s.getStudent_name().equals(null))
				{
					s.setStudent_name("佚名");
				}
				//把学生的名称字段放在实战要求字段里面
				sList.get(i).setUser_name(s.getStudent_name());
			}else
			{
				sList.get(i).setUser_name("佚名");
			}
			
			PracticalProjectCode cn=practicalProjectCodeService.getPracticalProjectCodeByCode(sList.get(i).getPpcode());
			if(cn!=null)
			{
				if(cn.getPptype_name()==null)
				{
					cn.setPptype_name("未知类型");
				}
				sList.get(i).setPpcode_name(cn.getPptype_name());
			}else
			{
				sList.get(i).setPpcode_name("未知类型");
			}
			
		}
		model.addAttribute("sList", sList);
	}

	/**
	 * 审题的跳转方法
	 * @param model
	 * @throws SQLException
	 */
	private void shenTiMethod(ModelMap model) throws SQLException {
		List<AlternativePracticalProject> aList=alternativePracticalProjectService.getAllAlternativePracticalProject(null);
		for(int i=0;i<aList.size();i++)
		{
			Student s=studentService.selectByPrimaryKey(aList.get(i).getUser_id());
			if(s!=null)
			{
				if(s.getStudent_name().equals(null))
				{
					s.setStudent_name("佚名");
				}
				//把学生的名称字段放在实战要求字段里面
				aList.get(i).setRequirement(s.getStudent_name());
			}else
			{
				aList.get(i).setRequirement("佚名");
			}
			
		}
		//获取实战项目类型
		List<PracticalProjectCode> pList=practicalProjectCodeService.getAllPracticalProjectCode(null);
		model.addAttribute("aList",aList);
		model.addAttribute("pList",pList);
	}

	//获取基础实战及其条数
	//auther by jianming
	private void getbasicPracticalProject(ModelMap model,HttpServletRequest request, HttpServletResponse response) throws SQLException
	{
		response.setContentType("text/html; charset=UTF-8");
		Map<String, Object> params =new HashMap<String, Object>();
		String page=request.getParameter("page");
		int[] num;
		if(request.getParameter("ppcode")!=null&&!request.getParameter("ppcode").equals("-1"))//如果选择了类型
		{
			System.out.println("类型选择:"+request.getParameter("ppcode"));
			params.put("ppcode", request.getParameter("ppcode"));//设置实战类型
			model.addAttribute("ppcode", request.getParameter("ppcode"));
			num=getpagenumArray(request.getParameter("ppcode"));
		}
		else
		{
			num=getpagenumArray(null);
		}
		if(request.getParameter("page")==null)//如果无页数
		{
			page="1";
		}
		else
		{
			page=dealWithPagenumber(page,num[num.length-1]);//处理后的page
			System.out.println("num[num.length-1]="+num[num.length-1]);
		}
		params.put("page", page);//设置页数
		

		if(request.getParameter("sort_select")!=null)
		{
			//System.out.println("排序方式:"+request.getParameter("sort_select"));
		    params.put("sorttype", request.getParameter("sort_select"));//设置排序方式
		    model.addAttribute("sort_select",request.getParameter("sort_select"));
		}
		if( request.getParameter("semester_select")!=null&&!request.getParameter("semester_select").equals("-1"))
		{
			//System.out.println("学期选择:"+request.getParameter("semester_select"));
	    	params.put("semester_code", request.getParameter("semester_select"));//设置学期选择
	    	model.addAttribute("semester_select",request.getParameter("semester_select"));
		}
		if(request.getParameter("keyword")!=null&&!request.getParameter("keyword").equals("")&&request.getParameter("search_type")!=null)//判断搜索关键词和搜索方式是否为空
		{
			//System.out.println("搜索关键词:"+request.getParameter("keyword"));
		    if(request.getParameter("search_type").equals("project_stringcode"))//设置搜索方式
		    {
		    	params.put("project_stringcode",request.getParameter("keyword"));//设置为实战编码搜索
		    	//System.out.println("搜索方式:"+request.getParameter("search_type"));
		    }
		    else if(request.getParameter("search_type").equals("pp_name"))
		    {
			    params.put("pp_name",request.getParameter("keyword"));//设置为实战题目搜索
			    //System.out.println("搜索方式:"+request.getParameter("search_type"));
		    }
		    model.addAttribute("keyword",request.getParameter("keyword"));
		    model.addAttribute("search_type",request.getParameter("search_type"));
		}
		List<PracticalProject> practicalProjectList=IPracticalProjectService.getPageBasicPracticalProjects(params);
		setUserNameForPracticalProjectList(practicalProjectList);//给practicalProjectList设置用户名
		List<PracticalProjectCode> practicalProjectCodeList=practicalProjectCodeService.getAllPracticalProjectCode(null);//实战类型
		model.addAttribute("practicalProjectList",practicalProjectList);
		model.addAttribute("practicalProjectCodeList",practicalProjectCodeList);
		model.addAttribute("num",num);
		model.addAttribute("page",page);
		
		//获取权限
		int role=((User)request.getSession().getAttribute("loginuser")).getRole_code();
		model.addAttribute("loginuserRole", role);
	}
	//给实战的List（practicalProjectList）获取用户名和教师名(审题人名)
	//author by jianming 
	private void setUserNameForPracticalProjectList(List<PracticalProject> practicalProjectList) throws SQLException
	{
		int student=0;//0表示来源为学生
		int teacher=1;//1表示来源为教师
		for(int i=0;i<practicalProjectList.size();i++)
		{
			if(practicalProjectList.get(i).getSource()==student)
			{
				System.out.println("学生user_id:"+practicalProjectList.get(i).getUser_id());
				System.out.println("教师tea_user_id:"+practicalProjectList.get(i).getTea_user_id());
				String teachername="null";
				String studentname="null";
				Student studentpojo=studentService.selectByPrimaryKey(practicalProjectList.get(i).getUser_id());
				if(studentpojo!=null)
					studentname=studentpojo.getStudent_name();
				practicalProjectList.get(i).setUser_name(studentname);
				Teacher teacherpojo=teacherService.selectByPrimaryKey(practicalProjectList.get(i).getTea_user_id());
				if(teacherpojo!=null)
					teachername=teacherpojo.getTeacher_name();
				practicalProjectList.get(i).setTea_user_name(teachername);
			}
			else if(practicalProjectList.get(i).getSource()==teacher)
			{
				System.out.println("教师tea_user_id:"+practicalProjectList.get(i).getTea_user_id());
				Teacher teacherpojo=teacherService.selectByPrimaryKey(practicalProjectList.get(i).getTea_user_id());
				String teachername="null";
				if(teacherpojo!=null)
					teachername=teacherpojo.getTeacher_name();
				practicalProjectList.get(i).setUser_name(teachername);
				practicalProjectList.get(i).setTea_user_name(teachername);
			}
		}
		
	}
	//显示添加实战的页面
	//author by jianming
	@RequestMapping(value ="/showAddPracticingFrame")
	public ModelAndView showAddPracticingFrame(ModelMap model,HttpServletRequest request, HttpServletResponse response)
	{
		Map<String, Object> params =new HashMap<String, Object>();
		List<PracticalProjectCode> practicalProjectCodeList=practicalProjectCodeService.getAllPracticalProjectCode(params);
		model.addAttribute("practicalProjectCodeList", practicalProjectCodeList);
		
		int role=((User)request.getSession().getAttribute("loginuser")).getRole_code();
		String user_id=((User)request.getSession().getAttribute("loginuser")).getUser_id();
		model.addAttribute("role", role);
		model.addAttribute("user_id", user_id);
		return new ModelAndView("practicing/addPracticing");
	}
	//添加实战的Servlet
	//author by jianming
	/*
	 * @param String pp_name(项目名),String ppcode(项目类型号),String semester_code(学期), String requirement(要求)
	 */
	@RequestMapping(value="/AddPracticing")
	public void addParcticing(ModelMap model,HttpServletRequest request, HttpServletResponse response, String pp_name,String ppcode,String semester_code, String requirement,int role,String user_id) throws IOException, FileUploadException
	{
		pp_name=java.net.URLDecoder.decode(pp_name,"utf-8");
		requirement=java.net.URLDecoder.decode(requirement,"utf-8");
		System.out.println("文件:"+request.getParameter("dir"));
		System.out.println("项目名称:"+pp_name);
		System.out.println("项目类型号:"+ppcode);
		System.out.println("学期:"+semester_code);
		System.out.println("项目要求:"+requirement);
		System.out.println("权限:"+role);
		System.out.println("号:"+user_id);
		int teacher=3;
		int manager=6;
		//int student=2;
		if(role==teacher||role==manager)//教师添加项目
		{
			teacherAddPracticing(model,request,response,pp_name,ppcode,semester_code,requirement,user_id);
		}
		else//学生添加字段
		{
			System.out.println("学生");
			studentAddPracticing(model,request,response,pp_name,ppcode,semester_code,requirement,user_id);
		}
	}
	//教师添加实战项目
	//author by jianming
	private void teacherAddPracticing(ModelMap model,HttpServletRequest request, HttpServletResponse response, String pp_name,String ppcode,String semester_code, String requirement,String user_id) throws IOException, FileUploadException
	{
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		PracticalProject practicalProject=new PracticalProject();
		practicalProject.setPp_name(pp_name);
		practicalProject.setPpcode(Integer.parseInt(ppcode));
		practicalProject.setSemester_code(Integer.parseInt(semester_code));
		practicalProject.setRequirement(requirement);
		practicalProject.setSource(1);//教师为1
		practicalProject.setTea_user_id(user_id);
		practicalProject.setUser_id(user_id);
		if(request.getParameter("dir")==null)//如果无文件
		{
			System.out.println("成功进入！");
			IPracticalProjectService.addPracticalProject(practicalProject);
			out.print("success");
		}
		else//如果有文件
		{
			JSONObject json  = UploadJSON.uploadFile(request, response);
			if(json.getString("error").equals("0"))
			{
				String ppd_name=json.getString("fileName");//设置课件名
				String ppd_link=json.getString("url");//设置文件链接（相对路径）
				IPracticalProjectService.teacherAddPracticingWithDoc(practicalProject,ppd_name,ppd_link);	
			}
			else
			{
				out.println("shibai");
			}
		}
	}
	//学生添加实战
	private void studentAddPracticing(ModelMap model,HttpServletRequest request, HttpServletResponse response, String pp_name,String ppcode,String semester_code, String requirement,String user_id) throws IOException, FileUploadException
	{
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		AlternativePracticalProject alternativePracticalProject=new AlternativePracticalProject();
		alternativePracticalProject.setPp_name(pp_name);
		alternativePracticalProject.setRequirement(requirement);
		alternativePracticalProject.setSemester_code(Integer.parseInt(semester_code));
		alternativePracticalProject.setUser_id(user_id);
		if(request.getParameter("dir")==null)//如果无文件
		{
			alternativePracticalProjectService.addAlternativePracticalProject(alternativePracticalProject);
			out.print("success");
		}
		else
		{
			JSONObject json  = UploadJSON.uploadFile(request, response);
			if(json.getString("error").equals("0"))
			{
				String ppd_name=json.getString("fileName");//设置课件名
				String ppd_link=json.getString("url");//设置文件链接（相对路径）
				alternativePracticalProject.setPpd_name(ppd_name);
				alternativePracticalProject.setPpd_link(ppd_link);
				alternativePracticalProjectService.addAlternativePracticalProject(alternativePracticalProject);
				out.print("success");
			}
			else
			{
				out.print("shibai");
			}
		}
	}
	//根据ppcode和ppnumber获取单个实战的所有信息
	//author by jianming
	@RequestMapping(value ="/basicpracticing/getOnePracticing")
	public ModelAndView getOnePracticing(ModelMap model,HttpServletRequest request, HttpServletResponse response,String ppcode,String ppnumber) throws IOException, SQLException
	{
			Map<String, Object> params =new HashMap<String, Object>();
			params.put("ppcode", ppcode);
			params.put("ppnumber", ppnumber);
			
			PracticalProject practicalProject=IPracticalProjectService.getPracticalProjectByCode(params);//获取项目名称
			model.addAttribute("practicalProject",practicalProject);//传到页面的项目信息
			//params.put("user_id", practicalProject.getTea_user_id());
			Teacher teacher=teacherService.selectByPrimaryKey( practicalProject.getTea_user_id());//获取教师信息（审题人）
			if(teacher==null)
				teacher=new Teacher();
			if(practicalProject.getSource()==0)//判断来源是否为学生，是则获取学生姓名
			{
				Student student=studentService.selectByPrimaryKey(practicalProject.getUser_id());//根据来源的用户名获取学生信息
				if(student==null)
					student=new Student();
				model.addAttribute("student_name",student.getStudent_name());
				System.out.println("学生名:"+student.getStudent_name());
			}
			model.addAttribute("teacher_name",teacher.getTeacher_name());
			System.out.println("项目名称:"+practicalProject.getPp_name());
			System.out.println("教师名:"+teacher.getTeacher_name());

			//PracticalProject practicalProject=IPracticalProjectService.getPracticalProjectByCode(params);
			model.addAttribute("practicalProject",practicalProject);
			return new ModelAndView("practicing/showDetailPracticing");
    }

	//获取基础实战的实战文档
	//author by jianming
	@RequestMapping(value ="/basicpracticing/getDocuments")
	public ModelAndView getPractingDocument(ModelMap model,HttpServletRequest request, HttpServletResponse response,String ppcode,String ppnumber,String pp_name) throws UnsupportedEncodingException
	{
		System.out.println("实战项目类型号:"+request.getParameter("ppcode"));
		System.out.println("实战项目序号:"+request.getParameter("ppnumber"));
		pp_name=java.net.URLDecoder.decode(pp_name,"utf-8");
		Map<String, Object> params =new HashMap<String, Object>();
		params.put("ppcode", ppcode);
		params.put("ppnumber", ppnumber);
		List<PracticalProjectDocument> practicalProjectDocumentList=practicalProjectDocumentService.getAllPracticalProjectDocumentByCodeAndNum(params);
		if(practicalProjectDocumentList!=null&&practicalProjectDocumentList.size()!=0)
		{
			model.addAttribute("practicalProjectDocumentList", practicalProjectDocumentList);
		}
		model.addAttribute("ppcode",ppcode);//把实战的标识传过文档页面
		model.addAttribute("ppnumber",ppnumber);//把实战的标识传过文档页面
		model.addAttribute("pp_name", pp_name);//把实战的名称传过去文档页面
		int role=((User)request.getSession().getAttribute("loginuser")).getRole_code();
		model.addAttribute("loginuserRole", role);
		return new ModelAndView("practicing/show_basicDocuments");
	}
	//基础实战切换页面
	//author by jianming
	@RequestMapping(value ="/basicpracticing/pageChange")
	public ModelAndView changePageBasicpracticing(ModelMap model,HttpServletRequest request, HttpServletResponse response) throws SQLException
	{
		getbasicPracticalProject(model,request,response);
		return new ModelAndView("practicing/show-practicing");
	}
	//基础实战页面的搜索、排序、筛选
	//author by jianming
	@RequestMapping(value ="/basicpracticing/search")
	public ModelAndView searchtypeBasicpracticing(ModelMap model,HttpServletRequest request, HttpServletResponse response) throws SQLException
	{
		getbasicPracticalProject(model,request,response);
		return new ModelAndView("practicing/show-practicing");
	}
	//获取页数的数组
	//author by jianming
	private int[] getpagenumArray(String ppcode)
	{
		Map<String, Object> params =new HashMap<String, Object>();
		params.put("ppcode", ppcode);
		int[] num=IPracticalProjectService.getBasicPracticalProjectsPagenum(params);//获取页数总数的数组
		return num;
	}
	//处理传过来的page值，小于1时改为1，大于最大页数时改为最大页数
	//author by jianming
	private String dealWithPagenumber(String page,int maxpagenum)
	{
		if( Integer.parseInt(page)<1)
			page="1";
		else if(Integer.parseInt(page)>maxpagenum)
			page=Integer.toString(maxpagenum);
		return page;
	}
	
	//处理参加实战
	//author by Jianming
	@RequestMapping(value ="/joinPracticing")
	public void joinPracticing(ModelMap model,HttpServletRequest request, HttpServletResponse response,String ppcode,String ppnumber,String tea_user_id) throws SQLException, IOException
	{
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		StudentPracticalRecord studentPracticalRecord=new StudentPracticalRecord();
		studentPracticalRecord.setPpcode(Integer.parseInt(ppcode));
		studentPracticalRecord.setPpnumber(ppnumber);
		studentPracticalRecord.setTea_user_id(tea_user_id);
		User user = (User)request.getSession().getAttribute("loginuser");
    	studentPracticalRecord.setUser_id(user.getUser_id()); //未正式设定
		if(studentPracticalRecordService.insert(studentPracticalRecord))
		{
			out.print("success");
			System.out.println("添加记录成功!");
		}
		else
		{
			out.print("fail");
		}
	}
	//删除实现项目
	@RequestMapping(value ="/deletePracticing")
	public void deletePracticing(ModelMap model,HttpServletRequest request, HttpServletResponse response,String ppcode,String ppnumber) throws SQLException, IOException
	{
		response.setContentType("text/html; charset=UTF-8");
		String phyPath = request.getSession().getServletContext().getRealPath("/");
		Map<String, Object> params =new HashMap<String, Object>();
		params.put("ppcode", ppcode);
		params.put("ppnumber", ppnumber);
		System.out.println("ppcode:"+ppcode);
		System.out.println("ppnumber:"+ppnumber);
		List<PracticalProjectDocument> practicalProjectDocumentList=practicalProjectDocumentService.getAllPracticalProjectDocumentByCodeAndNum(params);
		System.out.println("大小::"+practicalProjectDocumentList.size());
		for(int i=0;i<practicalProjectDocumentList.size();i++)//删除文档
		{
			String docPath=phyPath+practicalProjectDocumentList.get(i).getPpd_link();
			System.out.println("路径"+docPath);
			Map<String, Object> temp =new HashMap<String, Object>();
			temp.put("ppd_code", practicalProjectDocumentList.get(i).getPpd_code());
			if(practicalProjectDocumentService.deletePracticalProjectDocument(temp))
			{
				practicalProjectDocumentService.deleteRelatedDoc(docPath);
			}
		}
		PracticalProject practicalProject=new PracticalProject();
		practicalProject.setPpcode(Integer.parseInt(ppcode));
		practicalProject.setPpnumber(ppnumber);
		practicalProjectService.deletePracticalProject(practicalProject);
		}
	//通过审核处理函数
	@RequestMapping(value ="/practicing/pass" ,method = RequestMethod.GET)
	public void pass(int number,int code,HttpServletResponse response,HttpServletRequest request) {//参数为实战项目备选表的序号 和实战题目类型号
		
		PrintWriter out=null;
		try {
			out=response.getWriter();
			User user = (User)request.getSession().getAttribute("loginuser");
			if(practicalProjectService.shenTiTransationMethod(number, code, 0, user.getUser_id()))//jianming改动过
			{			 
				out.print(true);			
			}else
			{
				out.print(false);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			if(out!=null)
			   out.close();
		}
		
	}
	
	//删除处理函数
	@RequestMapping(value ="/practicing/delete",method = RequestMethod.GET)
	public void delete(int number,HttpServletResponse response) {//参数为实战项目备选表的序号
		AlternativePracticalProject a=new AlternativePracticalProject();
		a.setEquence_number(number);
		Boolean f3=alternativePracticalProjectService.deleteAlternativePracticalProject(a);
		PrintWriter out=null;
		try {
			out=response.getWriter();
			if(f3)
			{			 
				out.print(true);			
			}else
			{
				out.print(false);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			if(out!=null)
			   out.close();
		}
	}
	
	//在线预览处理函数
	@RequestMapping(value ="/practicing/show",method = RequestMethod.GET)
	public void show(int number,HttpServletRequest request,HttpServletResponse response) throws FileNotFoundException, NullPointerException, TransformerException, IOException, ParserConfigurationException {//参数为实战项目备选表的序号
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		String path = request.getContextPath();
		String basePath = request.getSession().getServletContext().getRealPath("/");
		AlternativePracticalProject a=alternativePracticalProjectService.getAlternativePracticalProjectByEquence_number(number);
		/*String foldername=a.getPpd_link().substring(0,a.getPpd_link().lastIndexOf("/"))+"/";
		String filePath=outLineService.word2HtmlConverter(basePath+a.getPpd_link(),basePath+foldername);
		System.out.println("图片文件夹=="+foldername);
		System.out.println("html文件=="+filePath);*/
		String l=a.getPpd_link();
		l=l.replaceFirst("..", "");
		String docPath=basePath.split("biyesheji")[0]+l.substring(1);
		String filePath=outLineService.word2HtmlConverter(docPath
				         ,l);
		
		String s=WordToHtml.readFile2String(filePath,"GB2312").toString();
		
		PrintWriter out=null;
		try {
			out=response.getWriter();			 
			out.print(s);			
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			if(out!=null)
			   out.close();
		}
	}
	
	//教师打分的处理
	@SuppressWarnings("null")
	@RequestMapping(value ="/practicing/score",method = RequestMethod.POST,produces="text/html;charset=UTF-8")
	public void score(int ppcode,float score, String words,HttpServletResponse response) throws UnsupportedEncodingException{//参数为实战项目备选表的序号
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		//words=java.net.URLDecoder.decode(words,"utf-8");
		StudentPracticalRecord record=new StudentPracticalRecord();
		/*record.setPpcode(ppcode);
		record.setPpnumber(ppnumber);*/
		System.out.println("评分的id---"+ppcode+"--fenshu--"+score);
		record.setRecord_code(ppcode);
		record.setComment(words);
		System.out.println("==================================="+words);
		/*BigDecimal bg=new BigDecimal((double)score);
		score=bg.setScale(2, 4).floatValue();*/
		record.setTeacher_score(score);
		record.setIs_audit(1);
		PrintWriter out=null;
		try {
			out=response.getWriter();
			boolean f3=studentPracticalRecordService.updateByPrimaryKeySelective(record);
			if(f3)
			{			 
				out.print(true);			
			}else
			{
				out.print(false);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			out.print(false);
			e.printStackTrace();
		}finally{
			if(out!=null)
			   out.close();
		}
	}
	
	/**
	 * 接收上传的文件
	 * @param code
	 * @return
	 * @throws FileUploadException 
	 * @throws IOException 
	 */
	@RequestMapping(value ="/uploaddoc",method = RequestMethod.POST)
	public void receiveDoc(int ppcode,HttpServletRequest request,HttpServletResponse response) throws IOException, FileUploadException {
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		//String b=request.getSession().getServletContext().getRealPath("/");
		//File filepath = new File(b);
		//String basePath = filepath.getParent()+"/uploads/shizhan/";
		//D:\Java\MyEclipse\.metadata\.me_tcat\webapps/uploads/testdocx_218618/testdocx.docx
		System.out.println("ppcode---"+ppcode);
		//@RequestParam(value = "file", required = false) MultipartFile file, 
		//String f=outLineService.saveDocToFolder(file, basePath);	
		StudentPracticalRecord record=new StudentPracticalRecord();
		////////////////////////////////////////////////////////////
		JSONObject json  = UploadJSON.uploadFile(request, response);
		record.setRecord_code(ppcode);
		record.setLink(json.getString("url"));
		record.setUpload_date(new Date());
		record.setAnswer_name(json.getString("fileName"));
		////////////////////////////////////////////////////////////
		/*record.setRecord_code(ppcode);
		record.setLink("../uploads"+f.split("uploads")[1]);
		record.setUpload_date(new Date());
		record.setAnswer_name(f.substring(f.lastIndexOf("/")+1));*/
		boolean f1 = false;
		try {
			f1 = studentPracticalRecordService.updateByPrimaryKeySelective(record);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

			PrintWriter out = null;
			try {
				out = response.getWriter();		
				out.print(true);
			} catch (IOException e) {
				e.printStackTrace();
			}finally
			{
				out.close();
			}
	}
	
	//通过审核处理函数
	@RequestMapping(value ="/practicing/zijidafen" ,method = RequestMethod.GET)
	public void zijidafen(int ppcode,String val,HttpServletResponse response) {//参数为实战项目备选表的序号 和实战题目类型号
		StudentPracticalRecord record=new StudentPracticalRecord();
		/*record.setPpcode(ppcode);
		record.setPpnumber(ppnumber);*/
		record.setRecord_code(ppcode);
		record.setSelf_score(val);
		boolean f = false;
		try {
			f = studentPracticalRecordService.updateByPrimaryKeySelective(record);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		PrintWriter out=null;
		try {
			out=response.getWriter();
			if(f)
			{			 
				out.print(true);			
			}else
			{
				out.print(false);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			if(out!=null)
			   out.close();
		}
		
	}
}
