package com.cqjt.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileUploadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;
import com.cqjt.pojo.PracticalProjectDocument;
import com.cqjt.service.IPracticalProjectDocumentService;
import com.cqjt.util.UploadJSON;

/**
 * 实战演练文档的Controller
 * @author Jianming
 *
 */
@Controller
@RequestMapping(value = "/practicingDocument")
public class PracticalProjectDocumentController 
{
	@Autowired
	private IPracticalProjectDocumentService practicalProjectDocumentService;
	//下载文档
	@RequestMapping(value ="/downPracticingDocument")
	public void downPracticingDocument(ModelMap model,HttpServletRequest request, HttpServletResponse response,String ppd_code) throws IOException
	{
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		Map<String, Object> params =new HashMap<String, Object>();
		params.put("ppd_code", ppd_code);
		Boolean success = practicalProjectDocumentService.updatePracticalProjectDocumentDownCount(params);
		if (success) {
			out.print("success");
		} else {
			out.print("fail");
		}
	}
	//上传文档
	@RequestMapping(value ="/uploadPracticingDocument")
	public void uploadPracticingDocument(ModelMap model,HttpServletRequest request, HttpServletResponse response,String ppcode,String ppnumber) throws IOException, FileUploadException
	{
		System.out.println("实战类型号:"+ppcode);
		System.out.println("实战序号:"+ppnumber);
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		JSONObject json  = UploadJSON.uploadFile(request, response);
		if(json.getString("error").equals("0"))
		{
			PracticalProjectDocument practicalProjectDocument=new PracticalProjectDocument();
			practicalProjectDocument.setPpcode(Integer.parseInt(ppcode));
			practicalProjectDocument.setPpnumber(ppnumber);
			practicalProjectDocument.setPpd_name(json.getString("fileName"));
			practicalProjectDocument.setPpd_link(".."+json.getString("url"));
			practicalProjectDocumentService.addPracticalProjectDocument(practicalProjectDocument);
		}
		else{
			out.println("shibai");
			System.out.println("shibai");
		}
	}
	
	//删除文档
	@RequestMapping(value ="/deletePracticingDocument")
	public void deletePracticingDocument(ModelMap model,HttpServletRequest request, HttpServletResponse response,String ppd_code,String ppd_link) throws IOException
	{
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		String phyPath = request.getSession().getServletContext().getRealPath("/");
		String docPath=phyPath+ppd_link;
		System.out.println("文档路径:"+docPath);
		System.out.println("文档ppd_code:"+ppd_code);
		System.out.println("文档ppd_link:"+ppd_link);
		PrintWriter out = response.getWriter();
		Map<String, Object> params =new HashMap<String, Object>();
		params.put("ppd_code", ppd_code);
		if(practicalProjectDocumentService.deletePracticalProjectDocument(params))
		{
			practicalProjectDocumentService.deleteRelatedDoc(docPath);
			out.print("success");
		}
	}
}
