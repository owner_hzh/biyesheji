package com.cqjt.util;

import java.util.Arrays;

public class StringUtil {
	/**
	 * 对字符串字母排序输出
	 * @param answer
	 * @return
	 */
	public static String sort(String answer) {
		String sortStr = answer.trim(); 
		char[] arrayCh = sortStr.toCharArray(); //1，把sortStr转换为字符数组
		Arrays.sort(arrayCh);//2，利用数组帮助类自动排序
		return new String(arrayCh);
	}
}
