package com.cqjt.util;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

public class FileOperation {
	/**
	 * 保存上传的文件
	 * @param MultipartFile file 文件
	 * @param path 路径
	 * @return 保存到服务器之后的文件 File file
	 */
	public static File saveFile(MultipartFile file, String path) {
						
		String fileName = file.getOriginalFilename();  
        
        String fileExt = fileName.substring(  
        		fileName.lastIndexOf(".") + 1).toLowerCase();  

        // new file name  
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd_HHmmss");  
        String newFileName = df.format(new Date()) + "_"  
                + RandomNumber.getRandomNumber() + "." + fileExt;
        
        File targetFile = new File(path, newFileName);  
        if(!targetFile.exists()){  
            targetFile.mkdirs();  
        }  
  
        //保存  
        try {  
            file.transferTo(targetFile);  
        } catch (Exception e) {  
            e.printStackTrace();  
        }
		return targetFile;
	}
	
	/**
	 * 把文件保存到指定路径下 返回文件的全路径以及文件名
	 * @param file 文件
	 * @param path 路径
	 * @param name 
	 * @return  返回文件的全路径以及文件名
	 */
	public static String saveFileReturnPath(MultipartFile file, String path) {
		
		String fileName = Hanzi2Pinyin.getFullSpell(file.getOriginalFilename());  
        
        /*String fileExt = fileName.substring(  
        		fileName.lastIndexOf(".")+1).toLowerCase();  
        // new file name  
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd_HHmmss");  
        String newFileName = df.format(new Date()) + "_"  
                + RandomNumber.getRandomNumber() + "." + fileExt;*/
        
        File targetFile = new File(path, fileName);  
        if(!targetFile.exists()){  
            targetFile.mkdirs();  
        }  
  
        //保存  
        try {  
            file.transferTo(targetFile);  
        } catch (Exception e) {  
            e.printStackTrace();  
        }
		return path+fileName;
	}
	
    /**
     * 删除单个文件
     * @param   sPath    被删除文件的文件名
     * @return 单个文件删除成功返回true，否则返回false
     */
    public static boolean deleteFile(String sPath) {
        boolean flag = false;
        File file = new File(sPath);
        // 路径为文件且不为空则进行删除
        if (file.isFile() && file.exists()) {
            file.delete();
            flag = true;
        }
        return flag;
    }
    
    /**
     * 删除目录（文件夹）以及目录下的文件
     * @param   sPath 被删除目录的文件路径
     * @return  目录删除成功返回true，否则返回false
     */
    public static boolean deleteDirectory(String sPath) {
        //如果sPath不以文件分隔符结尾，自动添加文件分隔符
        if (!sPath.endsWith(File.separator)) {
            sPath = sPath + File.separator;
        }
        File dirFile = new File(sPath);
        //如果dir对应的文件不存在，或者不是一个目录，则退出
        if (!dirFile.exists() || !dirFile.isDirectory()) {
            return false;
        }
        boolean flag = true;
        //删除文件夹下的所有文件(包括子目录)
        File[] files = dirFile.listFiles();
        for (int i = 0; i < files.length; i++) {
            //删除子文件
            if (files[i].isFile()) {
                flag = deleteFile(files[i].getAbsolutePath());
                if (!flag) break;
            } //删除子目录
            else {
                flag = deleteDirectory(files[i].getAbsolutePath());
                if (!flag) break;
            }
        }
        if (!flag) return false;
        //删除当前目录
        if (dirFile.delete()) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     *  根据路径删除指定的目录或文件，无论存在与否
     *@param sPath  要删除的目录或文件
     *@return 删除成功返回 true，否则返回 false。
     */
    public static boolean DeleteFolder(String sPath) {
        boolean flag = false;
        File file = new File(sPath);
        // 判断目录或文件是否存在
        if (!file.exists()) {  // 不存在返回 false
            return flag;
        } else {
            // 判断是否为文件
            if (file.isFile()) {  // 为文件时调用删除文件方法
                return deleteFile(sPath);
            } else {  // 为目录时调用删除目录方法
                return deleteDirectory(sPath);
            }
        }
    }
}
