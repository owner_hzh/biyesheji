package com.cqjt.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.artofsolving.jodconverter.DocumentConverter;
import com.artofsolving.jodconverter.openoffice.connection.OpenOfficeConnection;
import com.artofsolving.jodconverter.openoffice.connection.SocketOpenOfficeConnection;
import com.artofsolving.jodconverter.openoffice.converter.OpenOfficeDocumentConverter;

public class DocConverter {
/*	private File sourceFile;		//转换源文件
	private File pdfFile;			//PDF目标文件
	private File swfFile;			//SWF目标文件
*/	private Runtime r;

	/**
	 * 多线程内部类
	  * 读取转换时cmd进程的标准输出流和错误输出流，这样做是因为如果不读取流，进程将死锁
	  * @author iori
	 */
	private static class DoOutput extends Thread {
	   public InputStream is;
	 
	   //构造方法
	    public DoOutput(InputStream is) {
	       this.is = is;
	   }
	 
	   public void run() {
	       BufferedReader br = new BufferedReader(new InputStreamReader(this.is));
	       String str = null;
	       try {
	           //这里并没有对流的内容进行处理，只是读了一遍
	             while ((str = br.readLine()) != null);
	       } catch (IOException e) {
	           e.printStackTrace();
	       } finally {
	           if (br != null) {
	               try {
	                   br.close();
	               } catch (IOException e) {
	                   e.printStackTrace();
	               }
	           }
	       }
	   }
	}
		
	public static String doconvert(HttpServletRequest req, HttpServletResponse resp) throws ConnectException{
		// 项目部署在服务器上的配置路径
		String wpath = req.getSession().getServletContext().getRealPath("/");   
		File file = new File(wpath);
		String location = req.getParameter("location");
		String savePath = file.getParent()+location;//原文件路径
		String path = savePath.substring(0, savePath.lastIndexOf(".")+1);//文件上级路径
		//System.out.println(savePath+"*******************"+path);
		File sourceFile = new File(savePath);
		File pdfFile = new File(path+"pdf");
		File swfFile = new File(path+"swf");
		Runtime r;
		String url = swfFile.getPath();
		//System.out.println("第一步：生成文件对象，准备转换");
		//long b_time = new Date().getTime();
		if(!swfFile.exists()){
			//转换成pdf文件
			if(sourceFile.exists()) {
				if(!pdfFile.exists()) {
					OpenOfficeConnection connection = new SocketOpenOfficeConnection(8100);
					try {
						connection.connect();
						DocumentConverter converter = new OpenOfficeDocumentConverter(connection);   
						converter.convert(sourceFile, pdfFile);
						pdfFile.createNewFile();
						connection.disconnect();  
						//System.out.println("第二步：转换为PDF格式	路径" + pdfFile.getPath());
					} catch (java.net.ConnectException e) {
						e.printStackTrace();
						System.out.println("OpenOffice服务未启动");
						throw e;
					} catch (com.artofsolving.jodconverter.openoffice.connection.OpenOfficeException e) {
						e.printStackTrace();
						System.out.println("读取文件失败");
						throw e;
					} catch (Exception e){
						e.printStackTrace();
						try {
							throw e;
						} catch (Exception e1) {
							e1.printStackTrace();
						}
					}
				} else {
					System.out.println("已转换为PDF，无需再次转换");
				}
			} else {
				System.out.println("要转换的文件不存在");
				return null;
			} 
			//long e1_time = new Date().getTime();
			//System.out.println("office------>pft耗时："+(b_time-e1_time)+"ms");
			
			
			//转换成swf文件
			//long b1_time = new Date().getTime();
			r = Runtime.getRuntime();
			if(!swfFile.exists()){
				if(pdfFile.exists()) {
					try {
						String cmd= "\"C:/Program Files/SWFTools/pdf2swf.exe\" \""+pdfFile.getPath()+"\" -o \""+swfFile.getPath()+"\" -T 9";
						System.out.println(">>>>>>>>>>>>>>"+cmd);
						Process p = r.exec(cmd);// 调用外部程序
						 //非要读取一遍cmd的输出，要不不会flush生成文件（多线程）
				        new DoOutput(p.getInputStream()).start();
				        new DoOutput(p.getErrorStream()).start();
				        try {
				            //调用waitFor方法，是为了阻塞当前进程，直到cmd执行完
				             p.waitFor();
				        } catch (InterruptedException e) {
				           e.printStackTrace();
				        }
						//p.waitFor();//等待子进程的结束，子进程就是系统调用文件转换这个新进程
						swfFile.createNewFile();
						//System.out.println("第三步：转换为SWF格式	路径：" + swfFile.getPath());
						//System.out.println("第si步：转换为SWF格式：" + swfFile.getName());
						if(pdfFile.exists()) {
							pdfFile.delete();
						}
					} catch (Exception e) {
						e.printStackTrace();
						try {
							throw e;
						} catch (Exception e1) {
							e1.printStackTrace();
						}
					}
				} else {
					System.out.println("PDF文件不存在，无法转换");
				}
			} else {
				System.out.println("已经转为SWF文件，无需再次转换");
			}
			//long e2_time = new Date().getTime();
			//System.out.println("office------>pft耗时："+(b1_time-e2_time)+"ms");
			//System.out.println("*******************"+savePath+swfFile.getName());
			url = swfFile.getPath();
		}else{
			System.out.println("已经转为SWF文件，无需再次转换");
		}
		HttpSession session = req.getSession();
		String fileName = url.substring(url.indexOf("file"));
		fileName = fileName.replaceAll("\\\\", "/");
		session.setAttribute("fileName",fileName);
		return url;
	}
}
