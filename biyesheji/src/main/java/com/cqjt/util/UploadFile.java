package com.cqjt.util;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartFile;

/**
 * 上传控制
 * @author Jianming
 *
 */
public class UploadFile 
{
	private static String rootSavePart="/uploads";
	public static String TEACHINGVIDEOTAPE="/teachingvideo/";
	public static String COURSEWARE="/courseware/";
	public static String OUTLINE="/outline/";
	public static String TEACHINGDATA="/teachingdata/";
	public static String ASSIGNMENT="/assignment/";
	public static String PRACTICALPROJECDOCUMENT="/practicalprojecdocument/";
	/**
	 * 保存上传的文件
	 * @param MultipartFile file 文件
	 * @param path 路径(该类的静态属性)
	 * @return String类型的相对路径
	 */
	public String Upload(MultipartFile file, HttpServletRequest request,HttpServletResponse response,String folder) throws IllegalStateException, IOException, SelectException
	{
		//判断是否穿上对应文件夹，否则抛出自定义异常SelectException()
		isSelectRight(folder);
		
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		String wpath = request.getSession().getServletContext().getRealPath("/");
		String savePath=wpath+rootSavePart+folder;
		String filename=Hanzi2Pinyin.getFullSpell(file.getOriginalFilename());
		//以文件名加六位随机数字创建文件夹
		String foldername=filename.substring(0, filename.lastIndexOf("."))+"_"+RandomNumber.getRandomNumber();
		File realyfile=new File(savePath+foldername,filename);
		realyfile.mkdirs();
		//相对路径
		String relativeWay=rootSavePart+folder+foldername+filename;
		System.out.println("绝对路径"+realyfile.getPath());
		System.out.println("相对路径"+relativeWay);
		file.transferTo(realyfile);
		return relativeWay;
	}
	private void isSelectRight(String folder) throws SelectException
	{
		if(folder!=COURSEWARE&&folder!=TEACHINGVIDEOTAPE&&folder!=COURSEWARE&&folder!=OUTLINE&&folder!=TEACHINGDATA&&folder!=ASSIGNMENT&&folder!=PRACTICALPROJECDOCUMENT)
			throw new SelectException();
	}
	//自定义的异常
    public class SelectException extends Exception 
	{
		public SelectException()
		{
			super("文件夹选择错误（参数String folder应为类中的静态字段）！");
		}
	}
}
