package com.cqjt.util;

import java.security.MessageDigest;
/**
 * MD5加密
 * @author LIJIN
 *e10adc3949ba59abbe56e057f20f883e
 */
public class MD5Util {
	public static String MD5(String str) {
		MessageDigest md5 = null;
		try {
			md5 = MessageDigest.getInstance("MD5");//生成一个 MessageDigest 对象，它实现指定的摘要算法。
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
		char[] charArray = str.toCharArray();//将字符串转化成字符数组
		byte[] byteArray = new byte[charArray.length];//申明一个和字符数组同样的长度的byte数组
		for (int i = 0; i < charArray.length; i++) {
			byteArray[i] = (byte) charArray[i];//将字符数组复制给byte数组
		}
		byte[] md5Bytes = md5.digest(byteArray);//使用指定的字节数组执行对摘要最后的修改，然后完成摘要计算。 即，这个方法首先对数组调用 update，然后调用 digest()。 
		StringBuffer hexValue = new StringBuffer();
		for (int i = 0; i < md5Bytes.length; i++) {
			int val = ((int) md5Bytes[i]) & 0xff;
			if (val < 16) {
				hexValue.append("0");
			}
			hexValue.append(Integer.toHexString(val));
		}
		return hexValue.toString();
	}
}
