package com.cqjt.util;

import java.io.IOException;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

public class PageCondition extends TagSupport {
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int pageSize = 10;
	 private int pageNo = 1;
	 private int totalCount; // 总记录数
	 private String url; // 目的地URL  
	 private List<?> dataList;
	 private String show;
	 
	 
	 public String getShow() {
		return show;
	}

	public void setShow(String show) {
		this.show = show;
	}

	public PageCondition() {
		 
	 }
	 
	/**
	 * 
	 * @param pageSize int每页数据大小
	 * @param pageNo int 第几页
	 * @param totalCount int 总记录数
	 */
	 public PageCondition(int pageSize, int pageNo,int totalCount) {
		 this.pageSize = pageSize;
		 this.pageNo = pageNo;
		 this.totalCount = totalCount;
	 }
	 
	 public int getPageSize() {
		return pageSize;
	}

	public int getPageNo() {
		return pageNo;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public String getUrl() {
		return url;
	}

	public int getStartNo() {
		 return (pageNo-1) * pageSize;
	 }
	 
	 
	 
	public List<?> getDataList() {
		return dataList;
	}
	public void setDataList(List<?> dataList) {
		this.dataList = dataList;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	public void settotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	
	public int doStartTag() throws JspException {
		
		if(totalCount == 0){
			return super.doStartTag();
		}            
		        // 总页数  
		        int pageCount = (totalCount + pageSize - 1) / pageSize;   
		        // 页号越界处理         
		        if (pageNo > pageCount) { 
		        	pageNo = pageCount;
		        }  
		        if (pageNo < 1) { 
		        	pageNo = 1;  
		        }            
		        StringBuilder sb = new StringBuilder();            
		        sb.append("<form name='pageController' id='pageController' action='' method='get'>\r\n") .append("<input type='hidden' id='pageNo' name='pageNo' value='" + pageNo + "' />\r\n");            
		        //------------------------------------ 获取所有請求中的参数  
		        HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();  
		        Enumeration<String> enumeration = request.getParameterNames();
		        String name = null;
		        String value = null;  
		        //把请求中的所有参数当作隐藏表单域在页面中写出)
		        while (enumeration.hasMoreElements()) {
		        	name =  enumeration.nextElement();
		        	value = request.getParameter(name);   
		            // 去除页号  
		            if (name.equals("pageNo")) {
		                if (null != value && !"".equals(value)) {
		                	pageNo = Integer.parseInt(value);
		              }  
		                continue; 
		             }
		            sb.append("<input type='hidden' name='") .append(name).append("' value='").append(value).append("'/>\r\n");
		           }  
		        //----------------------------------------------------          
		        sb.append("当前第&nbsp;" + pageNo + "&nbsp;页 / ").append(" 总共&nbsp;" + pageCount + "&nbsp;页, ").append(totalCount+"&nbsp;条数据&nbsp;&nbsp;&nbsp;&nbsp;");         
		    //    sb.append(" 总共有" + pageCount + "页,") .append("当前是第" + pageNo + "页 \r\n");            
		        if (pageNo == 1) {              
		        	sb.append("&nbsp;首页&nbsp;"); 
		        	sb.append(" ");  
		            sb.append("&nbsp;上一页&nbsp;\r\n");  
		        } else {  
		            sb.append("<a href='#' onclick='turnOverPage(1)'>&nbsp;首页&nbsp;</a>\r\n"); 
		            sb.append(" ");  
		            sb.append("<a href='#' onclick='turnOverPage(") .append((pageNo - 1)).append(")'>&nbsp;上一页&nbsp;</a>\r\n");
		         }   
		        sb.append(" ");  
		        if (pageNo == pageCount) {
		        	sb.append("&nbsp;下一页&nbsp;");
		        	sb.append(" ");  
		            sb.append("&nbsp;尾页&nbsp;&nbsp;\r\n");   
		        } else {  
		            sb.append("<a href='#' onclick='turnOverPage(") .append((pageNo + 1)).append(")'>&nbsp;下一页&nbsp;</a>\r\n"); 
		            sb.append(" ");  
		            sb.append("<a href='#' onclick='turnOverPage(").append(pageCount).append(")'>&nbsp;尾页&nbsp;&nbsp;</a>\r\n");
		         }  
		          
		        sb.append(" 跳到<select onChange='turnOverPage(this.value)'>\r\n");
		        for (int i = 1; i <= pageCount; i++) {
		        	if (i == pageNo) {  
		                sb.append("  <option value='").append(i).append("' selected='selected'>第") .append(i) .append("页</option>\r\n"); 
		              } else {  
		                sb.append("  <option value='").append(i) .append("'>第") .append(i).append("页</option>\r\n");
		              } 
		        }  
		        sb.append("</select>\r\n");
		        sb.append(" \r\n");
		        sb.append("</form>\r\n");            
		        // 生成提交表单的JS  
		        sb.append("<script language='javascript'>\r\n"); 
		        sb.append("  //翻页函数\t\n");  
		        sb.append("  function turnOverPage(no){\r\n");  
		        sb.append("    var form = document.pageController;\r\n"); 
		        sb.append("    //页号越界处理\r\n");  
		        sb.append("    if(no").append(">").append(pageCount).append(") {\r\n");  
		        sb.append("        no=").append(pageCount).append(";\r\n");
		        sb.append("    }\r\n");  
		        sb.append("    if(no").append("< 1){\r\n"); 
		        sb.append("        no=1;\r\n");  
		        sb.append("    }\r\n");  
		        sb.append("    form.").append("pageNo").append(".value=no;\r\n");
		        sb.append("    form.action='").append(url).append("';\r\n"); 
		        sb.append("    form.submit();\r\n"); 
		        sb.append("  }\r\n"); 
		        sb.append("</script>\r\n");            
		        try {  
		        	//System.out.println(sb.toString());
		            pageContext.getOut().println(sb.toString()); 
		            } catch (IOException e) { 
		            	e.printStackTrace();
		            }  
		        return super.doStartTag();     
		} 
	
}
	


