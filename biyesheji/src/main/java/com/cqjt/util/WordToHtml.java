package com.cqjt.util;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.usermodel.Picture;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.poi.hwpf.converter.PicturesManager;
import org.apache.poi.hwpf.converter.WordToHtmlConverter;
import org.apache.poi.hwpf.usermodel.PictureType;
import org.apache.poi.poifs.filesystem.DirectoryEntry;
import org.apache.poi.poifs.filesystem.DocumentEntry;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xwpf.converter.core.FileImageExtractor;
import org.apache.poi.xwpf.converter.core.FileURIResolver;
import org.apache.poi.xwpf.converter.core.IURIResolver;
import org.apache.poi.xwpf.converter.core.XWPFConverterException;
import org.apache.poi.xwpf.converter.xhtml.XHTMLConverter;
import org.apache.poi.xwpf.converter.xhtml.XHTMLOptions;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.w3c.dom.Document;

/**
 * @author: owner
 * @since: 2014 04 01
 * @modified: 2014 04 01
 * @version:1.0
 */
public class WordToHtml {

	public static void main(String argv[]) {
		/*try {
			convert2Html("D:\\hh.doc","D:\\hh.html","D:\\test\\");
		} catch (Exception e) {
			e.printStackTrace();
		}*/
		
		//String docPath="resources/doc/test.doc";
		/*docPath=docPath.substring(0, docPath.lastIndexOf("/")+1);
		System.out.println("docPath=="+docPath);*/
		//String filename=docPath.substring(0,docPath.lastIndexOf("/")+1);
		//filename=filename.substring(0, filename.lastIndexOf("."));
		//System.out.println("filename=="+filename);
		//System.out.println("file====>"+File.separator);
		/*try {
			html2Word("D:\\bb.html","D:\\bb.doc");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		/*String str = "<div><title> ..>dsijiswer*dfhjgf</h3></div><table><h3>sdsd</title></table>";
	    Pattern p = Pattern.compile("<title.*?/title>");
	    Matcher m = p.matcher(str);
	    while (m.find()) {
	        System.out.println(m.group());
	    }*/
		try {
			int i=5/1;
			System.out.println("try");
		} catch (Exception e) {
			System.out.println("catch");
		}
		System.out.println("out");
		
	}
	//private static List<String> pics=new ArrayList<String>();
	
	/**
	 * html文件转为word文件
	 * @param htmlfile 输入的html文件物理全路径包括名称
	 * @param wordfile 输出的word文件全路径包括名称
	 * @throws IOException
	 */
	public static void html2Word(String htmlfile,String wordfile) throws IOException{

		XWPFDocument doc = new XWPFDocument();
		//此处为html源码来源可为文件也可从页面获取
		StringBuffer info = readFile2String(htmlfile,"GB2312");

		//生成文件名
		//String filePath = "QrySaleMainTestFileMS" + ".doc";
		File file=new File(wordfile);
		if(file.exists())
		{
			file.delete();
		}
		//指定文件存放路径
		OutputStream out = new FileOutputStream(wordfile);


		byte b[] = info.toString().getBytes();
		ByteArrayInputStream bais = new ByteArrayInputStream(b);
		POIFSFileSystem poifs = new POIFSFileSystem();
		DirectoryEntry directory = poifs.getRoot();
		DocumentEntry documentEntry = directory.createDocument("WordDocument",
				bais);
		poifs.writeFilesystem(out);
		bais.close();
		doc.write(out);
		out.close();		 
	}
    
	/**
	 * 把指定文件读取出来 为String
	 * @param htmlfile  指定的文件
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static StringBuffer readFile2String(String htmlfile,String charset)
			throws UnsupportedEncodingException, FileNotFoundException,
			IOException {
		InputStreamReader sr = new InputStreamReader(new FileInputStream(htmlfile),charset);

		String str;
		StringBuffer info=new StringBuffer();
		BufferedReader reader = new BufferedReader(sr);
		while((str=reader.readLine())!=null){
			info.append(str);
		}
		return info;
	}

	/**
	 * 把内容写到文件里面
	 * @param content 内容
	 * @param charset 字符编码
	 * @param path    路径全名
	 */
	public static void writeFile(String content, String path,String charset) {
		PrintWriter bw = null;
		try {
			File file = new File(path);		
			if(!file.exists())
			{
				file.createNewFile();
			}
			bw = new PrintWriter(file, charset);
			bw.write(content);
			bw.flush();			
		} catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} finally {
			if (bw != null)
				bw.close();
		}
	}

	/**
	 * 把.doc文档转换成html文件
	 * @param fileName     文档物理路径
	 * @param outPutFile   html文档物理路径
	 * @param picPath      图片保存物理路径
	 * @param netPicPath   图片显示网络路径
	 * @throws TransformerException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	public static void convert2Html(String fileName, String outPutFile,final String picPath,final String netPicPath)
			throws TransformerException, IOException,
			ParserConfigurationException ,FileNotFoundException,NullPointerException{	
		//System.out.println(picPath);
		File f=new File(picPath);
		if(!f.exists())
		{
			f.mkdirs();
		}
		if(fileName.endsWith(".doc"))
		{
			HWPFDocument wordDocument = new HWPFDocument(new FileInputStream(fileName));//WordToHtmlUtils.loadDoc(new FileInputStream(inputFile));
			WordToHtmlConverter wordToHtmlConverter = new WordToHtmlConverter(
					DocumentBuilderFactory.newInstance().newDocumentBuilder()
							.newDocument());
			 wordToHtmlConverter.setPicturesManager( new PicturesManager()
	         {
	             public String savePicture( byte[] content,
	                     PictureType pictureType, String suggestedName,
	                     float widthInches, float heightInches )
	             {
	                 return netPicPath+suggestedName;
	             }
	         } );
			wordToHtmlConverter.processDocument(wordDocument);
			//save pictures
			List<?> pics=wordDocument.getPicturesTable().getAllPictures();
			if(pics!=null){
				for(int i=0;i<pics.size();i++){
					Picture pic = (Picture)pics.get(i);
					pic.writeImageContent(new FileOutputStream(picPath
								+ pic.suggestFullFileName()));
				}
			}
			Document htmlDocument = wordToHtmlConverter.getDocument();
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			DOMSource domSource = new DOMSource(htmlDocument);
			StreamResult streamResult = new StreamResult(out);

			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer serializer = tf.newTransformer();
			serializer.setOutputProperty(OutputKeys.ENCODING, "GB2312");
			serializer.setOutputProperty(OutputKeys.INDENT, "yes");
			serializer.setOutputProperty(OutputKeys.METHOD, "html");
			serializer.transform(domSource, streamResult);
			out.close();
			writeFile(new String(out.toByteArray()), outPutFile,"GB2312");
		}else if(fileName.endsWith(".docx"))
		{
			// 1) Load DOCX into XWPFDocument
			InputStream in = new FileInputStream(new File(fileName));
			XWPFDocument document = new XWPFDocument(in);

			// 2) Prepare XHTML options (here we set the IURIResolver to
			// load images from a "word/media" folder)
			File imageFolderFile = new File(picPath);
			XHTMLOptions options = XHTMLOptions.create();
			options.setExtractor(new FileImageExtractor(imageFolderFile));
			//options.URIResolver(new FileURIResolver(imageFolderFile));
			options.URIResolver(new IURIResolver() {
				
				@Override
				public String resolve(String uri) {
                    //页面引用图片的路径返回值
					return netPicPath+uri;
				}
			} );
			// 3) Convert XWPFDocument to XHTML
			OutputStream out = new FileOutputStream(new File(outPutFile));
			XHTMLConverter.getInstance().convert(document, out, options);
		}
		
	}
	
	//将byte数组写入文件  
    public static void writeByteToFile(String file, byte[] content){  
        FileOutputStream fos=null;
		try {
			fos = new FileOutputStream(file);
			fos.write(content);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			try {
				if(fos!=null)
				  fos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
          
          
    } 
}


/*public class WordExcelToHtml {

	*//**
	 * 回车符ASCII码
	 *//*
	private static final short ENTER_ASCII = 13;

	*//**
	 * 空格符ASCII码
	 *//*
	private static final short SPACE_ASCII = 32;

	*//**
	 * 水平制表符ASCII码
	 *//*
	private static final short TABULATION_ASCII = 9;

	public static String htmlText = "";
	public static String htmlTextTbl = "";
	public static int counter=0;
	public static int beginPosi=0;
	public static int endPosi=0;
	public static int beginArray[];
	public static int endArray[];
	public static String htmlTextArray[];
	public static boolean tblExist=false;
	
	public static final String inputFile="D://bb.doc";
	public static void main(String argv[])
	{		
		try {
			getWordAndStyle(inputFile);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	*//**
	 * 读取每个文字样式
	 * 
	 * @param fileName
	 * @throws Exception
	 *//*

	
	public static void getWordAndStyle(String fileName) throws Exception {
		FileInputStream in = new FileInputStream(new File(fileName));
		HWPFDocument doc = new HWPFDocument(in);
		
	   	 Range rangetbl = doc.getRange();//得到文档的读取范围   
		 TableIterator it = new TableIterator(rangetbl); 
		 int num=100;		 
		
		 
		 beginArray=new int[num];
		 endArray=new int[num];
		 htmlTextArray=new String[num];
		 
		 
		 
		 
		 

		// 取得文档中字符的总数
		int length = doc.characterLength();
		// 创建图片容器
		PicturesTable pTable = doc.getPicturesTable();
        
		htmlText = "<html><head><title>" + doc.getSummaryInformation().getTitle() + "</title></head><body>";
		// 创建临时字符串,好加以判断一串字符是否存在相同格式
		
		 if(it.hasNext())
		 {
			 readTable(it,rangetbl);
		 }
		 
		 int cur=0;
		 	
		String tempString = "";
		for (int i = 0; i < length - 1; i++) {
			// 整篇文章的字符通过一个个字符的来判断,range为得到文档的范围
			Range range = new Range(i, i + 1, doc);
			
			
			
			CharacterRun cr = range.getCharacterRun(0); 
			//beginArray=new int[num];
			 //endArray=new int[num];
			 //htmlTextArray=new String[num];
			if(tblExist)
			{
				if(i==beginArray[cur])
				{		 
					htmlText+=tempString+htmlTextArray[cur];
					tempString="";
					i=endArray[cur]-1;
					cur++;
					continue;
				}
			}
			if (pTable.hasPicture(cr)) {
				htmlText +=  tempString ;				
				// 读写图片				
				readPicture(pTable, cr);
				tempString = "";				
			} 
			else {
				 		
				Range range2 = new Range(i + 1, i + 2, doc);
				// 第二个字符
				CharacterRun cr2 = range2.getCharacterRun(0);
				char c = cr.text().charAt(0);
				
				System.out.println(i+"::"+range.getEndOffset()+"::"+range.getStartOffset()+"::"+c);
				
				// 判断是否为回车符
				if (c == ENTER_ASCII)
					{
					tempString += "<br/>";
					
					}
				// 判断是否为空格符
				else if (c == SPACE_ASCII)
					tempString += "&nbsp;";
				// 判断是否为水平制表符
				else if (c == TABULATION_ASCII)
					tempString += " &nbsp;&nbsp;&nbsp;";
				// 比较前后2个字符是否具有相同的格式
				boolean flag = compareCharStyle(cr, cr2);
				if (flag)
					tempString += cr.text();
				else {
					String fontStyle = "<span style='font-family:'" + cr.getFontName() + ";font-size:" + cr.getFontSize() / 2 + ";color: "+ColorUtils.getHexColor(cr.getIco24()) + ";"+"pt;";
									
					if (cr.isBold())
						fontStyle += "font-weight:bold;";
					if (cr.isItalic())
						fontStyle += "font-style:italic;";
					
					htmlText += fontStyle + "'mce_style='font-family:" + cr.getFontName() + ";font-size:" + cr.getFontSize() / 2 + "pt;";
									
					if (cr.isBold())
						fontStyle += "font-weight:bold;";
					if (cr.isItalic())
						fontStyle += "font-style:italic;";
					
					htmlText += fontStyle + ">" + tempString + cr.text() + "</span>";
					tempString = "";
				}
			}
		}

		htmlText += tempString+"</body></html>";
		writeFile(htmlText);
	}
	
	*//**
	 * 读写文档中的表格
	 * 
	 * @param pTable
	 * @param cr
	 * @throws Exception
	 *//*
	public static void readTable(TableIterator it, Range rangetbl) throws Exception {

		htmlTextTbl="";
   	 	//迭代文档中的表格  
		
        counter=-1;
        while (it.hasNext()) 
        { 
        	tblExist=true;
	         htmlTextTbl="";
	       	 Table tb = (Table) it.next();    
	       	 beginPosi=tb.getStartOffset() ;
	       	 endPosi=tb.getEndOffset();
	       	 
	       	 System.out.println("............"+beginPosi+"...."+endPosi);
	       	 counter=counter+1;
	       	 //迭代行，默认从0开始
	       	 beginArray[counter]=beginPosi;
	       	 endArray[counter]=endPosi;
	       	 
	       	 htmlTextTbl+="<table border>";
	   		 for (int i = 0; i < tb.numRows(); i++) {      
			 TableRow tr = tb.getRow(i);   
			 
			 htmlTextTbl+="<tr>";
			 //迭代列，默认从0开始   
			 for (int j = 0; j < tr.numCells(); j++) {      
				 TableCell td = tr.getCell(j);//取得单元格
				 int cellWidth=td.getWidth();
				 
				 //取得单元格的内容   
				 for(int k=0;k<td.numParagraphs();k++){      
			             Paragraph para =td.getParagraph(k);      
			             String s = para.text().toString().trim();   
			             if(s=="")
			             {
			            	 s=" ";
			             }
			             System.out.println(s);   
			             htmlTextTbl += "<td width="+cellWidth+ ">"+s+"</td>";
			             System.out.println(i+":"+j+":"+cellWidth+":"+s);
			        } //end for       
			     }   //end for   
			  }   //end for   
	   		htmlTextTbl+="</table>" ;    
	   		htmlTextArray[counter]=htmlTextTbl;
  
        } //end while 
	}	
	
	*//**
	 * 读写文档中的图片
	 * 
	 * @param pTable
	 * @param cr
	 * @throws Exception
	 *//*
	public static void readPicture(PicturesTable pTable, CharacterRun cr) throws Exception {
		// 提取图片
		Picture pic = pTable.extractPicture(cr, false);
		// 返回POI建议的图片文件名
		String afileName = pic.suggestFullFileName();
		OutputStream out = new FileOutputStream(new File("D://test" + File.separator + afileName));
		pic.writeImageContent(out);
		htmlText += "<img src="+"'D://test//'" + afileName + " mce_src='D://test//'" + afileName +"/>";
	}

	public static boolean compareCharStyle(CharacterRun cr1, CharacterRun cr2) 
	{
		boolean flag = false;
		if (cr1.isBold() == cr2.isBold() && cr1.isItalic() == cr2.isItalic() && cr1.getFontName().equals(cr2.getFontName()) && cr1.getFontSize() == cr2.getFontSize() && cr1.getColor() == cr2.getColor()) 
		{
			flag = true;
		}
		return flag;
	}
	

	*//**
	 * 写文件
	 * 
	 * @param s
	 *//*
	public static void writeFile(String s) {
		FileOutputStream fos = null;
		PrintWriter bw = null;
		try {
			File file = new File("D://abc.html");
			fos = new FileOutputStream(file);
			bw = new BufferedWriter(new OutputStreamWriter(fos));
			bw.write(s);
			
			bw = new PrintWriter(file, "GB2312");
			bw.write(s);
			bw.flush();
			bw.close();
			
		} catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} finally {
			try {
				if (bw != null)
					bw.close();
				if (fos != null)
					fos.close();
			} catch (IOException ie) {
			}
		}
	}


}*/

