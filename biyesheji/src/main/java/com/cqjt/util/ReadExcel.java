package com.cqjt.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.xssf.usermodel.*;

public class ReadExcel {
	
	// 对外提供读取excel文件的接口
	public static List<List<Object>> readExcel(File file) throws IOException {
		String fName = file.getName();
		String extension = fName.lastIndexOf(".") == -1 ? "" : fName.substring(fName.lastIndexOf(".") + 1);
		if ("xls".equals(extension)) {// 2003
		System.err.println("读取excel2003文件内容");
		return read2003Excel(file);
		} else if ("xlsx".equals(extension)) {// 2007
		System.err.println("读取excel2007文件内容");
		return read2007Excel(file);
		} else {
		throw new IOException("不支持的文件类型:" + extension);
		}
	}

	/**
	* 读取2003excel
	*
	* @param file
	* @return
	*/
	private static List<List<Object>> read2003Excel(File file)throws IOException {
		List<List<Object>> dataList = new ArrayList<List<Object>>();
		HSSFWorkbook wb = new HSSFWorkbook(new FileInputStream(file));
		HSSFSheet sheet = wb.getSheetAt(0);
		HSSFRow row = null;
		HSSFCell cell = null;
		Object val = null;
		DecimalFormat df = new DecimalFormat("0.00");// 格式化数字
		DecimalFormat dfid = new DecimalFormat("0");// 格式化数字
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 格式化日期字符串
		for (int i = sheet.getFirstRowNum(); i < sheet.getPhysicalNumberOfRows(); i++) {
			row = sheet.getRow(i);
			if (row == null) {
				continue;
			}
			List<Object> objList = new ArrayList<Object>();
			for (int j = row.getFirstCellNum(); j < row.getLastCellNum(); j++) {
				cell = row.getCell(j);
				if (cell == null) {
					val = null;
					objList.add(val);
					continue;
				}
				String strCell = "";      
		        switch (cell.getCellType()) {      
		        case HSSFCell.CELL_TYPE_STRING:      
		            strCell = cell.getStringCellValue();      
		            break;      
		        case HSSFCell.CELL_TYPE_NUMERIC:      
		            strCell = String.valueOf(cell.getNumericCellValue());      
		            break;      
		        case HSSFCell.CELL_TYPE_BOOLEAN:      
		            strCell = String.valueOf(cell.getBooleanCellValue());      
		            break;      
		        case HSSFCell.CELL_TYPE_BLANK:      
		            strCell = "";      
		            break;      
		        default:      
		            strCell = "";      
		            break;      
		        }    
				objList.add(strCell);
			}
			dataList.add(objList);
		}
		return dataList;
	}

	/**
	* 读取excel表头
	*
	* @param file
	* @return
	* @throws IOException
	*/
	public static String[] readExcelHead(File file) throws IOException {
		HSSFWorkbook wb = new HSSFWorkbook(new FileInputStream(file));
		HSSFSheet sheet = wb.getSheetAt(0);
		HSSFRow row = null;
		HSSFCell cell = null;
		row = sheet.getRow(0);
		String[] buff = new String[row.getLastCellNum()];
		for (int i = row.getFirstCellNum(); i < row.getLastCellNum(); i++) {
			cell = row.getCell(i);
			buff[i] = cell.getStringCellValue();
		}
		return buff;
	}

	/**
	* 读取2007excel
	*
	* @param file
	* @return
	*/

	private static List<List<Object>> read2007Excel(File file)throws IOException {
		List<List<Object>> dataList = new ArrayList<List<Object>>();
		XSSFWorkbook xwb = new XSSFWorkbook(new FileInputStream(file));
		XSSFSheet sheet = xwb.getSheetAt(0);
		XSSFRow row = null;
		XSSFCell cell = null;
		Object val = null;
		DecimalFormat df = new DecimalFormat("0");// 格式化数字
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 格式化日期字符串
		for (int i = sheet.getFirstRowNum(); i < sheet.getPhysicalNumberOfRows(); i++) {
			row = sheet.getRow(i);
			if (row == null) {
				continue;
			}
			List<Object> objList = new ArrayList<Object>();
			for (int j = row.getFirstCellNum(); j < row.getLastCellNum(); j++) {
				cell = row.getCell(j);
				if (cell == null || cell.equals("") || cell.equals(" ")) {
					val = null;
					//objList.add(val);
					System.out.println("为空");
					continue;
				}
				String strCell = "";      
		        switch (cell.getCellType()) {      
		        case HSSFCell.CELL_TYPE_STRING:      
		            strCell = cell.getStringCellValue();   
		            break;      
		        case HSSFCell.CELL_TYPE_NUMERIC:      
		            strCell = String.valueOf(cell.getNumericCellValue()); 
		            break;      
		        case HSSFCell.CELL_TYPE_BOOLEAN:      
		            strCell = String.valueOf(cell.getBooleanCellValue()); 
		            break;      
		        case HSSFCell.CELL_TYPE_BLANK:      
		            strCell = "";      
		            break;      
		        default:      
		            strCell = "";      
		            System.out.println("haishikong");
		            break;      
		        }
		        if(strCell!= null && !strCell.equals("") && !strCell.equals(" ") && !strCell.equals("null")){
		        	objList.add(strCell);
		        }
			}
			for (Object object : objList) {
				System.out.println(object);
			}
			dataList.add(objList);
		}
		return dataList;
	}

}
