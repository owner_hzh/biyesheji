package com.cqjt.util;

public class PageUtil {

	/**
	 * 输出网站分页html<br/>
	 * 
	 * @param total
	 *            总条数在<br/>
	 * @param limit 每页条数
	 * @param pagenu
	 *            页码<br/>
	 * @param url
	 *            页面连接<br/>
	 * @param showNumber
	 *            显示多少个页<br/>
	 * @param isPrevAndNext
	 * @return
	 */
	public static String setPageHtmlStr(long total, int limit, long pagenu,
			String url, Integer showNumber, boolean isPrevAndNext) {
		String tip = "&&";
		if (url.split("[?]").length > 1) {
			tip = "&&";
		} else {
			tip = "?";
		}
		//总页数
		
		if (total > 0) {
			long totalPage = total % limit > 0 ? total / limit + 1 : total
					/ limit;
			String pageHtmlStr = "<div class='table-view'>"
					+ "<div class='cus-pagerWell clearfix'>"
					+ "<ul class='pagination'>";
			if (isPrevAndNext) {
				pageHtmlStr = (pagenu > 1) ? pageHtmlStr + "<li"
						+ " title='上一页'><a href='" + url + tip + "page="
						+ (pagenu - 1) + "'>&laquo;</a></i>" : pageHtmlStr
						+ "<li"
						+ " title='上一页'><a href='javascript:;'>&laquo;</a></i>";
			}
			if (totalPage > showNumber) {
				Integer k = (showNumber / 2) + 1;
				// 当当前页<=需要显示页的一半
				if (pagenu <= k) {
					for (int i = 1; i <= showNumber; i++) {
						pageHtmlStr = (i == pagenu) ? pageHtmlStr
								+ "<li class='active'><a href='javascript:;'>"
								+ i + "</a></li>" : pageHtmlStr
								+ "<li><a href='" + url + tip + "page=" + i
								+ "'>" + i + "</a></li>";
					}
				}
				if (pagenu > k && pagenu < totalPage - (showNumber - k)) {
					for (long i = 1 + (pagenu - k); i <= (pagenu - k)
							+ showNumber; i++) {
						pageHtmlStr = (i == pagenu) ? pageHtmlStr
								+ "<li class='active'><a href='javascript:;'>"
								+ i + "</a></li>" : pageHtmlStr
								+ "<li><a href='" + url + tip + "page=" + i
								+ "'>" + i + "</a></li>";
					}
				}
				if (pagenu >= totalPage - (showNumber - k)) {
					for (long i = (totalPage - showNumber) + 1; i <= totalPage; i++) {
						pageHtmlStr = (i == pagenu) ? pageHtmlStr
								+ "<li class='active'><a href='javascript:;'>"
								+ i + "</a></li>" : pageHtmlStr
								+ "<li><a href='" + url + tip + "page=" + i
								+ "'>" + i + "</a></li>";
					}
				}
			}
			if (totalPage <= showNumber) {
				for (int i = 1; i <= totalPage; i++) {
					pageHtmlStr = (i == pagenu) ? pageHtmlStr
							+ "<li class='active'><a href='javascript:;'>" + i
							+ "</a></li>" : pageHtmlStr + "<li><a href='" + url
							+ tip + "page=" + i + "'>" + i + "</a></li>";
				}
			}
			if (isPrevAndNext)
				pageHtmlStr = (pagenu != totalPage) ? pageHtmlStr + "<li"
						+ " title='下一页'><a href='" + url + tip + "page="
						+ (pagenu + 1) + "'>&raquo;</a></li>"
						: pageHtmlStr
								+ "<li"
								+ " title='下一页'><a href='javascript:;'>&raquo;</a></li>";
			return pageHtmlStr
					+ "</ul>"
					+ "<input type='button' id='go' class='btn btn-sm' style='float:right;margin:3px' value='GO' onclick='location.href=\""
					+ url + tip
					+ "page=\"+document.getElementById(\"gopage\").value;' />"
					+ "<input type='text' id='gopage' value='" + pagenu
					+ "' style='float:right;width:60px;height:28px;margin:3px 5px' onkeyup=\"value=value.replace(/[^(\\d+)$]/ig,'')\" onblur=\"if(this.value>"+totalPage+"){alert('你输入的页码不存在');document.getElementById('go').disabled=true;}else{document.getElementById('go').disabled=false;}\"/>"
					+ "<div style='float:right;margin:9px 5px;font-size:17px;'>共" + total + "条记录，第" + pagenu
					+ "/"+totalPage+"页</div></div></div>";
		} else {
			return "";
		}
	}
}
