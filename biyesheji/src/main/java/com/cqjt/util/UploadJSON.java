package com.cqjt.util;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.alibaba.fastjson.JSONObject;

/**
 * json格式的文件上传处理
 * @author LIJIN
 *
 */
public class UploadJSON {
	
	/**
	 * 上传文件失败时调用的文件
	 * @param message
	 * @return
	 */
	private static JSONObject getError(String message) {
		JSONObject obj = new JSONObject();
		obj.put("error", "1");
		obj.put("message", message);
		return obj;
	}
	public static JSONObject uploadFile(HttpServletRequest request,HttpServletResponse response) throws IOException,
			FileUploadException {
		// 项目部署在服务器上的配置路径
		String wpath = request.getSession().getServletContext().getRealPath("/");      
		File file = new File(wpath);
		String savePath = file.getParent()+"/uploads/";
		File fileiii = new File(savePath);
		if(!fileiii.exists()){
			fileiii.mkdirs();
		}
		// 文件实际的存储地址
		String createPath = "/uploads/";
		// 定义允许上传的文件扩展名
		HashMap<String, String> extMap = new HashMap<String, String>();
		extMap.put("image", "gif,jpg,jpeg,png,bmp");
		extMap.put("flash", "swf,flv");
		extMap.put("media", "swf,flv,mp3,wav,wma,wmv,mid,avi,mpg,asf,rm,rmvb,mp4");
		extMap.put("file", "doc,docx,xls,xlsx,ppt,htm,html,txt,zip,rar,gz,bz2");
		extMap.put("excel", "xls,xlsx");
		// 最大文件大小
		long maxSize = 200000000;

		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		if (!ServletFileUpload.isMultipartContent(request)) {
			out.print(getError("请选择文件。"));
			return getError("请选择文件。");
		}
		// 检查目录
		File uploadDir = new File(savePath);
		if (!uploadDir.isDirectory()) {
			out.print(getError("上传目录不存在。"));
			return getError("上传目录不存在。");
		}
		// 检查目录写权限
		if (!uploadDir.canWrite()) {
			out.print(getError("上传目录没有写权限。"));
			return getError("上传目录没有写权限。");
		}

		String dirName = request.getParameter("dir");
		if (dirName == null) {
			dirName = "image";
		}
		if (!extMap.containsKey(dirName)) {
			out.print(getError("目录名不正确。"));
			return getError("目录名不正确。");
		}

		// 创建文件夹
		savePath += dirName + "/";
		createPath += dirName + "/";
		File saveDirFile = new File(savePath);
		if (!saveDirFile.exists()) {
			saveDirFile.mkdirs();
		}
		DateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String ymd = sdf.format(new Date());
		savePath += ymd + "/";
		createPath += ymd + "/";
		File dirFile = new File(savePath);
		if (!dirFile.exists()) {
			dirFile.mkdirs();
		}
		//创建磁盘文件工厂
		FileItemFactory factory = new DiskFileItemFactory();
		//创建servlet文件上传组件 
		ServletFileUpload upload = new ServletFileUpload(factory);
		upload.setHeaderEncoding("UTF-8");
		//解析request从而得到前台传过来的文件  
		List items = upload.parseRequest(request);
		Iterator itr = items.iterator();
		boolean ismake = false;
		JSONObject obj = new JSONObject();
		String newFileName = "";//重命名后的文件名
		String fileName = "";//文件原始名
		String fileExt  = "";//文件你后缀名
		while (itr.hasNext()) {
			FileItem item = (FileItem) itr.next();
			fileName = item.getName();
			//long fileSize = item.getSize();
			if (!item.isFormField()) {
				// 检查文件大小
				/*if (item.getSize() > maxSize) {
					out.print(getError("上传文件大小超过限制。"));
					return getError("上传文件大小超过限制。");
				}*/
				// 检查扩展名
				fileExt = fileName.substring(
						fileName.lastIndexOf(".") + 1).toLowerCase();
				System.out.println(fileName+"后缀名："+fileExt);
				if (!Arrays.<String> asList(extMap.get(dirName).split(","))
						.contains(fileExt)) {
					System.out.println(dirName);
					System.out.println(getError("上传文件扩展名是不允许的扩展名。\n只允许"
							+ extMap.get(dirName) + "格式。"));
					out.print(getError("上传文件扩展名是不允许的扩展名。\n只允许"
							+ extMap.get(dirName) + "格式。"));
					return getError("上传文件扩展名是不允许的扩展名。\n只允许"
							+ extMap.get(dirName) + "格式。");
				}

				SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
				newFileName = df.format(new Date()) + "_"
						+ new Random().nextInt(1000) + "." + fileExt;
				try {
					File uploadedFile = new File(savePath, newFileName);
					item.write(uploadedFile);
					ismake = true;
					obj.put("error", "0");//文件是否上传成功
					obj.put("url", createPath+newFileName); //相对路径
					obj.put("allpath", savePath+newFileName);//全路径
					obj.put("fileName", fileName);//文件原始名称
					obj.put("extension", fileExt);//文件后缀
				} catch (Exception e) {
					out.print(getError("上传文件失败。"));
					return getError("上传文件失败。");
				}
			}
		}
		out.print(obj.toJSONString());
		System.out.println(obj.get("extension"));
		return obj;
	}
}
