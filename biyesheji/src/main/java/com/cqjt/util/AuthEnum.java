package com.cqjt.util;

import java.util.HashMap;
import java.util.Map;

/**
 * 权限枚举
 * @author LIJIN
 *
 */
public enum AuthEnum {

/*	TYPE1(1,"系统维护"), TYPE2(2,"专业介绍 "), TYPE3(3,"课程体系"), TYPE4(4,"师资情况"), TYPE5(5,"大纲"), 
	TYPE6(6,"课件"), TYPE7(7,"教辅资料"), TYPE8(8,"教学录相"), TYPE9(9,"作业"), TYPE10(10,"在线测试"), 
	TYPE11(11,"实战演练"),TYPE12(12,"互动平台");*/
	TYPE1(1,"1"), TYPE2(2,"2"), TYPE3(3,"3"), TYPE4(4,"6");
	private String typeName;
	private Integer type;
		
	private AuthEnum( Integer type,String typeName) {
		this.type = type;
		this.typeName = typeName;
	}

	public Integer getType() {
		return type;
	}

	public String getTypeName() {
		return typeName;
	}

	/**
	 * 通过类型查询名称
	 * 
	 * @param type
	 * @return
	 */
	public static String getTypeName(Integer type) {
		AuthEnum[] authTypeEnums = AuthEnum.values();
		String typename = "";
		for (AuthEnum authTypeEnum : authTypeEnums) {
			if (authTypeEnum.getType().intValue() == type.intValue()) {
				typename = authTypeEnum.getTypeName();
				break;
			}
		}
		return typename;
	}

	/**
	 * 所以类型的map
	 * 
	 * @return
	 */
	public static Map<Integer, String> getMap() {
		AuthEnum[] authTypeEnums = AuthEnum.values();
		Map<Integer, String> map = new HashMap<Integer, String>();
		for (AuthEnum authTypeEnum : authTypeEnums) {
			map.put(authTypeEnum.getType(), authTypeEnum.getTypeName());
		}
		
		return map;
	}

	/**
	 * action和权限码的对应
	 * 
	 * @return
	 */
	public static Map<String,String> getMapAuths() {
		Map<String, String> map = new HashMap<String,String>();
		//1游客
		map.put("/biyesheji/outline/outline",AuthEnum.TYPE1.getTypeName());//大纲初始化显示数据
		//课件controller
		map.put("/biyesheji/curriculum/show","236");//展示
		map.put("/biyesheji/courseware/show","236");//展示
		map.put("/biyesheji/courseware/upload","36");//上传
		map.put("/biyesheji/courseware/down","236");//下载
		map.put("/biyesheji/courseware/delete","36");//删除
		map.put("/biyesheji/courseware/onlinelook","236");//在线预览
		
		//2学生
		
		//3教师
		
		//4管理员
		
/*		//5大纲
		map.put("/biyesheji/outline/outline",AuthEnum.TYPE5.getTypeName());//大纲初始化显示数据
		map.put("/biyesheji/outline/getDataByCode",AuthEnum.TYPE5.getTypeName());//以code查询大纲信息 返回json
		map.put("/biyesheji/outline/page-office",AuthEnum.TYPE5.getTypeName());//只读方式打开doc
		map.put("/biyesheji/outline/editOffice",AuthEnum.TYPE5.getTypeName());//编辑状态打开doc
		map.put("/biyesheji/outline/SaveFile",AuthEnum.TYPE5.getTypeName());//保存doc
		map.put("/biyesheji/outline/nofile",AuthEnum.TYPE5.getTypeName());//文件不存在的跳转
		map.put("/biyesheji/outline/outlineInitial",AuthEnum.TYPE5.getTypeName());//outline页面初始化iframe显示的一个页面
		map.put("/biyesheji/outline/deleteDoc",AuthEnum.TYPE5.getTypeName());// 删除文档
		map.put("/biyesheji/outline/deleteDoc1",AuthEnum.TYPE5.getTypeName());// 删除文档
		map.put("/biyesheji/outline/uploadDoc",AuthEnum.TYPE5.getTypeName());// 接收上传的文件
		map.put("/biyesheji/outline/outlineList",AuthEnum.TYPE5.getTypeName());//outline-list页面跳转 
		map.put("/biyesheji/outline/edithtml",AuthEnum.TYPE5.getTypeName());//编辑html页面
		map.put("/biyesheji/outline/savehtml",AuthEnum.TYPE5.getTypeName());// 保存修改后的html文件
		map.put("/biyesheji/outline/ckeditorupload",AuthEnum.TYPE5.getTypeName());//ckeditor上传图片
		
		//6课件
		map.put("/biyesheji/courseware",AuthEnum.TYPE6.getTypeName());//跳转到课程课件
		map.put("/biyesheji/courseware/show",AuthEnum.TYPE6.getTypeName());//显示所有课件
		map.put("/biyesheji/courseware/upload",AuthEnum.TYPE6.getTypeName());//上传课件
		map.put("/biyesheji/courseware/down",AuthEnum.TYPE6.getTypeName());//下载课件
		map.put("/biyesheji/courseware/delete",AuthEnum.TYPE6.getTypeName());//删除课件
		map.put("/biyesheji/courseware/onlinelook",AuthEnum.TYPE6.getTypeName());//预览课件
		
		//7教辅资料
		
		//8教学录相
		
		//9作业
		map.put("/biyesheji/assignment/showAssignment",AuthEnum.TYPE9.getTypeName());//显示相关课程的作业
		map.put("/biyesheji/assignment/delAssignment",AuthEnum.TYPE9.getTypeName());//删除作业
		map.put("/biyesheji/assignment/uploadAssignment",AuthEnum.TYPE9.getTypeName());//上传课程的作业
		map.put("/biyesheji/assignment/downAssignment",AuthEnum.TYPE9.getTypeName());//下载课程的作业
		//10在线测试 
		
		//11实战演练
		
		//12互动平台
*/		
		return map;
	}
	
	public static String getAction(String url){
		
		return getMapAuths().get(url);//更具key取相应的value
		
	}
}
