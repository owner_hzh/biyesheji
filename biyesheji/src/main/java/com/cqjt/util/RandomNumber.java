package com.cqjt.util;

import java.util.Random;

public class RandomNumber {
	/**
	 * 生成六位随机数字
	 * @return 六位随机数字字符串
	 */
    public static String getRandomNumber()
    {
    	Random random = new Random();
    	String result="";
	    for(int i=0;i<6;i++){
	       result+=random.nextInt(10);
	    }
	    return result;
    }
}
