package com.cqjt.pojo;

import java.sql.Date;

/**
 * 教学录像实体
 * @author 见鸣
 *
 */
public class Teachingvideotape 
{
	private int teachingvideotape_code;//教学录像代码 bigint
	private int curriculum_code;//课程代码  bigint
	private String filename;//文件名 varchar(20)
	private String location;//链接位置 varchar(100)
	private Date uploaddate;//上传时间
	private int down_count;//下载次数
	
	public int getTeachingvideotape_code() {
		return teachingvideotape_code;
	}
	public void setTeachingvideotape_code(int teachingvideotape_code) {
		this.teachingvideotape_code = teachingvideotape_code;
	}
	public int getCurriculum_code() {
		return curriculum_code;
	}
	public void setCurriculum_code(int curriculum_code) {
		this.curriculum_code = curriculum_code;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public Date getUploaddate() {
		return uploaddate;
	}
	public void setUploaddate(Date uploaddate) {
		this.uploaddate = uploaddate;
	}
	public int getDown_count() {
		return down_count;
	}
	public void setDown_count(int down_count) {
		this.down_count = down_count;
	}
	
}
