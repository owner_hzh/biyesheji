package com.cqjt.pojo;

/**
 * <p>Title: CurriculumCharacter.java</p>
 * <p>Description: MyEclipse 平台软件</p>
 * <p>Copyright: Copyright (c) 毕业设计小分队</p>
 * @author xiaoshujun
 * @version 1.0 创建时间：2014-3-20 下午9:30:34
 */
/*课程性质*/
public class CurriculumCharacter {
	
	private int cc_code;//性质代码
	private String cc_name;//性质名称
	/**
	 * @return the cc_code
	 */
	public int getCc_code() {
		return cc_code;
	}
	/**
	 * @param cc_code the cc_code to set
	 */
	public void setCc_code(int cc_code) {
		this.cc_code = cc_code;
	}
	/**
	 * @return the cc_name
	 */
	public String getCc_name() {
		return cc_name;
	}
	/**
	 * @param cc_name the cc_name to set
	 */
	public void setCc_name(String cc_name) {
		this.cc_name = cc_name;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CurriculumCharacter [cc_code=" + cc_code + ", cc_name="
				+ cc_name + "]";
	}
	
	
}
