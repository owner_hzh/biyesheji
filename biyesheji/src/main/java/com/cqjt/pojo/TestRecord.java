package com.cqjt.pojo;

import java.sql.Timestamp;
/*
 * author:xiaoshujun
 * 2014-4-22
 * 考试记录	类
 * 
 */

public class TestRecord {
	private int number;	//试卷编码	
	private String user_id;	//用户名
	
	private String student_name;			//姓名
	
	private String tea_user_id;	//审核人
	private String tea_user_name;	//审核人
	private int test_code;	//考试卷编码
	private Timestamp test_date;//考试日期
	private float selfallscore;	//自我评价总成绩
	private float teaallscore;	//审核总成绩
	private int is_check;	//审核状态
	private Timestamp check_time;//审核时间
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getTea_user_id() {
		return tea_user_id;
	}
	public void setTea_user_id(String tea_user_id) {
		this.tea_user_id = tea_user_id;
	}
	public int getTest_code() {
		return test_code;
	}
	public void setTest_code(int test_code) {
		this.test_code = test_code;
	}
	public Timestamp getTest_date() {
		return test_date;
	}
	public void setTest_date(Timestamp test_date) {
		this.test_date = test_date;
	}
	public float getSelfallscore() {
		return selfallscore;
	}
	public void setSelfallscore(float selfallscore) {
		this.selfallscore = selfallscore;
	}
	public float getTeaallscore() {
		return teaallscore;
	}
	public void setTeaallscore(float teaallscore) {
		this.teaallscore = teaallscore;
	}
	public int getIs_check() {
		return is_check;
	}
	public void setIs_check(int is_check) {
		this.is_check = is_check;
	}
	public Timestamp getCheck_time() {
		return check_time;
	}
	public void setCheck_time(Timestamp check_time) {
		this.check_time = check_time;
	}
	public String getTea_user_name() {
		return tea_user_name;
	}
	public void setTea_user_name(String tea_user_name) {
		this.tea_user_name = tea_user_name;
	}
	public String getStudent_name() {
		return student_name;
	}
	public void setStudent_name(String student_name) {
		this.student_name = student_name;
	}


	
}
