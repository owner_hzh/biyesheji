package com.cqjt.pojo;
/*
 * author:xiaoshujun
 * 2014-5-3
 * bbs 板块	类
 * 
 */
public class BBSSection {
	private int sid; //版块编号 
	private String sname;//版块名称   
	private String smasterid ;//版主编号    
	private String sstatement; //版块说明  
	private int sclickcount ;//版块点击次数  
	private int stopiccount ;//版块主题数 
	public int getSid() {
		return sid;
	}
	public void setSid(int sid) {
		this.sid = sid;
	}
	public String getSname() {
		return sname;
	}
	public void setSname(String sname) {
		this.sname = sname;
	}
	public String getSmasterid() {
		return smasterid;
	}
	public void setSmasterid(String smasterid) {
		this.smasterid = smasterid;
	}
	public String getSstatement() {
		return sstatement;
	}
	public void setSstatement(String sstatement) {
		this.sstatement = sstatement;
	}
	public int getSclickcount() {
		return sclickcount;
	}
	public void setSclickcount(int sclickcount) {
		this.sclickcount = sclickcount;
	}
	public int getStopiccount() {
		return stopiccount;
	}
	public void setStopiccount(int stopiccount) {
		this.stopiccount = stopiccount;
	}
	@Override
	public String toString() {
		return "BBSSection [sid=" + sid + ", sname=" + sname + ", smasterid="
				+ smasterid + ", sstatement=" + sstatement + ", sclickcount="
				+ sclickcount + ", stopiccount=" + stopiccount + "]";
	}
	
}
