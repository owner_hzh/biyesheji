package com.cqjt.pojo;
/*
 * author:xiaoshujun
 * 2014-4-22
 * 实战项目编码  类
 * 
 */
public class PracticalProjectCode {
	private int ppcode;				//实战题目类型号
	private String pptype_name; 	//类型名
	public int getPpcode() {
		return ppcode;
	}
	public void setPpcode(int ppcode) {
		this.ppcode = ppcode;
	}
	public String getPptype_name() {
		return pptype_name;
	}
	public void setPptype_name(String pptype_name) {
		this.pptype_name = pptype_name;
	}
	@Override
	public String toString() {
		return "PracticalProjectCode [ppcode=" + ppcode + ", pptype_name="
				+ pptype_name + "]";
	}
	
	
}
