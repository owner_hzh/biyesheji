package com.cqjt.pojo;
/*
 * author:Wen 2014-4-20
 * 教师性质	类
 * 
 */
public class TeacherCharacter {
	
	private int tc_code;		//性质代码
	private String tc_name;		//性质名称
	public int getTc_code() {
		return tc_code;
	}
	public void setTc_code(int tc_code) {
		this.tc_code = tc_code;
	}
	public String getTc_name() {
		return tc_name;
	}
	public void setTc_name(String tc_name) {
		this.tc_name = tc_name;
	}
	@Override
	public String toString() {
		return "TeacherCharacter [tc_code=" + tc_code + ", tc_name=" + tc_name
				+ "]";
	}
	
	
}
