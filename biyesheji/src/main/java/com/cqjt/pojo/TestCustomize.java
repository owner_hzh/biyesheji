package com.cqjt.pojo;

import java.util.List;

/*
 * author by xiaoshujun 2014-4-20
 * 定制试卷 类
 */
public class TestCustomize {
	
	private int curriculum_code;		//课程代码
	private int number;					//试卷编码	
	private int qt_code;				//题型代码
	//private int	qusetion_code;			//试题序号
	private int question_score;			//题目分值
	private int question_num;			//题型数量
	private int question_allscore;		//题型总分
	private List<SelfTest> selfTests;//该题型下要定制的题
	
	private List<TestQuestion> tQuestions;//题型下的题
	private String qt_name;//该题型名称
	
	public String getQt_name() {
		return qt_name;
	}

	public void setQt_name(String qt_name) {
		this.qt_name = qt_name;
	}

	public int getCurriculum_code() {
		return curriculum_code;
	}
	
	public void setCurriculum_code(int curriculum_code) {
		this.curriculum_code = curriculum_code;
	}

	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public int getQt_code() {
		return qt_code;
	}
	public void setQt_code(int qt_code) {
		this.qt_code = qt_code;
	}

	public int getQuestion_score() {
		return question_score;
	}
	public void setQuestion_score(int question_score) {
		this.question_score = question_score;
	}
	public int getQuestion_num() {
		return question_num;
	}
	public void setQuestion_num(int question_num) {
		this.question_num = question_num;
	}
	public int getQuestion_allscore() {
		return question_allscore;
	}
	public void setQuestion_allscore(int question_allscore) {
		this.question_allscore = question_allscore;
	}
	


	public List<SelfTest> getSelfTests() {
		return selfTests;
	}

	public void setSelfTests(List<SelfTest> selfTests) {
		this.selfTests = selfTests;
	}

	public List<TestQuestion> gettQuestions() {
		return tQuestions;
	}

	public void settQuestions(List<TestQuestion> tQuestions) {
		this.tQuestions = tQuestions;
	}

	@Override
	public String toString() {
		return "TestCustomize ["
				+ ", number=" + number + ", qt_code=" + qt_code
				 + ", question_score="
				+ question_score + ", question_num=" + question_num
				+ ", question_allscore=" + question_allscore + "]";
	}
	
	
}
