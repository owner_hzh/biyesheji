package com.cqjt.pojo;
/*
 * author:xiaoshujun
 * 2014-4-22
 * 实战项目  类
 * 
 */
public class PracticalProject {
	
	private int ppcode;			//实战题目类型号
	private String pptype_name; //实战题目类型名字   PS：不在PracticalProject中，为了才显示添加的字段
 	private String ppnumber;	//序号
 	private int semester_code;	//开课学期
 	private String user_id;		//用户ID
 	private String user_name;   //用户名   PS：不在PracticalProject中，为了才显示添加的字段
 	private String tea_user_id;	//教师用户ID
 	private String tea_user_name;//教师名 PS：不在PracticalProject中，为了才显示添加的字段
 	private String requirement;	//实战要求
 	private int source;			//项目来源
	private String pp_name;		//实战题目
 	private String project_stringcode;//项目编码
 	/*以下非数据库中的字段*/
 	private String source_CN;   //项目来源的判断，1为教师，0为学生
 	
 	
	public String getPptype_name() {
		return pptype_name;
	}

	public void setPptype_name(String pptype_name) {
		this.pptype_name = pptype_name;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getTea_user_name() {
		return tea_user_name;
	}

	public void setTea_user_name(String tea_user_name) {
		this.tea_user_name = tea_user_name;
	}

	public void setSource(int source) 
	{
		this.source = source;
		if(source==0)
			source_CN="学生";
		else
			source_CN="教师";
	}
 	
	public String getSource_CN() {
		return source_CN;
	}

 	

	public int getPpcode() {
		return ppcode;
	}
	public void setPpcode(int ppcode) {
		this.ppcode = ppcode;
	}
	public String getPpnumber() {
		return ppnumber;
	}
	public void setPpnumber(String ppnumber) {
		this.ppnumber = ppnumber;
	}
	public int getSemester_code() {
		return semester_code;
	}
	public void setSemester_code(int semester_code) {
		this.semester_code = semester_code;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getTea_user_id() {
		return tea_user_id;
	}
	public void setTea_user_id(String tea_user_id) {
		this.tea_user_id = tea_user_id;
	}
	public String getRequirement() {
		return requirement;
	}
	public void setRequirement(String requirement) {
		this.requirement = requirement;
	}
	public int getSource() {
		return source;
	}
	public String getPp_name() {
		return pp_name;
	}
	public void setPp_name(String pp_name) {
		this.pp_name = pp_name;
	}
 	public String getProject_stringcode() {
		return project_stringcode;
	}
	public void setProject_stringcode(String project_stringcode) {
		this.project_stringcode = project_stringcode;
	}
	@Override
	public String toString() {
		return "PracticalProject [ppcode=" + ppcode + ", ppnumber=" + ppnumber
				+ ", semester_code=" + semester_code + ", user_id=" + user_id
				+ ", tea_user_id=" + tea_user_id + ", requirement="
				+ requirement + ", source=" + source + ", pp_name=" + pp_name
				+ "]";
	}
 	
 	
}
