package com.cqjt.pojo;

/*
 *	author: xiaoshujun
 * date: 2014-4-20
 * 试题类
 */
public class TestQuestion {
		private int qt_code;	//题型代码
		private int curriculum_code;  //课程代码
		private int tq_id;						// 试题代码
		private String introduction; //题干
		private int chapter;	//所属章
		private String answer; //参考答案
		private Integer self_test_id;//自测试卷表中的主键（不存数据库）方便页面传回答案
		public int getQt_code() {
			return qt_code;
		}
		public void setQt_code(int qt_code) {
			this.qt_code = qt_code;
		}
		public int getCurriculum_code() {
			return curriculum_code;
		}
		public void setCurriculum_code(int curriculum_code) {
			this.curriculum_code = curriculum_code;
		}
		public int getTq_id() {
			return tq_id;
		}
		public void setTq_id(int tq_id) {
			this.tq_id = tq_id;
		}
		public String getIntroduction() {
			return introduction;
		}
		public void setIntroduction(String introduction) {
			this.introduction = introduction;
		}
		public int getChapter() {
			return chapter;
		}
		public void setChapter(int chapter) {
			this.chapter = chapter;
		}

		public String getAnswer() {
			return answer;
		}
		public void setAnswer(String answer) {
			this.answer = answer;
		}
		
		public Integer getSelf_test_id() {
			return self_test_id;
		}
		public void setSelf_test_id(Integer self_test_id) {
			this.self_test_id = self_test_id;
		}
		@Override
		public String toString() {
			return "TestQuestion [qt_code=" + qt_code + ", curriculum_code="
					+ curriculum_code + ", tq_id=" + tq_id + ", introduction="
					+ introduction + ", chapter=" + chapter + ", answer="
					+ answer + "]";
		}

		
		
}
