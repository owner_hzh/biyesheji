package com.cqjt.pojo;

import java.util.List;

/**
 * 试卷编码实体
 * @author LIJIN
 *
 */
public class TestCode {
	private Integer curriculum_code;//课程代码
	private Integer number;//试卷编码
	private Integer start;//开始章号
	private Integer end;//结束章号
	private String title;//试卷标题
	private String create_date;//出题日期
	private String modify_date;//修改日期
	private String user_id;//出题教师
	private String user_name;//出题教师名称

	private int totalscore;//试卷的总分
	
	public int getTotalscore() {
		return totalscore;
	}
	public void setTotalscore(int totalscore) {
		this.totalscore = totalscore;
	}

	private List<TestRecord> testRecords;//考试记录

	public Integer getCurriculum_code() {
		return curriculum_code;
	}
	public void setCurriculum_code(Integer curriculum_code) {
		this.curriculum_code = curriculum_code;
	}
	public Integer getNumber() {
		return number;
	}
	public void setNumber(Integer number) {
		this.number = number;
	}
	public Integer getStart() {
		return start;
	}
	public void setStart(Integer start) {
		this.start = start;
	}
	public Integer getEnd() {
		return end;
	}
	public void setEnd(Integer end) {
		this.end = end;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCreate_date() {
		return create_date;
	}
	public void setCreate_date(String create_date) {
		this.create_date = create_date;
	}
	public String getModify_date() {
		return modify_date;
	}
	public void setModify_date(String modify_date) {
		this.modify_date = modify_date;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public List<TestRecord> getTestRecords() {
		return testRecords;
	}
	public void setTestRecords(List<TestRecord> testRecords) {
		this.testRecords = testRecords;
	}
	
	
}
