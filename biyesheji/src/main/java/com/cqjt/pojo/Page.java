package com.cqjt.pojo;

public class Page {
   private int pageSize=15;//每页的条数  
   private int pageNo=1;//第几页
   private int totalCount;//总条数
   private int totalPage;//总页数
   private int currentCount;//当前页的条数
   
   public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
   
public int getPageNo() {
	return pageNo;
}
public void setPageNo(int pageNo) {
	this.pageNo = pageNo;
}
public int getTotalCount() {
	return totalCount;
}
public void setTotalCount(int totalCount) {
	this.totalCount = totalCount;
	this.totalPage = totalCount/getPageSize()+1;
}
public int getTotalPage() {
	return totalPage;
}
public void setTotalPage(int totalPage) {
	this.totalPage = totalPage;
}
public int getCurrentCount() {
	return currentCount;
}
public void setCurrentCount(int currentCount) {
	this.currentCount = currentCount;
}
}
