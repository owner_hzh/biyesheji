package com.cqjt.pojo;

import java.sql.Date;

/*
 * author:xiaoshujun
 * 2014-4-22
 * 实战项目编码  类
 * 
 */
public class PracticalProjectDocument {
	
	private int ppcode;			//实战题目类型号
	private String ppnumber;	//序号
	private String ppd_name;	//参考文档名称
	private String ppd_link;	//链接位置
	private int down_count;     //下载次数
	private Date upload_time;   //上传时间
	private int ppd_code;      //实战文档序号,主键
	
	public int getPpd_code() {
		return ppd_code;
	}
	public void setPpd_code(int ppd_code) {
		this.ppd_code = ppd_code;
	}
	public int getDown_count() {
		return down_count;
	}
	public void setDown_count(int down_count) {
		this.down_count = down_count;
	}
	public Date getUpload_time() {
		return upload_time;
	}
	public void setUpload_time(Date upload_time) {
		this.upload_time = upload_time;
	}
	public int getPpcode() {
		return ppcode;
	}
	public void setPpcode(int ppcode) {
		this.ppcode = ppcode;
	}
	public String getPpnumber() {
		return ppnumber;
	}
	public void setPpnumber(String ppnumber) {
		this.ppnumber = ppnumber;
	}
	public String getPpd_name() {
		return ppd_name;
	}
	public void setPpd_name(String ppd_name) {
		this.ppd_name = ppd_name;
	}
	public String getPpd_link() {
		return ppd_link;
	}
	public void setPpd_link(String ppd_link) {
		this.ppd_link = ppd_link;
	}
	@Override
	public String toString() {
		return "PracticalProjectDocument [ppcode=" + ppcode + ", ppnumber="
				+ ppnumber + ", ppd_name=" + ppd_name + ", ppd_link="
				+ ppd_link + "]";
	}
	
}
