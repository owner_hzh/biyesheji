package com.cqjt.pojo;
/*
 * author:xiaoshujun
 * 2014-4-22
 * 用户	类
 * 
 */
public class User
{
	private String user_id;					//用户名
	private int	role_code;					//角色代码
	private String password;				//密码
	private int is_teachingAdministrator;	//教学管理员
	private int is_departmentHead;			//系主任
	private int is_systemAdministrator;		//系统管理员
	private String name;				    //附加字段
	private int major_code;				//附加字段
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public int getRole_code() {
		return role_code;
	}
	public void setRole_code(int role_code) {
		this.role_code = role_code;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getIs_teachingAdministrator() {
		return is_teachingAdministrator;
	}
	public void setIs_teachingAdministrator(int is_teachingAdministrator) {
		this.is_teachingAdministrator = is_teachingAdministrator;
	}
	public int getIs_departmentHead() {
		return is_departmentHead;
	}
	public void setIs_departmentHead(int is_departmentHead) {
		this.is_departmentHead = is_departmentHead;
	}
	public int getIs_systemAdministrator() {
		return is_systemAdministrator;
	}
	public void setIs_systemAdministrator(int is_systemAdministrator) {
		this.is_systemAdministrator = is_systemAdministrator;
	}
	
	public int getMajor_code() {
		return major_code;
	}
	public void setMajor_code(int major_code) {
		this.major_code = major_code;
	}
	@Override
	public String toString() {
		return "User [user_id=" + user_id + ", role_code=" + role_code
				+ ", password=" + password + ", is_teachingAdministrator="
				+ is_teachingAdministrator + ", is_departmentHead="
				+ is_departmentHead + ", is_systemAdministrator="
				+ is_systemAdministrator + ", name=" + name + "]";
	}
	
}
