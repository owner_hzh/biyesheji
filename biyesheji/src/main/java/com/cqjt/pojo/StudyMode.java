package com.cqjt.pojo;

/**
 * <p>Title: StudyMode.java</p>
 * <p>Description: MyEclipse 平台软件</p>
 * <p>Copyright: Copyright (c) 毕业设计小分队</p>
 * @author xiaoshujun
 * @version 1.0 创建时间：2014-3-20 下午9:34:06
 */
/*修读方式*/
public class StudyMode {
	private int studymode_id;		//方式编号
	private String studymode_name;	//方式名称
	/**
	 * @return the studymode_id
	 */
	public int getStudymode_id() {
		return studymode_id;
	}
	/**
	 * @param studymode_id the studymode_id to set
	 */
	public void setStudymode_id(int studymode_id) {
		this.studymode_id = studymode_id;
	}
	/**
	 * @return the studymode_name
	 */
	public String getStudymode_name() {
		return studymode_name;
	}
	/**
	 * @param studymode_name the studymode_name to set
	 */
	public void setStudymode_name(String studymode_name) {
		this.studymode_name = studymode_name;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "StudyMode [studymode_id=" + studymode_id + ", studymode_name="
				+ studymode_name + "]";
	}
	
}
