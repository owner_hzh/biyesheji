package com.cqjt.pojo;
/*
 * author:xiaoshujun
 * 2014-4-22
 * 自测试卷  类
 * 
 */
public class SelfTest {
	
	private int id;	//主键
	private int number;	//试卷编码	
	private int qt_code;//题型代码
	private int test_code;//考试卷编码
	private int tq_id;	//试题编号
	private String self_answer;	//参考答案
	private String answer;//答案
	
	String introduction;//题干
	/*String qt_name;//题型名称
	int question_score;//题目分值
	int question_num;//题目数量
	int question_allscore;//题型总分
*/	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public int getQt_code() {
		return qt_code;
	}
	public void setQt_code(int qt_code) {
		this.qt_code = qt_code;
	}
	public int getTest_code() {
		return test_code;
	}
	public void setTest_code(int test_code) {
		this.test_code = test_code;
	}
	public int getTq_id() {
		return tq_id;
	}
	public void setTq_id(int tq_id) {
		this.tq_id = tq_id;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public String getSelf_answer() {
		return self_answer;
	}
	public void setSelf_answer(String self_answer) {
		this.self_answer = self_answer;
	}
	public String getIntroduction() {
		return introduction;
	}
	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}	
}
