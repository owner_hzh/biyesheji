package com.cqjt.pojo;

import java.util.List;

public class Major{
	
	private int major_code;
	private String major_name;
	private String core_curriculum;
	private String occupational_direction;
	private String goal;
	private String history;
	
	private String schooling_length;
	private String degree_awarding;
	private String required;
	
	private List<Curriculum> curriculums;
	
	@Override
	public String toString() {
		return "Major [major_code=" + major_code + ", major_name=" + major_name
				+ ", core_curriculum=" + core_curriculum
				+ ", occupational_direction=" + occupational_direction
				+ ", goal=" + goal + ", history=" + history + ", curriculums="
				+ curriculums + "]";
	}
	public int getMajor_code() {
		return major_code;
	}
	public void setMajor_code(String major_code) {
		if(major_code.contains("."))
		{
			this.major_code=(int) Double.parseDouble(major_code);
		}else
		{
			this.major_code = Integer.parseInt(major_code);
		}
	}
	public void setMajor_code(int major_code) {
			this.major_code = major_code;
	}
	public String getMajor_name() {
		return major_name;
	}
	public void setMajor_name(String major_name) {
		this.major_name = major_name;
	}
	public String getCore_curriculum() {
		return core_curriculum;
	}
	public void setCore_curriculum(String core_curriculum) {
		this.core_curriculum = core_curriculum;
	}

	public String getGoal() {
		return goal;
	}
	public void setGoal(String goal) {
		this.goal = goal;
	}
	public String getHistory() {
		return history;
	}
	public void setHistory(String history) {
		this.history = history;
	}
	public String getOccupational_direction() {
		return occupational_direction;
	}
	public void setOccupational_direction(String occupational_direction) {
		this.occupational_direction = occupational_direction;
	}
	public List<Curriculum> getCurriculums() {
		return curriculums;
	}
	public void setCurriculums(List<Curriculum> curriculums) {
		this.curriculums = curriculums;
	}
	public String getSchooling_length() {
		return schooling_length;
	}
	public void setSchooling_length(String schooling_length) {
		this.schooling_length = schooling_length;
	}
	public String getDegree_awarding() {
		return degree_awarding;
	}
	public void setDegree_awarding(String degree_awarding) {
		this.degree_awarding = degree_awarding;
	}
	public String getRequired() {
		return required;
	}
	public void setRequired(String required) {
		this.required = required;
	}

}
