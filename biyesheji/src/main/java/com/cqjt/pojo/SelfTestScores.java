package com.cqjt.pojo;
/*
 * author:xiaoshujun
 * 2014-4-22
 * 自测考试成绩	类
 * 
 */

public class SelfTestScores {
	private Integer test_code;	//考试卷编码
	private Integer number;		//试卷编码	
	private Integer qt_code;	//题型代码
	private Float stscore;	//自测评分
	private Float teacher_score;//教师评分
	private Float all_stscore;//自测评分总成绩
	private Float all_teacher_score;//教师评分总成绩
	
	public Integer getTest_code() {
		return test_code;
	}

	public void setTest_code(Integer test_code) {
		this.test_code = test_code;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public Integer getQt_code() {
		return qt_code;
	}

	public void setQt_code(Integer qt_code) {
		this.qt_code = qt_code;
	}

	public Float getStscore() {
		return stscore;
	}

	public void setStscore(Float stscore) {
		this.stscore = stscore;
	}

	public Float getTeacher_score() {
		return teacher_score;
	}

	public void setTeacher_score(Float teacher_score) {
		this.teacher_score = teacher_score;
	}

	public Float getAll_stscore() {
		return all_stscore;
	}

	public void setAll_stscore(Float all_stscore) {
		this.all_stscore = all_stscore;
	}

	public Float getAll_teacher_score() {
		return all_teacher_score;
	}

	public void setAll_teacher_score(Float all_teacher_score) {
		this.all_teacher_score = all_teacher_score;
	}

	@Override
	public String toString() {
		return "SelfTestScores [test_code=" + test_code + ", number=" + number
				+ ", qt_code=" + qt_code + ", stscore=" + stscore
				+ ", teacher_score=" + teacher_score + ", all_stscore="
				+ all_stscore + ", all_teacher_score=" + all_teacher_score
				+ "]";
	}

}
