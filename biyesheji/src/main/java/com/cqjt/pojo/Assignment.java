package com.cqjt.pojo;
import java.sql.Date;
/**
 * 作业实体
 * @author 见鸣
 *
 */
public class Assignment 
{
	private int assignment_code;//作业代码 bigint
	private int curriculum_code;//课程代码  bigint
	private String filename;//文件名 varchar(56)
	private String location;//链接位置 varchar(512)
	private Date uploaddate;//上传日期
	private int down_count;//下载次数
	
	public Date getUploaddate() {
		return uploaddate;
	}
	public void setUploaddate(Date uploaddate) {
		this.uploaddate = uploaddate;
	}
	public int getDown_count() {
		return down_count;
	}
	public void setDown_count(int down_count) {
		this.down_count = down_count;
	}
	public int getAssignment_code() {
		return assignment_code;
	}
	public void setAssignment_code(int assignment_code) {
		this.assignment_code = assignment_code;
	}
	public int getCurriculum_code() {
		return curriculum_code;
	}
	public void setCurriculum_code(int curriculum_code) {
		this.curriculum_code = curriculum_code;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}

}
