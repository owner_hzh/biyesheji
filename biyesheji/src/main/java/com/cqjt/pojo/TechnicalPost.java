package com.cqjt.pojo;
/*
 * author by Wen 2014-4-20
 *	职称类
 */
public class TechnicalPost {
	
	private int technical_code;	//职称代码
	private String technical_name;	//职称名
	
	public int getTechnical_code() {
		return technical_code;
	}
	public void setTechnical_code(int technical_code) {
		this.technical_code = technical_code;
	}
	public String getTechnical_name() {
		return technical_name;
	}
	public void setTechnical_name(String technical_name) {
		this.technical_name = technical_name;
	}
	
	@Override
	public String toString() {
		return "TechnicalPost [technical_code=" + technical_code
				+ ", technical_name=" + technical_name + "]";
	}
	
}
