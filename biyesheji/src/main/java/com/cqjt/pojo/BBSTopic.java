package com.cqjt.pojo;

import java.sql.Timestamp;

import com.sun.star.util.DateTime;
/*
 * author:xiaoshujun
 * 2014-5-3
 * bbs 主贴	类
 * 
 */
public class BBSTopic {
	private int tid ;//主贴编号 
	
	private int tsid ;//主贴板块编号 
	private String tuid ;//主贴用户编号 
	private int treplycount ;//主贴回复次数 
	private String temotion ;//主贴表情 
	private String ttopic  ;//主贴标题 
	private String tcontents ;//主贴内容 
	private Timestamp ttime  ;//发帖时间 
	
	private int tclickcount ;//主贴点击次数  
	private Timestamp tlastclickt   ;//主贴最后点击时间  
	public int getTid() {
		return tid;
	}
	public void setTid(int tid) {
		this.tid = tid;
	}
	public int getTsid() {
		return tsid;
	}
	public void setTsid(int tsid) {
		this.tsid = tsid;
	}
	public String getTuid() {
		return tuid;
	}
	public void setTuid(String tuid) {
		this.tuid = tuid;
	}
	public int getTreplycount() {
		return treplycount;
	}
	public void setTreplycount(int treplycount) {
		this.treplycount = treplycount;
	}
	public String getTemotion() {
		return temotion;
	}
	public void setTemotion(String temotion) {
		this.temotion = temotion;
	}
	public String getTtopic() {
		return ttopic;
	}
	public void setTtopic(String ttopic) {
		this.ttopic = ttopic;
	}
	public String getTcontents() {
		return tcontents;
	}
	public void setTcontents(String tcontents) {
		this.tcontents = tcontents;
	}
	public Timestamp getTtime() {
		return ttime;
	}
	public void setTtime(Timestamp ttime) {
		this.ttime = ttime;
	}
	public int getTclickcount() {
		return tclickcount;
	}
	public void setTclickcount(int tclickcount) {
		this.tclickcount = tclickcount;
	}
	public Timestamp getTlastclickt() {
		return tlastclickt;
	}
	public void setTlastclickt(Timestamp tlastclickt) {
		this.tlastclickt = tlastclickt;
	}
	@Override
	public String toString() {
		return "BBSTopic [tid=" + tid + ", tsid=" + tsid + ", tuid=" + tuid
				+ ", treplycount=" + treplycount + ", temotion=" + temotion
				+ ", ttopic=" + ttopic + ", tcontents=" + tcontents
				+ ", ttime=" + ttime + ", tclickcount=" + tclickcount
				+ ", tlastclickt=" + tlastclickt + "]";
	}
	
}
