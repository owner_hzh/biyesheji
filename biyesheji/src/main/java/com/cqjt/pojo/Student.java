package com.cqjt.pojo;

import java.sql.Timestamp;

/*
 * author:xiaoshujun
 * 2014-4-22
 * 学生	类
 * 
 */
public class Student {
	
	private String user_id;					//用户名
	private int major_code;					//专业代码
	private String student_name;			//姓名
	private int student_sex;				//学生性别
	private int grade;						//年级
	private String gradute_date;			//毕业时间
	private int	role_code;					//角色代码
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public int getMajor_code() {
		return major_code;
	}
	public void setMajor_code(int major_code) {
		this.major_code = major_code;
	}
	public String getStudent_name() {
		return student_name;
	}
	public void setStudent_name(String student_name) {
		this.student_name = student_name;
	}
	public int getStudent_sex() {
		return student_sex;
	}
	public void setStudent_sex(int student_sex) {
		this.student_sex = student_sex;
	}
	public int getGrade() {
		return grade;
	}
	public void setGrade(int grade) {
		this.grade = grade;
	}
	public String getGradute_date() {
		return gradute_date;
	}
	public void setGradute_date(String gradute_date) {
		this.gradute_date = gradute_date;
	}
	
	public int getRole_code() {
		return role_code;
	}
	public void setRole_code(int role_code) {
		this.role_code = role_code;
	}
	@Override
	public String toString() {
		return "Student [user_id=" + user_id + ", major_code=" + major_code
				+ ", student_name=" + student_name + ", student_sex="
				+ student_sex + ", grade=" + grade + ", gradute_date="
				+ gradute_date + "]";
	}
	
	
}
