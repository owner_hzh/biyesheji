package com.cqjt.pojo;
/*
 * author:xiaoshujun
 * 2014-4-22
 * 自测题主观题答卷  类
 * 
 */

public class SelfTestAnswer {
	
	private int curriculum_code;	//课程代码
	private int number;				//试卷编码	
	private int qt_code;			//题型代码
	private int testpaper_code;		//考试卷编码
	private String user_id;			//用户名
	private String answer_name;		//文件名
	private String answer_link;		//链接位置
	private int is_upload;			//已上传
	private int is_self_assessment;	//已自评
	private int is_read;			//已阅卷
	public int getCurriculum_code() {
		return curriculum_code;
	}
	public void setCurriculum_code(int curriculum_code) {
		this.curriculum_code = curriculum_code;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public int getQt_code() {
		return qt_code;
	}
	public void setQt_code(int qt_code) {
		this.qt_code = qt_code;
	}
	public int getTestpaper_code() {
		return testpaper_code;
	}
	public void setTestpaper_code(int testpaper_code) {
		this.testpaper_code = testpaper_code;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getAnswer_name() {
		return answer_name;
	}
	public void setAnswer_name(String answer_name) {
		this.answer_name = answer_name;
	}
	public String getAnswer_link() {
		return answer_link;
	}
	public void setAnswer_link(String answer_link) {
		this.answer_link = answer_link;
	}
	public int getIs_upload() {
		return is_upload;
	}
	public void setIs_upload(int is_upload) {
		this.is_upload = is_upload;
	}
	public int getIs_self_assessment() {
		return is_self_assessment;
	}
	public void setIs_self_assessment(int is_self_assessment) {
		this.is_self_assessment = is_self_assessment;
	}
	public int getIs_read() {
		return is_read;
	}
	public void setIs_read(int is_read) {
		this.is_read = is_read;
	}
	@Override
	public String toString() {
		return "SelfTestAnswer [curriculum_code=" + curriculum_code
				+ ", number=" + number + ", qt_code=" + qt_code
				+ ", testpaper_code=" + testpaper_code + ", user_id=" + user_id
				+ ", answer_name=" + answer_name + ", answer_link="
				+ answer_link + ", is_upload=" + is_upload
				+ ", is_self_assessment=" + is_self_assessment + ", is_read="
				+ is_read + "]";
	}
	
	
}
