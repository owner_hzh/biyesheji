package com.cqjt.pojo;

import java.sql.Date;


/**
 * 课件
 * @author LIJIN
 *
 */
public class Courseware {
	private Long curriculum_code;//课程代码
	private Long courseware_code;//课件代码
	private String filename;//课件名
	private String location;//链接位置
	private Date uploaddate;//上传日期
	private String sourcetype;//资源类型
	private Integer onlinelook;//查看次数
	private Integer downcount;//下载次数
	private String courseware_version;//课件版本
	private String user_id;//上传人

	
	public Long getCourseware_code() {
		return courseware_code;
	}
	public void setCourseware_code(Long courseware_code) {
		this.courseware_code = courseware_code;
	}
	public String getCourseware_version() {
		return courseware_version;
	}
	public void setCourseware_version(String courseware_version) {
		this.courseware_version = courseware_version;
	}
	public Date getUploaddate() {
		return uploaddate;
	}
	public void setUploaddate(Date uploaddate) {
		this.uploaddate = uploaddate;
	}
	public String getSourcetype() {
		return sourcetype;
	}
	public void setSourcetype(String sourcetype) {
		this.sourcetype = sourcetype;
	}
	public Integer getOnlinelook() {
		return onlinelook;
	}
	public void setOnlinelook(Integer onlinelook) {
		this.onlinelook = onlinelook;
	}
	public Integer getDowncount() {
		return downcount;
	}
	public void setDowncount(Integer downcount) {
		this.downcount = downcount;
	}
	public Long getCurriculum_code() {
		return curriculum_code;
	}
	public void setCurriculum_code(Long curriculum_code) {
		this.curriculum_code = curriculum_code;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	
	
}
