package com.cqjt.pojo;
/*
 * author:xiaoshujun
 * 2014-4-22
 * 实战项目备选表 类
 * 
 */
public class AlternativePracticalProject {
	private String pp_name;			//实战题目
	private String requirement;		//实战要求
	private int semester_code;		//开课学期
	private String ppd_name;		//参考文档名称
	private String ppd_link;		//链接位置
	private int equence_number;		//序号
	private String user_id;			//用户代码
	
	public String getPp_name() {
		return pp_name;
	}
	public void setPp_name(String pp_name) {
		this.pp_name = pp_name;
	}
	public String getRequirement() {
		return requirement;
	}
	public void setRequirement(String requirement) {
		this.requirement = requirement;
	}
	public int getSemester_code() {
		return semester_code;
	}
	public void setSemester_code(int semester_code) {
		this.semester_code = semester_code;
	}
	public String getPpd_name() {
		return ppd_name;
	}
	public void setPpd_name(String ppd_name) {
		this.ppd_name = ppd_name;
	}
	public String getPpd_link() {
		return ppd_link;
	}
	public void setPpd_link(String ppd_link) {
		this.ppd_link = ppd_link;
	}
	public int getEquence_number() {
		return equence_number;
	}
	public void setEquence_number(int equence_number) {
		this.equence_number = equence_number;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	@Override
	public String toString() {
		return "AlternativePracticalProject [pp_name=" + pp_name
				+ ", requirement=" + requirement + ", semester_code="
				+ semester_code + ", ppd_name=" + ppd_name + ", ppd_link="
				+ ppd_link + ", equence_number=" + equence_number
				+ ", user_id=" + user_id + "]";
	}
	
}
