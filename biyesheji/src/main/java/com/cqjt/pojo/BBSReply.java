package com.cqjt.pojo;

import java.sql.Timestamp;

import com.sun.star.util.DateTime;
/*
 * author:xiaoshujun
 * 2014-5-3
 * bbs 回贴	类
 * 
 */
public class BBSReply {
	private int rid  ;//回复编号  
	private int rtid ;//回复帖子编号  
	private int rsid;//回复版块编号        
	private String ruid ;//回复用户编号  
	private String remotion ;//回复表情  
	private String rtopic ;//回帖主题 
	private String rcontents ;//回帖内容 
	private Timestamp rtime  ;//回帖时间 
	private int rclickcount ;//回帖点击次数 
	
	public int getRid() {
		return rid;
	}
	public void setRid(int rid) {
		this.rid = rid;
	}
	public int getRtid() {
		return rtid;
	}
	public void setRtid(int rtid) {
		this.rtid = rtid;
	}
	public int getRsid() {
		return rsid;
	}
	public void setRsid(int rsid) {
		this.rsid = rsid;
	}
	public String getRuid() {
		return ruid;
	}
	public void setRuid(String ruid) {
		this.ruid = ruid;
	}
	public String getRemotion() {
		return remotion;
	}
	public void setRemotion(String remotion) {
		this.remotion = remotion;
	}
	public String getRtopic() {
		return rtopic;
	}
	public void setRtopic(String rtopic) {
		this.rtopic = rtopic;
	}
	public String getRcontents() {
		return rcontents;
	}
	public void setRcontents(String rcontents) {
		this.rcontents = rcontents;
	}
	public Timestamp getRtime() {
		return rtime;
	}
	public void setRtime(Timestamp timestamp) {
		this.rtime = timestamp;
	}
	public int getRclickcount() {
		return rclickcount;
	}
	public void setRclickcount(int rclickcount) {
		this.rclickcount = rclickcount;
	}
	@Override
	public String toString() {
		return "BBSReply [rid=" + rid + ", rtid=" + rtid + ", rsid=" + rsid
				+ ", ruid=" + ruid + ", remotion=" + remotion + ", rtopic="
				+ rtopic + ", rcontents=" + rcontents + ", rtime=" + rtime
				+ ", rclickcount=" + rclickcount + "]";
	}
	
	
}
