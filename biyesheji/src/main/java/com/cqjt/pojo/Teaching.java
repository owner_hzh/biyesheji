package com.cqjt.pojo;
/*
 * author:xiaoshujun
 * 2014-4-22
 * 教师信息	类
 * 
 */
public class Teaching {
	
	private String user_id;					//用户名
	private int curriculum_code	;			//bigint 课程代码
	
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public int getCurriculum_code() {
		return curriculum_code;
	}
	public void setCurriculum_code(int curriculum_code) {
		this.curriculum_code = curriculum_code;
	}
	@Override
	public String toString() {
		return "Teaching [user_id=" + user_id + ", curriculum_code="
				+ curriculum_code + "]";
	}
	
}
