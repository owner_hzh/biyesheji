package com.cqjt.pojo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/*
 *	author: xiaoshujun
 * date: 2014-4-20
 * 试题类型类
 */

public class QuestionTypes 
{
	
	private int qt_code;				//题型代码
	private String qt_name;			//题型名称
	private String describe;			//标题描述
	private int is_objective;			//是否客观题
	private String is_objective_CN;   //是否客观题的中文描述
	private int total;   //做试卷定制时用到的，表示这个题型一共有多少道题

	

	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public String getIs_objective_CN() 
	{
		return is_objective_CN;
	}
	public int getQt_code() 
	{
		return qt_code;
	}
	public void setQt_code(int qt_code) {
		this.qt_code = qt_code;
	}
	public String getQt_name() {
		return qt_name;
	}
	public void setQt_name(String qt_name) {
		this.qt_name = qt_name;
	}
	public String getDescribe() {
		return describe;
	}
	public void setDescribe(String describe) {
		this.describe = describe;
	}
	public int getIs_objective() {
		return is_objective;
	}
	public void setIs_objective(int is_objective) 
	{
		this.is_objective = is_objective;
		if(is_objective==0)
			is_objective_CN="否";
		else
			is_objective_CN="是";
	}
	@Override
	public String toString() {
		return "QuestionTypes [qt_code=" + qt_code + ", qt_name=" + qt_name
				+ ", describe=" + describe + ", is_objective=" + is_objective
				+"]";
	}
	
	
}
