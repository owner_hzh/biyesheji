package com.cqjt.pojo;

import java.sql.Timestamp;


public class Teacher {

	String teacher_name	;//varchar(20)教师姓名
	String technical_name;
    String tc_name;
	String user_id;//	char(12)
	private int	role_code;					//角色代码
	private int technical_code;				//职称代码
	private int tc_code;					//性质代码
	private int teacher_sex;				//教师性别
	//private Timestamp teacher_date;			//出生日期
	private String teacher_date;
	
	public String getTechnical_name() {
		return technical_name;
	}
	public void setTechnical_name(String technical_name) {
		this.technical_name = technical_name;
	}
	public String getTc_name() {
		return tc_name;
	}
	public void setTc_name(String tc_name) 
	{
		this.tc_name = tc_name;
	}

	public int getRole_code() {
		return role_code;
	}


	public void setRole_code(int role_code) {
		this.role_code = role_code;
	}


	public String getUser_id() {
		return user_id;
	}


	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}


	public String getTeacher_name() {
		return teacher_name;
	}


	public void setTeacher_name(String teacher_name) {
		this.teacher_name = teacher_name;
	}

	

	public int getTechnical_code() {
		return technical_code;
	}


	public void setTechnical_code(int technical_code) {
		this.technical_code = technical_code;
	}


	public int getTc_code() {
		return tc_code;
	}


	public void setTc_code(int tc_code) {
		this.tc_code = tc_code;
	}


	public int getTeacher_sex() {
		return teacher_sex;
	}


	public void setTeacher_sex(int teacher_sex) {
		this.teacher_sex = teacher_sex;
	}


	public String getTeacher_date() {
		return teacher_date;
	}


	public void setTeacher_date(String teacher_date) {
		this.teacher_date = teacher_date;

	}


	@Override
	public String toString()
	{
		return "Teacher [user_id=" + user_id + ", teacher_name=" + teacher_name
				+ "]";
	}


}
