package com.cqjt.pojo;

import java.sql.Timestamp;

/*
 * author:Wen 2014-4-20
 * 教师信息	类
 * 
 */
public class TeacherInfor {
	
	private String user_id;					//用户名
	private int technical_code;			//职称代码
	private int tc_code;							//性质代码
	private String teacher_name;		//教师姓名
	private int teacher_sex;					//教师性别
	// Timestamp teacher_date;		//出生日期
	private String teacher_date;		//出生日期
	private String teacher_nation;				//民族
	private String teacher_degree;			//学位
	private String teacher_features;			//政治面貌
	private String iconpath;
	
	private String technical_name;
	private String tc_name;
	private int	role_code;					//角色代码
	
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public int getTechnical_code() {
		return technical_code;
	}
	public void setTechnical_code(int technical_code) {
		this.technical_code = technical_code;
	}
	public int getTc_code() {
		return tc_code;
	}
	public void setTc_code(int tc_code) {
		this.tc_code = tc_code;
	}
	public String getTeacher_name() {
		return teacher_name;
	}
	public void setTeacher_name(String teacher_name) {
		this.teacher_name = teacher_name;
	}
	public int getTeacher_sex() {
		return teacher_sex;
	}
	public void setTeacher_sex(int teacher_sex) {
		this.teacher_sex = teacher_sex;
	}
/*	public Timestamp getTeacher_date() {
		return teacher_date;
	}
	public void setTeacher_date(Timestamp teacher_date) {
		this.teacher_date = teacher_date;
	}*/
	public String getTeacher_nation() {
		return teacher_nation;
	}
	public String getTeacher_date() {
		return teacher_date;
	}
	public void setTeacher_date(String teacher_date) {
		this.teacher_date = teacher_date;
	}
	public void setTeacher_nation(String teacher_nation) {
		this.teacher_nation = teacher_nation;
	}
	public String getTeacher_degree() {
		return teacher_degree;
	}
	public void setTeacher_degree(String teacher_degree) {
		this.teacher_degree = teacher_degree;
	}
	public String getTeacher_features() {
		return teacher_features;
	}
	public void setTeacher_features(String teacher_features) {
		this.teacher_features = teacher_features;
	}
	public String getIconpath() {
		return iconpath;
	}
	public void setIconpath(String iconpath) {
		this.iconpath = iconpath;
	}
	public String getTechnical_name() {
		return technical_name;
	}
	public void setTechnical_name(String technical_name) {
		this.technical_name = technical_name;
	}
	public String getTc_name() {
		return tc_name;
	}
	public void setTc_name(String tc_name) {
		this.tc_name = tc_name;
	}
	public int getRole_code() {
		return role_code;
	}
	public void setRole_code(int role_code) {
		this.role_code = role_code;
	}
	@Override
	public String toString() {
		return "TeacherInfor [user_id=" + user_id + ", technical_code="
				+ technical_code + ", tc_code=" + tc_code + ", teacher_name="
				+ teacher_name + ", teacher_sex=" + teacher_sex
				+ ", teacher_date=" + teacher_date + ", teacher_nation="
				+ teacher_nation + ", teacher_degree=" + teacher_degree
				+ ", teacher_features=" + teacher_features + ", iconpath="
				+ iconpath + "]";
	}
}
