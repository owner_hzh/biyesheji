package com.cqjt.pojo;

/*
 * author:xiaoshujun
 * 2014-4-16
 */
public class TeachingData {
    private	String filename;
	private String location;
	private int teachingdata_code;
	private int curriculum_code;
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public int getTeachingdata_code() {
		return teachingdata_code;
	}
	public void setTeachingdata_code(int teachingdata_code) {
		this.teachingdata_code = teachingdata_code;
	}
	public int getCurriculum_code() {
		return curriculum_code;
	}
	public void setCurriculum_code(int curriculum_code) {
		this.curriculum_code = curriculum_code;
	}
	@Override
	public String toString() {
		return "TeachingData [filename=" + filename + ", location=" + location
				+ ", teachingdata_code=" + teachingdata_code
				+ ", curriculum_code=" + curriculum_code + "]";
	}
	
}
