package com.cqjt.pojo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
/*
 * author:xiaoshujun
 * 2014-4-23
 * 学生实战记录	类
 * 
 */
public class StudentPracticalRecord {
	
	private int record_code;          //主键
	private int ppcode;					//实战题目类型号
	private String ppcode_name;
 	private String ppnumber;			//序号
 	private String user_id;				//用户名		
 	private String user_name;
	private String tea_user_id;			//教师用户名
	private String tea_user_name;
	private Date download_date;	//下载题目时间
	private Date upload_date;		//上传结果时间
    private	String self_score;			//自测结果
	
	private float teacher_score=0;		//教师评分
	private String comment;				//审核评语
	private int is_audit;				//已审核
   	private String answer_name;			//文件名
   	private String link;				//链接位置  
   	
   	private String pp_name;
   	   	
   	public String getPp_name() {
		return pp_name;
	}
	public void setPp_name(String pp_name) {
		this.pp_name = pp_name;
	}

	PracticalProject practicalProject;
   	
   	
   	public String getSelf_score() {
		return self_score;
	}
	public void setSelf_score(String self_score) {
		this.self_score = self_score;
	}
	public int getPpcode() {
		return ppcode;
	}
	public void setPpcode(int ppcode) {
		this.ppcode = ppcode;
	}
	public String getPpnumber() {
		return ppnumber;
	}
	public void setPpnumber(String ppnumber) {
		this.ppnumber = ppnumber;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getTea_user_id() {
		return tea_user_id;
	}
	public void setTea_user_id(String tea_user_id) {
		this.tea_user_id = tea_user_id;
	}
	
	public float getTeacher_score() {
		return teacher_score;
	}
	public void setTeacher_score(float teacher_score) {
		this.teacher_score = teacher_score;
	}
	public void setTeacher_score(String teacher_score) {
		if(teacher_score!=null&&!teacher_score.equals("null")&&!teacher_score.equals(""))
		{
			this.teacher_score = Float.parseFloat(teacher_score);
		}else
		{
			this.teacher_score = 0;
		}
		   
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public int getIs_audit() {
		return is_audit;
	}
	public void setIs_audit(int is_audit) {
		this.is_audit = is_audit;
	}
	public String getAnswer_name() {
		return answer_name;
	}
	public String getPpcode_name() {
		return ppcode_name;
	}
	public void setPpcode_name(String ppcode_name) {
		this.ppcode_name = ppcode_name;
	}
	public void setAnswer_name(String answer_name) {
		this.answer_name = answer_name;
	}
	public String getLink() {
		return link;
	}
	public String getTea_user_name() {
		return tea_user_name;
	}
	public void setTea_user_name(String tea_user_name) {
		this.tea_user_name = tea_user_name;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public void setLink(String link) {
		this.link = link;
	}
	
	
	public Date getDownload_date(){
		return download_date;
	}
	
	public void setDownload_date(Date download_date) {
		this.download_date = download_date;
	}
	public int getRecord_code() {
		return record_code;
	}
	public void setRecord_code(int record_code) {
		this.record_code = record_code;
	}
	public Date getUpload_date() {
		return upload_date;
	}
	public PracticalProject getPracticalProject() {
		return practicalProject;
	}
	public void setPracticalProject(PracticalProject practicalProject) {
		this.practicalProject = practicalProject;
	}
	public void setUpload_date(Date upload_date) {
		this.upload_date = upload_date;
	}
	@Override
	public String toString() {
		return "StudentPracticalRecord [ppcode=" + ppcode + ", ppnumber="
				+ ppnumber + ", user_id=" + user_id + ", tea_user_id="
				+ tea_user_id + ", download_date=" + download_date
				+ ", upload_date=" + upload_date + ", self_score=" + self_score
				+ ", teacher_score=" + teacher_score + ", comment=" + comment
				+ ", is_audit=" + is_audit + ", answer_name=" + answer_name
				+ ", link=" + link + "]";
	}
	
	private Date DateParse(Date d){
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String s = sdf.format(d);
		System.out.println("时间==="+s);
		Date _date = null;
		try {
			_date = sdf.parse(s);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return _date;
	}
	
}
